module "gitlab" {
  source = "../../modules/gitlab/gcp"

  region  = var.region
  project = var.project_id

  instance_name = "gitlab-${var.instance_name}"
  zone    = var.zone

  domain = var.gitlab_domain
  opstrace_domain = var.domain
  cloud_dns_zone = var.cloud_dns_zone

  registry_username = var.registry_username
  registry_auth_token = var.registry_auth_token
  gitlab_image = var.gitlab_image
}
module "gcp" {
  source = "../../modules/opstrace/gcp"

  project_id = var.project_id

  instance_name = var.instance_name
  region        = var.region
  location      = var.location
  num_nodes     = var.num_nodes

  kubeconfig_path = var.kubeconfig_path
}

provider "kubectl" {
  host                   = module.gcp.kubernetes_cluster_host
  cluster_ca_certificate = module.gcp.kubernetes_cluster_certificate
  token                  = module.gcp.google_client_token
  load_config_file       = false
}

resource "kubectl_manifest" "cluster" {
  yaml_body = yamlencode({
    "apiVersion": "opstrace.com/v1alpha1"
    "kind": "Cluster"
    "metadata": {
      "name": var.instance_name,
      # We may want to make this configurable at some point. The Scheduler Operator can be deployed
      # to any namespace.
      "namespace": "default"
    }
    "spec": {
      "target": "gcp",
      "dns": {
        "certificateIssuer": var.cert_issuer,
        "domain": var.domain,
        "acmeEmail": var.acme_email,
        "acmeServer": var.acme_server,
        "dns01Challenge": {
          # Use google cloud DNS (where our domain is hosted).
          "cloudDNS": {
            "project": var.project_id
          }
        },
        "externalDNSProvider": {
          # Use google cloud DNS (where our domain is hosted).
          "gcp": {
            "dnsServiceAccountName": module.gcp.externaldns_service_account
          }
        },
        "firewallSourceIPsAllowed": ["0.0.0.0/0"]
        # Use google cloud DNS (where our domain is hosted).
        "gcpCertManagerServiceAccount": module.gcp.certmanager_service_account
      },
      "gitlab": {
        "instanceUrl": module.gitlab.gitlab_url,
        "groupAllowedAccess": "*",
        "groupAllowedSystemAccess": "*",
        "oauthClientId": module.gitlab.oauth_client_id,
        "oauthClientSecret": module.gitlab.oauth_client_secret,
        "internalEndpointToken": module.gitlab.internal_endpoint_token
      },
      "overrides": {
        #
        # This is where we can specify affinities and node selectors or override any
        # aspect of any component. More nodeSelectors/overrides capabilities may need to be added to this CRD
        # for resources that are created by the tenant-operator (Argus, Jaeger CR, Opentelemetry API, etc).
        # Currently those overrides can only be specified on the Group CR, which is not tracked in terraform because
        # its an internal dynamic resource.
        #
        "caInjector": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
            "statefulset": {}
          }
        },
        "certManager": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        },
        "clickhouse": {
          "components": {
            # deployment represents the clickhouse CR. Overrides for this CR are not plumbed yet because
            # we are changing the operator
            "deployment": {},
            "service": {},
            "serviceMonitor": {}
          }
        },
        "clickhouseOperator": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        },
        "externalDNS": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        },
        "gatekeeper": {
          "components": {
            "deployment": {
              "spec": {
                "template": {
                  "spec": {
                    "replicas": 3,
                    "containers": [{
                      "name": "gatekeeper"
                    }]
                  }
                }
              }
            },
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        },
        "jaegerOperator": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        },
        "nginxIngress": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        },
        "prometheusOperator": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        },
        "redis": {
          "components": {
            "deployment": {},
            "serviceMonitor": {}
          }
        },
        "redisOperator": {
          "components": {
            "deployment": {},
            "ingress": {},
            "service": {},
            "serviceMonitor": {},
          }
        }
      }
    }
  })

  sensitive_fields = [
    "spec.gitlab.oauthClientId",
    "spec.gitlab.oauthClientSecret",
  ]
  wait = true
}
