
output "gitlab_url" {
  value = module.gitlab.gitlab_url
  description = "GitLab URL for Opstrace to connect to"
}

output "admin_token" {
  value = module.gitlab.admin_token
  description = "Admin auth token for the GitLab instance. Can be used to create/manage users/groups for testing"
}

output "root_password" {
  value = module.gitlab.root_password
  description = "Root User Password for GitLab"
}
