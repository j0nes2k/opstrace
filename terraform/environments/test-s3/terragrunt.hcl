locals {}

generate "backend" {
    path = "backend.tf"
    if_exists = "overwrite_terragrunt"
    contents = <<EOF
terraform {
    backend "local" {
        path="${get_terragrunt_dir()}/terraform.tfstate"
    }
}
EOF
}