inputs = {
    environment = "test-s3"
    project_id = "vast-pad-240918"
    # Unique name within project_id for naming resources
    instance_name = "arun-s3-dev"
    # Number of nodes for Opstrace instance
    num_nodes = 3

    region = "europe-west4"
    location = "europe-west4-a"
    zone = "europe-west4-a"

    # Domain for Opstrace. This requires setting up a DNS zone in the GCP project manually.
    domain = "gitlab.asori.dev"
    opstrace_domain = "asori.dev"
    # GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
    gitlab_domain = "gitlab.asori.dev"
    # GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
    # It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
    cloud_dns_zone = "asori"

    # Add values for deploying cluster-specific secrets
    cluster_secret_name = "auth-secret"
    cluster_secret_namespace = "default"

    # Optionally set an email to opt in for certificate expiry notices from LetsEncrypt
    # https://letsencrypt.org/docs/expiration-emails
    cert_issuer = "letsencrypt-staging"
    acme_server = ""
    acme_email = "asori@gitlab.com"

    # GitLab registry username for pulling a specific GitLab image
    # Any GitLab image can be used that is not public, i.e. images created in merge_requests, so we can
    # test against any work in development
    registry_username = "arun.sori"

    ################ NOTE #########################
    # Personal Auth Token to access the GitLab registry https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
    # Set this as an environment variable to prevent leaking it in SCM so instead of:
    # registry_auth_token = "your_token"
    # -> export TF_VAR_registry_auth_token = "your_token"

    # The image to use when deploying GitLab
    gitlab_image = "registry.gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/gitlab-ee:97d8e671393db2fc695845ba08ec2cd1398adae9"

    clickhouse_s3_bucket = "clickhouse-s3-bucket"
    aws_region = "eu-west-3"
    s3_secret_namespace = "default"
    clickhouse_s3_user_name = "clickhouse-user"

}