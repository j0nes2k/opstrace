terraform {
    source = "${get_terragrunt_dir()}/../../../modules//gitlab/gcp"
}

include {
    path = find_in_parent_folders()
}

dependency "gke" {
    config_path = "../gke"
    skip_outputs = true
}

locals {
    common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

inputs = {
    instance_name = local.common_vars.inputs.instance_name

    project = local.common_vars.inputs.project_id

    region        = local.common_vars.inputs.region
    location      = local.common_vars.inputs.location
    zone          = local.common_vars.inputs.zone

    # Domain for Opstrace. This requires setting up a DNS zone in the GCP project manually.
    opstrace_domain = local.common_vars.inputs.opstrace_domain
    # Optionally set an email to opt in for certificate expiry notices from LetsEncrypt
    # https://letsencrypt.org/docs/expiration-emails
    acme_email = local.common_vars.inputs.acme_email

    # GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
    domain = local.common_vars.inputs.domain
    gitlab_domain  = local.common_vars.inputs.gitlab_domain
    # GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
    # It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
    cloud_dns_zone = local.common_vars.inputs.cloud_dns_zone

    # GitLab registry username for pulling a specific GitLab image
    # Any GitLab image can be used that is not public, i.e. images created in merge_requests, so we can
    # test against any work in development
    registry_username = local.common_vars.inputs.registry_username
    ################ NOTE #########################
    # Personal Auth Token to access the GitLab registry https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
    # Set this as an environment variable to prevent leaking it in SCM so instead of:
    # registry_auth_token = "your_token"
    # -> export TF_VAR_registry_auth_token = "your_token"

    # The image to use when deploying GitLab
    gitlab_image = local.common_vars.inputs.gitlab_image
}