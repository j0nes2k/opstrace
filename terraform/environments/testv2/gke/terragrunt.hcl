terraform {
    source = "${get_terragrunt_dir()}/../../../modules//gke"
}

include {
    path = find_in_parent_folders()
}

locals {
    common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

inputs = {
    instance_name   = local.common_vars.inputs.instance_name
    project_id      = local.common_vars.inputs.project_id
    region          = local.common_vars.inputs.region
    location        = local.common_vars.inputs.location
    zone            = local.common_vars.inputs.zone
    kubeconfig_path = "${get_terragrunt_dir()}/../.kubeconfig"
    global_labels = {
        gl_realm = local.common_vars.inputs.gl_realm
        gl_env_type = local.common_vars.inputs.gl_env_type
        gl_env_name = local.common_vars.inputs.gl_env_name
        gl_owner_email_handle = local.common_vars.inputs.gl_owner_email_handle
        gl_dept = local.common_vars.inputs.gl_dept
        gl_dept_group = local.common_vars.inputs.gl_dept_group
    }
}