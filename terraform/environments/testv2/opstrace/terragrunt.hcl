terraform {
    source = "${get_terragrunt_dir()}/../../../..//terraform/modules/opstrace"
}

include {
    path = find_in_parent_folders()
}

locals {
    common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

dependency "gitlab" {
    config_path = "../gitlab"
    skip_outputs = true
}

dependency "gke" {
    config_path = "../gke"
    mock_outputs = {
        kubernetes_cluster_host = "https://mock"
        kubernetes_cluster_certificate = "mock"
        kubeconfig_path = "~/.kube/config"
        postgres_dsn_endpoint = "mock"
        global_labels = {}
    }
    mock_outputs_allowed_terraform_commands = ["validate"]
}

inputs = {
    cluster_endpoint = dependency.gke.outputs.kubernetes_cluster_host
    cluster_ca_certificate = dependency.gke.outputs.kubernetes_cluster_certificate
    kubeconfig_path = dependency.gke.outputs.kubeconfig_path
    postgres_dsn_endpoint = dependency.gke.outputs.postgres_dsn_endpoint
    global_labels = {
        gl_realm = local.common_vars.inputs.gl_realm
        gl_env_type = local.common_vars.inputs.gl_env_type
        gl_env_name = local.common_vars.inputs.gl_env_name
        gl_owner_email_handle = local.common_vars.inputs.gl_owner_email_handle
        gl_dept = local.common_vars.inputs.gl_dept
        gl_dept_group = local.common_vars.inputs.gl_dept_group
    }
}