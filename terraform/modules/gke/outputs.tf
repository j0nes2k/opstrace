output "region" {
  value       = var.region
  description = "GCloud Region"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = "https://${data.google_container_cluster.primary.endpoint}"
  description = "GKE Cluster Host"
}

output "google_client_token" {
  sensitive   = true
  value       = data.google_client_config.primary.access_token
  description = "Google client token"
}

output "kubernetes_cluster_certificate" {
  value = base64decode(
    data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate,
  )
  description = "Kubernetes cluster certificate"
}

output "primary_node_pool_name" {
  value       = google_container_node_pool.primary_nodes.name
  description = "GKE primary node pool name"
}

output "externaldns_service_account" {
  value       = google_service_account.externaldns_service_account.email
  description = "ExternalDNS service account"
}

output "certmanager_service_account" {
  value       = google_service_account.certmanager_service_account.email
  description = "CertManager service account"
}

output "kubeconfig_path" {
  value       = local_file.kubeconfig.filename
  description = "kubeconfig path"
}

output "vpc_id" {
  value       = google_compute_network.vpc.id
  description = "google vpc ID"
}

output "vpc_name" {
  value       = google_compute_network.vpc.name
  description = "google vpc name"
}