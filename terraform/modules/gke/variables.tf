variable "project_id" {
  description = "project id"
}

variable "global_labels" {
  default     = {}
  type        = map
  description = "Map consisting key=value pairs added as labels to all resources provisioned by this module"
}

variable "region" {
  description = "region"
}

variable "gke_username" {
  default     = ""
  description = "gke username"
}

variable "gke_password" {
  default     = ""
  description = "gke password"
}

variable "location" {
  default     = ""
  description = "The location (region or zone) in which the cluster master will be created, as well as the default node location. If you specify a zone (such as us-central1-a), the cluster will be a zonal cluster with a single cluster master. If you specify a region (such as us-west1), the cluster will be a regional cluster with multiple masters spread across zones in the region, and with default node locations in those zones as well"
}

variable "instance_name" {
  default     = "example"
  description = "the Opstrace instance name"
}

variable "num_nodes" {
  default     = 3
  description = "number of gke nodes"
}

variable "kubeconfig_path" {
  default     = ".kubeconfig"
  description = "Path to kubeconfig"
}

variable "primary_gke_ip_cidr_range" {
  default     = "10.20.0.0/20"
  type        = string
  description = "Primary subnet range to be used with GKE nodes. See https://cloud.google.com/kubernetes-engine/docs/concepts/alias-ips"
}

variable "secondary_gke_ip_cidr_range_pods" {
  type        = string
  default     = "10.16.0.0/14"
  description = "Secondary subnet range to be used with GKE pods. See https://cloud.google.com/kubernetes-engine/docs/concepts/alias-ips"
}

variable "secondary_gke_ip_cidr_range_services" {
  type        = string
  default     = "10.8.0.0/20"
  description = "Secondary subnet range to be used with GKE services. See https://cloud.google.com/kubernetes-engine/docs/concepts/alias-ips"
}

# We rely on consistent egress IPs as they are expected to firewalled against another clusters.
# As such, we use manual mode for IP allocation in Cloud NAT.
# 64,512 ports are available per address and will be divided among `num_nodes` (VM count) of the cluster.
# Maximum number of ports available per VM can be configured via code (currently 4096).
variable "num_manual_ip_addresses_nat" {
  type        = number
  default     = 1
  description = "Number of static IPs that will be reserved for manual allocation for the Cloud NAT. See https://cloud.google.com/nat/docs/ports-and-addresses#port-reservation-examples for details."
}
