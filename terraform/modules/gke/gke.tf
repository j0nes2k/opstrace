# GKE cluster
resource "google_container_cluster" "primary" {
  name            = var.instance_name
  resource_labels = merge(var.global_labels, tomap({}))
  location        = var.location

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  private_cluster_config {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = "172.16.0.16/28"
  }

  master_authorized_networks_config {
    cidr_blocks {
      display_name = "all"
      cidr_block   = "0.0.0.0/0"
    }
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = google_compute_subnetwork.subnet.secondary_ip_range.0.range_name
    services_secondary_range_name = google_compute_subnetwork.subnet.secondary_ip_range.1.range_name
  }

  lifecycle {
    ignore_changes = [
      # Any changes to node_config must not replace the entire cluster
      node_config,
    ]
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name = "${google_container_cluster.primary.name}-node-pool"
  # location - (Optional) The location (region or zone) in which the cluster
  # master will be created, as well as the default node location. If you specify
  # a zone (such as us-central1-a), the cluster will be a zonal cluster with a
  # single cluster master. If you specify a region (such as us-west1), the
  # cluster will be a regional cluster with multiple masters spread across zones
  # in the region, and with default node locations in those zones as well
  location = var.location

  cluster    = google_container_cluster.primary.name
  node_count = var.num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = merge(var.global_labels, tomap({
      env = var.project_id
    }))

    # preemptible  = true
    # TODO: make this a variable?
    machine_type = "n1-standard-4"
    # tags can be later used to reference resources (like while creating firewall rules for _these_ node tags)
    tags = ["gke-node", "${var.project_id}-gke", "${var.project_id}-${var.instance_name}-gke-node"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

# Configure kubernetes provider with Oauth2 access token.
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config
# This fetches a new token, which will expire in 1 hour.
data "google_client_config" "primary" {
  depends_on = [google_container_node_pool.primary_nodes]
}

# Defer reading the cluster data until the GKE cluster exists.
data "google_container_cluster" "primary" {
  name       = var.instance_name
  depends_on = [google_container_cluster.primary]
}
