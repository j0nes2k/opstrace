variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "kubeconfig_path" {
  type    = string
  default = ".kubeconfig"
}

variable "namespace" {
  type        = string
  default     = "thanos"
  description = "Namespace for thanos components"
}

variable "thanos_bucket_name" {
  type        = string
  description = "GCS bucket name where thanos will store the data"
}

variable "bucket_location" {
  type        = string
  description = "Location for the GCS bucket"
}

variable "project_id" {
  type        = string
  description = "Google Cloud project where resources will be created"
}

variable "gke_cluster_name" {
  type        = string
  description = "Cluster/Instance name of the GKE cluster"
}

variable "gke_cluster_region" {
  type        = string
  description = "Region of the GKE cluster"
}

variable "gke_vpc_name" {
  type        = string
  description = "VPC name of the Cluster where the thanos components will be installed"
}

variable "opstrace_cluster_name" {
  type        = string
  description = "Cluster/Instance name of the GKE cluster where opstrace is available"
}

variable "source_ip_ranges_allowed_access" {
  type        = string
  description = "List of IPs that will be allowed access to the ingress resources (thanos-receiver & thanos-store) in comma separated format"
}

variable "thanos_receive_remote_write_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that receives remote writes"
}

variable "thanos_receive_store_api_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that exposes the store API of thanos receiver"
}

variable "thanos_store_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that exposes thanos-store"
}

variable "thanos_query_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that exposes thanos-query"
}

variable "thanos_query_sd_domains" {
  type        = list(string)
  default     = []
  description = "List of domains(addr:port) to add to the store sd config of thanos-query. Make sure that the stores are reachable from the cluster."
}