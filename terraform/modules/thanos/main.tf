data "kustomization_build" "thanos-config" {
  path = "${path.module}/config"
}

resource "kustomization_resource" "thanos-manifests" {
  for_each = data.kustomization_build.thanos-config.ids
  manifest = data.kustomization_build.thanos-config.manifests[each.value]
  depends_on = [
    kubernetes_namespace.thanos-namespace,
    kubernetes_secret.storage-creds,
    kubernetes_config_map.thanos-storage-config,
    kubernetes_config_map.prometheus-config,
    kubernetes_config_map.thanos-rule-alerts,
    kubernetes_config_map.thanos-query-config
  ]
}

resource "kubernetes_namespace" "thanos-namespace" {
  provider = kubernetes
  metadata {
    name = var.namespace
  }
}

locals {
  storage_svc_account_name = "${substr("${var.gke_cluster_name}", 0, 12)}-thanos-storage"
}

resource "google_service_account" "storage-service-account" {
  project      = var.project_id
  account_id   = local.storage_svc_account_name
  display_name = "GCP SA used by thanos storage to interact with GCS"
}

resource "google_project_iam_member" "storage-service-account-binding" {
  project = var.project_id
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.storage-service-account.email}"
}

resource "google_service_account_key" "storage-service-account-key" {
  service_account_id = google_service_account.storage-service-account.name
}

# Apparently this method has been deprecated in favor of workload identities; however thanos expects the default
# credentials to be available when it starts.
resource "kubernetes_secret" "storage-creds" {
  provider = kubernetes
  metadata {
    name      = "thanos-storage"
    namespace = var.namespace
  }

  data = {
    "credentials.json" = base64decode(google_service_account_key.storage-service-account-key.private_key)
  }
}

resource "random_id" "bucket_name_suffix" {
  byte_length = 16
}

resource "google_storage_bucket" "thanos-bucket" {
  name     = "${var.thanos_bucket_name}-${random_id.bucket_name_suffix.hex}"
  location = var.bucket_location
  project  = var.project_id
}


# Config to access to object storage buckets
# used-by: thanos-store and thanos-receiver
locals {
  storage_config = yamlencode({
    "type" : "GCS"
    "config" : {
      "bucket" : google_storage_bucket.thanos-bucket.name
    }
  })
}

resource "kubernetes_config_map" "thanos-storage-config" {
  metadata {
    name      = "thanos-storage"
    namespace = var.namespace
  }
  data = {
    "config.yaml" = local.storage_config
  }
  depends_on = [google_storage_bucket.thanos-bucket]
}

locals {
  prometheus_config = templatefile("${path.module}/config/resources/prometheus.yaml.tmpl", {})
  thanos_rule_alert = templatefile("${path.module}/config/resources/thanos-rule-alerts.yaml", {})
}

# Prometheus configuration
# used-by: Prometheus
resource "kubernetes_config_map" "prometheus-config" {
  metadata {
    name      = "prometheus"
    namespace = var.namespace
  }
  data = {
    "prometheus.yaml.tmpl" = local.prometheus_config
  }
}

# alerts to monitor Thanos infrastructure
# used-by: Prometheus
resource "kubernetes_config_map" "thanos-rule-alerts" {
  metadata {
    name      = "thanos-rule-alerts"
    namespace = var.namespace
  }
  data = {
    "thanos-rule.yaml" = local.thanos_rule_alert
  }
}

# Service discovery config of Prometheus instances to aggregate query results
# used-by: Thanos Query
locals {
  thanos_query_config = templatefile("${path.module}/config/resources/store-sd.yaml.tftpl", {
    additional_stores = var.thanos_query_sd_domains
  })
}
resource "kubernetes_config_map" "thanos-query-config" {
  metadata {
    name      = "thanos-query"
    namespace = var.namespace
  }
  data = {
    "store-sd.yaml" = local.thanos_query_config
  }
}

resource "kubernetes_ingress_v1" "thanos-receiver-ingress" {
  metadata {
    name        = "thanos-receiver-remote-write"
    namespace   = var.namespace
    annotations = {}
  }

  spec {
    ingress_class_name = "nginx"
    rule {
      host = var.thanos_receive_remote_write_domain
      http {
        path {
          path_type = "Prefix"
          path      = "/"
          backend {
            service {
              name = "thanos-receive"
              port {
                number = 19291 # remote write port
              }
            }
          }
        }
      }
    }
  }
  depends_on = [kustomization_resource.nginx-ingress, google_compute_firewall.allow_ingress_rule]
}


# Note: Address will have to created before hand with -target option as terraform fails to recognise the dependence
# of address on the ingress controller
resource "google_compute_address" "ing-address" {
  name    = "${var.gke_cluster_name}-nginx-ingress-ip"
  region  = var.gke_cluster_region
  project = var.project_id
}

locals {
  allowed_ips = split(",", var.source_ip_ranges_allowed_access)

  ingress_patch = jsonencode({
    "apiVersion" = "v1",
    "kind"       = "Service",
    "metadata" = {
      "name" = "ingress-nginx-controller"
    }
    "spec" = {
      "loadBalancerSourceRanges" = [for ip in local.allowed_ips : ip]
      "loadBalancerIP" : google_compute_address.ing-address.address
    }
  })
}

data "kustomization_overlay" "nginx-ingress-manifest" {
  resources = [
    "${path.module}/nginx-ingress",
  ]

  patches {
    target {
      kind = "Service"
      name = "ingress-nginx-controller"
    }
    patch = local.ingress_patch
  }
}

resource "kustomization_resource" "nginx-ingress" {
  for_each = data.kustomization_overlay.nginx-ingress-manifest.ids
  manifest = data.kustomization_overlay.nginx-ingress-manifest.manifests[each.value]
}

# Adds a firewall rule to allow connections on 8443 from cluster master nodes to worker nodes
# This is required for the nginx-ingress
# See https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke
resource "google_compute_firewall" "allow_ingress_rule" {

  name        = "gke-${var.gke_cluster_name}-allow-nginx-ingress-fw"
  network     = var.gke_vpc_name
  project     = var.project_id
  description = "Allow traffic from cluster master nodes to work nodes in thanos cluster on 8443/tcp"

  priority  = 1000
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports    = [8443]
  }

  # node tags for thanos cluster
  target_tags = ["${var.project_id}-${var.gke_cluster_name}-gke-node"]
  # This is hard-coded in `gke` module
  # master_ipv4_cidr_block of GKE configuration
  source_ranges = ["172.16.0.16/28"]

  # turn on logging
  # log_config {
  #    metadata = "INCLUDE_ALL_METADATA"
  #  }
}

# Note: Address will have to created before hand with -target option as terraform fails to recognise the dependence
# of address on the ingress controller.
# Example:
# `gcloud compute addresses create $gke_cluster_name-traefik-ip --region $gke_cluster_region`
# `terraform import module.thanos.google_compute_address.traefik-address projects/$proj/regions/$reg/addresses/$name`
resource "google_compute_address" "traefik-address" {
  name    = "${var.gke_cluster_name}-traefik-ip"
  region  = var.gke_cluster_region
  project = var.project_id
}

locals {
  svc_patch_traefik = jsonencode({
    "apiVersion" = "v1",
    "kind"       = "Service",
    "metadata" = {
      "name" = "traefik"
    }
    "spec" = {
      "loadBalancerIP" : google_compute_address.traefik-address.address
    }
  })
}

data "kustomization_overlay" "traefik-manifest" {
  resources = ["${path.module}/traefik"]
  patches {
    target {
      kind = "Service"
      name = "traefik"
    }
    patch = local.svc_patch_traefik
  }
}

resource "kubernetes_namespace" "traefik-namespace" {
  provider = kubernetes
  metadata {
    name = "traefik"
  }
}

resource "kustomization_resource" "traefik" {
  for_each   = data.kustomization_overlay.traefik-manifest.ids
  manifest   = data.kustomization_overlay.traefik-manifest.manifests[each.value]
  depends_on = [kubernetes_namespace.traefik-namespace]
}

resource "kubernetes_manifest" "traefik-source-ip-whitelist" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "Middleware"

    "metadata" = {
      "name"      = "source-ip-whitelist-traefik",
      "namespace" = var.namespace,
    },

    "spec" = {
      "ipWhiteList" : {
        "sourceRange" : [for ip in local.allowed_ips : ip]
      }
    }
  }
  depends_on = [kustomization_resource.traefik]
}

# IngressRoute objects that expose store API of thanos-receive and thanos-store
# Note that this won't be available as ingress but rather CRD of type ingress route
resource "kubernetes_manifest" "traefik-ingressroute-store" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "IngressRoute"

    "metadata" = {
      "name"      = "thanos-store-plain",
      "namespace" = var.namespace,
    },

    "spec" = {
      "entryPoints" = [
        "web",
      ]
      "routes" = [
        {
          "kind"  = "Rule",
          "match" = "Host(`${var.thanos_store_domain}`)",
          "middlewares" = [
            {
              "name"      = "source-ip-whitelist-traefik",
              "namespace" = var.namespace,
            },
          ],
          "services" = [
            {
              "kind"      = "Service",
              "name"      = "thanos-store",
              "namespace" = var.namespace,
              "port"      = "10901",
              "scheme"    = "h2c",
            },
          ],
        },
      ],
    }
  }
  depends_on = [kustomization_resource.traefik]
}

resource "kubernetes_manifest" "traefik-ingressroute-receive" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "IngressRoute"

    "metadata" = {
      "name"      = "thanos-receive-plain",
      "namespace" = var.namespace,
    },

    "spec" = {
      "entryPoints" = [
        "web",
      ]
      "routes" = [
        {
          "kind"  = "Rule",
          "match" = "Host(`${var.thanos_receive_store_api_domain}`)",
          "middlewares" = [
            {
              "name"      = "source-ip-whitelist-traefik",
              "namespace" = var.namespace,
            },
          ],
          "services" = [
            {
              "kind"      = "Service",
              "name"      = "thanos-receive",
              "namespace" = var.namespace,
              "port"      = "10901",
              "scheme"    = "h2c",
            },
          ],
        },
      ],
    }
  }
  depends_on = [kustomization_resource.traefik]
}

resource "kubernetes_manifest" "traefik-ingressroute-query" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "IngressRoute"

    "metadata" = {
      "name"      = "thanos-query-plain",
      "namespace" = var.namespace,
    },

    "spec" = {
      "entryPoints" = [
        "web",
      ]
      "routes" = [
        {
          "kind"  = "Rule",
          "match" = "Host(`${var.thanos_query_domain}`)",
          "middlewares" = [
            {
              "name"      = "source-ip-whitelist-traefik",
              "namespace" = var.namespace,
            },
          ],
          "services" = [
            {
              "kind"      = "Service",
              "name"      = "thanos-query",
              "namespace" = var.namespace,
              "port"      = "10901",
              "scheme"    = "h2c",
            },
          ],
        },
      ],
    }
  }
  depends_on = [kustomization_resource.traefik]
}
