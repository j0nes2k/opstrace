output "postgres_dsn_endpoint" {
  sensitive = true
  value = "postgres://${google_sql_user.opstrace.name}:${google_sql_user.opstrace.password}@${google_sql_database_instance.postgres.private_ip_address}:5432/"
  description = "Postgres DSN endpoint"
}