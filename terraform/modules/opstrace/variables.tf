variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "kubeconfig_path" {
  type    = string
  default = ".kubeconfig"
}

variable "scheduler_image" {
  type    = string
  default = "SCHEDULER_IMAGE_NAME"
}

variable "postgres_dsn_endpoint" {
  type        = string
  sensitive   = true
  description = "Postgres DSN endpoint to put in a secret"
}

variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

variable "location" {
  default     = ""
  description = "The location (region or zone) in which the cluster master will be created, as well as the default node location. If you specify a zone (such as us-central1-a), the cluster will be a zonal cluster with a single cluster master. If you specify a region (such as us-west1), the cluster will be a regional cluster with multiple masters spread across zones in the region, and with default node locations in those zones as well"
}

variable "global_labels" {
  default     = {}
  description = "Map consisting key=value pairs added as labels to all resources provisioned by this module"
}

variable "gke_cluster_name" {
  description = "Name of the cluster to which the nodes should be attached to"
}

variable "ch_nodepool_nodes_number" {
  description = "Number of nodes the dedicated clikhouse node pool should have"
}
