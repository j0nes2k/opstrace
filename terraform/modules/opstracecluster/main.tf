resource "kubernetes_secret" "cluster" {
  provider = kubernetes
  metadata {
    name      = var.cluster_secret_name
    namespace = var.cluster_secret_namespace
  }

  data = {
    gitlab_oauth_client_id     = var.gitlab_oauth_client_id
    gitlab_oauth_client_secret = var.gitlab_oauth_client_secret
    internal_endpoint_token    = var.internal_endpoint_token
  }

  immutable = true
}

resource "kubernetes_secret" "cloudflare" {
  provider = kubernetes

  metadata {
    name = var.cloudflare_secret_name
    namespace = var.cloudflare_secret_namespace
  }

  data = {
    "CF_API_TOKEN" = var.cloudflare_api_token
  }

  immutable = true
}

resource "kubectl_manifest" "cluster" {
  yaml_body = var.cluster_manifest
}
