# to be `source`d as part of teardown
# assume that current working directory is OPSTRACE_BUILD_DIR, i.e.
# the directory where the opstrace CLI is executed.

kubectl describe all --all-namespaces 2> kubectl_describe_all.stderr > kubectl_describe_all-${OPSTRACE_CLUSTER_NAME}.log

# Filter out a specific warning that may appear thousands of times on AWS:
# See opstrace-prelaunch/issues/1029
cat kubectl_describe_all.stderr | grep -v 'many client instances from the same exec auth config can cause performance problems'

# Get a quick overview of all pod status.
kubectl get pods -A -o wide > kubectl_get_pods.log

kubectl logs --namespace=kube-system deployment.apps/opstrace-controller \
    > kubectl_controller-${OPSTRACE_CLUSTER_NAME}.log

kubectl describe --namespace=kube-system deployment.apps/opstrace-controller \
    > kubectl_controller_describe-${OPSTRACE_CLUSTER_NAME}.log

# System Prometheus pushes system metrics into Cortex
kubectl logs statefulset.apps/prometheus-system-prometheus \
 --container prometheus --namespace=system-tenant > clusterlogs_systemprom-${OPSTRACE_CLUSTER_NAME}.log

# collect logs from all the pods in the following namespaces
for NAMESPACE in application clickhouse clickhouse-operator-system cortex cortex-operator-system jaeger-operator-system
do
    kubectl get pods --no-headers -o custom-columns=":metadata.name" --namespace=${NAMESPACE} 2>/dev/null | \
    while read PNAME; do \
        echo "get logs for $NAMESPACE/$PNAME" ; \
        kubectl logs $PNAME --namespace=${NAMESPACE} --all-containers=true > clusterlogs_${NAMESPACE}-${PNAME}-${OPSTRACE_CLUSTER_NAME}.log; \
    done \
done

# Fetch Cortex config
kubectl get configmap --namespace=cortex cortex -o yaml 2>/dev/null > clusterlogs_cortex_configmap.log

# See opstrace-prelaunch/issues/1319
for API in cortex dd
do
    for K8SNAMESPACE in system-tenant default-tenant
    do
        kubectl get all --namespace=${K8SNAMESPACE} 2>/dev/null | awk '{print $1}' | \
            grep "pod/${API}-api" | while read PNAME; do echo "get logs for $PNAME" | \
            tee /dev/stderr ; kubectl logs $PNAME --namespace=${K8SNAMESPACE} --all-containers=true; done \
            > clusterlogs_${API}-http-api-proxy-${OPSTRACE_CLUSTER_NAME}-${K8SNAMESPACE}.log
    done
done

# Collect cert-manager resources to help root cause possible issues when issuing
# certificates.
kubectl describe clusterissuer > clusterlogs_clusterissuers-${OPSTRACE_CLUSTER_NAME}-ingress.log
for RESOURCE in certificate certificaterequest order challenge
do
    kubectl describe ${RESOURCE} --all-namespaces > clusterlogs_${RESOURCE}-${OPSTRACE_CLUSTER_NAME}-ingress.log
done

cp opstrace_cli_*log ${OPSTRACE_ARTIFACT_DIR}
cp kubectl_* ${OPSTRACE_ARTIFACT_DIR}
cp clusterlogs_* ${OPSTRACE_ARTIFACT_DIR}
