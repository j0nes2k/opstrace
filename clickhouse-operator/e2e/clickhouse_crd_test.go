package e2e

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"reflect"
	"strings"
	"time"

	clickhouseclient "github.com/ClickHouse/clickhouse-go/v2"
	"github.com/anthhub/forwarder"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/opstrace/opstrace/go/pkg/testutils"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
)

const (
	// use latest-alpine to test against so we integrate against current release.
	clickHouseServer = "clickhouse/clickhouse-server:latest-alpine"
)

var _ = Describe("ClickHouse cluster", func() {
	var clickhouseCluster *clickhousev1alpha1.ClickHouse

	JustBeforeEach(func() {
		// tests must initialize clickhouseCluster variable
		By("create cluster definition")
		Expect(k8sClient.Create(ctx, clickhouseCluster)).To(Succeed())

		Eventually(func(g Gomega) {
			ss := &appsv1.StatefulSetList{}
			g.Expect(k8sClient.List(ctx, ss)).To(Succeed())
			g.Expect(len(ss.Items)).To(BeNumerically(">=", *clickhouseCluster.Spec.Replicas))

			// TODO(joe): StatefulSets should have labels that we query
			By("check statefulsets exist for our cluster")
			replicas := []appsv1.StatefulSet{}
			for _, s := range ss.Items {
				if strings.HasPrefix(s.Name, clickhouseCluster.Name) {
					replicas = append(replicas, s)
				}
			}

			g.Expect(replicas).To(HaveLen(int(*clickhouseCluster.Spec.Replicas)))

			By("check each statefulset has one ready pod")
			for _, r := range replicas {
				g.Expect(r.Status.ReadyReplicas).To(BeNumerically("==", 1))
			}
		}).Should(Succeed())
	})

	AfterEach(func() {
		By("delete the cluster")
		Expect(k8sClient.Delete(ctx, clickhouseCluster)).To(Succeed())

		By("all managed resources should be deleted")
		Eventually(func(g Gomega) {
			managed := []client.ObjectList{
				&appsv1.StatefulSetList{},
				&corev1.ServiceList{},
				&corev1.SecretList{},
				&corev1.ConfigMapList{},
			}
			for _, m := range managed {
				g.Expect(k8sClient.List(ctx, m, client.InNamespace(clickhouseCluster.Namespace))).To(Succeed())
				v := reflect.ValueOf(m)
				// Note(joe): I can't find a nice way of doing this without reflection or lots of boilerplate for each type.
				items := reflect.Indirect(v).FieldByName("Items")
				// all cleared up.
				if items.IsNil() {
					continue
				}
				// anything with an owner reference should not be owned by the cluster resource.
				for i := 0; i < items.Len(); i++ {
					meta := items.Index(i).Addr().Interface().(metav1.Object)
					for _, o := range meta.GetOwnerReferences() {
						g.Expect(o.UID).NotTo(Equal(clickhouseCluster.UID))
					}
				}
			}
		}).Should(Succeed())
	})

	Context("with 2 replicas (without proxy)", func() {

		BeforeEach(func() {
			replicas := int32(2)
			clickhouseCluster = &clickhousev1alpha1.ClickHouse{
				ObjectMeta: metav1.ObjectMeta{
					// generates a name for our e2e test
					GenerateName: "cluster-e2e-test-",
					Namespace:    "default",
				},
				Spec: clickhousev1alpha1.ClickHouseSpec{
					// TODO(joe): run these tests with different image versions
					// need to define a support matrix of images for this operator
					Image:       clickHouseServer,
					Replicas:    &replicas,
					StorageSize: resource.MustParse("512M"),
					// Note(joe): assumes default storage class will work
				},
			}
		})

		It("should create the cluster", func() {
			By("check that clickhouse status ready is set")

			Eventually(func(g Gomega) {
				g.Expect(
					k8sClient.Get(ctx,
						types.NamespacedName{Namespace: clickhouseCluster.Namespace, Name: clickhouseCluster.Name}, clickhouseCluster)).
					Should(Succeed())
				g.Expect(clickhouseCluster.Status.Status).To(Equal(clickhousev1alpha1.StatusCompleted))
			})
		})

		It("should allow connection to a replica through named service", func() {
			By("port-forward load balanced CH service")

			ports, close := testutils.PortForward(ctx, restConfig, &forwarder.Option{
				RemotePort:  9000,
				ServiceName: clickhouseCluster.Name,
			}, Default)
			defer close()

			By("ping the database with the Golang client")
			db := clickhouseclient.OpenDB(&clickhouseclient.Options{
				Addr: []string{fmt.Sprintf("0.0.0.0:%d", ports[0].Local)},
			})
			Expect(db.Ping()).To(Succeed())
		})

	})

	waitForProxyDeployment := func() {
		Eventually(func(g Gomega) {
			replicaCount := int32(1)
			if clickhouseCluster.Spec.Proxy.Replicas != nil {
				replicaCount = *clickhouseCluster.Spec.Proxy.Replicas
			}

			ds := &appsv1.DeploymentList{}
			g.Expect(k8sClient.List(ctx, ds)).To(Succeed())
			g.Expect(len(ds.Items)).To(BeNumerically(">=", replicaCount))

			By("check Deployments exist for proxy")
			replicas := []appsv1.Deployment{}
			for _, d := range ds.Items {
				if strings.HasPrefix(d.Name, clickhouseCluster.Name) && strings.HasSuffix(d.Name, "-proxy") {
					replicas = append(replicas, d)
				}
			}

			g.Expect(replicas).To(HaveLen(int(replicaCount)))

			By("check Deployment has one ready pod")
			for _, r := range replicas {
				g.Expect(r.Status.ReadyReplicas).To(BeNumerically("==", 1))
			}
		}).Should(Succeed())
	}

	assertProxySuccessBehaviour := func() {
		It("should create the cluster", func() {
			By("check stateful sets exist and come online")

			waitForProxyDeployment()
		})
	}

	Context("with 3 replicas with proxy", func() {
		var secrets []*corev1.Secret

		BeforeEach(func() {
			By("reading example proxy manifest")
			// Read the example manifest
			filename, _ := filepath.Abs("../config/samples/example-with-proxy.yaml")
			exampleYamlBytes, err := ioutil.ReadFile(filename)
			Expect(err).NotTo(HaveOccurred())

			// Break up the yaml into separate strings
			exampleYamlRecs := strings.Split(string(exampleYamlBytes), "---")
			Expect(len(exampleYamlRecs)).Should(Equal(3))

			// Add the Secret, referenced by the CH object
			secrets = []*corev1.Secret{}
			By("creating Secrets")
			for i := 0; i < 2; i++ {
				userSecret := &corev1.Secret{}
				err = yaml.Unmarshal([]byte(exampleYamlRecs[i]), userSecret)
				Expect(err).NotTo(HaveOccurred())
				Expect(k8sClient.Create(ctx, userSecret)).Should(Succeed())
				secrets = append(secrets, userSecret)
			}

			// Add the CH object
			By("loading a new ClickHouse")
			clickhouseCluster = &clickhousev1alpha1.ClickHouse{}
			Expect(yaml.Unmarshal([]byte(exampleYamlRecs[2]), &clickhouseCluster)).To(Succeed())
			// Note(joe): parent BeforeEach will now create the cluster
		})

		assertProxySuccessBehaviour()

		It("should allow connection to a replica through named service", func() {
			By("port-forward load balanced CH service")

			waitForProxyDeployment()

			ports, close := testutils.PortForward(ctx, restConfig, &forwarder.Option{
				// HTTP port
				RemotePort:  80,
				ServiceName: fmt.Sprintf("%s-proxy", clickhouseCluster.Name),
			}, Default)
			defer close()

			By("ping the proxy over HTTP")

			url := fmt.Sprintf("http://127.0.0.1:%d/?query=SELECT%%201", ports[0].Local)
			userName := clickhouseCluster.Spec.Proxy.Users[0].Name
			userPass := ""
			for _, s := range secrets {
				if s.Name == clickhouseCluster.Spec.Proxy.Users[0].PasswordSecret.Name {
					userPass = string(s.Data[clickhouseCluster.Spec.Proxy.Users[0].PasswordSecret.Key])
				}
			}
			userDetails := fmt.Sprintf("%s:%s", userName, userPass)
			authzHeader := base64.StdEncoding.Strict().EncodeToString([]byte(userDetails))

			c := http.Client{
				Timeout: time.Minute,
			}
			req, _ := http.NewRequest("GET", url, nil)
			req.Header.Add("Authorization", fmt.Sprintf("Basic %s", authzHeader))

			Eventually(func(g Gomega) {
				rsp, err := c.Do(req)

				g.Expect(err).To(Succeed())
				g.Expect(rsp.StatusCode).To(Equal(http.StatusOK))
			}).Should(Succeed())
		})

		AfterEach(func() {
			By("delete Secrets")
			for _, s := range secrets {
				Expect(k8sClient.Delete(ctx, s)).To(Succeed())
			}
		})
	})

	Context("with 3 replicas with proxy and ssl enabled", func() {
		var secrets []*corev1.Secret
		var sslEnabled = true
		BeforeEach(func() {
			By("reading example proxy manifest")
			// Read the example manifest
			filename, _ := filepath.Abs("../config/samples/example-with-proxy.yaml")
			exampleYamlBytes, err := ioutil.ReadFile(filename)
			Expect(err).NotTo(HaveOccurred())

			// Break up the yaml into separate strings
			exampleYamlRecs := strings.Split(string(exampleYamlBytes), "---")
			Expect(len(exampleYamlRecs)).Should(Equal(3))

			// Add the Secret, referenced by the CH object
			secrets = []*corev1.Secret{}
			By("creating Secrets")
			for i := 0; i < 2; i++ {
				userSecret := &corev1.Secret{}
				err = yaml.Unmarshal([]byte(exampleYamlRecs[i]), userSecret)
				Expect(err).NotTo(HaveOccurred())
				Expect(k8sClient.Create(ctx, userSecret)).Should(Succeed())
				secrets = append(secrets, userSecret)
			}

			// Add the CH object
			By("loading a new ClickHouse")
			clickhouseCluster = &clickhousev1alpha1.ClickHouse{}
			Expect(yaml.Unmarshal([]byte(exampleYamlRecs[2]), &clickhouseCluster)).To(Succeed())
			clickhouseCluster.Spec.InternalSSL = &sslEnabled
			// Note(joe): parent BeforeEach will now create the cluster
		})

		assertProxySuccessBehaviour()

		AfterEach(func() {
			By("delete Secrets")
			for _, s := range secrets {
				Expect(k8sClient.Delete(ctx, s)).To(Succeed())
			}
		})
	})
})
