package controllers

import (
	"testing"

	"github.com/stretchr/testify/assert"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func Test_newClickHouseCluster(t *testing.T) {
	const defaultKeeperImage = "clickhouse/clickhouse-keeper:latest"
	const defaultClientConfig = `<config>
  <user>default</user>
</config>
`
	const defaultOpenSSLConfig = `<openSSL>
    <server>
      <!-- openssl req -subj "/CN=localhost" -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout /etc/clickhouse-server/server.key -out /etc/clickhouse-server/server.crt -->
      <certificateFile>/etc/clickhouse-server/ssl/tls.crt</certificateFile>
      <privateKeyFile>/etc/clickhouse-server/ssl/tls.key</privateKeyFile>
      <caConfig>/etc/clickhouse-server/ssl/ca.crt</caConfig>
      <!-- openssl dhparam -out /etc/clickhouse-server/dhparam.pem 4096 -->
      <dhParamsFile>/etc/clickhouse-server/ssl/dhparam.pem</dhParamsFile>
      <verificationMode>relaxed</verificationMode>
      <loadDefaultCAFile>true</loadDefaultCAFile>
      <cacheSessions>true</cacheSessions>
      <disableProtocols>sslv2,sslv3,tlsv1,tlsv1_1</disableProtocols>
      <preferServerCiphers>true</preferServerCiphers>
    </server>
    <client>
      <loadDefaultCAFile>true</loadDefaultCAFile>
      <caConfig>/etc/clickhouse-server/ssl/ca.crt</caConfig>
      <cacheSessions>true</cacheSessions>
      <disableProtocols>sslv2,sslv3,tlsv1,tlsv1_1</disableProtocols>
      <preferServerCiphers>true</preferServerCiphers>
      <!-- Use for self-signed: <verificationMode>none</verificationMode> -->
      <verificationMode>relaxed</verificationMode>
      <invalidCertificateHandler>
        <!-- Use for self-signed: <name>AcceptCertificateHandler</name> -->
        <name>RejectCertificateHandler</name>
      </invalidCertificateHandler>
    </client>
  </openSSL>`

	mkCR := func(name string, replicas int32, ssl bool, s3Enabled bool) *clickhousev1alpha1.ClickHouse {
		var (
			endPoint   = "https://clickhouse-data.s3.eu-west-3.amazonaws.com/root/"
			region     = "eu-west-3"
			objStorage *clickhousev1alpha1.ClickHouseObjectStorage
		)
		if s3Enabled {
			objStorage = &clickhousev1alpha1.ClickHouseObjectStorage{
				Backend:     "S3",
				EndpointURL: endPoint,
				Region:      &region,
			}
		}
		return &clickhousev1alpha1.ClickHouse{
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: "foo",
			},
			Spec: clickhousev1alpha1.ClickHouseSpec{
				Image:         "clickhouse/clickhouse-server:latest",
				Replicas:      &replicas,
				InternalSSL:   &ssl,
				ObjectStorage: objStorage,
			},
		}
	}
	type args struct {
		cr *clickhousev1alpha1.ClickHouse
	}
	tests := []struct {
		name string
		args args
		// xml parts are not directly compared as the maintenance of this test is too high
		want *clickHouseCluster
		// checks ConfigXML contains these strings
		wantConfigs []string
		// checks UsersXML contains these strings
		wantUsers []string
		// checks KeeperXML contains these strings
		wantKeeperConfigs []string
		wantErr           bool
	}{
		{
			name: "image name is required",
			args: args{
				&clickhousev1alpha1.ClickHouse{},
			},
			wantErr: true,
		},
		{
			"single replica results in single host",
			args{
				mkCR("test1", 1, false, false),
			},
			&clickHouseCluster{
				ClickHouse:      mkCR("test1", 1, false, false),
				KeeperImage:     defaultKeeperImage,
				Shards:          1,
				Replicas:        1,
				Hosts:           []string{"test1-0-0"},
				ClientConfigXML: defaultClientConfig,
			},
			// test entire configs here just as a base case
			[]string{`<clickhouse>
  <remote_servers>
    <test1>
      <shard>
        <internal_replication>true</internal_replication>
        <replica>
          <host>test1-0-0</host>
          <port>9000</port>
          <user>replica</user>
        </replica>
      </shard>
    </test1>
  </remote_servers>

  <macros>
    <cluster>test1</cluster>
    <shard>0</shard>
    <!-- Replica is full replica service name, not just index -->
    <replica from_env="HOST"/>
  </macros>

  <default_replica_name>{replica}</default_replica_name>
  <default_replica_path>/clickhouse/{cluster}/tables/{shard}/{database}/{table}/{uuid}</default_replica_path>

  <!-- Listen wildcard address to allow accepting connections from other containers and host network. -->
  <listen_host>::</listen_host>
  <listen_host>0.0.0.0</listen_host>
  <listen_try>1</listen_try>

  <logger>
    <!-- Possible levels: https://github.com/pocoproject/poco/blob/develop/Foundation/include/Poco/Logger.h#L105 -->
    <level>information</level>
    <log>/var/log/clickhouse-server/clickhouse-server.log</log>
    <errorlog>/var/log/clickhouse-server/clickhouse-server.err.log</errorlog>
    <size>1000M</size>
    <count>10</count>
    <!-- Default behavior is autodetection (log to console if not daemon mode and is tty) -->
    <console>1</console>
  </logger>

  <prometheus>
    <endpoint>/metrics</endpoint>
    <port>8001</port>
    <metrics>true</metrics>
    <events>true</events>
    <asynchronous_metrics>true</asynchronous_metrics>
  </prometheus>

  <query_log replace="1">
    <database>system</database>
    <table>query_log</table>
    <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
    <flush_interval_milliseconds>7500</flush_interval_milliseconds>
  </query_log>

  <query_thread_log remove="1"/>

  <part_log replace="1">
    <database>system</database>
    <table>part_log</table>
    <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
    <flush_interval_milliseconds>7500</flush_interval_milliseconds>
  </part_log>

  <zookeeper>
    <node>
      <host>127.0.0.1</host>
      <port>2181</port>
    </node>
    <session_timeout_ms>30000</session_timeout_ms>
    <operation_timeout_ms>10000</operation_timeout_ms>
  </zookeeper>
  <distributed_ddl>
    <path>/clickhouse/cluster/task_queue/ddl</path>
  </distributed_ddl>
</clickhouse>
`},
			[]string{`<clickhouse>
  <users>
    <!-- default user local pod access only -->
    <default>
      <networks>
        <ip>::1</ip>
        <ip>127.0.0.1</ip>
      </networks>
      <profile>default</profile>
      <quota>default</quota>
    </default>

    <!-- replica user for distributed queries -->
    <replica>
      <no_password/>
      <networks>
        <host>test1-0-0.foo.svc.cluster.local</host>
      </networks>
      <profile>default</profile>
      <quota>default</quota>
    </replica>

    <!--
        FIXME: If we want to allow client access from the operator for e.g. DB maintenance:
        1. Uncomment this section.
        2. Generate a random password and store it in a Secret. Do not regenerate if Secret already exists.
        3. Configure a CLICKHOUSE_OPERATOR_PASSWORD envvar to use that generated Secret.
        4. (optional) Figure out how to restrict access to only the clickhouse-operator pod?
           But this doesn't work if the operator is being run directly from a workstation.
    -->
    <!--
    <clickhouse_operator>
      <networks>
        <ip>127.0.0.1</ip>
        <ip>0.0.0.0/0</ip>
      </networks>
      <password from_env="CLICKHOUSE_OPERATOR_PASSWORD"/>
      <profile>clickhouse_operator</profile>
      <quota>default</quota>
    </clickhouse_operator>
    -->
  </users>

  <profiles>
    <clickhouse_operator>
      <log_queries>0</log_queries>
      <skip_unavailable_shards>1</skip_unavailable_shards>
      <http_connection_timeout>10</http_connection_timeout>
    </clickhouse_operator>

    <default>
      <log_queries>1</log_queries>
      <connect_timeout_with_failover_ms>1000</connect_timeout_with_failover_ms>
      <distributed_aggregation_memory_efficient>1</distributed_aggregation_memory_efficient>
      <parallel_view_processing>1</parallel_view_processing>
    </default>
  </profiles>
</clickhouse>
`},
			[]string{`<clickhouse>
  <listen_host>::</listen_host>
  <listen_host>0.0.0.0</listen_host>
  <listen_try>1</listen_try>

  <logger>
    <level>information</level>
    <log>/var/log/clickhouse-keeper/clickhouse-keeper.log</log>
    <errorlog>/var/log/clickhouse-keeper/clickhouse-keeper.err.log</errorlog>
    <size>1000M</size>
    <count>10</count>
    <console>1</console>
  </logger>

  <max_connections>4096</max_connections>

  <keeper_server>
    <tcp_port>2181</tcp_port>
    <server_id from_env="REPLICA"/>

    <log_storage_path>/var/lib/clickhouse/coordination/log</log_storage_path>
    <snapshot_storage_path>/var/lib/clickhouse/coordination/snapshots</snapshot_storage_path>

    <coordination_settings>
      <operation_timeout_ms>10000</operation_timeout_ms>
      <min_session_timeout_ms>10000</min_session_timeout_ms>
      <session_timeout_ms>100000</session_timeout_ms>
      <raft_logs_level>information</raft_logs_level>
      <!-- All settings listed in https://github.com/ClickHouse/ClickHouse/blob/master/src/Coordination/CoordinationSettings.h -->
    </coordination_settings>

    <raft_configuration>
      <server>
        <id>0</id>
        <hostname>test1-0-0</hostname>
        <port>9444</port>
      </server>
    </raft_configuration>
  </keeper_server>
</clickhouse>`},
			false,
		},
		{
			"multiple replicas results in same number of hosts with fixed shard",
			args{
				mkCR("test2", 3, false, false),
			},
			&clickHouseCluster{
				ClickHouse:      mkCR("test2", 3, false, false),
				KeeperImage:     defaultKeeperImage,
				Shards:          1,
				Replicas:        3,
				Hosts:           []string{"test2-0-0", "test2-0-1", "test2-0-2"},
				ClientConfigXML: defaultClientConfig,
			},
			[]string{`<remote_servers>
    <test2>
      <shard>
        <internal_replication>true</internal_replication>
        <replica>
          <host>test2-0-0</host>
          <port>9000</port>
          <user>replica</user>
        </replica>
        <replica>
          <host>test2-0-1</host>
          <port>9000</port>
          <user>replica</user>
        </replica>
        <replica>
          <host>test2-0-2</host>
          <port>9000</port>
          <user>replica</user>
        </replica>
      </shard>
    </test2>
  </remote_servers>`},
			[]string{`<networks>
        <host>test2-0-0.foo.svc.cluster.local</host>
        <host>test2-0-1.foo.svc.cluster.local</host>
        <host>test2-0-2.foo.svc.cluster.local</host>
      </networks>`},
			[]string{`<raft_configuration>
      <server>
        <id>0</id>
        <hostname>test2-0-0</hostname>
        <port>9444</port>
      </server>
      <server>
        <id>1</id>
        <hostname>test2-0-1</hostname>
        <port>9444</port>
      </server>
      <server>
        <id>2</id>
        <hostname>test2-0-2</hostname>
        <port>9444</port>
      </server>
    </raft_configuration>`},
			false,
		},
		{
			"multiple replicas results in same number of hosts with fixed shard and ssl enabled",
			args{
				mkCR("test2", 3, true, false),
			},
			&clickHouseCluster{
				ClickHouse:  mkCR("test2", 3, true, false),
				KeeperImage: defaultKeeperImage,
				Shards:      1,
				Replicas:    3,
				Hosts:       []string{"test2-0-0", "test2-0-1", "test2-0-2"},
				InternalSSL: true,
				DHParams: `-----BEGIN DH PARAMETERS-----
MIICCAKCAgEAk/L1+jJH7UjtmnnHvhTPbtTxUpx6E1UWPMq0EfLyprtDNSNd3VT0
6XvPJKg8I1XNzYGyh1WUl8kFWNc8s9WLuIRQVxa2CFt82spTgryU3p8T+gHh9xGM
KfRzcZ+1pqT+QOz4gqtyOjF6+U3Vw82x+ZhU6PGWyRTsm1nLHYPm+OLK+jU/bZmI
2Tt88jNLrfKwFVn85+47adEolBK5gInCbmBiVVHkK+m0CvONMZFrE887M8zsJ4Vd
zlJc9Ef9rbPtLTaa6AEFv2xv0mxqU0c0MolnrtaNoG1g8oB0HMMJic+iQ6zOgEjE
dBhJ70RF5t0P/zlc+uZKyTA2kwvBIhNVNcL4fVg3l8b3Wzra17YahMqtlU+CH8Qh
27P58PRh1EvO91nVXYYiWgfxt6Tb3/XPhE2R7SciNYykhQaP3KmuS5RIx41w2M4B
WQ0ugG5JiSOvco5eHtvC3u9aJxDQ1T4ftdB+LVnFF121egC5tyRVro9KPqziVvmM
PBj/2cxmKM8js8LH7ut0tVDkJDxpb2pT261G3PH7SjX4+PrCBrT664jHcrhrQgYx
SEKYR5eYxt/I1o9B9XH3i/QB1X7bpowCKy508pG0uLYUIEM/DexetI4Jh3oTLv0L
Q/J1TW1SxajqurgRsXSvxqXtsMTm5L6CYmRrtEXueRARIpNZPUW7ISsCAQI=
-----END DH PARAMETERS-----
`,
				ClientConfigXML: `<config>
  <user>default</user>
  <secure>true</secure>
  <openSSL>
    <client>
      <caConfig>/etc/clickhouse-server/ssl/ca.crt</caConfig>
      <loadDefaultCAFile>true</loadDefaultCAFile>
      <cacheSessions>true</cacheSessions>
      <disableProtocols>sslv2,sslv3,tlsv1,tlsv1_1</disableProtocols>
      <preferServerCiphers>true</preferServerCiphers>
    </client>
  </openSSL>
</config>
`,
			},
			[]string{`<remote_servers>
    <test2>
      <shard>
        <internal_replication>true</internal_replication>
        <replica>
          <host>test2-0-0</host>
          <port>9440</port>
          <secure>1</secure>
          <user>replica</user>
        </replica>
        <replica>
          <host>test2-0-1</host>
          <port>9440</port>
          <secure>1</secure>
          <user>replica</user>
        </replica>
        <replica>
          <host>test2-0-2</host>
          <port>9440</port>
          <secure>1</secure>
          <user>replica</user>
        </replica>
      </shard>
    </test2>
  </remote_servers>`, `<https_port>8443</https_port>
  <tcp_port_secure>9440</tcp_port_secure>
  <interserver_https_port>9010</interserver_https_port>
  <interserver_http_port remove="1"/>`,
				defaultOpenSSLConfig},
			[]string{`<replica>
      <no_password/>
      <networks>
        <host>test2-0-0.foo.svc.cluster.local</host>
        <host>test2-0-1.foo.svc.cluster.local</host>
        <host>test2-0-2.foo.svc.cluster.local</host>
      </networks>
      <profile>default</profile>
      <quota>default</quota>
    </replica>`},
			[]string{`<keeper_server>
    <tcp_port_secure>9281</tcp_port_secure>`,
				`<raft_configuration>
      <server>
        <id>0</id>
        <hostname>test2-0-0</hostname>
        <port>9444</port>
        <secure>1</secure>
      </server>
      <server>
        <id>1</id>
        <hostname>test2-0-1</hostname>
        <port>9444</port>
        <secure>1</secure>
      </server>
      <server>
        <id>2</id>
        <hostname>test2-0-2</hostname>
        <port>9444</port>
        <secure>1</secure>
      </server>
    </raft_configuration>`,
				defaultOpenSSLConfig},
			false,
		},
		{
			"single replica results with s3 support enabled",
			args{
				mkCR("test1", 1, false, true),
			},
			&clickHouseCluster{
				ClickHouse:           mkCR("test1", 1, false, true),
				KeeperImage:          defaultKeeperImage,
				ObjectStorageEnabled: true,
				Shards:               1,
				Replicas:             1,
				Hosts:                []string{"test1-0-0"},
				ClientConfigXML:      defaultClientConfig,
			},
			[]string{`<storage_configuration>
  <disks>
    <disk_s3>
      <type>s3</type>
      <endpoint>https://clickhouse-data.s3.eu-west-3.amazonaws.com/root/</endpoint>
      <use_environment_credentials>false</use_environment_credentials>
      <access_key_id from_env="ACCESS_KEY_ID"/>
      <secret_access_key from_env="SECRET_ACCESS_KEY"/>
      <region from_env="STORAGE_REGION"/>
<!--      <server_side_encryption_customer_key_base64 from_env="SSE_CUSTOMER_KEY"/>-->
      <connect_timeout_ms>10000</connect_timeout_ms>
      <request_timeout_ms>5000</request_timeout_ms>
      <retry_attempts>10</retry_attempts>
      <single_read_retries>4</single_read_retries>
      <min_bytes_for_seek>1000</min_bytes_for_seek>
      <metadata_path>/var/lib/clickhouse/disks/s3/</metadata_path>
      <cache_enabled>true</cache_enabled>
      <cache_path>/var/lib/clickhouse/disks/s3/cache/</cache_path>
      <skip_access_check>false</skip_access_check>
    </disk_s3>
  </disks>
  <policies>
    <policy_s3_only>
      <volumes>
        <volume_s3>
          <disk>disk_s3</disk>
        </volume_s3>
      </volumes>
    </policy_s3_only>
  </policies>
  </storage_configuration>`},
			nil,
			nil,
			false,
		},
		{
			"keeper image name can be explicitly set",
			args{
				func() *clickhousev1alpha1.ClickHouse {
					ch := mkCR("test", 1, false, false)
					i := "myspecialkeeperimage"
					ch.Spec.KeeperImage = &i
					return ch
				}(),
			},
			&clickHouseCluster{
				ClickHouse: func() *clickhousev1alpha1.ClickHouse {
					ch := mkCR("test", 1, false, false)
					i := "myspecialkeeperimage"
					ch.Spec.KeeperImage = &i
					return ch
				}(),
				KeeperImage:     "myspecialkeeperimage",
				Shards:          1,
				Replicas:        1,
				Hosts:           []string{"test-0-0"},
				ClientConfigXML: defaultClientConfig,
			},
			nil,
			nil,
			nil,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := newClickHouseCluster(tt.args.cr)
			if (err == nil) == tt.wantErr {
				t.Errorf("wantErr: %v, err: %v", tt.wantErr, err)
			}

			if err != nil {
				assert.Nil(t, got)
				return
			}

			checkXML := func(got string, want []string, message string) {
				for _, w := range want {
					assert.Contains(t, got, w, message)
				}
			}
			checkXML(got.ConfigXML, tt.wantConfigs, "Config XML")
			checkXML(got.UsersXML, tt.wantUsers, "Users XML")
			checkXML(got.KeeperXML, tt.wantKeeperConfigs, "Keeper XML")
			got.ConfigXML = ""
			got.UsersXML = ""
			got.KeeperXML = ""
			assert.Equal(t, tt.want, got)
		})
	}
}
