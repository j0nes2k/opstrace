/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"crypto/sha256"
	"fmt"
	"strings"

	certmanagerv1 "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ClickHouseController runs reconciliation of a ClickHouse object.
type ClickHouseController struct {
	client.Client
	Log      logr.Logger
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
}

const (
	StatefulSetOwnerKey = ".metadata.controller"
	CHDataVolumeName    = "data"
)

// Configure permissions to manage ClickHouse CRDs:
//+kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses/finalizers,verbs=update

// Configure permissions to deploy ClickHouse instances based on CRDs:
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=persistentvolumeclaims,verbs=get;list;watch;update;patch
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch
//+kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//nolint
//+kubebuilder:rbac:groups=cert-manager.io,resources=clusterissuers;issuers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=cert-manager.io,resources=issuers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=cert-manager.io,resources=certificates,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=cert-manager.io,resources=certificaterequests,verbs=get;list;watch;create;update;patch;delete

// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.11.0/pkg/reconcile
func (r *ClickHouseController) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("clickhouse", req.NamespacedName)

	crd := &clickhousev1alpha1.ClickHouse{}
	if err := r.Get(ctx, req.NamespacedName, crd); err != nil {
		if !apierrors.IsNotFound(err) {
			return ctrl.Result{}, fmt.Errorf("unable to get clickhouse CR: %w", err)
		}
		return ctrl.Result{}, nil
	}

	ch, err := newClickHouseCluster(crd)
	if err != nil {
		log.Error(err, "could not create cluster from resource")
		return ctrl.Result{}, err
	}

	reconciler := Reconciler{
		scheme: r.Scheme,
		client: r.Client,
		crd:    crd,
		log:    log,
	}
	log.Info("reconciling ClickHouse")

	kr := newClickHouseConfigMaps(req, ch)
	kr = append(kr, newClickHouseServices(req, ch)...)
	kr = append(kr, newClickHouseStatefulSets(req, ch)...)

	if ch.proxyEnabled() {
		krs, err := r.setupProxy(ctx, ch)
		if err != nil {
			log.Error(err, "could not set up proxy")
			return ctrl.Result{}, err
		}
		kr = append(kr, krs...)
	}

	if ch.InternalSSL {
		kr = append(kr, newCACertificateResources(ch)...)
		kr = append(kr, newCertificateResources(ch)...)
		kr = append(kr, newDHParamSecret(ch)...)
	}

	res, err := reconciler.ReconcileStorageSize(ctx, ch, req)
	if err != nil || res != nil {
		if err != nil {
			return ctrl.Result{}, err
		}
		return *res, nil
	}

	for _, entry := range kr {
		err := reconciler.Reconcile(ctx, entry)
		if err != nil {
			r.Recorder.Eventf(
				crd,
				corev1.EventTypeWarning,
				"ReconcileFailed",
				"failed to reconcile crd=%s resource=%s/%s err=%s",
				req.NamespacedName,
				entry.obj.GetObjectKind().GroupVersionKind().Kind,
				entry.obj.GetName(),
				err,
			)
			// TODO: handle error
			// nolint:errcheck
			_ = r.updateStatus(ctx, req, ch)
			return ctrl.Result{}, err
		}
	}

	r.Recorder.Eventf(
		crd,
		corev1.EventTypeNormal,
		"ReconcileSucceeded",
		"reconciled crd=%s",
		req.NamespacedName,
	)
	if err := r.updateStatus(ctx, req, ch); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ClickHouseController) SetupWithManager(mgr ctrl.Manager) error {
	groupVersion := clickhousev1alpha1.GroupVersion.String()
	// index created StatefulSets so we can quickly query these
	if err := mgr.GetFieldIndexer().IndexField(
		context.Background(), &appsv1.StatefulSet{}, StatefulSetOwnerKey, func(o client.Object) []string {
			// nolint:errcheck
			statefulSet := o.(*appsv1.StatefulSet)
			owner := metav1.GetControllerOf(statefulSet)
			if owner == nil {
				return nil
			}
			// check that the owner is for this controller
			if owner.APIVersion != groupVersion || owner.Kind != "ClickHouse" {
				return nil
			}

			return []string{owner.Name}
		}); err != nil {
		return fmt.Errorf("set field indexer: %w", err)
	}

	if err := ctrl.NewControllerManagedBy(mgr).
		For(&clickhousev1alpha1.ClickHouse{}).
		// add resource types we expect to "own" to trigger reconcile on changes
		Owns(&appsv1.StatefulSet{}).
		Owns(&corev1.Service{}).
		Owns(&corev1.Secret{}).
		Owns(&corev1.ConfigMap{}).
		Watches(
			&source.Kind{Type: &corev1.PersistentVolumeClaim{}},
			handler.EnqueueRequestsFromMapFunc(r.mapPVCToRequests),
		). // Used for reconciling status on resize
		Owns(&certmanagerv1.Issuer{}).
		Owns(&certmanagerv1.ClusterIssuer{}).
		Owns(&certmanagerv1.Certificate{}).
		Complete(r); err != nil {
		return fmt.Errorf("set up controller: %w", err)
	}
	return nil
}

func (r *ClickHouseController) mapPVCToRequests(object client.Object) []reconcile.Request {
	requests := []reconcile.Request{}

	labels := object.GetLabels()

	app, okApp := labels["app"]
	if !okApp || app != "clickhouse" {
		return requests
	}

	clusterName, okName := labels["name"]
	replica, okReplica := labels["replica"]
	shard, okShard := labels["shard"]
	if !okName || !okReplica || !okShard {
		return requests
	}

	// FIXME(prozlach): In case of very long cluster names, we may run into k8s
	// shortening the name of the PVC causing controller to miss an event.
	namePrefix := fmt.Sprintf("data-%s-%s-%s-", clusterName, shard, replica)
	if !strings.HasPrefix(object.GetName(), namePrefix) {
		return requests
	}

	requests = append(requests, reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      clusterName,
			Namespace: object.GetNamespace(),
		},
	})
	return requests
}

func (r *ClickHouseController) updateStatus(ctx context.Context,
	req ctrl.Request, ch *clickHouseCluster) error {
	log := r.Log.WithValues("clickhouse", req.NamespacedName)

	readyReplicas, readyShards, err := r.getStatefulSetStatus(ctx, req, ch)
	if err != nil {
		return fmt.Errorf("could not get managed StatefulSet status: %w", err)
	}

	readyStorage, err := r.getStorageStatus(ctx, req, ch)
	if err != nil {
		return fmt.Errorf("could not get PVC status: %w", err)
	}

	// get CR again incase it has already been updated somewhere else
	instance := &clickhousev1alpha1.ClickHouse{}
	if err := r.Get(ctx, req.NamespacedName, instance); err != nil {
		if apierrors.IsNotFound(err) {
			log.V(2).Info("ClickHouse probably deleted before final status update")
			return nil
		}
		return fmt.Errorf("get instance for status update: %w", err)
	}

	instance.Status.Status = clickhousev1alpha1.StatusInProgress
	if readyShards == ch.Shards && readyStorage == ch.Replicas {
		instance.Status.Status = clickhousev1alpha1.StatusCompleted
	}
	instance.Status.ReadyReplicas = readyReplicas
	instance.Status.ReadyShards = readyShards
	instance.Status.ReadyStorage = readyStorage
	instance.Status.ObservedGeneration = instance.Generation

	if err := r.Client.Status().Update(ctx, instance); err != nil {
		return fmt.Errorf("crd status update failed: %w", err)
	}
	log.V(5).Info("cluster status updated", "status", instance.Status.Status)
	return err
}

func (r *ClickHouseController) getStatefulSetStatus(ctx context.Context,
	req ctrl.Request, ch *clickHouseCluster) (readyReplicas, readyShards int32, err error) {
	var ownedStatefulSets appsv1.StatefulSetList
	if err = r.List(ctx, &ownedStatefulSets,
		client.InNamespace(req.Namespace), client.MatchingFields{StatefulSetOwnerKey: req.Name}); err != nil {
		err = fmt.Errorf("get managed StatefulSets: %w", err)
		return
	}
	readyReplicas = int32(0)
	for _, ss := range ownedStatefulSets.Items {
		ss := ss
		if isStsReady(&ss) {
			readyReplicas++
		}
	}
	// NOTE(joe): only 1 shard at the moment, just check all replicas are ready.
	if readyReplicas == ch.Replicas {
		readyShards = 1
	}
	return
}

func (r *ClickHouseController) getStorageStatus(
	ctx context.Context,
	req ctrl.Request,
	ch *clickHouseCluster,
) (int32, error) {
	var pvcs corev1.PersistentVolumeClaimList
	err := r.List(ctx, &pvcs, client.InNamespace(req.Namespace), client.MatchingLabels(ch.labels()))
	if err != nil {
		return 0, fmt.Errorf("unable to list PVCs: %w", err)
	}

	readyStorage := int32(0)
	for _, pvc := range pvcs.Items {
		if !strings.HasPrefix(pvc.Name, "data-") {
			// We are only interested in `data` volumes. `keeper` and `logs`
			// volume resizing is not supported.
			continue
		}

		requestedSpace, okReq := pvc.Spec.Resources.Requests[corev1.ResourceStorage]
		currentSpace, okCur := pvc.Status.Capacity[corev1.ResourceStorage]
		if !okReq || !okCur {
			// Wut??
			r.Log.V(5).Info(
				"Warning: unable to verify status of pvc, as required information is not (yet) available",
				"okReq", okReq, "okCur", okCur,
				"namespace", pvc.Namespace, "name", pvc.Name,
			)
			continue
		}
		if requestedSpace == currentSpace {
			readyStorage++
		}
	}

	return readyStorage, nil
}

func (r *ClickHouseController) populateSecret(ctx context.Context, namespace, name, key string) (string, error) {
	s := &corev1.Secret{}
	if err := r.Client.Get(ctx, client.ObjectKey{
		Name:      name,
		Namespace: namespace,
	}, s); err != nil {
		return "", fmt.Errorf("get secret: %w", err)
	}

	return string(s.Data[key]), nil
}

func (r *ClickHouseController) setupProxy(
	ctx context.Context, cluster *clickHouseCluster) ([]*KubernetesResource, error) {
	// Populate passwords
	passwords := map[string]string{}

	for _, u := range cluster.Spec.AdminUsers {
		data, err := r.populateSecret(ctx, cluster.Namespace, u.SecretKeyRef.Name, u.SecretKeyRef.Key)
		if err != nil {
			r.Log.Error(err, "Warning: failed to lookup secret",
				"secretName", u.SecretKeyRef.Name, "secretKey", u.SecretKeyRef.Key)
		} else {
			passwords[fmt.Sprintf("cluster/%s", u.Name)] = data
		}
	}

	for _, u := range cluster.Spec.Proxy.Users {
		data, err := r.populateSecret(ctx, cluster.Namespace, u.PasswordSecret.Name, u.PasswordSecret.Key)
		if err != nil {
			r.Log.Error(err, "Warning: failed to lookup secret",
				"secretName", u.PasswordSecret.Name, "secretKey", u.PasswordSecret.Key)
		} else {
			passwords[fmt.Sprintf("proxy/%s", u.Name)] = data
		}
	}

	if err := cluster.configureProxy(passwords); err != nil {
		return nil, err
	}

	h := sha256.New()
	h.Write([]byte(cluster.ProxyYAML))
	cfgChecksum := fmt.Sprintf("%x", h.Sum(nil))

	labels := cluster.labelsMerged(map[string]string{
		"app": "clickhouse-proxy",
	})
	kr := newProxyConfig(cluster, cluster.ProxyYAML, labels)
	kr = append(kr, newProxyDeployments(cluster, cfgChecksum, labels)...)
	kr = append(kr, newProxyServices(cluster, labels)...)

	return kr, nil
}
