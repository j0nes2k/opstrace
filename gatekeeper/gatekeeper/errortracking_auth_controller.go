package gatekeeper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"sync"
	"time"

	"github.com/opstrace/opstrace/go/pkg/constants"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/cache/v8"
	log "github.com/sirupsen/logrus"
)

var (
	sentryKeyRegex = regexp.MustCompile(`sentry_key=(\w+)`)
	// expected URL construction for global deployment
	globalErrorTrackingUrlRegex = regexp.MustCompile(`/errortracking/api/v1/projects/api/(\d+)/(store|envelope)`)
	// expected URL construction for per-group deployment
	perGroupErrorTrackingUrlRegex = regexp.MustCompile(`/v1/errortracking/(\d+)/projects/api/(\d+)/(store|envelope)`)
)

type IngestAuthHeader struct {
	SentryAuth string `header:"X-Sentry-Auth" binding:"required"`
}

func (h IngestAuthHeader) ParseSentryKey() (string, error) {
	// parse the header
	log.Debug("parsing x-sentry-auth header")

	match := sentryKeyRegex.FindStringSubmatch(h.SentryAuth)
	if match == nil {
		return "", fmt.Errorf("invalid x-sentry-auth header")
	}

	if match[1] == "" {
		return "", fmt.Errorf("empty sentry key")
	}

	return match[1], nil
}

func parseSentryKeyFromOriginalURL(ctx *gin.Context) (string, error) {
	origURL := ctx.GetHeader("X-Original-Url")

	if origURL == "" {
		return "", fmt.Errorf("parse sentry key from header failed, x-original-url header not set")
	}

	u, err := url.Parse(origURL)
	if err != nil {
		return "", fmt.Errorf("parse sentry key from header failed, invalid x-original-url: %w", err)
	}

	key := u.Query().Get("sentry_key")
	if key == "" {
		return "", fmt.Errorf("parse sentry key from header failed, empty sentry key")
	}

	return key, nil
}

type ErrorTrackingAuthHandler interface {
	HandleErrorTrackingAuth(ctx *gin.Context)
}

func NewErrorTrackingAuthHandler() ErrorTrackingAuthHandler {
	return &errorTrackingAuthHandler{
		store:     make(map[string]*sync.Mutex),
		storeLock: new(sync.Mutex),
	}
}

type errorTrackingAuthHandler struct {
	store     map[string]*sync.Mutex
	storeLock *sync.Mutex
}

// Handles the API basic auth request from nginx-ingress for error tracking API
// endpoints. See the swagger.yaml file in go/pkg/errortracking for more details
// on the error tracking API.
//
// Response codes are:
// - 200 for auth success (provided token is valid)
// - 401 for auth failure (public is missing, malformed, or invalid)
// - 500 for internal errors
// Note that the nginx-ingress external auth only supports these status codes.
// Any codes other than 200/401 will result in a 500 being sent to the client.
//
// See https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
func (h *errorTrackingAuthHandler) HandleErrorTrackingAuth(ctx *gin.Context) {
	origMethod := ctx.GetHeader("X-Original-Method")

	if origMethod == http.MethodPost {
		// A POST request to the store and envelope endpoints must include a
		// public key that is passed by Sentry Client SDKs.
		h.handleIngestRequest(ctx)
	} else {
		// Any other request must include a token that is shared with the gitlab
		// instance.
		h.handleReadRequest(ctx)
	}
}

// Handles POST request authentication for the error tracking ingest API.
func (h *errorTrackingAuthHandler) handleIngestRequest(ctx *gin.Context) {
	sentryKey, err := extractSentryKey(ctx)
	if err != nil {
		log.WithError(err).Debug("failed to extract sentry key")
		ctx.String(401, "Unauthorized")
		return
	}

	projectID, err := extractProjectID(ctx)
	if err != nil {
		log.WithError(err).Debug("failed to extract project_id from x-original-url")
		ctx.String(401, "Unauthorized")
		return
	}

	c := GetCache(ctx)
	key := fmt.Sprintf("%s-%s", sentryKey, projectID)
	var enabled bool

	err = c.Get(ctx, key, &enabled)
	// nolint:nestif
	if err != nil {
		h.storeLock.Lock()

		// acquire a lock to prevent sending multiple requests to gitlab
		// instance to validate the project id and sentry key pair
		m := h.store[key]
		if m == nil {
			m = &sync.Mutex{}
			h.store[key] = m
		}

		h.storeLock.Unlock()

		m.Lock()
		defer m.Unlock()

		// check if someone set the key while we waited to acquire the lock
		err = c.Get(ctx, key, &enabled)
		if err != nil {
			log.WithField("projectID", projectID).Info("sending auth request to gitlab instance")

			enabled, err = checkIfKeyIsEnabled(ctx, sentryKey, projectID)
			// don't cache the error and try again later
			if err != nil {
				log.WithField("err", err).Debug("invalid sentry key, project id pair")
				ctx.String(401, "Unauthorized")
				return
			}
			// request was made, now cache the response and ignore any errors
			err = c.Set(&cache.Item{
				Ctx:   ctx,
				Key:   key,
				Value: enabled,
				// TODO: bump the cache setting?
				// TODO: should it match the nginx ingress auth-cache setting?
				TTL: 2 * time.Minute,
			})
			if err != nil {
				log.WithField("err", err).Debug("failed to set cache key")
			}
		}
	}

	if !enabled {
		log.Debug("sentry key disabled")
		ctx.String(401, "Unauthorized")
		return
	}

	ctx.String(200, "Success")
}

type OpenAPIAuthHeader struct {
	Token string `header:"Gitlab-Error-Tracking-Token" binding:"required"`
}

// Handles authentication for error tracking management API. The token is sent
// in the HTTP header Gitlab-Error-Tracking-Token.
func (h *errorTrackingAuthHandler) handleReadRequest(ctx *gin.Context) {
	var authHeader OpenAPIAuthHeader

	err := ctx.ShouldBindHeader(&authHeader)
	if err != nil {
		log.WithError(err).Debug("auth header not set or invalid")
		ctx.String(401, "Unauthorized")
		return
	}

	cfg := GetConfig(ctx)
	if authHeader.Token != cfg.GitlabInternalEndpointToken {
		log.Debug("auth token mismatch")
		ctx.String(401, "Unauthorized")
		return
	}

	ctx.String(200, "Success")
}

func extractSentryKey(ctx *gin.Context) (string, error) {
	// From https://develop.sentry.dev/sdk/overview/#authentication
	//
	// An authentication header is expected to be sent along with the message
	// body, which acts as an ownership identifier:
	//
	// X-Sentry-Auth: Sentry sentry_version=7,
	//   sentry_client=<client version, arbitrary>,
	//   sentry_timestamp=<current timestamp>,
	//   sentry_key=<public api key>,
	//   sentry_secret=<secret api key>
	//
	// In situations where it’s not possible to send the custom X-Sentry-Auth
	// header, it’s possible to send these values via the querystring:
	//
	// ?sentry_version=7&sentry_key=<public api key>&sentry_secret=<secret api key>...
	//
	var authHeader IngestAuthHeader

	err := ctx.ShouldBindHeader(&authHeader)
	if err != nil {
		// fallback to parsing the x-original-url query parameters
		return parseSentryKeyFromOriginalURL(ctx)
	}

	return authHeader.ParseSentryKey()
}

func extractProjectID(ctx *gin.Context) (string, error) {
	origURL := ctx.GetHeader("X-Original-Url")

	if origURL == "" {
		return "", fmt.Errorf("failed to parse projectID, x-original-url header not set")
	}

	u, err := url.Parse(origURL)
	if err != nil {
		return "", fmt.Errorf("failed to parse projectID, error parsing x-original-url: %w", err)
	}

	// extract projectID from the url using a regex

	// expect to receive authentication requests from both global & per-group deployments
	// temporarily, while we migrate all clients over to per-group deployments.
	var (
		values    []string
		projectID string
	)
	// parse URL as from the global deployment first
	values = globalErrorTrackingUrlRegex.FindStringSubmatch(u.Path)
	if len(values) == 0 {
		// failing which, parse URL as having come from a per-group deployment
		values = perGroupErrorTrackingUrlRegex.FindStringSubmatch(u.Path)
		if len(values) == 0 {
			return "", fmt.Errorf("failed to parse projectID, failed to parse x-original-url")
		}
		projectID = values[2] // [0]: path, [1]: group ID, [2]: project ID
	} else {
		projectID = values[1] // [0]: path, [1]: project ID
	}

	return projectID, nil
}

// Makes a request to the Gitlab instance to validate the sentry key, project id
// pair.
// See: https://docs.gitlab.com/ee/development/internal_api/#authenticate-error-tracking-requests
func checkIfKeyIsEnabled(ctx *gin.Context, sentryKey, projectID string) (bool, error) {
	data := &struct {
		ProjectID string `json:"project_id"`
		PublicKey string `json:"public_key"`
	}{
		ProjectID: projectID,
		PublicKey: sentryKey,
	}

	body, err := json.Marshal(data)
	if err != nil {
		return false, fmt.Errorf("failed to generate pair to validate: %w", err)
	}

	cfg := GetConfig(ctx)

	req, err := http.NewRequest(http.MethodPost, cfg.GitlabInternalEndpointAddr, bytes.NewReader(body))
	if err != nil {
		return false, fmt.Errorf("failed to create http req: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set(constants.GitlabErrorTrackingTokenHeader, cfg.GitlabInternalEndpointToken)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return false, fmt.Errorf("error checking sentry key: %w", err)
	}

	if res.StatusCode != http.StatusOK {
		return false, fmt.Errorf("request to check sentry failed with status code: %s", res.Status)
	}

	// helper struct to decode incoming json payload
	post := &struct {
		Enabled bool `json:"enabled"`
	}{}
	err = json.NewDecoder(res.Body).Decode(post)
	if err != nil {
		return false, fmt.Errorf("failed do decode response: %w", err)
	}

	return post.Enabled, nil
}
