package gatekeeper

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path"
	"testing"

	"github.com/opstrace/opstrace/go/pkg/constants"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/cache/v8"
	"github.com/stretchr/testify/assert"
)

type pair struct {
	key   string
	value string
}

type mockRedis struct {
	store map[string]interface{}
}

func (m *mockRedis) Set(item *cache.Item) error {
	m.store[item.Key] = item.Value
	return nil
}

func (m *mockRedis) Get(ctx context.Context, key string, value interface{}) error {
	return fmt.Errorf("foo")
}

func newMockRedis() *mockRedis {
	return &mockRedis{
		store: make(map[string]interface{}),
	}
}

func TestHandleErrorTrackingAuth(t *testing.T) {
	gin.SetMode(gin.TestMode)
	gitlabInternalEndpointToken := "secret"

	failHandler := func(w http.ResponseWriter, r *http.Request) {
		assert.Fail(t, "the gitlab internal endpoint should not be called")
	}

	successHandler := func(w http.ResponseWriter, r *http.Request) {
		secret := r.Header.Get(constants.GitlabErrorTrackingTokenHeader)
		assert.Equal(t, gitlabInternalEndpointToken, secret, "invalid gitlab shared secret")
		assert.Equal(t, r.Method, http.MethodPost, "invalid request method")

		w.WriteHeader(200)
		w.Write([]byte(`{"enabled":true}`))
	}

	mockGitlab := httptest.NewServer(http.NewServeMux())
	defer mockGitlab.Close()

	// Parse gitlab address and set up the internal API endpoint
	internalEndpointAddr, err := url.Parse(mockGitlab.URL)
	assert.Nil(t, err)
	internalEndpointAddr.Path = path.Join(internalEndpointAddr.Path, constants.GitlabInternalErrorTrackingEndpoint)

	router := gin.Default()
	router.Use(Config(&ConfigOptions{
		GitlabAddr:                  mockGitlab.URL,
		GitlabInternalEndpointAddr:  internalEndpointAddr.String(),
		GitlabInternalEndpointToken: gitlabInternalEndpointToken,
	}))
	router.Use(func(ctx *gin.Context) {
		ctx.Set(cacheClientKey, newMockRedis())
	})
	SetRoutes(router)

	for _, tc := range []struct {
		name                          string
		headers                       []pair
		expected                      int
		gitlabInternalEndpointHandler http.HandlerFunc
	}{
		{
			name: "should fail with no header and no param set",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with random headers and params",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "foo", value: "bar"},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with invalid header",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Sentry-Auth", value: "foo=bar"},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should succeed with valid header",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Sentry-Auth", value: "sentry_key=glsec_randomword"},
				{key: "X-Original-Url", value: "/errortracking/api/v1/projects/api/123/store?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid header, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Sentry-Auth", value: "sentry_key=glsec_randomword"},
				{key: "X-Original-Url", value: "/v1/errortracking/123/projects/api/123/store?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid header plus other keys",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Sentry-Auth", value: "sentry_verion=7,sentry_key=glsec_randomword123456789,sentry_timestamp=0"},
				{key: "X-Original-Url", value: "/errortracking/api/v1/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid header plus other keys, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Sentry-Auth", value: "sentry_verion=7,sentry_key=glsec_randomword123456789,sentry_timestamp=0"},
				{key: "X-Original-Url", value: "/v1/errortracking/123/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should fail with empty header",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Sentry-Auth", value: "sentry_key="},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with empty header and other keys",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Sentry-Auth", value: "sentry_timestamp=0,sentry_key=,sentry_version=7"},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should succeed with valid query param in original url for envelope endpoint",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url", value: "/errortracking/api/v1/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid query param in original url for envelope endpoint, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url", value: "/v1/errortracking/123/projects/api/123/envelope?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid query param in original url for store endpoint",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url", value: "/errortracking/api/v1/projects/api/123/store?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid query param in original url for store endpoint, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url", value: "/v1/errortracking/123/projects/api/123/store?sentry_key=gl_randomword"},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid query param in original url",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/errortracking/api/v1/projects/api/123/store?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should succeed with valid query param in original url, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/v1/errortracking/123/projects/api/123/store?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected:                      http.StatusOK,
			gitlabInternalEndpointHandler: successHandler,
		},
		{
			name: "should fail with invalid project ID",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/errortracking/api/v1/projects/api/abc/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with invalid group ID, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/v1/errortracking/abc/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with invalid project ID, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/v1/errortracking/123/projects/api/abc/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with empty key in original url",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url", value: "/errortracking/api/v1/projects/api/123/store/?sentry_key="},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with empty key in original url, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url", value: "/v1/errortracking/123/projects/api/123/store/?sentry_key="},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with empty key in original url plus other keys",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url",
					value: "/errortracking/api/v1/projects/api/123/store?sentry_timestamp=0&sentry_key=&sentry_version=7"},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with empty key in original url plus other keys, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{key: "X-Original-Url",
					value: "/v1/errortracking/123/projects/api/123/store?sentry_timestamp=0&sentry_key=&sentry_version=7"},
			},
			expected:                      http.StatusUnauthorized,
			gitlabInternalEndpointHandler: failHandler,
		},
		{
			name: "should fail with disabled sentry key",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/errortracking/api/v1/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected: http.StatusUnauthorized,
			gitlabInternalEndpointHandler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(`{"enabled": false`))
			}),
		},
		{
			name: "should fail with disabled sentry key, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/v1/errortracking/123/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected: http.StatusUnauthorized,
			gitlabInternalEndpointHandler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(`{"enabled": false`))
			}),
		},
		{
			name: "should fail with invalid gitlab shared secret",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/errortracking/api/v1/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected: http.StatusUnauthorized,
			gitlabInternalEndpointHandler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// an invalid shared secret returns a 401 Unauthorized
				w.WriteHeader(http.StatusUnauthorized)
			}),
		},
		{
			name: "should fail with invalid gitlab shared secret, per-group deployment",
			headers: []pair{
				{key: "X-Original-Method", value: http.MethodPost},
				{
					key:   "X-Original-Url",
					value: "/v1/errortracking/123/projects/api/123/store/?sentry_version=7&sentry_key=gl_randomword&sentry_timestamp=0",
				},
			},
			expected: http.StatusUnauthorized,
			gitlabInternalEndpointHandler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// an invalid shared secret returns a 401 Unauthorized
				w.WriteHeader(http.StatusUnauthorized)
			}),
		},
	} {
		t.Logf("TEST CASE: %s ---\n", tc.name)
		w := httptest.NewRecorder()
		req, err := http.NewRequest("GET", "/v1/error_tracking/auth", nil)
		assert.Nil(t, err)

		// add headers to test case request
		for _, h := range tc.headers {
			req.Header.Set(h.key, h.value)
		}

		// set up mock gitlab internal endpoint handler for this test case
		mux := http.NewServeMux()
		mux.HandleFunc(constants.GitlabInternalErrorTrackingEndpoint, tc.gitlabInternalEndpointHandler)
		mockGitlab.Config.Handler = mux

		router.ServeHTTP(w, req)
		assert.Equal(t, tc.expected, w.Code, tc.name)
	}
}

func TestHandleErrorTrackingOpenAPIAuth(t *testing.T) {
	gin.SetMode(gin.TestMode)
	gitlabInternalEndpointToken := "secret"

	router := gin.Default()
	router.Use(Config(&ConfigOptions{
		GitlabInternalEndpointToken: gitlabInternalEndpointToken,
	}))
	router.Use(func(ctx *gin.Context) {
		ctx.Set(cacheClientKey, newMockRedis())
	})
	SetRoutes(router)

	for _, tc := range []struct {
		name     string
		headers  []pair
		expected int
	}{
		{
			name:     "should fail with no auth header set",
			headers:  []pair{},
			expected: http.StatusUnauthorized,
		},
		{
			name: "should fail with empty auth header set",
			headers: []pair{
				{key: constants.GitlabErrorTrackingTokenHeader, value: ""},
			},
			expected: http.StatusUnauthorized,
		},
		{
			name: "should fail with invalid token",
			headers: []pair{
				{key: constants.GitlabErrorTrackingTokenHeader, value: "foo"},
			},
			expected: http.StatusUnauthorized,
		},
		{
			name: "should accept request with valid token",
			headers: []pair{
				{key: constants.GitlabErrorTrackingTokenHeader, value: gitlabInternalEndpointToken},
			},
			expected: http.StatusOK,
		},
	} {
		w := httptest.NewRecorder()
		req, err := http.NewRequest("GET", "/v1/error_tracking/auth", nil)
		assert.Nil(t, err)

		// add headers to test case request
		for _, h := range tc.headers {
			req.Header.Set(h.key, h.value)
		}

		// run tests for PUT and GET methods
		for _, method := range []string{http.MethodPut, http.MethodGet} {
			req.Header.Set("X-Original-Method", method)
			router.ServeHTTP(w, req)
			assert.Equal(t, tc.expected, w.Code, tc.name)
		}
	}
}
