package gatekeeper

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
	client "github.com/go-redis/redis/v8"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
)

type SessionOptions struct {
	RedisAddr       string
	RedisPassword   string
	CookieName      string
	CookieSecret    string
	UseSecureCookie bool
	Registry        prometheus.Registerer
}

// This is only needed to support using a sentinel redis deployment.
// The underlying session library doesn't support sentinel and it seemed
// much more problematic to fork/update the library so we have a small wrapper
// that manages the session store across redis master failovers.
type SentinelSessionStore struct {
	mu           *sync.Mutex
	cookieSecret string
	sentinelAddr string
	password     string
	store        *redis.Store
	masterAddr   *string

	masterFailovers prometheus.Counter
	masterPingTime  prometheus.Histogram
}

func NewSentinelSessionStore(
	addr, pass, cookieSecret string,
	registry prometheus.Registerer,
) *SentinelSessionStore {
	store := &SentinelSessionStore{
		mu:           &sync.Mutex{},
		sentinelAddr: addr,
		password:     pass,
		cookieSecret: cookieSecret,

		masterFailovers: prometheus.NewCounter(
			prometheus.CounterOpts{
				Name: "master_failovers",
				Help: "number of times Redis master changed",
			},
		),
		masterPingTime: prometheus.NewHistogram(
			prometheus.HistogramOpts{
				Name:    "master_ping_time",
				Help:    "time it takes for master to respond to ping",
				Buckets: []float64{.001, .005, .01, .025, .05, .1, .15, .2, .5, 1},
			},
		),
	}

	registry.MustRegister(
		store.masterFailovers,
		store.masterPingTime,
	)

	return store
}

// Retrieves and sets the redis master host.
func (s *SentinelSessionStore) setMaster() error {
	sentinel := client.NewSentinelClient(&client.Options{
		Addr: s.sentinelAddr,
	})

	addresses, err := sentinel.GetMasterAddrByName(context.TODO(), "mymaster").Result()

	if err != nil || len(addresses) < 1 {
		return fmt.Errorf("failed to retrieve redis master from sentinel: %w", err)
	}
	addr := fmt.Sprintf("%s:6379", addresses[0])
	if s.masterAddr != nil && *s.masterAddr != addr {
		s.masterFailovers.Inc()
	}
	s.masterAddr = &addr

	return nil
}

// Ping current master to make sure it's healthy.
func (s *SentinelSessionStore) ping() error {
	master := client.NewClient(&client.Options{
		Addr:     *s.masterAddr,
		Password: s.password,
		DB:       0,
	})
	// using timeout suggested in redis docs for sentinel clients.
	// NOTE: if this timeout is too small, it will ultimately cause this process to
	// crash, which could cause all pods to crashloop
	ctx, cancel := context.WithTimeout(context.Background(), 200*time.Millisecond)
	defer cancel()

	startTime := time.Now()
	_, err := master.Ping(ctx).Result()
	latency := time.Since(startTime)
	s.masterPingTime.Observe(float64(latency.Milliseconds()) / 1000.0)

	// Close the client otherwise it leaks resources.
	defer master.Close()

	return err
}

// Creates and sets the session store using a redis sentinel
// to handle failover.
func (s *SentinelSessionStore) updateStore() {
	var err error
	var store redis.Store

	for retries := 10; retries > 0; retries-- {
		err = s.setMaster()
		if err == nil {
			store, err = redis.NewStore(
				10,
				"tcp",
				*s.masterAddr,
				s.password,
				[]byte(s.cookieSecret))

			if err == nil {
				s.store = &store
				return
			}
		}
		time.Sleep(50 * time.Millisecond)
	}
	log.Fatalf("failed to create new session store: %v", err)
}

func (s *SentinelSessionStore) Get() redis.Store {
	s.mu.Lock()
	if s.store == nil {
		s.updateStore()
	}
	// ping the existing master to make sure it's alive/still master
	err := s.ping()
	if err != nil {
		s.updateStore()
	}
	s.mu.Unlock()

	return *s.store
}

// Session middleware that stores session state in redis.
func Session(o *SessionOptions) gin.HandlerFunc {
	store := NewSentinelSessionStore(
		o.RedisAddr,
		o.RedisPassword,
		o.CookieSecret,
		o.Registry,
	)

	return func(ctx *gin.Context) {
		// Invoke the "github.com/gin-contrib/sessions" middleware
		sessions.Sessions(o.CookieName, store.Get())(ctx)
		// Start a session and set the session options
		session := sessions.Default(ctx)
		session.Options(sessions.Options{
			Secure:   o.UseSecureCookie,
			HttpOnly: o.UseSecureCookie,
		})
		// Call next handler
		ctx.Next()
	}
}
