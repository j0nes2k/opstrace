package gatekeeper

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"math/big"
	"net/http"
	"net/url"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/opstrace/opstrace/go/pkg/common"
	"golang.org/x/oauth2"
)

const (
	letterBytes          = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	sessionStateKey      = "oauth_state"
	redirectOnSuccessKey = "redirect_on_success"
)

// Create a random string to be used for the state parameter in oauth2.
func generateOpaqueStateParameter() (string, error) {
	b := make([]byte, 36)
	for i := range b {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letterBytes))))
		if err != nil {
			return "", err
		}
		b[i] = letterBytes[num.Int64()]
	}
	return string(b), nil
}

// Initiates the oauth2 authentication with GitLab.
func HandleAuthStart(ctx *gin.Context) {
	metrics := GetAuthMetrics(ctx)
	session := sessions.Default(ctx)
	// Generate an opaque state parameter that we can use
	// in the callback part of the oauth2 flow to validate
	// the callback request matches with this initiation
	state, err := generateOpaqueStateParameter()
	if err != nil {
		AbortWithError(ctx, 500, "failed to generate opaque state parameter", err, true)
		return
	}

	params := ctx.Request.URL.Query()
	redirectOnSuccess := params.Get("rt")
	// Check rt is a valid URI and set it on the session so we can navigate
	// the user back to rt once auth has successfully completed
	redirectTo, err := url.ParseRequestURI(redirectOnSuccess)
	if err != nil {
		AbortWithError(ctx, 400, "not a valid rt parameter in request, must be a valid URI", err, false)
		return
	}
	// Save the redirect-to so we can redirect there upon successful auth completion
	session.Set(redirectOnSuccessKey, redirectTo.String())
	// Save the oauth state so we can validate it in the callback controller
	session.Set(sessionStateKey, state)

	if err := session.Save(); err != nil {
		AbortWithError(ctx, 500, "failed to save session", err, true)
		return
	}
	auth := GetAuthConfig(ctx)
	// Redirect to the gitlab instance where the trusted
	// oauth2 application is configured
	url := auth.AuthCodeURL(state)

	metrics.LoginStarts.Inc()
	ctx.Redirect(302, url)
}

// Handles the callback from GitLab during oauth2.
//
// If user already has an active session, this handler will
// update it with the new auth token (in the case where the user
// changed identity in the GitLab instance).
func HandleAuthFinish(ctx *gin.Context) {
	c := context.Background()
	metrics := GetAuthMetrics(ctx)
	auth := GetAuthConfig(ctx)
	session := sessions.Default(ctx)
	redirect := session.Get(redirectOnSuccessKey)
	// Use the authorization code that is pushed to the redirect
	// URL. Exchange will do the handshake to retrieve the
	// initial access token. The HTTP Client returned by
	// conf.Client will refresh the token as necessary.
	params := ctx.Request.URL.Query()
	if !params.Has("code") {
		Abort(ctx, 400, "no code parameter in request", false)
		return
	}

	// Very important to make sure the returned state parameter is equal
	// to the one we created when starting the oauth2 flow
	if !params.Has("state") {
		Abort(ctx, 400, "no state parameter in request", false)
		return
	}

	if params.Get("state") != session.Get(sessionStateKey) {
		Abort(ctx, 400, "invalid state parameter", false)
		return
	}

	if common.LookupEnvOrBool("TLS_SKIP_INSECURE_VERIFY", false) {
		t := &http.Transport{
			// #nosec
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		tc := &http.Client{Transport: t}
		c = context.WithValue(c, oauth2.HTTPClient, tc)
	}

	tok, err := auth.Exchange(c, params.Get("code"))
	if err != nil {
		metrics.LoginFailures.Inc()
		AbortWithError(ctx, 401, "unauthorized", err, false)
		return
	}

	// Set the authToken in the session
	err = SetAuthToken(ctx, tok)
	if err != nil {
		AbortWithError(ctx, 401, "unauthorized", err, false)
		return
	}

	metrics.LoginSuccesses.Inc()
	ctx.Redirect(302, redirect.(string))
}
