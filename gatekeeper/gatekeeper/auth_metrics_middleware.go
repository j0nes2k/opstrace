package gatekeeper

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

type AuthMetricsData struct {
	LoginStarts    prometheus.Counter
	LoginSuccesses prometheus.Counter
	LoginFailures  prometheus.Counter
}

func AuthMetrics(registry prometheus.Registerer) gin.HandlerFunc {
	metrics := &AuthMetricsData{
		LoginStarts: prometheus.NewCounter(
			prometheus.CounterOpts{
				Name: "login_starts",
				Help: "number of times login flow was started",
			},
		),
		LoginSuccesses: prometheus.NewCounter(
			prometheus.CounterOpts{
				Name: "login_successes",
				Help: "number of times login flow succeeded",
			},
		),
		LoginFailures: prometheus.NewCounter(
			prometheus.CounterOpts{
				Name: "login_failures",
				Help: "number of times login flow failed",
			},
		),
	}
	registry.MustRegister(
		metrics.LoginStarts,
		metrics.LoginSuccesses,
		metrics.LoginFailures,
	)

	return func(ctx *gin.Context) {
		ctx.Set(authMetricsKey, metrics)
		ctx.Next()
	}
}
