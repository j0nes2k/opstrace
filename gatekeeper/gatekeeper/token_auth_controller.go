package gatekeeper

import (
	"fmt"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

// Handles the API token auth request from nginx-ingress for data API endpoints
// Response codes are:
// - 200 for auth success (provided token is valid)
// - 401 for auth failure (token is missing, malformed, or invalid)
// - 500 for internal errors
// Note that the nginx-ingress external auth only supports these status codes.
// Any codes other than 200/401 will result in a 500 being sent to the client.
//
// See https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
func HandleTokenAuth(ctx *gin.Context) {
	var header AuthWebhookHeader
	if err := ctx.BindHeader(&header); err != nil {
		AbortWithError(ctx, 403, "invalid header", err, false)
		return
	}
	targetNamespaceID := GetNamespace(ctx)
	argusURL, err := GetArgusURLFromNamespaceID(ctx, targetNamespaceID)
	if err != nil {
		log.Debug("GetArgusURLFromNamespaceID failed:", err)
		AbortWithError(ctx, 403, "unauthorized", err, false)
		return
	}
	// Attempt to use the bearerToken to retrieve the current namespace in argus.
	// Since a token is scoped to a Group in Argus, it should return
	// the Group it's scoped to and we can make sure that matches the requested namespace.
	bearerToken := header.Auth
	if _, token, hasBasicAuth := ctx.Request.BasicAuth(); hasBasicAuth {
		// take token (passed in as password field in basic auth) and create the bearer header value
		bearerToken = fmt.Sprintf("Bearer %s", token)
	}

	namespaceID, err := GetNamespaceFromArgusForToken(ctx, argusURL, bearerToken)
	if err != nil {
		log.Debug("GetNamespaceFromArgusForToken failed:", err)
		AbortWithError(ctx, 403, "unauthorized", err, false)
		return
	}
	// Only proceed if this token is valid for the target namespace
	tokenOk := namespaceID == targetNamespaceID
	log.Debugf("api token for namespace %s: ok=%v", namespaceID, tokenOk)

	if tokenOk {
		ctx.String(200, "Success")
	} else {
		Abort(ctx, 403, "not authorized", false)
	}
}
