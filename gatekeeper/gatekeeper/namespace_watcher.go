package gatekeeper

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	v1alpha1 "github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantOperator "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"

	ctrl "sigs.k8s.io/controller-runtime"

	"net/http"

	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var _ reconcile.Reconciler = &NamespaceWatcher{}

// NamespaceWatcher reconciles a Cluster object.
type NamespaceWatcher struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client    client.Client
	Scheme    *runtime.Scheme
	Transport *http.Transport
	Context   context.Context
	Cancel    context.CancelFunc
	Log       logr.Logger
	Namespace string
}

// SetupWithManager sets up the controller with the Manager.
func (r *NamespaceWatcher) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		// Track updates to GitLabNamespaces & Tenants in the local client Cache so we can
		// track status and have efficient lookups for existence & the argus URL
		For(&v1alpha1.GitLabNamespace{}).
		// Watch Tenants just so we keep track of them in the local cache.
		Watches(&source.Kind{Type: &tenantOperator.Tenant{}}, &handler.EnqueueRequestForObject{}).
		Complete(r)
}

// Although this is a reconciler, it's being used as a cache/watcher to log update events on the GitLabNamespace.
// This is helpful to track the status in the same process as we start the change
// (creation, updating, deleting) GitLabNamespaces.
// We can optionally in the future at a channel to push the status back to the gatekeeper API thread so that status
// can be communicated to the user.
func (r *NamespaceWatcher) Reconcile(ctx context.Context, request reconcile.Request) (ctrl.Result, error) {
	// GitLabNamespaces are all deployed in the same namespace that this controller is running.
	// Tenants are all deployed in other namespaces.
	if request.Namespace != r.Namespace {
		// Must be a Tenant. Don't report because he GitLabNamespace
		// already aggregates and reports on the respective Tenant below
		return reconcile.Result{}, nil
	}

	n := &v1alpha1.GitLabNamespace{}
	err := r.Client.Get(r.Context, request.NamespacedName, n)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			r.Log.Info(fmt.Sprintf("%s: successfully deleted", n.Name))

			return reconcile.Result{}, nil
		}
		r.Log.Error(err, fmt.Sprintf("%s: failed to read status", n.Name))
		return reconcile.Result{}, err
	}
	r.Log.Info(fmt.Sprintf("received update on GitlabNamespace CR: %s", n.Name))
	// Log the last condition for this GitLabNamespace CR
	if len(n.Status.Conditions) > 0 {
		lastCondition := n.Status.Conditions[len(n.Status.Conditions)-1]
		r.Log.Info(fmt.Sprintf(
			"%s: Ready=%s Condition=%s Message=%s",
			n.Name,
			lastCondition.Status,
			lastCondition.Reason,
			lastCondition.Message,
		))
	}
	// Log the last condition for the associated Group CR
	if len(n.Status.GroupConditions) > 0 {
		lastCondition := n.Status.GroupConditions[len(n.Status.GroupConditions)-1]
		r.Log.Info(fmt.Sprintf(
			"%s: GroupReady=%s GroupCondition=%s GroupMessage=%s",
			n.Name,
			lastCondition.Status,
			lastCondition.Reason,
			lastCondition.Message,
		))
	}
	// Log the last condition for the associated Tenant CR
	if len(n.Status.TenantConditions) > 0 {
		lastCondition := n.Status.TenantConditions[len(n.Status.TenantConditions)-1]
		r.Log.Info(fmt.Sprintf(
			"%s: TenantReady=%s TenantCondition=%s TenantMessage=%s",
			n.Name,
			lastCondition.Status,
			lastCondition.Reason,
			lastCondition.Message,
		))
	}

	return reconcile.Result{}, nil
}
