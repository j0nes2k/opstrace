# Gatekeeper

Gatekeeper is responsible for authentication/authorization in an Opstrace instance.
Gatekeeper uses the attached GitLab instance as the trusted IAM.

## Developing

Prerequisites:

1. Create a [GitLab Application](https://docs.gitlab.com/ee/integration/oauth_provider.html)
2. Run:

```sh
cp .env.dev.example .env.dev
```

and configure `.env.dev` with the credentials from the GitLab Application, and the instance URL of the GitLab instance where the credentials were created.

Build the dev container. Dev container uses [cosmtrek/air](https://github.com/cosmtrek/air) and a bind mount to live reload the gatekeeper server whenever any changes are made to it's code.

You only need to build the dev container once, unless you change the Dockerfile.

```sh
docker-compose build
```

Start dependent services (redis, postgres) and the gatekeeper server with live reload

```sh
docker-compose up
```

Start developing!

## Remote Development against a running Opstrace Instance

[Install telepresence](https://www.telepresence.io/docs/v2.5/install/)

The following command will launch the telepresence agent in the Opstrace instance and proxy all gatekeeper traffic inside the remote Opstrace kubernetes cluster to the instance running on your local machine. All outbound traffic from the local gatekeeper instance running on your local machine will be proxied through telepresence to the Opstrace instance's kubernetes cluster.

`cd` into the root `/go` directory and run:

```sh
make gatekeeper-start-remote-dev
```

Stop local development against the remote Opstrace instance. This will tear down the telepresence proxy and return the Opstrace instance back to it's original state (gatekeeper deployment controlled by the Opstrace controller)

```sh
make gatekeeper-stop-remote-dev
```

You only need to build the dev container once, unless you change the Dockerfile.

```sh
docker-compose build
```

Start gatekeeper server with live reload

```sh
docker-compose -f docker-compose-remote.yaml up
```

Now navigate to the Opstrace instance and you'll see requests to gatekeeper in the logs of your locally running gatekeeper.
