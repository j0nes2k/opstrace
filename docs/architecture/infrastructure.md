# Infrastructure overview

This document outlines environments we use for develop/test/deploy Gitlab Observability Platform (GOP).

## Infrastructure-as-a-code

All infrastructure environments are defined using Terraform.
The modules which define how the infrastructure should be created reside in production Gitlab instance [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/terraform/modules).
The `ops.gitlab.com` instance holds input data for these modules [here](https://ops.gitlab.net/opstrace-realm/environments/gcp)

## Multiple infrastructure environments

GOP infrastructure environments follow the [schema outlined for Gitlab](https://about.gitlab.com/handbook/engineering/infrastructure/environments/) expect for the fact that there are only three types currently in use:

* development
* staging
* production

## GCP

All of environments are deployed on GCP, with the Clickhouse being notable exception.
Due to Clickhouse not fully supporting Google Cloud Storage, we need to use Amazon S3 as backend for the time being.
There are three GCP projects, one for production, one for staging, one for dev.

To learn more, please go to [GCE webpage](https://cloud.google.com/), log into console, and then look for `opstrace-dev-`, `opstrace-prd-` and `opstrace-stg-` projects.

### Production environment

GOP stack runs on GKE cluster and connects to production instance `gitlab.com`.

![Infrastructure - production](../assets/infra-production.png)

Oauth tokens are created manually by Gitlab instance admin and passed to Terraform in `terraform.tfvars` file.

Our domain for production environment is `observe.gitlab.com`

### Staging environment

GOP stack runs on GKE cluster and connects to staging instance `staging.gitlab.com`.

![Infrastructure - production](../assets/infra-staging.png)

Oauth tokens are created manually by Gitlab instance admin and passed to Terraform in `terraform.tfvars` file.

Our domain for staging environment is `observe.staging.gitlab.com`

### Development Environment

Development environment allows for launching multiple GOP clusters.
If needed, a separate Gitlab instance can be launched for testing too (e.g. using omnibus package).

![Infrastructure - dev](../assets/infra-dev.png)

Each cluster needs its own domain though if a full stack is desired.
Please refer to the Wiki of the Project to see how one can be set up.
It is not required though to deploy every part of the stack if it is not required for testing.
Oauth tokens are created manually by Gitlab instance admin and passed to Terraform in `terraform.tfvars` file.

## Other documentation

Every project in `ops.gitlab.com` has its own wiki with some useful information as well.
Currently it is:

* setting up GCE credentials in order to be able to deploy
* setting up DNS for projects
* terraform and kubectl access

Additionally, each project also has a README.md file which could provide more details.
