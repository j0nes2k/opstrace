# Tracing deep dive

This documents delves into details of how tracing works.

We assume here that the infrastructure has already been provisioned and that the tenant, together with groups that it belongs to, was also created.
For details on how this is done please refer to other docs in `architecture` folder.

## Overview

Below you can find a diagram that provides a brief overview of the components of the Gitlab Observability Platform (GOP) that implement tracing.

![Tracing-overview](../assets/tracing_overview.png)

In the diagram above we assume that there are two `Group`s deployed: `1st` and `n-th` in the given `GitlabNamespace`.
Each one has corresponding OTEL collector and Jaeger instance, both are deployed by the tenant operator.

Traces are submitted through ingress to OTEL collector basing on the ID of the Group encoded in HTTP's requests path.
Ingress is responsible for routing requests to correct collector.

For example, for the Group.Id == 6 we will have two ingress objects:

```bash
$ kubectl get ingress -n 6 opentelemetry-6 opentelemetry-jaeger-6 -o wide
NAME                     CLASS    HOSTS       ADDRESS     PORTS   AGE
opentelemetry-6          <none>   localhost   localhost   80      6h40m
opentelemetry-jaeger-6   <none>   localhost   localhost   80      6h40m

$ k get ingress -n 6 opentelemetry-6 opentelemetry-jaeger-6 -o jsonpath="{range .items[*].spec.rules[0].http.paths[0]}{.path}{'\n'}{end}"
/v1/traces/6
/v1/jaegertraces/6
```

OTEL collector then submits the traces to Jaeger instance.

Jaeger instance uses [jaeger-clickhouse](https://github.com/jaegertracing/jaeger-clickhouse) plugin to submit these traces in turn to Clickhouse.
Each collector/Group uses a separate Clickhouse database.
Traces are retrieved from Clickhouse using the same plugin by Jaeger.
Gitlab Observability UI (GOUI) issues API calls to Jaeger to fetch traces, it does not connect directly to Clickhouse.

GOUI, Jaeger and OTEL collectors all use Gatekeeper for authorization and authentication.
The source of truth for authentication and authorization data is GOUI instance or Gitlab instance itself, depending on whether we use authentication token or session cookie.
Authentication token is used on the ingestion path, session cookie when accessing GOUI/Jaeger through browser.
Gatekeeper uses Redis for caching so that it does not overwhelm Gitlab instance with API calls.

The instrumented code may use Jaeger SDK to sent traces, though this is not recommended.
The reason for that is deprecation of Jaeger SDK (see [here](https://www.jaegertracing.io/docs/1.37/client-libraries/)).
The preferred way to send metrics is using OTEL SDK for the given language.

## OTEL collector

OTEL collector is a customized distribution of [the upstream OTEL collector](https://opentelemetry.io/docs/collector/).
We support at the moment only HTTP ingestion using [OTEL](https://github.com/open-telemetry/opentelemetry-specification/blob/main/specification/trace/api.md) and Jaeger protocols.
The metrics are then batched and sent to Jaeger using Jaeger protocol, plus additionally they are logged.

Please have a look at [this example tracing app](https://gitlab.com/ankitbhatnagar/opstraceware/-/blob/master/cmd/tracing/main.go) to see how traces can be send from the application.
Users may send traces directly to GOP, or if there is a need for single outgoing IP or batching/queueing of metrics - may use an intermediary otel collector that connects to the GOP one.

At the moment GitLab uses [Labkit](https://gitlab.com/gitlab-org/labkit/), but work is ongoing to either add support for sending metrics in OTEL format or to drop Labkit and use OTEL SDK directly (see for example [here](https://gitlab.com/gitlab-org/labkit/-/merge_requests/79#note_437451442)).

## Jaeger

We are relying currently on Jaeger for storing and retrieving traces from Clickhouse.
It uses default Clickhouse DB schema for now, but there is plan to improve it using suggestions from Clickhouse team (see [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1804) and [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1692)).

## Authentication and Authorization

Ingress together with Gatekeeper are responsible for authentication and authorization for tracing.
Ingress objects for metrics in OTEL format, metrics in Jaeger format and Jaeger instance itself are annotated with:

```yaml
nginx.ingress.kubernetes.io/auth-response-headers: X-WEBAUTH-EMAIL, X-GRAFANA-ORG-ID
nginx.ingress.kubernetes.io/auth-signin: http://localhost/v1/auth/start?rt=$escaped_request_uri
nginx.ingress.kubernetes.io/auth-url: http://gatekeeper.default.svc.cluster.local:3001/v1/auth/webhook/35?uri=$escaped_request_uri
```

This leverages the `ngx_http_auth_request_module` [Nginx module](https://nginx.org/en/docs/http/ngx_http_auth_request_module.html).
The request, before being forwarded to the backend, is first forwarded to gatekeeper.
If the subrequest returns a 2xx response code, the access is allowed.
If it returns 401 or 403, the access is denied with the corresponding error code.
Any other response code returned by the subrequest is considered an error.

If the request has `Authorization` token, an additional subrequest is done, this time to GOUI instance, which is used as authentication's and authorization's source of truth.
These path is taken usually by ingestion requests.

```go
func GetNamespaceFromArgusForToken(
    ctx *gin.Context, argusURL string, bearerToken string,
) (string, error) {
    group := argusapi.Group{}

    u, err := url.Parse(argusURL)
    if err != nil {
        return "", err
    }
    u.Path = "/api/group/"

    req, err := http.NewRequestWithContext(ctx, "GET", u.String(), nil)
    if err != nil {
        return "", err
    }
    req.Header.Add("Authorization", bearerToken)
    req.Header.Add("Content-Type", "application/json")

    log.Debug(fmt.Sprintf("GetNamespaceFromArgusForToken request: %+v", req))

    client := &http.Client{
        Timeout: time.Second * 5,
    }
    resp, err := client.Do(req)
    if err != nil {
        return "", err
    }
    defer resp.Body.Close()

    bodyContents, err := ioutil.ReadAll(resp.Body)

    if err != nil {
        return "", err
    }
    err = json.Unmarshal(bodyContents, &group)
    if err != nil {
        return "", err
    }

    if group.ID <= 1 {
        return "", fmt.Errorf("current namespace context returned a default value")
    }
    return fmt.Sprint(group.ID), nil
}
```

If this subrequest confirms that the subrequest's namespace is the same as the one returned by GOUI, the main request is permitted.

In the case when there is no `Authorization` token, it is assumed that the request originated from browser and an attempt is made to authorize basing on session cookie.

```go
// Check if current user can access the namespace by Id.
func (s *GitLabService) CanAccessNamespace(nid interface{}) (bool, gitlab.AccessLevelValue, error) {
    // A namespace could be a group or a user.
    ns, err := s.GetNamespace(nid)
    if err != nil || ns == nil {
        return false, gitlab.NoPermissions, err
    }
    if ns.Kind == GroupNamespaceType {
        membership, err := s.getGroupMembership(nid)
        if err != nil {
            return false, gitlab.NoPermissions, err
        }
        return membership.State == "active" && membership.AccessLevel > gitlab.NoPermissions,
            membership.AccessLevel,
            nil
    }
    if ns.Kind == UserNamespaceType {
        // User must be the owner of their own user namespace
        return true, gitlab.OwnerPermission, nil
    }
    return false, gitlab.NoPermissions, nil
}
```

In order for request to be permitted, the request must come from entity that is either a member of the group, or in case of the user-namespace - the user itself.
The source of truth in this case is Gitlab Instance.
