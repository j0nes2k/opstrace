# Architecture Overview

This document discusses the architecture of Gitlab Observability Stack (GOP).
In order to learn more about specific aspects of the stack, please refer to documents below:

* [Infrastructure overview](./infrastructure.md) - description of different environments where GOP is deployed.
* [Error-tracking](./error-tracking.md) - details on how error-tracking works.
* [Tracing](./tracing.md) - details on how tracing works.

## High-level view

Below is the very high-level diagram depicting GOP components and how they are interacting with each other.

![Architecture_overview](../assets/architecture_overview.png)

The Postgres instance and Postgres operator are launched and maintained independently of GOP.
The reason for that is, that we are using managed Postgres in cloud environments.

The main component that is responsible for provisioning is `scheduler` operator.
It consists of two controllers:

* `Cluster` controller
* `GitLabNamespace` controller

They are tightly aligned with how multi-tenancy is implemented in GOP.

## CRDs

Scheduler has few accompanying CRDs:

![Architecture_overview](../assets/architecture_crds.png)

### Cluster

`Cluster` object depicts infrastructure shared between `Tenants` as well as references the connected GitLab application and OAuth application secrets, so GitLab and GOP can provide a "seamless" experience.
It is reconciled by `Cluster` controller.
We currently support only single `Cluster` object per k8s cluster.

### Tenant

Usernames and group names fall under a special category called namespaces in GitLab API.
This Custom Resource represents a root level namespace in GitLab.
For instance, the GitLab Observability group: `https://gitlab.com/gitlab-org/opstrace` is a subgroup within the root level namespace `gitlab-org`.
The tenant operator owns it and will provision a GitLab Observability UI instance for each Tenant CR.
Each root level namespace in GitLab that enables observability will have its own instance of GitLab Observability UI where they can manage plugins, datasources etc in isolation.

Root level namespaces in GitLab are where account level capabilities are determined (by the namespace’s license).
Tenants map to root level namespaces in GitLab such that limits/capabilities can also be determined by the GitLab license for the associated root namespace in GitLab.
Resources like Postgres databases, ClickHouse databases, etc... in one Tenant cannot be accessed by entities in another Tenant.
Each root-level namespace is provisioned with separate Postgres/Clickhouse DB, which is then shared among tenants withing this namespace.
Resources like the UI and Jaeger/Otel that are deployed per tenant.

NOTE: Please be aware that ProjectIDs may overlap with NamespaceIDs (Users/Groups). So it is possible to provision a Tenant for UserID `6` while there also exists ProjectID `6`.

### Group

This Custom Resource represents a namespace in the subtree of the root level namespace.
gitlab-org will have it's own Tenant with its own instance of GitLab Observability UI and the group GitLab Observability will be managed as a group within that tenant.
A group is synonymous with a Grafana Org and a group will have it's own APIs for tracing, metrics, logs etc and users will log in to that group in the UI to visualize and manage their data.

### GitLabNamespace

These are created by Gatekeeper and reconciled by `GitLabNamespace` controller in scheduler.
Gatekeeper will receive a request to enable observability for a GitLab namespace, and if the correct permissions are present, Gatekeeper will create or update the GitLabNamespace Custom Resource that represents the GitLab namespace.
This keeps namespaces up to date in GitLab Observability, as they change in GitLab.
Rate-limits, integrations, [Gitlab Observability UI plugins, Datasources and Dashboards](https://gitlab.com/gitlab-org/opstrace/opstrace-ui), are managed on a per-GitlabNamespace basis.

## Multi-Tenant Architecture Overview

Each tenant in GitLab Observability has its own highly available [Gitlab Obervability UI](https://gitlab.com/gitlab-org/opstrace/opstrace-ui).

Subgroups within the GitLab root-level namespace map to organizations in the UI instance in GitLab Observability Platform (represented by the Group Custom Resource in the diagram).
We will rename “organizations” in Gitlab Observability UI to “groups” to align the concepts explicitly.

Inside an GitLab Observability tenant, an ingress resource will ensure the correct headers are set for the data ingestion path.
A group’s ingestion path may be: `https://opstrace.gitlab.com/api/{groupId}/metrics` and the ingress will set the OrgId header equal to the groupId so we maintain data separation between groups.

Namespaces in GitLab Observability are only created on an as-needed basis.
For a namespace mapped in Gitlab which hasn't yet enabled observability, if a user navigates to it, Gatekeeper will check their access level and if they are an Owner, present an option to enable observability.

## Scheduler

As mentioned earlier, scheduler is the central component of GOP that has two major components.

* `Cluster` controller
* `GitLabNamespace` controller

Its task is to launch and supervise all the components of GOP, which in turn take care of tenants, routing traffic, etc...

As we work toward sharding in the Observability SaaS (the ability to have several GitLab Observability clusters deployed in various regions), the scheduler operator will also be responsible for communicating to the sharding control plane, to communicate its available capacity and to receive assignments for tenants it should provision.

### `Cluster` controller

`Cluster` controller is one of two controllers that constitute the scheduler.
It launches and supervises components that are shared between tenants, namely:

* Prometheus-operator which in turn launches a Prometheus instance
* nginx-based ingress
* clickhouse operator which in turn launches clickhouse database
* cert-manager
* gatekeeper
* jaeger-operator
* errortracking-api
* Prometheus' node exporter
* redis operator which in turn launches redis instance

It reconciles the built-in manifests of the above components with overrides specified in the `Cluster` CR and the state of APIserver.
It then issues API CRUD calls so that the state of the cluster matches the desired state.

One of the benefits of this approach is that user-made changes will be reverted by the controller.
Another is having a central place (i.e. `Cluster` CRD) where all GOP configuration resides.
And finally - it ensures we do not attempt to over utilize the cluster, by making sure that we don't provision more tenants than we should based on the available resources within the kubernetes cluster.

#### `Cluster` Overrides

An override function has been built in such that overrides can be provided directly on the CR.
The controller will merge the overrides in, allowing an SRE to override any attribute of any Kubernetes resource.
Overrides on the CR make it simple to see what has been overridden.
Here's an example of the Cluster CR where we're overriding the number of replicas and the image for the gatekeeper deployment:

```yaml
apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  annotations:
  creationTimestamp: "2022-05-17T19:29:57Z"
  finalizers:
  - cluster.opstrace.com/finalizer
  name: mats-dev-cluster
spec:
  dns:
    acmeEmail: ""
    dns01Challenge: {}
    externalDNSProvider: {}
  gitlab:
    groupAllowedAccess: redacted
    groupAllowedSystemAccess: redacted
    instanceUrl: redacted
    authSecret:
      name: dev-secret
  namespace: ""
  overrides:
    gatekeeper:
      components:
        deployment:
          spec:
            template:
              spec:
                replicas: 5
                containers:
                - image: opstrace/gatekeeper:761da560668a6258f8f6352395b7840078f2dc23
                  name: gatekeeper
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
  target: kind
status:
  conditions:
  - lastTransitionTime: "2022-05-19T21:13:14Z"
    message: All components are in ready state
    observedGeneration: 17
    reason: Success
    status: "True"
    type: Ready
```

### `GitlabNamespace` controller

`GitLabNamespace` controller is the other controller that constitutes the scheduler.
It reconciles the required tenant capabilities and the subgroup capabilities by deploying a tenant operator, which in turn deploys tenant-level components.
Namely:

* creates necessary databases and users in Clickhouse DB and Postgres
* supplies Clickhouse/Postgres credentials in a safe manner to tenant/tenant components
* launches tenant operator for the root namespace along with Gitlab observability UI

## `Cluster` components

Below you can find a high-level overview of components on the `Cluster` level:

![Cluster_overview](../assets/architecture-cluster.png)

The diagram also depicts communication flows.

### Prometheus, Prometheus Operator

Prometheus operator is launched by scheduler.
It is in turn responsible for launching and keeping running a Prometheus instance that monitors other components, including itself and the operator.
The list of monitored objects can be obtained by:

```bash
$ k get servicemonitors.monitoring.coreos.com
NAME                      AGE
apiserver                 4h26m
certmanager               4h28m
clickhouse                4h31m
clickhouse-operator       4h31m
coredns                   4h26m
errortracking-api         4h26m
gatekeeper                4h26m
jaeger-operator           4h28m
kube-controller-manager   4h26m
kube-scheduler            4h26m
kube-state-metrics        4h26m
kubelet                   4h26m
nginx-ingress             4h28m
node-exporter             4h26m
Prometheus                4h26m
Prometheus-operator       4h28m
redis                     4h29m
```

We do not have any dashboard yet, any queries need to be created dynamically via Prometheus UI.

### Ingress

We currently use Nginx as ingress.
It forwards traffic basing on ingress objects to:

* gatekeeper:
  * authentication
* error-tracking api
  * errors ingestion
* tenant
  * Gitlab Observability UI

Worth mentioning is that the Oauth traffic flow goes through it as well during the authn&authz process of the user.

### Redis Operator

Launches Redis instance, which in turn is responsible for caching and session storage for gatekeeper.

### Node-exporter

Exposes Prometheus metrics for all the nodes in the k8s cluster.
Metrics then are pulled by Prometheus instance.

### Error-tracking API

Receives ingestion traffic from nginx-ingress and stores data into an instance of Clickhouse DB.
Sentry DSN that you copy-paste from GitLab UI points to the errortracking instance.

NOTE: currently there is only one instance for all tenants but this is going to change as [this MR](https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1697) has landed and we are in the migration phase.

### Gatekeeper

Is responsible for authentication and authorization and creates GitlabNamespace objects that are later reconciled by scheduler.
It is also the component that is responsible for the "provisioning namespace" wait-page that you see in quickstart - it simply counts the time and compares it with the usual time it takes to provision + waits for Ready status to become true.
Uses Redis for caching and has internal in-memory LRU cache as well.

### Jaeger operator

Launches Jaeger instances in the tenant.
CRs that drive it are created by scheduler while reconciling `Cluster` object.

## `GitlabNamespace` components

Below you can find a high-level overview of components on the `GitlabNamespace` level:

![Gitlabnamespace_overview](../assets/architecture-tenant.png)

The diagram also depicts communication flows.

### Tenant operator

It is responsible for:

* configuring dashboards for Gitlab Observability UI
* launching an instance of Gitlab Observability UI

Also, for every Group that belongs to given RootNamespace:

* launches jaeger instance by creating jaeger CR which in turn is reconciled by jaeger operator
* launches otel collector
* adds ingress routes for given group's otel collector and jaeger ingress

### Gitlab Observability UI

Its old name is Argus, you can still sometimes find it in the documentation.
Uses Clickhouse as backend for quering metrics and Postgres database to store configuration.
Will integrate with Gitlab Groups, single instance per tenant.
Receives traffic via nginx ingress.

### Jaeger instance, Otel-collector

Components responsible for tracing.
Otel collector gathers traces, which are then presented by Jaeger UI.
Clickhouse is used as backend.
