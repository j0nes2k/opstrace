# GitLab Observability Quick Start

Try GitLab Observability by cloning or forking this repo and creating a local installation.

## Step 0: Setup

### Install dependencies

Before setting up GitLab Observability Platform components, you must have the following third-party dependencies installed and configured.
These can be done manually by yourself OR [automatically using asdf](#automatically-using-asdf).

#### Automatically using `asdf`

Installing dependencies using [`asdf`](https://asdf-vm.com/#/core-manage-asdf) lets GitLab Observability manage them for you automatically:

1. Clone this repository into your preferred location, if you haven't previously:

```shell
git clone gitlab-org/opstrace/opstrace.git
```

1. Change into the project directory:

```shell
cd opstrace
```

1. Install dependencies using `asdf`:

If you already have `asdf` installed, run:

```shell
make bootstrap
```

If you need to install `asdf` as well, run:

```shell
make install-asdf
```

Do not forget to source your profile as suggested by the command above, then run `make bootstrap` to get all dependencies.


> We currently use versions of these dependencies as pinned in the `.tool-versions` file.

In any case, make sure you have the following:

* [Kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) for setting up a local Kubernetes cluster.
* [Docker](https://docs.docker.com/install) and [Docker Compose](https://docs.docker.com/compose/install) for sending dummy data to GitLab Observability.
* [Kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl) for interacting with GitLab Observability
* [jq](https://stedolan.github.io/jq/download/) for some makefile utilities
* [Go 1.17](https://go.dev/doc/install) - 1.17 is the only supported version. [Kubebuilder doesn't support 1.18](https://github.com/kubernetes-sigs/kubebuilder/issues/2559) yet, and 1.16 will probably work but not garaunteed.

```bash
kind --version
docker --version
docker-compose --version
kubectl version
jq --version
go version
```

If running on MacOS, be sure to make sure you have enough resources dedicated to docker desktop.
We recommend:

* CPUs: 4+
* Memory: 8GB+
* Swap: 1GB+

It's possible to run with lower resources, we just know that these work.

Now we need to create a local kind cluster

```bash
make kind
```

## Step 1: Install GitLab Observability

Now deploy the scheduler:

```bash
make deploy
```

Create a GitLab Application so we can use it for authentication.
In the GitLab instance you'd like to connect GitLab Observability to, [create an OAuth Application](https://docs.gitlab.com/ee/integration/oauth_provider.html#introduction-to-oauth).
This application can be a user owned, group owned or instance-wide application.
In production, we create an instance-wide application and select "trusted" so that users are explicitly authorized without the consent screen.
Here is an example of how to configure the application.
Be sure to select the API scope and to enter `http://localhost/v1/auth/callback` as the redirect URI:

![gitlab oauth application](./assets/create-gitlab-application.png)

Create the secret holding auth data:

```bash
kubectl create secret generic \
    --from-literal=gitlab_oauth_client_id=<YOUR CLIENT ID FROM YOUR GITLAB APPLICATION> \
    --from-literal=gitlab_oauth_client_secret=<YOUR CLIENT SECRET FROM YOUR GITLAB APPLICATION> \
    --from-literal=internal_endpoint_token=<ERROR TRACKING INTERNAL ENDPOINT TOKEN> \
        dev-secret
```

Replace `<YOUR CLIENT ID FROM YOUR GITLAB APPLICATION>` and `<YOUR CLIENT SECRET FROM YOUR GITLAB APPLICATION>` with the values from your GitLab application that you just created.
Replace `<ERROR TRACKING INTERNAL ENDPOINT TOKEN>` with any string for if you do not plan to use error tracking.
You can also look at [this](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91928) and see how you can obtain the token to test error tracking.
All the parameters above are not optional - must be set.
Next step is to create the cluster definition:

```bash
cat <<EOF > Cluster.yaml
apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  name: dev-cluster
spec:
  target: kind
  goui:
    image: "registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:c9fb6e70"
  dns:
    acmeEmail: ""
    dns01Challenge: {}
    externalDNSProvider: {}
  gitlab:
    groupAllowedAccess: '*'
    groupAllowedSystemAccess: "6543"
    instanceUrl: https://gitlab.com
    authSecret:
      name: dev-secret
EOF
```

If you are using locally running GDK, instead of `http://gitlab.com`, `instanceUrl` should point to your local GDK Gitlab instance.

```bash
kubectl apply -f Cluster.yaml
```

Wait for the cluster to be ready:

```bash
kubectl wait --for=condition=ready cluster/dev-cluster --timeout=600s
```

Once the above command exits, the cluster is ready.

## Step 2: Enable Observability on a GitLab namespace you own

Navigate to a namespace you own in the connected GitLab instance, and copy the Group ID below the group name, for example:

![copy-group-id](./assets/copy-group-id.png)

GOP can only be enabled for groups you own.
In order to list all the groups that your user owns, navigate to menu in upper right corner and select `Groups`->`Your Groups`.

![list-groups](./assets/listing_groups.png)

Now open your browser to [http://localhost/-/{GroupID}](http://localhost/-/{GroupID}). In the above group, we'd open [http://localhost/-/14485840](http://localhost/-/14485840)

Follow the on screen instructions to enable observability for the namespace.
This can take a couple of minutes if it's the first time observability has been enabled for the root level namespace (GitLab.org) in the above example.\
There are many optimizations we can make to reduce this provisioning time.

Once your namespace has been enabled and is ready, the page will automatically direct you to the GitLab Observability UI.

## Step 3: Send dummy traces data to GitLab Observability

[Follow this guide for sending traces to your namespace and checking them out in the UI.](./guides/user/sending-traces-locally.md)

## Step 4: Clean up

To tear down your locally running instance of GitLab Observability, run:

```bash
make destroy
```

## Running Gitlab Observability UI locally

There are few extra steps that are needed to enable GOUI embedding when running GOP together with GDK locally.

### Step 1: configure GDK

In GDK's root directory, create `env.runit` file if it does not exist already.
Add export `OVERRIDE_OBSERVABILITY_URL=http://localhost`
By default GOP will be available at `http://localhost` when running locally and following this quickstart's steps.
Adjust it to your own instance's address if this is not the case.
Please remember to not to add any trailing slashes to the URL.
Both GDK Gitlab instance and GOUI instance MUST be using the same protocol (http or https).

OPTIONAL:
Untill [this](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/96533) MR gets merged, an extra step is needed - we need to make sure that GDK is running a patched version of Gitlab.
From the root of GDK issue:

```bash
cd gitlab/
git fetch
git checkout rossetd-improve-obs-local-dev
git rebase origin/master
cd ../
```

Now we need to restart GDK:

```bash
gdk restart
```

The final step is to enable monitoring feature-flag.
Issue the follwing commands:

```bash
gdk rails console
Feature.enable(:observability_group_tab)
```

## Step 2: Apply overrides to GOP tenant

The ID of the group we used to provision the Tenant is also the name of the k8s namespace where GOUI pods were launched.
We will be thus refering to this GroupID.

Due to the fact that GDK and GOP, when running locally, use different domains (`gdk.test` vs. `localhost`), we need to apply few additional overrides.
Issue following command, once the tenant gets provisioned:

```bash
k edit tenant -n <GroupID> <GroupID>
```

and add the following keys:

```yaml
spec:
  overrides:
    argus:
      components:
        statefulset:
          spec:
            template:
              spec:
                containers:
                - image: registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:<TAG_TO_TEST>
                  name: argus
      config:
        security:
          allow_embedding: true
          content_security_policy: false
          cookie_samesite: none
          cookie_secure: true
```

Values above also illustrate how to override the docker image that GOUI is using.
It is not required to make GOUI work though and can be also done on the `Cluster` object (only relevant fields are shown):

```yaml
apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  name: dev-cluster
  namespace: default
spec:
  goui:
    image: registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:<TAG_TO_TEST>
```

Once the GOUI pods get restarted, we can verify the changes by:

```bash
$ k exec -ti -n 35 argus-sts-0 -- grep -A5 security /etc/argus/grafana.ini
Defaulted container "argus" out of: argus, argus-plugins-init (init)
[security]
allow_embedding = true
content_security_policy = false
cookie_samesite = none
cookie_secure = true

[server]
domain = localhost
```

and

```bash
$ k get pod -n 35 argus-sts-0 -o yaml | grep image:
    image: registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:<TAG_TO_TEST>
    image: quay.io/grafana-operator/grafana_plugins_init:0.0.5
    image: registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:<TAG_TO_TEST>
    image: quay.io/grafana-operator/grafana_plugins_init:0.0.5
```

## Step 3: Navigate to GOUI

Once the steps above are done, one can navigate to GOUI by on the `Observability` button in the left panel's menu:

![navigate-to-goui](./assets/navigate_to_goui.png)

and behold!

![goui](./assets/goui.png)

## Known Issues


1. If you are running on apple silicon (M1/M2) chip, you might face incorrect target architecture for kind/node image while running `make kind`. take a look at this [issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1802)

    Run below command in order to fix it.
    create a new Dockerfile

    ```Dockerfile
    FROM --platform=arm64 kindest/node:v1.23.4
    RUN arch
    ```

    then build it via

    ```bash
    docker build -t tempkind .
    ```

    Then run to create a cluster.

    ```bash
    kind create cluster --image tempkind
    ```
