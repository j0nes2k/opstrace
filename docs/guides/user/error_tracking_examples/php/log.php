<?
$dsn = $_ENV['SENTRY_DSN'];

require_once __DIR__ . '/vendor/autoload.php';

Sentry\init([
    'dsn' => $dsn,
    'attach_stacktrace' => true,
]);

Sentry\captureException(new BadMethodCallException('Message'));
?>
