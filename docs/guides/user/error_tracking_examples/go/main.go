package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
)

func prettyPrint(v interface{}) string {
	pp, _ := json.MarshalIndent(v, "", "    ")
	return string(pp)
}

func bazErr() {
	sentry.CaptureException(fmt.Errorf("baz error!"))
}

func main() {
	err := sentry.Init(sentry.ClientOptions{
		// Set this field to the DSN string you copied during the earlier steps
		Dsn: os.Getenv("SENTRY_DSN"),
		// Enable printing of SDK debug messages.
		// Useful when getting started or trying to figure something out.
		Debug: true,
		// // Enables attaching stacktraces to errors
		// AttachStacktrace: true,
		// Print the JSON messages sent to console before sending to ErrorTracking API
		BeforeSend: func(e *sentry.Event, h *sentry.EventHint) *sentry.Event {
			fmt.Println(prettyPrint(e))
			return e
		},
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}

	// Flush buffered events before the program terminates.
	// Set the timeout to the maximum duration the program can afford to wait.
	defer sentry.Flush(2 * time.Second)

	bazErr()
}
