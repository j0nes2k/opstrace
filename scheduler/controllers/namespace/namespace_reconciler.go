package namespace

import (
	"errors"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/errortracking"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	"github.com/opstrace/opstrace/tenant-operator/controllers/tenant/argus"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type GitLabNamespaceReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewGitLabNamespaceReconciler(teardown bool, logger logr.Logger) *GitLabNamespaceReconciler {
	return &GitLabNamespaceReconciler{
		Teardown: teardown,
		Log:      logger.WithName("gitlab-namespace"),
	}
}

func (i *GitLabNamespaceReconciler) Reconcile(state *GitLabNamespaceState, cr *v1alpha1.GitLabNamespace) common.DesiredState {
	desired := common.DesiredState{}
	if i.Teardown {
		// remove group first
		desired = desired.AddAction(i.getGroupDesiredState(cr))
		// wait for group to be gone before we delete the clickhouse credentials
		// otherwise the tenant-operator has to do some magic to tear the group
		// down without access to these credentials
		desired = desired.AddActions(i.getGroupGone(state))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		// If there are no more active groups in this tenant then we teardown the
		// entire tenant
		if state.TenantEmpty() {
			// remove Tenant. This will cause all groups within the tenant to
			// be torn down too (via the tenant-operator)
			desired = desired.AddAction(i.getTenantDesiredState(cr))
			// make sure tenant is gone before we remove it's operator
			desired = desired.AddActions(i.getTenantGone(state))
			// remove Tenant operator next
			desired = desired.AddAction(i.getDeploymentDesiredState(cr))
			// block until tenant-operator is removed
			desired = desired.AddActions(i.getTenantReadiness(state))

			desired = desired.AddActions(i.getRBACDesiredState(cr))
			desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
			desired = desired.AddActions(i.getPostgresDesiredState(state, cr))
			// remove namespace last
			desired = desired.AddAction(i.getNamespaceDesiredState(cr))
			// block until namespace is removed
			desired = desired.AddActions(i.getNamespaceGone(state))
		}
	} else {
		// create the namespace first
		desired = desired.AddAction(i.getNamespaceDesiredState(cr))
		desired = desired.AddActions(i.getRBACDesiredState(cr))
		desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
		desired = desired.AddActions(i.getPostgresDesiredState(state, cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getDeploymentDesiredState(cr))
		desired = desired.AddActions(i.getTenantReadiness(state))
		// reconcile tenant/group last once we know the operator is ready
		desired = desired.AddAction(i.getTenantDesiredState(cr))
		desired = desired.AddAction(i.getGroupDesiredState(cr))
	}

	return desired
}

func (i *GitLabNamespaceReconciler) getTenantReadiness(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Operator != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Operator,
					Msg: "check tenant-operator is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.Operator,
			Msg: "check tenant-operator readiness",
		},
	}
}

func (i *GitLabNamespaceReconciler) getGroupGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Group != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Group,
					Msg: "check group is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getTenantGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Tenant != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Tenant,
					Msg: "check tenant is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getNamespaceGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Namespace != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Namespace,
					Msg: "check namespace is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getRBACDesiredState(cr *v1alpha1.GitLabNamespace) []common.Action {
	objects, err := GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize tenant rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("tenant %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("tenant %s", obj.GetObjectKind().GroupVersionKind().Kind),
				// Don't want a GitlabNamespace CR to own the whole this because multiple
				// GitlabNamespace CRs map to the same resource
				SkipOwnerRef: true,
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *GitLabNamespaceReconciler) getNamespaceDesiredState(cr *v1alpha1.GitLabNamespace) common.Action {
	ns := Namespace(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: ns,
			Msg: "tenant namespace",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: ns,
		Msg: "tenant namespace",
		// Don't want a GitlabNamespace CR to own the whole namespace because multiple
		// GitlabNamespace CRs map to the same namespace
		SkipOwnerRef: true,
		Mutator: func() error {
			return NamespaceMutator(cr, ns)
		},
	}
}

func (i *GitLabNamespaceReconciler) getPostgresDesiredState(state *GitLabNamespaceState, cr *v1alpha1.GitLabNamespace) []common.Action {
	tenantEndpoint := state.GetTenantPostgresEndpoint(cr)
	endpoint := config.Get().PostgresEndpoint()
	if tenantEndpoint == nil || endpoint == nil {
		return []common.Action{
			common.LogAction{
				Msg:   "postgres endpoint not available yet",
				Error: errors.New("failed retrieving postgres endpoint from controller config"),
			},
		}
	}

	tenant := Tenant(cr)
	postgresSecret := argus.PostgresEndpointSecret(tenant, []byte(tenantEndpoint.String()))

	dbName := state.GetPostgresDatabaseName(cr)
	if i.Teardown {
		actions := []common.Action{}
		if state.PostgresDBExists {
			actions = append(actions, common.PostgresAction{
				Msg: fmt.Sprintf("drop postgres database: %s", dbName),
				SQL: fmt.Sprintf("DROP DATABASE %s", dbName),
				URL: endpoint.String(),
			})
		}
		actions = append(actions, common.GenericDeleteAction{
			Ref: postgresSecret,
			Msg: "tenant credentials",
		})
		return actions
	}

	actions := []common.Action{common.GenericCreateOrUpdateAction{
		Ref: postgresSecret,
		Msg: "tenant credentials",
		Mutator: func() error {
			argus.PostgresEndpointSecretMutator(postgresSecret, []byte(tenantEndpoint.String()))
			return nil
		},
	}}

	if !state.PostgresDBExists {
		actions = append(actions, common.PostgresAction{
			Msg: fmt.Sprintf("create postgres database: %s", dbName),
			SQL: fmt.Sprintf("CREATE DATABASE %s", dbName),
			URL: endpoint.String(),
		})
	} else {
		actions = append(actions, common.LogAction{
			Msg: fmt.Sprintf("postgres database already exists: %s", dbName),
		})
	}
	return actions
}

func (i *GitLabNamespaceReconciler) getClickHouseDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	desired := []common.Action{}
	desired = append(desired, i.getJaegerClickHouseDesiredState(state, cr)...)
	desired = append(desired, i.getErrorTrackingClickHouseDesiredState(state, cr)...)
	return desired
}

//nolint:funlen
func (i *GitLabNamespaceReconciler) getJaegerClickHouseDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	chEndpoints := config.Get().ClickHouseEndpoints()
	if chEndpoints == nil {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse endpoints not available yet",
				Error: errors.New("failed retrieving clickhouse endpoints from controller config"),
			},
		}
	}

	// keep in mind, chEndpoints will get mutated by the respective subsystem's `ClickHouseCredentialsSecret`
	// method to update the User, so we deep copy and pass it in so we can still use chEndpoints to execute
	// necessary SQL statements downstream.
	group := Group(cr)
	chEndpointsCopy := config.Get().ClickHouseEndpoints()
	jaegerCredentialsSecret := jaeger.ClickHouseCredentialsSecret(group, &chEndpointsCopy.Native)

	actions := []common.Action{}

	if i.Teardown {
		// we do not teardown the credential secret here, we defer that to last
		// after having deleted the corresponding user first.
	} else {
		actions = append(actions,
			common.GenericCreateOrUpdateAction{
				Ref: jaegerCredentialsSecret,
				Msg: "group jaeger-clickhouse credentials",
				Mutator: func() error {
					jaeger.ClickHouseCredentialsSecretMutator(group, jaegerCredentialsSecret, &chEndpoints.Native)
					return nil
				},
			},
		)
	}

	// get userinfo created by jaeger.ClickHouseCredentialsSecret
	userInfo, err := state.ClickHouseUserInfo(JaegerSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg:   fmt.Sprintf("clickhouse user credentials for jaeger-clickhouse in group %d not available yet", cr.Spec.ID),
				Error: err,
			},
		)
		return actions
	}

	user := userInfo.Username()
	pass, _ := userInfo.Password()

	// get quotas info created by jaeger.ClickHouseQuotasConfigmap
	quotas, err := state.ClickHouseQuotasInfo(JaegerSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg: fmt.Sprintf("clickhouse user quotas for jaeger in group %d not available yet", cr.Spec.ID),
				// we do not report errors here because quotas are reconciled lazily inside tenant-operator
			},
		)
	}

	if i.Teardown {
		actions = append(actions,
			common.ClickHouseAction{
				Msg:      fmt.Sprintf("clickhouse SQL drop user %s if exists", user),
				SQL:      fmt.Sprintf("DROP USER IF EXISTS %s ON CLUSTER '{cluster}'", user),
				URL:      chEndpoints.Native,
				Database: constants.JaegerDatabaseName,
			},
			common.GenericDeleteAction{
				Ref: jaegerCredentialsSecret,
				Msg: "group jaeger-clickhouse credentials",
			},
		)
	} else {
		// create user first
		actions = append(actions,
			ClickHouseUsersProvision(
				user,
				pass,
				[]string{"SELECT", "INSERT", "ALTER", "CREATE"},
				constants.JaegerDatabaseName,
				chEndpoints,
			)...,
		)
		// if we found quotas configured, create them next
		for quotaName, quotaDef := range quotas { // if quotas == nil, there will be no iterations
			actions = append(actions,
				ClickHouseQuotasProvision(
					quotaName,
					constants.JaegerDatabaseName,
					chEndpoints,
					quotaDef,
					user,
				)...,
			)
		}
	}

	return actions
}

//nolint:funlen
func (i *GitLabNamespaceReconciler) getErrorTrackingClickHouseDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	chEndpoints := config.Get().ClickHouseEndpoints()
	if chEndpoints == nil {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse endpoints not available yet",
				Error: errors.New("failed retrieving clickhouse endpoints from controller config"),
			},
		}
	}

	// keep in mind, chEndpoints will get mutated by the respective subsystem's `ClickHouseCredentialsSecret`
	// method to update the User, so we deep copy and pass it in so we can still use chEndpoints to execute
	// necessary SQL statements downstream.
	group := Group(cr)
	chEndpointsCopy := config.Get().ClickHouseEndpoints()
	errortrackingCredentialsSecret := errortracking.ClickHouseCredentialsSecret(group, &chEndpointsCopy.Native)

	actions := []common.Action{}

	if i.Teardown {
		// we do not teardown the credential secret here, we defer that to last
		// after having deleted the corresponding user first.
	} else {
		actions = append(actions,
			common.GenericCreateOrUpdateAction{
				Ref: errortrackingCredentialsSecret,
				Msg: "group errortracking-clickhouse credentials",
				Mutator: func() error {
					errortracking.ClickHouseCredentialsSecretMutator(group, errortrackingCredentialsSecret, &chEndpoints.Native)
					return nil
				},
			},
		)
	}

	// get userinfo created by errortracking.ClickHouseCredentialsSecret
	userInfo, err := state.ClickHouseUserInfo(ErrorTrackingSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg:   fmt.Sprintf("clickhouse user credentials for errortracking in group %d not available yet", cr.Spec.ID),
				Error: err,
			},
		)
		return actions
	}

	user := userInfo.Username()
	pass, _ := userInfo.Password()

	// get quotas info created by errortracking.ClickHouseQuotasConfigmap
	quotas, err := state.ClickHouseQuotasInfo(ErrorTrackingSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg: fmt.Sprintf("clickhouse user quotas for errortracking in group %d not available yet", cr.Spec.ID),
				// we do not report errors here because quotas are reconciled lazily inside tenant-operator
			},
		)
	}

	if i.Teardown {
		actions = append(actions,
			common.ClickHouseAction{
				Msg:      fmt.Sprintf("clickhouse SQL drop user %s if exists", user),
				SQL:      fmt.Sprintf("DROP USER IF EXISTS %s ON CLUSTER '{cluster}'", user),
				URL:      chEndpoints.Native,
				Database: constants.ErrorTrackingDatabaseName,
			},
			common.GenericDeleteAction{
				Ref: errortrackingCredentialsSecret,
				Msg: "group errortracking-clickhouse credentials",
			},
		)
	} else {
		// create user first
		actions = append(actions,
			ClickHouseUsersProvision(
				user,
				pass,
				[]string{"SELECT", "INSERT", "ALTER", "CREATE"},
				constants.ErrorTrackingDatabaseName,
				chEndpoints,
			)...,
		)
		// if we found quotas configured, create them next
		for quotaName, quotaDef := range quotas { // if quotas == nil, there will be no iterations
			actions = append(actions,
				ClickHouseQuotasProvision(
					quotaName,
					constants.ErrorTrackingDatabaseName,
					chEndpoints,
					quotaDef,
					user,
				)...,
			)
		}
	}

	return actions
}

func (i *GitLabNamespaceReconciler) getServiceAccountDesiredState(cr *v1alpha1.GitLabNamespace) common.Action {
	sa := ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "tenant service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "tenant service account",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return ServiceAccountMutator(cr, sa)
		},
	}
}

func (i *GitLabNamespaceReconciler) getGroupDesiredState(cr *v1alpha1.GitLabNamespace) common.Action {
	group := Group(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: group,
			Msg: "group cr",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: group,
		Msg: "group cr",
		Mutator: func() error {
			return GroupMutator(cr, group)
		},
	}
}

func (i *GitLabNamespaceReconciler) getTenantDesiredState(cr *v1alpha1.GitLabNamespace) common.Action {
	tenant := Tenant(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: tenant,
			Msg: "tenant cr",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: tenant,
		Msg: "tenant cr",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return TenantMutator(cr, tenant)
		},
	}
}

func (i *GitLabNamespaceReconciler) getDeploymentDesiredState(cr *v1alpha1.GitLabNamespace) common.Action {
	deploy := Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "tenant-operator deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "tenant-operator deployment",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return DeploymentMutator(cr, deploy)
		},
	}
}
