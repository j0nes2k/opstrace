package namespace

import (
	"context"
	"fmt"
	"net/url"

	"github.com/jackc/pgx/v4"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	tenantOperator "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/errortracking"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	tenant "github.com/opstrace/opstrace/tenant-operator/controllers/tenant"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type SubsystemName string

const (
	JaegerSubsystemName        SubsystemName = "jaeger"
	ErrorTrackingSubsystemName SubsystemName = "errortracking"
)

type ClickhouseAssets struct {
	Jaeger struct {
		Credentials *corev1.Secret
		Quotas      *corev1.ConfigMap
	}
	ErrorTracking struct {
		Credentials *corev1.Secret
		Quotas      *corev1.ConfigMap
	}
}

type GitLabNamespaceState struct {
	Tenant *tenantOperator.Tenant
	Group  *tenantOperator.Group
	// Track all remaining groups in tenant so we
	// can tear down the tenant if no more groups exist
	TenantGroups     *tenant.GroupsState
	Operator         *v1.Deployment
	Namespace        *corev1.Namespace
	ClickHouse       *ClickhouseAssets
	PostgresDBExists bool
}

func NewGitLabNamespaceState() *GitLabNamespaceState {
	return &GitLabNamespaceState{
		ClickHouse: &ClickhouseAssets{
			Jaeger: struct {
				Credentials *corev1.Secret
				Quotas      *corev1.ConfigMap
			}{},
			ErrorTracking: struct {
				Credentials *corev1.Secret
				Quotas      *corev1.ConfigMap
			}{},
		},
	}
}

// TenantEmpty returns true if no groups exist for the tenant
func (i *GitLabNamespaceState) TenantEmpty() bool {
	return i.TenantGroups.IsEmpty()
}

// Get the Clickhouse user and password
func (i *GitLabNamespaceState) ClickHouseUserInfo(subsystem SubsystemName) (*url.Userinfo, error) {
	if subsystem == "" {
		return nil, fmt.Errorf("must specify name of the subsystem, one of jaeger, errortracking")
	}

	var (
		s        *corev1.Secret
		user     string
		password string
	)

	switch subsystem {
	case JaegerSubsystemName:
		// parse jaeger specific credentials
		if i.ClickHouse.Jaeger.Credentials == nil {
			return nil, fmt.Errorf("jaeger ClickHouseCredentials not set in GitLabNamespaceState")
		}
		s = i.ClickHouse.Jaeger.Credentials
	case ErrorTrackingSubsystemName:
		// parse errortracking specific credentials
		if i.ClickHouse.ErrorTracking.Credentials == nil {
			return nil, fmt.Errorf("errortracking ClickHouseCredentials not set in GitlabNamespaceState")
		}
		s = i.ClickHouse.ErrorTracking.Credentials
	}

	if pwd, got := s.Data[constants.ClickHouseCredentialsPasswordKey]; got {
		password = string(pwd)
	} else {
		return nil, fmt.Errorf("%s ClickHouseCredentialsPasswordKey not set for ClickHouseUserInfo", subsystem)
	}
	if usr, got := s.Data[constants.ClickHouseCredentialsUserKey]; got {
		user = string(usr)
	} else {
		return nil, fmt.Errorf("%s ClickHouseCredentialsUserKey not set for ClickHouseUserInfo", subsystem)
	}

	return url.UserPassword(user, password), nil
}

func (i *GitLabNamespaceState) ClickHouseQuotasInfo(subsystem SubsystemName) (map[string]string, error) {
	if subsystem == "" {
		return nil, fmt.Errorf("must specify name of the subsystem, one of jaeger, errortracking")
	}

	var c *corev1.ConfigMap

	switch subsystem {
	case JaegerSubsystemName:
		// parse jaeger specific credentials
		if i.ClickHouse.Jaeger.Quotas == nil {
			return nil, fmt.Errorf("jaeger clickhouse quotas not set in GitLabNamespaceState")
		}
		c = i.ClickHouse.Jaeger.Quotas
	case ErrorTrackingSubsystemName:
		// parse errortracking specific credentials
		if i.ClickHouse.ErrorTracking.Quotas == nil {
			return nil, fmt.Errorf("errortracking clickhouse quotas not set in GitlabNamespaceState")
		}
		c = i.ClickHouse.ErrorTracking.Quotas
	}

	return c.Data, nil
}

func (i *GitLabNamespaceState) GetPostgresDatabaseName(cr *v1alpha1.GitLabNamespace) string {
	// Postgres doesn't seem to like database names that are purely numerical
	return fmt.Sprintf("g_%s", cr.Namespace())
}

// Adds the database path to the postgres endpoint for this tenant.
// Each tenant has it's own database in postgres
func (i *GitLabNamespaceState) GetTenantPostgresEndpoint(cr *v1alpha1.GitLabNamespace) *url.URL {
	endpoint := config.Get().PostgresEndpoint()
	if endpoint == nil {
		return nil
	}

	endpoint.Path = fmt.Sprintf("/%s", i.GetPostgresDatabaseName(cr))

	return endpoint
}

func (i *GitLabNamespaceState) Read(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	err := i.readTenantState(ctx, cr, client)
	if err != nil {
		return err
	}

	if err = i.readJaegerClickHouseCredentials(ctx, cr, client); err != nil {
		return err
	}

	if err = i.readJaegerClickHouseQuotas(ctx, cr, client); err != nil {
		return err
	}

	if err = i.readErrorTrackingClickHouseCredentials(ctx, cr, client); err != nil {
		return err
	}

	if err = i.readErrorTrackingClickHouseQuotas(ctx, cr, client); err != nil {
		return err
	}

	err = i.readGroupState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOperatorState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readNamespaceState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readPostgresDBExists(cr)
	if err != nil {
		return err
	}

	tenantGroupsState := tenant.NewGroupsState()
	i.TenantGroups = &tenantGroupsState
	err = tenantGroupsState.Read(ctx, Tenant(cr), client)

	return err
}

func (i *GitLabNamespaceState) readPostgresDBExists(cr *v1alpha1.GitLabNamespace) error {
	ctx := context.Background()
	url := config.Get().PostgresEndpoint()
	if url == nil {
		return fmt.Errorf("postgres endpoint not available yet")
	}
	// Mat: maybe we should keep this open somewhere?
	conn, err := pgx.Connect(ctx, url.String())
	if err != nil {
		return err
	}
	defer conn.Close(ctx)
	rows, err := conn.Query(ctx, fmt.Sprintf("SELECT datname FROM pg_database where datname = '%s'", i.GetPostgresDatabaseName(cr)))
	if err != nil {
		return err
	}
	i.PostgresDBExists = rows.Next()
	return rows.Err()
}

func (i *GitLabNamespaceState) readJaegerClickHouseCredentials(
	ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client,
) error {
	currentState := &corev1.Secret{}
	selector := jaeger.ClickHouseCredentialsSecretSelector(Group(cr))
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			i.ClickHouse.Jaeger.Credentials = nil
			return nil
		}
		return err
	}
	i.ClickHouse.Jaeger.Credentials = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readJaegerClickHouseQuotas(
	ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client,
) error {
	currentState := &corev1.ConfigMap{}
	selector := jaeger.ClickHouseQuotasConfigMapSelector(Group(cr))
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			i.ClickHouse.Jaeger.Quotas = nil
			return nil
		}
		return err
	}
	i.ClickHouse.Jaeger.Quotas = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readErrorTrackingClickHouseCredentials(
	ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client,
) error {
	currentState := &corev1.Secret{}
	selector := errortracking.ClickHouseCredentialsSecretSelector(Group(cr))
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			i.ClickHouse.ErrorTracking.Credentials = nil
			return nil
		}
		return err
	}
	i.ClickHouse.ErrorTracking.Credentials = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readErrorTrackingClickHouseQuotas(
	ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client,
) error {
	currentState := &corev1.ConfigMap{}
	selector := errortracking.ClickHouseQuotasConfigMapSelector(Group(cr))
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			i.ClickHouse.ErrorTracking.Quotas = nil
			return nil
		}
		return err
	}
	i.ClickHouse.ErrorTracking.Quotas = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readTenantState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &tenantOperator.Tenant{}
	selector := TenantSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Tenant = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readGroupState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &tenantOperator.Group{}
	selector := GroupSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Group = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readOperatorState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &v1.Deployment{}
	selector := DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Operator = currentState.DeepCopy()
	return nil
}

func (i *GitLabNamespaceState) readNamespaceState(ctx context.Context, cr *v1alpha1.GitLabNamespace, client client.Client) error {
	currentState := &corev1.Namespace{}
	selector := NamespaceSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Namespace = currentState.DeepCopy()
	return nil
}
