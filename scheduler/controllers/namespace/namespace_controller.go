package namespace

import (
	"context"
	"fmt"
	"net/http"
	"reflect"
	"time"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	v1alpha1 "github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	tenantOperator "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	ctrl "sigs.k8s.io/controller-runtime"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	finalizerName = "gitlabnamespace.opstrace.com/finalizer"
)

// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabnamespaces,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabnamespaces/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabnamespaces/finalizers,verbs=update
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=groups,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=groups/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers;statefulsets;statefulsets/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=namespaces;configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=namespaces/status;,verbs=get;list;watch

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileGitLabNamespace) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		WithOptions(controller.Options{
			// The same object will not be reconciled in parallel.
			// This allows for many GitLabNamespaces to be reconciled
			// in parallel. A sufficiently large number here
			// could cause Kubernetes API rate-limits to kick in.
			MaxConcurrentReconciles: 50,
		}).
		For(&v1alpha1.GitLabNamespace{}).
		Owns(&tenantOperator.Tenant{}).
		Owns(&tenantOperator.Group{}).
		Owns(&appsv1.Deployment{}).
		Owns(&corev1.Namespace{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileGitLabNamespace{}

// ReconcileGitLabNamespace reconciles a GitLabNamespace object
type ReconcileGitLabNamespace struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client    client.Client
	Scheme    *runtime.Scheme
	Transport *http.Transport
	Context   context.Context
	Cancel    context.CancelFunc
	Recorder  record.EventRecorder
	Log       logr.Logger
	Teardown  bool
}

// Reconcile: The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true. Upon completion, we just requeue the Request explicitly to be reconciled every minute
// thereafter.
//
// One valid use-case of the repeated reconcile(s) of the underlying `GitlabNamespace` object is the need for
// updating a `Tenant` to inherit changes in `ClusterSpec.GOUI`. Since `Cluster`` updates do not trigger
// `GitlabNamespace` and by proxy `Tenant` reconciliation, th controller attempts to reconcile all underlying
// objects every X period of time.
func (r *ReconcileGitLabNamespace) Reconcile(ctx context.Context, request reconcile.Request) (ctrl.Result, error) {
	gitlabNamespace := &v1alpha1.GitLabNamespace{}
	err := r.Client.Get(r.Context, request.NamespacedName, gitlabNamespace)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			r.Log.Info("GitLabNamespace has been removed from API")
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := gitlabNamespace.DeepCopy()

	if err := r.setDeletionFinalizer(ctx, cr); err != nil {
		return reconcile.Result{}, err
	}

	configLoaded := config.Get().Loaded
	pgEndpointLoaded := config.Get().PostgresEndpoint() != nil
	chEndpointsLoaded := config.Get().ClickHouseEndpoints() != nil

	if !configLoaded || !pgEndpointLoaded || !chEndpointsLoaded {
		r.Log.Info("cluster configuration not yet loaded, delaying reconciliation")
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}
	// Read current state
	currentState := NewGitLabNamespaceState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err, request)
		return reconcile.Result{}, err
	}
	// Propagate status from some of the components read in currentState
	// back to this CR so it's easy to get a glimpse of the underlying Tenant
	// and Group status
	if currentState.Tenant != nil && len(currentState.Tenant.Status.Conditions) > 0 {
		// Set last condition from tenant
		apimeta.SetStatusCondition(
			&cr.Status.TenantConditions,
			*(currentState.Tenant.Status.Conditions[len(currentState.Tenant.Status.Conditions)-1]).DeepCopy(),
		)
	}
	if currentState.Group != nil && len(currentState.Group.Status.Conditions) > 0 {
		// Set last condition from tenant
		apimeta.SetStatusCondition(
			&cr.Status.GroupConditions,
			*(currentState.Group.Status.Conditions[len(currentState.Group.Status.Conditions)-1]).DeepCopy(),
		)
	}

	// Get the actions required to reach the desired state
	// The order of process matters here. In general, we should first update
	// the lowest order dependencies and then work our way up the stack.
	//
	// Reconciliation will block (and retry) until each action returns successfully.
	// This means that readiness check actions will return errors
	// until each readiness check passes, which blocks downstream actions until the readiness
	// check passes. So in the following case, ClickHouse gitlabNamespace
	// won't we upgraded/reconciled until the ClickHouse operator has been reconciled
	// and passes the readiness check.
	nsReconciler := NewGitLabNamespaceReconciler(r.Teardown, r.Log)
	desiredState := nsReconciler.Reconcile(currentState, cr)

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err, request)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	if r.Teardown {
		return reconcile.Result{}, r.teardownFinalizer(ctx, cr)
	}

	return reconcile.Result{RequeueAfter: time.Minute}, r.manageSuccess(cr, currentState, request)
}

func (r *ReconcileGitLabNamespace) setDeletionFinalizer(ctx context.Context, cr *v1alpha1.GitLabNamespace) error {
	r.Teardown = false

	// examine DeletionTimestamp to determine if object is under deletion
	if cr.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,
		// then lets add the finalizer and update the object. This is equivalent
		// registering our finalizer.
		if !controllerutil.ContainsFinalizer(cr, finalizerName) {
			controllerutil.AddFinalizer(cr, finalizerName)

			if err := r.Client.Update(ctx, cr); err != nil {
				return fmt.Errorf("adding finalizer: %w", err)
			}
		}
	} else {
		r.Teardown = true
	}

	return nil
}

func (r *ReconcileGitLabNamespace) teardownFinalizer(ctx context.Context, cr *v1alpha1.GitLabNamespace) error {
	if controllerutil.ContainsFinalizer(cr, finalizerName) {
		// Successfully deleted everything we care about.
		// Remove our finalizer from the list and update it
		controllerutil.RemoveFinalizer(cr, finalizerName)

		if err := r.Client.Update(ctx, cr); err != nil {
			return fmt.Errorf("removing finalizer: %w", err)
		}
	}
	return nil
}

// Handle success case
func (r *ReconcileGitLabNamespace) manageSuccess(cr *v1alpha1.GitLabNamespace, state *GitLabNamespaceState, request reconcile.Request) error {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	// Check conditions of group and tenant and only then set condition to true.
	if !apimeta.IsStatusConditionTrue(cr.Status.TenantConditions, common.ConditionTypeReady) {
		condition.Status = metav1.ConditionFalse
		condition.Reason = common.ReconciliationFailedReason
		condition.Message = "Tenant not Ready"
	} else if !apimeta.IsStatusConditionTrue(cr.Status.GroupConditions, common.ConditionTypeReady) {
		condition.Status = metav1.ConditionFalse
		condition.Reason = common.ReconciliationFailedReason
		condition.Message = "Group not Ready"
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	if state.Tenant != nil && state.Tenant.Status.Argus.URL != nil {
		cr.Status.ArgusURL = state.Tenant.Status.Argus.URL
	}

	instance := &v1alpha1.GitLabNamespace{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		return err
	}

	r.Log.Info("gitlabNamespace successfully reconciled", "gitlabNamespace", cr.Name)
	if !reflect.DeepEqual(instance.Status, cr.Status) {
		err := r.Client.Status().Update(r.Context, cr)
		if err != nil {
			// Ignore conclicts. Resource might just be outdated.
			if k8serrors.IsConflict(err) {
				return nil
			}
			r.Log.Error(err, "error updating gitlabNamespace status")
		}
	}
	return nil
}

// Handle error case: update gitlabNamespace with error message and status
func (r *ReconcileGitLabNamespace) manageError(cr *v1alpha1.GitLabNamespace, issue error, request reconcile.Request) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	instance := &v1alpha1.GitLabNamespace{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		r.Log.Error(err, "failed to retrieve instance of gitlabNamespace")
	}

	if !reflect.DeepEqual(instance.Status, cr.Status) {
		err := r.Client.Status().Update(r.Context, cr)
		if err != nil {
			// Ignore conclicts. Resource might just be outdated.
			if k8serrors.IsConflict(err) {
				return
			}
			r.Log.Error(err, "error updating gitlabNamespace status")
		}
	}
}
