package namespace

import (
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getNamespaceName(cr *v1alpha1.GitLabNamespace) string {
	return cr.Namespace()
}

func getNamespaceLabels(current map[string]string) map[string]string {
	desired := map[string]string{
		"cert-manager.io/disable-validation": "true",
		constants.TenantLabelIdentifier:      "true",
	}
	if current == nil {
		return desired
	}
	return common.MergeMap(current, desired)
}

func getNamespaceAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func Namespace(cr *v1alpha1.GitLabNamespace) *v1.Namespace {
	return &v1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getNamespaceName(cr),
			Labels:      getNamespaceLabels(nil),
			Annotations: getNamespaceAnnotations(cr, nil),
		},
	}
}

func NamespaceSelector(cr *v1alpha1.GitLabNamespace) client.ObjectKey {
	return client.ObjectKey{
		Name: getNamespaceName(cr),
	}
}

func NamespaceMutator(cr *v1alpha1.GitLabNamespace, current *v1.Namespace) error {
	current.Labels = getNamespaceLabels(current.Labels)
	current.Annotations = getNamespaceAnnotations(cr, current.Annotations)

	return nil
}
