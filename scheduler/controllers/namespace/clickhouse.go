package namespace

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
)

func ClickHouseUsersProvision(
	username string,
	password string,
	grants []string,
	databaseName string,
	chEndpoints *config.ClickHouseEndpoints,
) []common.Action {
	actions := []common.Action{}

	actions = append(actions,
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL create user %s if not exists with password [...]", username),
			SQL: fmt.Sprintf("CREATE USER IF NOT EXISTS %s ON CLUSTER '{cluster}' IDENTIFIED WITH plaintext_password BY '%s' HOST ANY DEFAULT DATABASE default",
				username,
				password,
			),
			URL:      chEndpoints.Native,
			Database: databaseName,
		},
		// REMOTE source privilege required for Distributed tables
		// This can't be specified on the database level
		common.ClickHouseAction{
			Msg: fmt.Sprintf("clickhouse SQL grant remote to user %s", username),
			SQL: fmt.Sprintf("GRANT ON CLUSTER '{cluster}' REMOTE ON *.* TO %s",
				username,
			),
			URL:      chEndpoints.Native,
			Database: databaseName,
		},
	)

	// add grants as requested
	if len(grants) > 0 {
		grantsStr := strings.Join(grants, ",")
		actions = append(actions,
			common.ClickHouseAction{
				Msg: fmt.Sprintf("clickhouse SQL grant permissions to user %s", username),
				SQL: fmt.Sprintf("GRANT ON CLUSTER '{cluster}' %s ON %s.* TO %s",
					grantsStr,
					databaseName,
					username,
				),
				URL:      chEndpoints.Native,
				Database: databaseName,
			},
		)
	}

	return actions
}

/*
ref: https://clickhouse.com/docs/en/operations/quotas/

<!-- Quota name. -->
<default>
	<!-- Restrictions for a time period. You can set many intervals with different restrictions. -->
	<interval>
		<!-- Length of the interval. -->
		<duration>3600</duration>

		<!-- Unlimited. Just collect data for the specified time interval. -->
		<queries>0</queries>
		<query_selects>0</query_selects>
		<query_inserts>0</query_inserts>
		<errors>0</errors>
		<result_rows>0</result_rows>
		<read_rows>0</read_rows>
		<execution_time>0</execution_time>
	</interval>
</default>

ref: https://clickhouse.com/docs/en/sql-reference/statements/create/quota/

Syntax:
CREATE QUOTA [IF NOT EXISTS | OR REPLACE] name [ON CLUSTER cluster_name]
    [KEYED BY {user_name | ip_address | client_key | client_key,user_name | client_key,ip_address} | NOT KEYED]
    [FOR [RANDOMIZED] INTERVAL number {second | minute | hour | day | week | month | quarter | year}
        {MAX { {queries | query_selects | query_inserts | errors | result_rows | result_bytes | read_rows | read_bytes | execution_time} = number } [,...] |
         NO LIMITS | TRACKING ONLY} [,...]]
    [TO {role [,...] | ALL | ALL EXCEPT role [,...]}]
*/

//nolint:funlen
func ClickHouseQuotasProvision(
	quotaName string,
	databaseName string,
	chEndpoints *config.ClickHouseEndpoints,
	quotaDefn string,
	username string,
) []common.Action {
	if quotaName == "" {
		return []common.Action{
			common.LogAction{
				Msg:   "creating clickhouse quota",
				Error: fmt.Errorf("quota name cannot be empty"),
			},
		}
	}

	// unmarshall quota definition string into its individual components
	var quotas map[string]string
	if err := json.Unmarshal([]byte(quotaDefn), &quotas); err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "unmarshalling clickhouse quota configmap",
				Error: err,
			},
		}
	}

	// [KEYED BY {user_name | ip_address | client_key | client_key,user_name | client_key,ip_address} | NOT KEYED]
	var keyedByString string
	if _, ok := quotas["keyedBy"]; !ok {
		keyedByString = "NOT KEYED"
	} else {
		keyedByString = quotas["keyedBy"]
	}

	// [FOR [RANDOMIZED] INTERVAL number {second | minute | hour | day | week | month | quarter | year}
	// 		{MAX { {queries | query_selects | query_inserts | errors | result_rows | result_bytes | read_rows | read_bytes | execution_time} = number } [,...] |
	// 			NO LIMITS | TRACKING ONLY} [,...]]
	var quotasString string
	if _, ok := quotas["quotas"]; !ok {
		quotasString = "FOR INTERVAL 3600 second MAX queries = 100"
	} else {
		quotasString = quotas["quotas"]
	}

	// [TO {role [,...] | ALL | ALL EXCEPT role [,...]}]
	if username == "" {
		username = "CURRENT USER"
	}

	var usersString string
	if _, ok := quotas["users"]; !ok {
		usersString = username // use the passed username unless overridden by quota definition string
	} else {
		usersString = quotas["users"]
	}

	//
	// Because the controller reconciles all quotas continuously, we use
	// a CREATE IF NOT EXISTS, immediately followed by an ALTER IF EXISTS
	// to make sure we update necessary changes in-place. Note that, we
	// cannot use a CREATE OR REPLACE because that resets the quota interval
	// to start from when the query was executed, which in our case would
	// essentially reset configured quotas every reconcile loop.
	//
	// Bear in mind, altering a quota by name with changes to the concerned
	// duration ends up creating a new quota altogether, since Clickhouse
	// quotas are indexed/maintained specific to their "duration".
	//
	// e.g.
	// Changing quota:foo, duration:3600s, max_queries:100
	//       to quota:foo, duration: 600s, max_queries:500
	// will end up creating a new quota with the latter definition.
	//
	// To make in-place updates to quotas, we must keep the duration consistent.
	//
	createQuotaSQL := fmt.Sprintf("CREATE QUOTA IF NOT EXISTS %s ON CLUSTER '{cluster}' %s %s TO %s",
		quotaName,
		keyedByString,
		quotasString,
		usersString,
	)

	alterQuotaSQL := fmt.Sprintf("ALTER QUOTA IF EXISTS %s ON CLUSTER '{cluster}' %s %s TO %s",
		quotaName,
		keyedByString,
		quotasString,
		usersString,
	)

	return []common.Action{
		common.ClickHouseAction{
			Msg:      fmt.Sprintf("clickhouse SQL create quota %s if not exists [...]", quotaName),
			SQL:      createQuotaSQL,
			URL:      chEndpoints.Native,
			Database: databaseName,
		},
		common.ClickHouseAction{
			Msg:      fmt.Sprintf("clickhouse SQL alter quota %s if exists [...]", quotaName),
			SQL:      alterQuotaSQL,
			URL:      chEndpoints.Native,
			Database: databaseName,
		},
	}
}
