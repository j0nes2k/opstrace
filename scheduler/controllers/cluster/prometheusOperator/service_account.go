package prometheusOperator

import (
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getPrometheusOperatorServiceAccountName() string {
	return GetPrometheusOperatorDeploymentName()
}

func getPrometheusOperatorServiceAccountLabels() map[string]string {
	return GetPrometheusOperatorDeploymentSelector()
}

func getPrometheusOperatorServiceAccountAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPrometheusOperatorServiceAccountImagePullSecrets(cr *v1alpha1.Cluster) []v1.LocalObjectReference {
	return cr.Spec.ImagePullSecrets
}

func PrometheusOperatorServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getPrometheusOperatorServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getPrometheusOperatorServiceAccountLabels(),
			Annotations: getPrometheusOperatorServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getPrometheusOperatorServiceAccountImagePullSecrets(cr),
	}
}

func PrometheusOperatorServiceAccountSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      getPrometheusOperatorServiceAccountName(),
	}
}

func PrometheusOperatorServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	current.Labels = getPrometheusOperatorServiceAccountLabels()
	current.Annotations = getPrometheusOperatorServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getPrometheusOperatorServiceAccountImagePullSecrets(cr)
}
