package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/nginxIngress"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type NginxIngressReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewNginxIngressReconciler(teardown bool, logger logr.Logger) *NginxIngressReconciler {
	return &NginxIngressReconciler{
		Teardown: teardown,
		Log:      logger.WithName("nginx-ingress"),
	}
}

func (i *NginxIngressReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getNginxIngressServiceAccountDesiredState(cr))
	desired = desired.AddActions(i.getRBACDesiredState(cr))
	desired = desired.AddAction(i.getIngressConfigDesiredState(cr))
	desired = desired.AddAction(i.getServiceDesiredState(cr))
	desired = desired.AddAction(i.getMetricsServiceDesiredState(cr))
	desired = desired.AddAction(i.getIngressClassDesiredState(cr))
	desired = desired.AddAction(i.getDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
	desired = desired.AddActions(i.getReadiness(state))

	return desired
}

func (i *NginxIngressReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.NginxIngress.Deployment != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.NginxIngress.Deployment,
					Msg: "check nginx-ingress deployment is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.NginxIngress.Deployment,
			Msg: "check nginx-ingress deployment readiness",
		},
	}
}

func (i *NginxIngressReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := nginxIngress.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize nginx-ingress rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("nginx-ingress %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("nginx-ingress %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *NginxIngressReconciler) getIngressConfigDesiredState(cr *v1alpha1.Cluster) common.Action {
	config := nginxIngress.Config(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: config,
			Msg: "nginx-ingress config",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: config,
		Msg: "nginx-ingress config",
		Mutator: func() error {
			nginxIngress.ConfigMutator(config)
			return nil
		},
	}
}

func (i *NginxIngressReconciler) getIngressClassDesiredState(cr *v1alpha1.Cluster) common.Action {
	class := nginxIngress.IngressClass(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: class,
			Msg: "nginx-ingress ingress-class",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: class,
		Msg: "nginx-ingress ingress-class",
		Mutator: func() error {
			nginxIngress.IngressClassMutator(cr, class)
			return nil
		},
	}
}

func (i *NginxIngressReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	svc := nginxIngress.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "nginx-ingress service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "nginx-ingress service",
		Mutator: func() error {
			return nginxIngress.ServiceMutator(cr, svc)
		},
	}
}

func (i *NginxIngressReconciler) getMetricsServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	svc := nginxIngress.MetricsService(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "nginx-ingress metrics service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "nginx-ingress metrics service",
		Mutator: func() error {
			return nginxIngress.MetricsServiceMutator(cr, svc)
		},
	}
}
func (i *NginxIngressReconciler) getNginxIngressServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := nginxIngress.ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "nginx-ingress service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "nginx-ingress service account",
		Mutator: func() error {
			nginxIngress.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *NginxIngressReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	sm := nginxIngress.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: "nginx-ingress servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sm,
		Msg: "nginx-ingress servicemonitor",
		Mutator: func() error {
			return nginxIngress.ServiceMonitorMutator(cr, sm)
		},
	}
}

func (i *NginxIngressReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := nginxIngress.Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "nginx-ingress deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "nginx-ingress deployment",
		Mutator: func() error {
			return nginxIngress.DeploymentMutator(cr, deploy)
		},
	}
}
