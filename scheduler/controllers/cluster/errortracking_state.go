package cluster

import (
	"context"

	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	errortrackingapi "github.com/opstrace/opstrace/scheduler/controllers/cluster/errorTrackingAPI"
	appsv1 "k8s.io/api/apps/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ErrorTrackingAPIState struct {
	// Track deployment readiness
	Deployment *appsv1.Deployment
	// Track ingress readiness
	Ingress *netv1.Ingress
}

func NewErrorTrackingAPIState() *ErrorTrackingAPIState {
	return &ErrorTrackingAPIState{}
}

func (i *ErrorTrackingAPIState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readDeployment(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readIngress(ctx, cr, client)

	return err
}

func (r *ErrorTrackingAPIState) readDeployment(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := errortrackingapi.DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	r.Deployment = currentState.DeepCopy()
	return nil
}

func (r *ErrorTrackingAPIState) readIngress(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := errortrackingapi.IngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	r.Ingress = currentState.DeepCopy()
	return nil
}
