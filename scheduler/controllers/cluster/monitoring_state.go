package cluster

import (
	"context"

	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "github.com/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type MonitoringState struct {
	APIServer struct {
		ApiserverPrometheusRule  *monitoring.PrometheusRule
		KubernetesPrometheusRule *monitoring.PrometheusRule
		ServiceMonitor           *monitoring.ServiceMonitor
	}
	CoreDNS struct {
		ServiceMonitor *monitoring.ServiceMonitor
	}
	KubeControllerManager struct {
		ServiceMonitor *monitoring.ServiceMonitor
	}
	KubeScheduler struct {
		PrometheusRule *monitoring.PrometheusRule
		ServiceMonitor *monitoring.ServiceMonitor
	}
	KubeStateMetrics struct {
		Deployment     *appsv1.Deployment
		ServiceAccount *v1.ServiceAccount
		ServiceMonitor *monitoring.ServiceMonitor
		Service        *v1.Service
	}
	Kubelet struct {
		ServiceMonitor *monitoring.ServiceMonitor
	}
	Node struct {
		PrometheusRule *monitoring.PrometheusRule
	}
	NodeExporter struct {
		Daemonset      *appsv1.DaemonSet
		PrometheusRule *monitoring.PrometheusRule
		ServiceAccount *v1.ServiceAccount
		ServiceMonitor *monitoring.ServiceMonitor
		Service        *v1.Service
	}
}

func NewMonitoringState() *MonitoringState {
	return &MonitoringState{}
}

//nolint:funlen,cyclop
func (m *MonitoringState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	// APIServer
	{
		pr, err := m.readPrometheusRule(ctx, cr, client, monitors.APIServer)
		if err != nil {
			return err
		}
		m.APIServer.ApiserverPrometheusRule = pr

		pr, err = m.readPrometheusRule(ctx, cr, client, monitors.Kubernetes)
		if err != nil {
			return err
		}
		m.APIServer.KubernetesPrometheusRule = pr

		sm, err := m.readServiceMonitor(ctx, cr, client, monitors.APIServer)
		if err != nil {
			return err
		}
		m.APIServer.ServiceMonitor = sm
	}
	// CoreDNS
	{
		sm, err := m.readServiceMonitor(ctx, cr, client, monitors.CoreDNS)
		if err != nil {
			return err
		}
		m.CoreDNS.ServiceMonitor = sm
	}
	// KubeControllerManager
	{
		sm, err := m.readServiceMonitor(ctx, cr, client, monitors.KubeControllerManager)
		if err != nil {
			return err
		}
		m.KubeControllerManager.ServiceMonitor = sm
	}
	// KubeScheduler
	{
		pr, err := m.readPrometheusRule(ctx, cr, client, monitors.KubeScheduler)
		if err != nil {
			return err
		}
		m.KubeScheduler.PrometheusRule = pr

		sm, err := m.readServiceMonitor(ctx, cr, client, monitors.KubeScheduler)
		if err != nil {
			return err
		}
		m.KubeScheduler.ServiceMonitor = sm
	}
	// KubeStateMetrics
	{
		sa, err := m.readServiceAccount(ctx, cr, client, monitors.KubeStateMetrics)
		if err != nil {
			return err
		}
		m.KubeStateMetrics.ServiceAccount = sa

		service, err := m.readService(ctx, cr, client, monitors.KubeStateMetrics)
		if err != nil {
			return err
		}
		m.KubeStateMetrics.Service = service

		deployment, err := m.readDeployment(ctx, cr, client, monitors.KubeStateMetrics)
		if err != nil {
			return err
		}
		m.KubeStateMetrics.Deployment = deployment

		sm, err := m.readServiceMonitor(ctx, cr, client, monitors.KubeStateMetrics)
		if err != nil {
			return err
		}
		m.KubeStateMetrics.ServiceMonitor = sm
	}
	// Kubelet
	{
		sm, err := m.readServiceMonitor(ctx, cr, client, monitors.Kubelet)
		if err != nil {
			return err
		}
		m.Kubelet.ServiceMonitor = sm
	}
	// Node
	{
		pr, err := m.readPrometheusRule(ctx, cr, client, monitors.Node)
		if err != nil {
			return err
		}
		m.Node.PrometheusRule = pr
	}
	// NodeExporter
	{
		ds, err := m.readDaemonset(ctx, cr, client, monitors.NodeExporter)
		if err != nil {
			return err
		}
		m.NodeExporter.Daemonset = ds

		pr, err := m.readPrometheusRule(ctx, cr, client, monitors.NodeExporter)
		if err != nil {
			return err
		}
		m.NodeExporter.PrometheusRule = pr

		sa, err := m.readServiceAccount(ctx, cr, client, monitors.NodeExporter)
		if err != nil {
			return err
		}
		m.NodeExporter.ServiceAccount = sa

		sm, err := m.readServiceMonitor(ctx, cr, client, monitors.NodeExporter)
		if err != nil {
			return err
		}
		m.NodeExporter.ServiceMonitor = sm

		service, err := m.readService(ctx, cr, client, monitors.NodeExporter)
		if err != nil {
			return err
		}
		m.NodeExporter.Service = service
	}
	return nil
}

func (m *MonitoringState) readPrometheusRule(
	ctx context.Context,
	cr *v1alpha1.Cluster,
	c client.Client,
	name string,
) (*monitoring.PrometheusRule, error) {
	currentState := &monitoring.PrometheusRule{}
	selector := client.ObjectKey{
		Name:      name,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}
	return currentState.DeepCopy(), nil
}

func (m *MonitoringState) readServiceMonitor(ctx context.Context, cr *v1alpha1.Cluster, c client.Client, name string) (*monitoring.ServiceMonitor, error) {
	currentState := &monitoring.ServiceMonitor{}
	selector := client.ObjectKey{
		Name:      name,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}
	return currentState.DeepCopy(), nil
}

func (m *MonitoringState) readServiceAccount(ctx context.Context, cr *v1alpha1.Cluster, c client.Client, name string) (*v1.ServiceAccount, error) {
	currentState := &v1.ServiceAccount{}
	selector := client.ObjectKey{
		Name:      name,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}
	return currentState.DeepCopy(), nil
}

func (m *MonitoringState) readDeployment(ctx context.Context, cr *v1alpha1.Cluster, c client.Client, name string) (*appsv1.Deployment, error) {
	currentState := &appsv1.Deployment{}
	selector := client.ObjectKey{
		Name:      name,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}
	return currentState.DeepCopy(), nil
}

func (m *MonitoringState) readService(ctx context.Context, cr *v1alpha1.Cluster, c client.Client, name string) (*v1.Service, error) {
	currentState := &v1.Service{}
	selector := client.ObjectKey{
		Name:      name,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}
	return currentState.DeepCopy(), nil
}

func (m *MonitoringState) readDaemonset(ctx context.Context, cr *v1alpha1.Cluster, c client.Client, name string) (*appsv1.DaemonSet, error) {
	currentState := &appsv1.DaemonSet{}
	selector := client.ObjectKey{
		Name:      name,
		Namespace: cr.Namespace(),
	}
	if err := c.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}
	return currentState.DeepCopy(), nil
}
