package certManager

import (
	cmacme "github.com/cert-manager/cert-manager/pkg/apis/acme/v1"
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getDNSChallenge(cr *v1alpha1.Cluster) *cmacme.ACMEChallengeSolverDNS01 {
	return &cr.Spec.DNS.DNS01Challenge
}

func getACMEEmail(cr *v1alpha1.Cluster) string {
	return cr.Spec.DNS.ACMEEmail
}

func getProdDNSServer(cr *v1alpha1.Cluster) string {
	s := cr.Spec.DNS.ACMEserver
	if s != nil {
		return *s
	}
	return "https://acme-v02.api.letsencrypt.org/directory"
}

func ProdClusterIssuer(cr *v1alpha1.Cluster) *certmanager.ClusterIssuer {
	return &certmanager.ClusterIssuer{
		ObjectMeta: v1.ObjectMeta{
			Name: constants.LetsEncryptProd,
		},
		Spec: certmanager.IssuerSpec{
			IssuerConfig: certmanager.IssuerConfig{
				ACME: &cmacme.ACMEIssuer{
					Server: getProdDNSServer(cr),
					Email:  getACMEEmail(cr),
					PrivateKey: cmmeta.SecretKeySelector{
						LocalObjectReference: cmmeta.LocalObjectReference{
							Name: "letsencrypt-prod",
						},
					},
					Solvers: []cmacme.ACMEChallengeSolver{{
						DNS01: getDNSChallenge(cr),
					}},
				},
			},
		},
	}
}

func ProdClusterIssuerMutator(cr *v1alpha1.Cluster, current *certmanager.ClusterIssuer) {
	issuer := ProdClusterIssuer(cr)
	current.Spec.SelfSigned = issuer.Spec.SelfSigned
}

func ProdClusterIssuerSelector() client.ObjectKey {
	return client.ObjectKey{
		Name: constants.LetsEncryptProd,
	}
}
