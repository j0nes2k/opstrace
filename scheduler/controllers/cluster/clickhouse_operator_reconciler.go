package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/clickhouseOperator"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ClickHouseOperatorReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewClickHouseOperatorReconciler(teardown bool, logger logr.Logger) *ClickHouseOperatorReconciler {
	return &ClickHouseOperatorReconciler{
		Teardown: teardown,
		Log:      logger.WithName("clickhouse-operator"),
	}
}

func (i *ClickHouseOperatorReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddActions(i.getRBACDesiredState(cr))
	desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
	desired = desired.AddAction(i.getDeploymentDesiredState(state, cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))

	desired = desired.AddActions(i.getReadiness(state))

	return desired
}

func (i *ClickHouseOperatorReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.ClickHouse.OperatorDeployment != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.ClickHouse.OperatorDeployment,
					Msg: "check clickhouse-operator deployment is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.ClickHouse.OperatorDeployment,
			Msg: "check clickhouse-operator deployment readiness",
		},
	}
}

func (i *ClickHouseOperatorReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := clickhouseOperator.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize clickhouse-operator rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("clickhouse-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("clickhouse-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *ClickHouseOperatorReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := clickhouseOperator.ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "clickhouse-operator service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "clickhouse-operator service account",
		Mutator: func() error {
			clickhouseOperator.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *ClickHouseOperatorReconciler) getDeploymentDesiredState(state *ClusterState, cr *v1alpha1.Cluster) common.Action {
	deploy := clickhouseOperator.Deployment(cr)

	if i.Teardown {
		// only delete once the cluster has been torn down
		if state.ClickHouse.Cluster != nil {
			return common.LogAction{
				Msg: "skipping clickhouse-operator teardown until cluster is torn down",
			}
		}
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "clickhouse-operator deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "clickhouse-operator deployment",
		Mutator: func() error {
			return clickhouseOperator.DeploymentMutator(cr, deploy)
		},
	}
}

func (i *ClickHouseOperatorReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := clickhouseOperator.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "clickhouse-operator servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "clickhouse-operator servicemonitor",
		Mutator: func() error {
			return clickhouseOperator.ServiceMonitorMutator(cr, monitor)
		},
	}
}
