package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/certManager"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type CertManagerReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewCertManagerReconciler(teardown bool, logger logr.Logger) *CertManagerReconciler {
	return &CertManagerReconciler{
		Teardown: teardown,
		Log:      logger.WithName("certmanager"),
	}
}

func (i *CertManagerReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getCertManagerServiceAccountDesiredState(cr))
	desired = desired.AddAction(i.getCainjectorServiceAccountDesiredState(cr))
	desired = desired.AddActions(i.getRBACDesiredState(cr))
	desired = desired.AddAction(i.getServiceDesiredState(cr))
	desired = desired.AddAction(i.getSelfSignedIssuerDesiredState())
	desired = desired.AddAction(i.getDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getCainjectorDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
	desired = desired.AddActions(i.getReadiness(state))

	if cr.Spec.UseTLS() {
		desired = desired.AddAction(i.getProdIssuerDesiredState(cr))
		desired = desired.AddAction(i.getStagingIssuerDesiredState(cr))
		desired = desired.AddAction(i.getTLSCertDesiredState(cr))
		desired = desired.AddActions(i.getTLSSecretDesiredState(state))
	} else {
		i.Log.Info("TLS on ingress not enabled")
	}

	return desired
}

func (i *CertManagerReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		actions := []common.Action{}
		if state.CertMananger.CertManagerDeployment != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.CertMananger.CertManagerDeployment,
					Msg: "check certmanager deployment is gone",
				},
			)
		}
		if state.CertMananger.CaInjectorDeployment != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.CertMananger.CaInjectorDeployment,
					Msg: "check cainjector deployment is gone",
				},
			)
		}
		return actions
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.CertMananger.CertManagerDeployment,
			Msg: "check certmanager deployment readiness",
		},
		common.DeploymentReadyAction{
			Ref: state.CertMananger.CaInjectorDeployment,
			Msg: "check cainjector deployment readiness",
		},
	}
}

func (i *CertManagerReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := certManager.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize certmanager rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("certmanager %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("certmanager %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *CertManagerReconciler) getSelfSignedIssuerDesiredState() common.Action {
	issuer := certManager.SelfSignedClusterIssuer()

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: issuer,
			Msg: "certmanager self-signed clusterIssuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: issuer,
		Msg: "certmanager self-signed clusterIssuer",
		Mutator: func() error {
			certManager.SelfSignedClusterIssuerMutator(issuer)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getProdIssuerDesiredState(cr *v1alpha1.Cluster) common.Action {
	issuer := certManager.ProdClusterIssuer(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: issuer,
			Msg: "certmanager letsencrypt-prod clusterIssuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: issuer,
		Msg: "certmanager letsencrypt-prod clusterIssuer",
		Mutator: func() error {
			certManager.ProdClusterIssuerMutator(cr, issuer)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getStagingIssuerDesiredState(cr *v1alpha1.Cluster) common.Action {
	issuer := certManager.StagingClusterIssuer(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: issuer,
			Msg: "certmanager letsencrypt-staging clusterIssuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: issuer,
		Msg: "certmanager letsencrypt-staging clusterIssuer",
		Mutator: func() error {
			certManager.StagingClusterIssuerMutator(cr, issuer)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getTLSCertDesiredState(cr *v1alpha1.Cluster) common.Action {
	cert := certManager.Certificate(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: cert,
			Msg: "tls certificate",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: cert,
		Msg: "tls certificate",
		Mutator: func() error {
			certManager.CertificateMutator(cr, cert)
			return nil
		},
	}
}

// copy the TLS secret to all tenant namespaces
func (i *CertManagerReconciler) getTLSSecretDesiredState(state *ClusterState) []common.Action {
	if state.TLSSecret == nil {
		return []common.Action{common.LogAction{
			Msg: "TLS secret not found, certmanager is likely still processing it",
		}}
	}

	actions := []common.Action{}

	for _, ns := range state.Tenants.Items {
		tmp := state.TLSSecret.DeepCopy()
		// Create a new secret to avoid copying all the fields of a previously existing object over
		secret := &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      tmp.Name,
				Namespace: ns.Name,
			},
			Data:       tmp.Data,
			StringData: tmp.StringData,
			Type:       tmp.Type,
		}

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: secret,
				Msg: fmt.Sprintf("TLS secret in namespace: %s", ns.Name),
			})
		}

		actions = append(actions, common.GenericCreateOrUpdateAction{
			Ref: secret,
			Msg: fmt.Sprintf("TLS secret in namespace: %s", ns.Name),
			Mutator: func() error {
				secret.Data = state.TLSSecret.Data
				return nil
			},
		})
	}
	return actions
}

func (i *CertManagerReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	svc := certManager.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "certmanager service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "certmanager service",
		Mutator: func() error {
			return certManager.ServiceMutator(cr, svc)
		},
	}
}

func (i *CertManagerReconciler) getCertManagerServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := certManager.CertManagerServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "certmanager service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "certmanager service account",
		Mutator: func() error {
			certManager.CertManagerServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getCainjectorServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := certManager.CainjectorServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "cainjector service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "cainjector service account",
		Mutator: func() error {
			certManager.CainjectorServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	sm := certManager.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: "certmanager servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sm,
		Msg: "certmanager servicemonitor",
		Mutator: func() error {
			return certManager.ServiceMonitorMutator(cr, sm)
		},
	}
}

func (i *CertManagerReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := certManager.CertManagerDeployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "certmanager deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "certmanager deployment",
		Mutator: func() error {
			return certManager.CertManagerDeploymentMutator(cr, deploy)
		},
	}
}

func (i *CertManagerReconciler) getCainjectorDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := certManager.CainjectorDeployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "cainjector deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "cainjector deployment",
		Mutator: func() error {
			return certManager.CainjectorDeploymentMutator(cr, deploy)
		},
	}
}
