package cluster

import (
	"context"

	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/gatekeeper"
	appsv1 "k8s.io/api/apps/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type GatekeeperState struct {
	// Track deployment readiness
	Deployment *appsv1.Deployment
	// Track ingress readiness
	Ingress *netv1.Ingress
}

func NewGatekeeperState() *GatekeeperState {
	return &GatekeeperState{}
}

func (i *GatekeeperState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readDeployment(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readIngress(ctx, cr, client)

	return err
}

func (i *GatekeeperState) readDeployment(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := gatekeeper.DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Deployment = currentState.DeepCopy()
	return nil
}

func (i *GatekeeperState) readIngress(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := gatekeeper.IngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Ingress = currentState.DeepCopy()
	return nil
}
