package nginxIngress

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/opstrace/opstrace/go/pkg/common"
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "200m"
	MemoryLimit   = "512Mi"
	CpuLimit      = "500m"
)

var Replicas int32 = 2

func GetNginxIngressDeploymentName() string {
	return constants.NginxIngressName
}

func GetNginxIngressDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.NginxIngressName,
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	var maxUnaval = intstr.FromInt(25)
	var maxSurge = intstr.FromInt(25)

	return v1.DeploymentStrategy{
		Type: v1.RollingUpdateDeploymentStrategyType,
		RollingUpdate: &v1.RollingUpdateDeployment{
			MaxUnavailable: &maxUnaval,
			MaxSurge:       &maxSurge,
		},
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.NginxIngressName,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(existing map[string]string) map[string]string {
	hash := GetNginxControllerConfigHash()

	return utils.MergeMap(existing, map[string]string{
		constants.LastConfigAnnotation: hash,
	})
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.NginxIngressName,
	}
}

func getContainerEnv() []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name: "POD_NAME",
			ValueFrom: &corev1.EnvVarSource{
				FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.name"},
			},
		},
		{
			Name: "POD_NAMESPACE",
			ValueFrom: &corev1.EnvVarSource{
				FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.namespace"},
			},
		},
	}
}

func getPorts() []corev1.ContainerPort {
	return []corev1.ContainerPort{
		{
			ContainerPort: 443,
			HostPort:      443,
			Name:          "https",
		},
		{
			ContainerPort: 80,
			HostPort:      80,
			Name:          "http",
		},
		{
			ContainerPort: 10254,
			HostPort:      10254,
			Name:          "metrics",
		},
	}
}

func getReadinessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Port:   intstr.FromInt(10254),
				Path:   "/healthz",
				Scheme: corev1.URISchemeHTTP,
			},
		},
		PeriodSeconds:    10,
		SuccessThreshold: 1,
		TimeoutSeconds:   10,
	}
}

func getLivenessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Port:   intstr.FromInt(10254),
				Path:   "/healthz",
				Scheme: corev1.URISchemeHTTP,
			},
		},
		InitialDelaySeconds: 10,
		PeriodSeconds:       10,
		SuccessThreshold:    1,
		TimeoutSeconds:      10,
	}
}

func getContainers(cr *v1alpha1.Cluster) []corev1.Container {
	var allowPrivilegeEscalation bool = true
	var user int64 = 101
	var args = []string{
		"/nginx-ingress-controller",
		"--election-id=ingress-controller-leader",
		fmt.Sprintf("--controller-class=%s", constants.IngressControllerName),
		fmt.Sprintf("--ingress-class=%s", constants.IngressClassName),
		"--configmap=$(POD_NAMESPACE)/nginx-ingress",
		"--annotations-prefix=nginx.ingress.kubernetes.io",
		"--watch-ingress-without-class=true",
	}
	if cr.Spec.Target == common.KIND {
		args = append(args, "--publish-status-address=localhost")
	} else {
		args = append(args, "--publish-service=$(POD_NAMESPACE)/nginx-ingress")
	}

	return []corev1.Container{{
		Name:            constants.NginxIngressName,
		Image:           constants.OpstraceImages().NginxIngressImage,
		Args:            args,
		Env:             getContainerEnv(),
		Ports:           getPorts(),
		Resources:       getResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		SecurityContext: &corev1.SecurityContext{
			AllowPrivilegeEscalation: &allowPrivilegeEscalation,
			Capabilities: &corev1.Capabilities{
				Drop: []corev1.Capability{"ALL"},
				Add:  []corev1.Capability{"NET_BIND_SERVICE"},
			},
			RunAsUser: &user,
		},
		LivenessProbe:  getLivenessProbe(),
		ReadinessProbe: getReadinessProbe(),
		Lifecycle: &corev1.Lifecycle{
			PreStop: &corev1.LifecycleHandler{
				Exec: &corev1.ExecAction{
					Command: []string{"/wait-shutdown"},
				},
			},
		},
	}}
}

func getDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	var terminationGracePeriod int64 = 30
	var nodeSelector = map[string]string{}

	if cr.Spec.Target == common.KIND {
		// See Kind.yaml for how this is added to one of the nodes.
		// The Ingress service will be bound to a nodeport on the machine
		// with the following node label
		nodeSelector["ingress-ready"] = "true"
		// Only run a single instance to simplify debugging ingress
		Replicas = 1
	}

	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetNginxIngressDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetNginxIngressDeploymentName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:                    getContainers(cr),
				ServiceAccountName:            GetNginxIngressDeploymentName(),
				TerminationGracePeriodSeconds: &terminationGracePeriod,
				NodeSelector:                  nodeSelector,
				Affinity: common.WithPodAntiAffinity(metav1.LabelSelector{
					MatchLabels: GetNginxIngressDeploymentSelector(),
				}, nil),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetNginxIngressDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetNginxIngressDeploymentName(),
	}
}

func DeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.NginxIngress.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.NginxIngress.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.NginxIngress.Components.Deployment.Labels,
	)

	return nil
}
