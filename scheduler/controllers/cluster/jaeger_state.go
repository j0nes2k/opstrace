package cluster

import (
	"context"

	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/jaegerOperator"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type JaegerState struct {
	// Track deployment readiness
	OperatorDeployment *appsv1.Deployment
}

func NewJaegerState() *JaegerState {
	return &JaegerState{}
}

func (i *JaegerState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readDeployment(ctx, cr, client)

	return err
}

func (i *JaegerState) readDeployment(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := jaegerOperator.DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OperatorDeployment = currentState.DeepCopy()
	return nil
}
