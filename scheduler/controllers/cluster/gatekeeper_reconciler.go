package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/gatekeeper"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type GatekeeperReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewGatekeeperReconciler(teardown bool, logger logr.Logger) *GatekeeperReconciler {
	return &GatekeeperReconciler{
		Teardown: teardown,
		Log:      logger.WithName("gatekeeper"),
	}
}

func (i *GatekeeperReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getCookieSecretDesiredState(cr))
	desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
	desired = desired.AddActions(i.getRBACDesiredState(cr))
	desired = desired.AddAction(i.getServiceDesiredState(cr))
	desired = desired.AddAction(i.getDeploymentDesiredState(cr, state))
	desired = desired.AddAction(i.getIngressDesiredState(cr))
	desired = desired.AddActions(i.getReadiness(state))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))

	return desired
}

func (i *GatekeeperReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.Gatekeeper.Deployment != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Gatekeeper.Deployment,
					Msg: "check gatekeeper deployment is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.Gatekeeper.Deployment,
			Msg: "check gatekeeper deployment readiness",
		},
		common.IngressReadyAction{
			Ref: state.Gatekeeper.Ingress,
			Msg: "check gatekeeper ingress readiness",
		},
	}
}

func (i *GatekeeperReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := gatekeeper.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize gatekeeper rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("gatekeeper %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("gatekeeper %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *GatekeeperReconciler) getCookieSecretDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := gatekeeper.SessionCookie(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "gatekeeper session cookie",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "gatekeeper session cookie",
		Mutator: func() error {
			return gatekeeper.SessionCookieMutator(cr, sa)
		},
	}
}

func (i *GatekeeperReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := gatekeeper.ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "gatekeeper service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "gatekeeper service account",
		Mutator: func() error {
			gatekeeper.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *GatekeeperReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := gatekeeper.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "gatekeeper service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "gatekeeper service",
		Mutator: func() error {
			return gatekeeper.ServiceMutator(cr, sa)
		},
	}
}

func (i *GatekeeperReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster, state *ClusterState) common.Action {
	deploy := gatekeeper.Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "gatekeeper deployment",
		}
	}

	postgresEndpoint, err := state.GetPostgresEndpoint()
	if err != nil {
		return common.LogAction{
			Msg:   "failed to retrieve postgresEndpoint",
			Error: err,
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "gatekeeper deployment",
		Mutator: func() error {
			return gatekeeper.DeploymentMutator(cr, deploy, postgresEndpoint)
		},
	}
}

func (i *GatekeeperReconciler) getIngressDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := gatekeeper.Ingress(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "gatekeeper ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "gatekeeper ingress",
		Mutator: func() error {
			return gatekeeper.IngressMutator(cr, deploy)
		},
	}
}

func (i *GatekeeperReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := gatekeeper.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "gatekeeper servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "gatekeeper servicemonitor",
		Mutator: func() error {
			return gatekeeper.ServiceMonitorMutator(cr, monitor)
		},
	}
}
