package errortrackingapi

import (
	"fmt"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	netv1 "k8s.io/api/networking/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getIngressLabels(cr *v1alpha1.Cluster) map[string]string {
	return map[string]string{}
}

func getIngressAnnotations(
	existing map[string]string,
	cr *v1alpha1.Cluster,
) map[string]string {
	gatekeeperURL := fmt.Sprintf("http://gatekeeper.%s.svc.cluster.local:3001/v1/error_tracking/auth", cr.Namespace())

	return common.MergeMap(existing, map[string]string{
		"kubernetes.io/ingress.class":                         "nginx",
		"nginx.ingress.kubernetes.io/mergeable-ingress-type":  "minion",
		"nginx.ingress.kubernetes.io/client-body-buffer-size": "1m",
		// Forward Basic Auth requests to gatekeeper
		"nginx.ingress.kubernetes.io/auth-url": gatekeeperURL,
		// Send the original request in the X-Original-Url header
		"nginx.ingress.kubernetes.io/proxy-add-original-uri-header": "true",
		// Cache the auth response to avoid querying the auth endpoint too
		// frequently.
		// TODO: consider adding $request_uri in the auth cache key in case the
		// public key does not account for the project id"
		"nginx.ingress.kubernetes.io/auth-cache-key": "$remote_user",
		// 401: If something fails auth then it should try again a few seconds later to avoid overloading the service
		// 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
		// 50x: If Gatekeeper is returning 500 errors, allow them to clear up quickly when Gatekeeper comes back,
		//      but allow some caching to avoid the Gatekeep getting hammered with retries when down.
		"nginx.ingress.kubernetes.io/auth-cache-duration": "401 10s, 200 202 2m, 500 503 30s",
		// errortracking-api now defaults to "/" as the base path, so keeping current usage to be
		// backward compatible, we rewrite URLs temporarily. When we get rid of per-cluster deployments,
		// this code path gets deleted altogether
		"nginx.ingress.kubernetes.io/rewrite-target": "/$2",
	})
}

func getIngressSpec(cr *v1alpha1.Cluster) netv1.IngressSpec {
	pathType := netv1.PathTypePrefix

	return netv1.IngressSpec{
		// TLS section is commented on purpose because the master ingress
		// defined in the gatekeeper ingress handles tls for the host.
		// TLS:
		Rules: []netv1.IngressRule{
			{
				// Route all the requests to ${domain}/errortracking to the
				// error tracking API. Authorization is handled by gatekeeper
				// using the nginx ingress annotations set above.
				Host: cr.Spec.GetHost(),
				IngressRuleValue: netv1.IngressRuleValue{
					HTTP: &netv1.HTTPIngressRuleValue{
						Paths: []netv1.HTTPIngressPath{
							{
								Path:     "/errortracking/api/v1(/|$)(.*)",
								PathType: &pathType,
								Backend: netv1.IngressBackend{
									Service: &netv1.IngressServiceBackend{
										Name: GetErrorTrackingAPIDeploymentName(),
										Port: netv1.ServiceBackendPort{
											Name: "http",
										},
									},
									Resource: nil,
								},
							},
						},
					},
				},
			},
		},
	}
}

func Ingress(cr *v1alpha1.Cluster) *netv1.Ingress {
	return &netv1.Ingress{
		ObjectMeta: v1.ObjectMeta{
			Name:        GetErrorTrackingAPIDeploymentName(),
			Namespace:   cr.Namespace(),
			Labels:      getIngressLabels(cr),
			Annotations: getIngressAnnotations(nil, cr),
		},
		Spec: getIngressSpec(cr),
	}
}

func IngressMutator(cr *v1alpha1.Cluster, current *netv1.Ingress) error {
	currentSpec := &current.Spec
	spec := getIngressSpec(cr)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Ingress.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getIngressAnnotations(current.Annotations, cr),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Ingress.Annotations,
	)
	current.Labels = common.MergeMap(
		getIngressLabels(cr),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Ingress.Labels,
	)

	return nil
}
func IngressSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetErrorTrackingAPIDeploymentName(),
	}
}
