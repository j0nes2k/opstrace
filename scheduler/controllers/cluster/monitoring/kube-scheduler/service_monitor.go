package kubescheduler

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "github.com/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(),
		},
		Spec: getServiceMonitorSpec(),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Cluster, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec()
	// Apply default overrides
	if err := utils.PatchObject(currentSpec, &spec); err != nil {
		return err
	}
	// Apply CR overrides
	crOverrides := cr.Spec.Overrides.PrometheusOperator.Components.ServiceMonitor.Spec
	if err := utils.PatchObject(currentSpec, &crOverrides); err != nil {
		return err
	}
	current.Spec = *currentSpec
	return nil
}

func getServiceMonitorName() string {
	return monitors.KubeScheduler
}

func getServiceMonitorLabels() map[string]string {
	return map[string]string{
		"tenant":  "system",
		"k8s-app": monitors.KubeScheduler,
	}
}

func getServiceMonitorAnnotations() map[string]string {
	return map[string]string{}
}

func getServiceMonitorSpec() monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"k8s-app": "kube-scheduler",
			},
		},
		JobLabel: "k8s-app",
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{"kube-system"},
		},
	}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:        "http-metrics",
			HonorLabels: true,
			Interval:    "30s",
		},
	}
}
