package clickhouse

import (
	"fmt"
	"net/url"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// GetCredentialsSecretName returns name of secret containing user credentials
func GetCredentialsSecretName() string {
	return constants.ClickHouseSchedulerCredentialsSecretName
}

// Generate random password and don't change it once created
func getCredentialsData(cr *v1alpha1.Cluster) map[string][]byte {
	pass := utils.RandStringRunes(10)
	user := constants.ClickHouseSchedulerUsername
	hostname := fmt.Sprintf("%s.%s.svc.cluster.local", constants.ClickHouseClusterServiceName, cr.Namespace())
	nativeEndpoint := url.URL{
		Host:   fmt.Sprintf("%s:9000", hostname),
		User:   url.UserPassword(user, pass),
		Scheme: "tcp",
	}
	httpEndpoint := url.URL{
		Host:   fmt.Sprintf("%s:8123", hostname),
		User:   url.UserPassword(user, pass),
		Scheme: "http",
	}

	return map[string][]byte{
		constants.ClickHouseCredentialsHTTPEndpointKey:   []byte(httpEndpoint.String()),
		constants.ClickHouseCredentialsNativeEndpointKey: []byte(nativeEndpoint.String()),
		constants.ClickHouseCredentialsPasswordKey:       []byte(pass),
	}
}

func Credentials(cr *v1alpha1.Cluster) *v1.Secret {
	secret := &v1.Secret{}
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      GetCredentialsSecretName(),
		Namespace: cr.Namespace(),
	}

	secret.Data = getCredentialsData(cr)

	return secret
}

// Don't mutate to ensure we don't change the password after
// it's first created
func CredentialsMutator(cr *v1alpha1.Cluster, current *v1.Secret) error {
	return nil
}

func CredentialsSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      constants.ClickHouseSchedulerCredentialsSecretName,
	}
}
