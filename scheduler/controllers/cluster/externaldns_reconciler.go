package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/externalDNS"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ExternalDNSReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewExternalDNSReconciler(teardown bool, logger logr.Logger) *ExternalDNSReconciler {
	return &ExternalDNSReconciler{
		Teardown: teardown,
		Log:      logger.WithName("externaldns"),
	}
}

func (i *ExternalDNSReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	if cr.Spec.UseExternalDNS() {
		desired = desired.AddActions(i.getServiceAccountDesiredState(cr))
		desired = desired.AddActions(i.getRBACDesiredState(cr))
		desired = desired.AddAction(i.getDeploymentDesiredState(cr))
		desired = desired.AddActions(i.getReadiness(state))
	} else {
		i.Log.Info("skipping externaldns setup")
	}

	return desired
}

func (i *ExternalDNSReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.ExternalDNS.Deployment != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.ExternalDNS.Deployment,
					Msg: "check externalnds deployment is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.ExternalDNS.Deployment,
			Msg: "check externaldns deployment readiness",
		},
	}
}

func (i *ExternalDNSReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := externalDNS.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize externaldns rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("externaldns %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("externaldns %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *ExternalDNSReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) []common.Action {
	sa := externalDNS.ExternalDNSServiceAccount(cr)
	if sa == nil { // for providers that do not need a service account, they return a nil here
		return []common.Action{} // nothing to do
	}

	if i.Teardown {
		return []common.Action{
			common.GenericDeleteAction{
				Ref: sa,
				Msg: "externaldns service account",
			},
		}
	}

	return []common.Action{
		common.GenericCreateOrUpdateAction{
			Ref: sa,
			Msg: "externaldns service account",
			Mutator: func() error {
				externalDNS.ExternalDNSServiceAccountMutator(cr, sa)
				return nil
			},
		},
	}
}

func (i *ExternalDNSReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := externalDNS.ExternalDNSDeployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "externaldns deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "externaldns deployment",
		Mutator: func() error {
			return externalDNS.ExternalDNSDeploymentMutator(cr, deploy)
		},
	}
}
