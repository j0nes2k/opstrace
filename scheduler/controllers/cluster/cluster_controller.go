package cluster

import (
	"context"
	"fmt"
	"net/http"
	"reflect"

	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	redis "github.com/spotahome/redis-operator/api/redisfailover/v1"
	ctrl "sigs.k8s.io/controller-runtime"

	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

const (
	finalizerName = "cluster.opstrace.com/finalizer"
)

// +kubebuilder:rbac:groups=opstrace.com,resources=clusters,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=clusters/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=clusters/finalizers,verbs=update
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers;statefulsets;statefulsets/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles;clusterroles;rolebindings;clusterrolebindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors;prometheusrules;prometheuses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=cert-manager.io,resources=certificates;issuers,verbs=get;list;watch;create;update;patch;delete

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileCluster) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Cluster{}).
		Owns(&appsv1.Deployment{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&netv1.Ingress{}).
		Owns(&v1.ConfigMap{}).
		Owns(&v1.Service{}).
		Owns(&v1.Secret{}).
		Owns(&v1.ServiceAccount{}).
		Owns(&monitoring.ServiceMonitor{}).
		Owns(&monitoring.PrometheusRule{}).
		Owns(&monitoring.Prometheus{}).
		Owns(&redis.RedisFailover{}).
		Owns(&clickhousev1alpha1.ClickHouse{}).
		Owns(&certmanager.Certificate{}).
		Owns(&certmanager.Issuer{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileCluster{}

// ReconcileCluster reconciles a Cluster object
type ReconcileCluster struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client    client.Client
	Scheme    *runtime.Scheme
	Transport *http.Transport
	Context   context.Context
	Cancel    context.CancelFunc
	Recorder  record.EventRecorder
	Log       logr.Logger
	Teardown  bool
}

// Reconcile , The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileCluster) Reconcile(ctx context.Context, request reconcile.Request) (ctrl.Result, error) {
	cluster := &opstracev1alpha1.Cluster{}
	err := r.Client.Get(r.Context, request.NamespacedName, cluster)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			r.Log.Info("Cluster has been removed from API")

			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := cluster.DeepCopy()

	// Update ControllerState for other controllers to consume (i.e. namespace controller)
	config.Get().SetClusterSpec(*cr)

	if err := r.setTeardownFinalizer(ctx, cr); err != nil {
		return reconcile.Result{}, err
	}

	// Read current state
	currentState := NewClusterState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err, request)
		return reconcile.Result{}, err
	}

	desiredState := r.getDesiredState(currentState, cr)

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err, request)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	// when we're not tearing down everything, lets build up internal config for later use
	if !r.Teardown {
		// Update clickhouse credentials for other controllers to consume
		clickHouseEndpoints, err := currentState.ClickHouse.GetEndpoints()
		if err != nil {
			r.Log.Error(err, "error retrieving and storing clickhouse endpoints in controller config")
			r.manageError(cr, err, request)
			return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
		}
		config.Get().SetClickHouseEndpoints(clickHouseEndpoints)

		// Update postgres endpoint for other controllers to consume
		postgresEndpoint, err := currentState.GetPostgresEndpoint()
		if err != nil {
			r.Log.Error(err, "error retrieving and storing postgres endpoint in controller config")
			r.manageError(cr, err, request)
			return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
		}
		config.Get().SetPostgresEndpoint(postgresEndpoint)
	}

	if r.Teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)

			if err := r.Client.Update(ctx, cr); err != nil {
				return reconcile.Result{}, err
			}
		}
		return reconcile.Result{}, nil
	} else {
		err = r.manageSuccess(cr, request)

		return reconcile.Result{}, err
	}
}

func (r *ReconcileCluster) setTeardownFinalizer(ctx context.Context, cr *opstracev1alpha1.Cluster) error {
	r.Teardown = false
	// examine DeletionTimestamp to determine if object is under deletion
	if cr.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,
		// then lets add the finalizer and update the object. This is equivalent
		// registering our finalizer.
		if !controllerutil.ContainsFinalizer(cr, finalizerName) {
			controllerutil.AddFinalizer(cr, finalizerName)

			if err := r.Client.Update(ctx, cr); err != nil {
				return fmt.Errorf("finalizer update: %w", err)
			}
		}
	} else {
		r.Teardown = true
	}
	return nil
}

func (r *ReconcileCluster) getDesiredState(currentState *ClusterState, cr *opstracev1alpha1.Cluster) common.DesiredState {
	desiredState := common.DesiredState{}
	if r.Teardown {
		// Get the actions required to reach the desired state
		// The order of process matters here. Since we're tearing down,
		// we want to do so in the opposite order (in general) than we
		// do when we're creating/updating.
		prometheus := NewPrometheusReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, prometheus.Reconcile(currentState, cr)...)

		monitoring := NewMonitoringReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, monitoring.Reconcile(currentState, cr)...)

		nginxIngress := NewNginxIngressReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, nginxIngress.Reconcile(currentState, cr)...)

		certManager := NewCertManagerReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, certManager.Reconcile(currentState, cr)...)

		errorTracking := NewErrorTrackingAPIReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, errorTracking.Reconcile(currentState, cr)...)

		gatekeeper := NewGatekeeperReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, gatekeeper.Reconcile(currentState, cr)...)

		redis := NewRedisReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, redis.Reconcile(currentState, cr)...)

		ch := NewClickHouseReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, ch.Reconcile(currentState, cr)...)

		chOperator := NewClickHouseOperatorReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, chOperator.Reconcile(currentState, cr)...)

		jaegerOperator := NewJaegerOperatorReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, jaegerOperator.Reconcile(currentState, cr)...)

		prometheusOperator := NewPrometheusOperatorReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, prometheusOperator.Reconcile(currentState, cr)...)

		storage := NewStorageReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, storage.Reconcile(cr)...)

		externalDNS := NewExternalDNSReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, externalDNS.Reconcile(currentState, cr)...)
	} else {
		// Get the actions required to reach the desired state
		// The order of process matters here. In general, we should first update
		// the lowest order dependencies and then work our way up the stack.
		//
		// Reconciliation will block (and retry) until each action returns successfully.
		// This means that readiness check actions will return errors
		// until each readiness check passes, which blocks downstream actions until the readiness
		// check passes. So in the following case, ClickHouse cluster
		// won't we upgraded/reconciled until the ClickHouse operator has been reconciled
		// and passes the readiness check.
		chOperator := NewClickHouseOperatorReconciler(r.Teardown, r.Log)
		desiredState = chOperator.Reconcile(currentState, cr)

		storage := NewStorageReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, storage.Reconcile(cr)...)

		ch := NewClickHouseReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, ch.Reconcile(currentState, cr)...)

		redis := NewRedisReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, redis.Reconcile(currentState, cr)...)

		// Update other system dependencies
		certManager := NewCertManagerReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, certManager.Reconcile(currentState, cr)...)

		nginxIngress := NewNginxIngressReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, nginxIngress.Reconcile(currentState, cr)...)

		jaegerOperator := NewJaegerOperatorReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, jaegerOperator.Reconcile(currentState, cr)...)

		prometheusOperator := NewPrometheusOperatorReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, prometheusOperator.Reconcile(currentState, cr)...)

		externalDNS := NewExternalDNSReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, externalDNS.Reconcile(currentState, cr)...)

		gatekeeper := NewGatekeeperReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, gatekeeper.Reconcile(currentState, cr)...)

		errorTracking := NewErrorTrackingAPIReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, errorTracking.Reconcile(currentState, cr)...)

		monitoring := NewMonitoringReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, monitoring.Reconcile(currentState, cr)...)

		prometheus := NewPrometheusReconciler(r.Teardown, r.Log)
		desiredState = append(desiredState, prometheus.Reconcile(currentState, cr)...)
	}

	return desiredState
}

// Handle success case
func (r *ReconcileCluster) manageSuccess(cluster *opstracev1alpha1.Cluster, request reconcile.Request) error {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cluster.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cluster.Status.Conditions, condition)

	instance := &opstracev1alpha1.Cluster{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		return err
	}

	r.Log.Info("cluster successfully reconciled", "cluster", cluster.Name)
	if !reflect.DeepEqual(cluster.Status, instance.Status) {
		err := r.Client.Status().Update(r.Context, cluster)
		if err != nil {
			// Ignore conclicts. Resource might just be outdated.
			if k8serrors.IsConflict(err) {
				return nil
			}
			r.Log.Error(err, "error updating cluster status")
		}
	}
	return nil
}

// Handle error case: update cluster with error message and status
func (r *ReconcileCluster) manageError(cluster *opstracev1alpha1.Cluster, issue error, request reconcile.Request) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cluster.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cluster.Status.Conditions, condition)

	instance := &opstracev1alpha1.Cluster{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		r.Log.Error(err, "error retrieving latest instance of cluster")
	}

	if !reflect.DeepEqual(cluster.Status, instance.Status) {
		err := r.Client.Status().Update(r.Context, cluster)
		if err != nil {
			// Ignore conclicts. Resource might just be outdated.
			if k8serrors.IsConflict(err) {
				return
			}
			r.Log.Error(err, "error updating cluster status")
		}
	}
}
