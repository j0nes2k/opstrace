package cluster

import (
	"context"
	"fmt"

	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/redis"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/redisOperator"
	v1redis "github.com/spotahome/redis-operator/api/redisfailover/v1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type RedisState struct {
	// Keep track of Credentials so we can pass them to our Redis client
	Credentials *v1.Secret
	// Track deployment readiness
	Operator *appsv1.Deployment
	// Track Redis CR
	Redis *v1redis.RedisFailover
	// Created by Redis operator, read to track readiness
	Sentinel *appsv1.Deployment
	// Created by Redis operator, read to track readiness
	Replicas *appsv1.StatefulSet
}

func NewRedisState() *RedisState {
	return &RedisState{}
}

func (i *RedisState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readCredentials(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readOperator(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readRedis(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readSentinel(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readReplicas(ctx, cr, client)
	if err != nil {
		return err
	}

	return nil
}

// Get Redis password
func (i *RedisState) GetPassword() *string {
	if i.Credentials == nil {
		return nil
	}
	pass := string(i.Credentials.Data["password"])
	return &pass
}

func (i *RedisState) readCredentials(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &v1.Secret{}
	selector := redis.CredentialsSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Credentials = currentState.DeepCopy()
	return nil
}

func (i *RedisState) readOperator(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := redisOperator.DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Operator = currentState.DeepCopy()
	return nil
}

func (i *RedisState) readRedis(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &v1redis.RedisFailover{}
	selector := redis.RedisSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Redis = currentState.DeepCopy()
	return nil
}

func (i *RedisState) readSentinel(ctx context.Context, cr *v1alpha1.Cluster, c client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      fmt.Sprintf("rfs-%s", constants.RedisName),
	}
	err := c.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Sentinel = currentState.DeepCopy()
	return nil
}

func (i *RedisState) readReplicas(ctx context.Context, cr *v1alpha1.Cluster, c client.Client) error {
	currentState := &appsv1.StatefulSet{}
	selector := client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      fmt.Sprintf("rfr-%s", constants.RedisName),
	}
	err := c.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Replicas = currentState.DeepCopy()
	return nil
}
