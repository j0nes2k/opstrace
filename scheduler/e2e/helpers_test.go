package e2e

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	schedulerv1alpha1 "github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
)

const (
	// OS env var constants that we expect
	gitLabOauthClientIDEnvironmentVar = "TEST_GITLAB_OAUTH_CLIENT_ID"
	// #nosec
	gitLabOauthClientSecretEnvironmentVar = "TEST_GITLAB_OAUTH_CLIENT_SECRET"
)

// wait for the cluster to have a successful ready condition
func expectClusterReady(cluster *schedulerv1alpha1.Cluster) {
	By("check cluster CR has ready condition True")
	Eventually(func(g Gomega) {
		cr := &schedulerv1alpha1.Cluster{}
		g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(cluster), cr)).To(Succeed())

		g.Expect(
			meta.IsStatusConditionTrue(cr.Status.Conditions, common.ConditionTypeReady)).
			To(BeTrue())
	}).WithTimeout(time.Minute * 20).Should(Succeed())
}

// wait for GitLabNamespace resource to have successful ready condition
func expectGitLabNamespaceReady(ns *schedulerv1alpha1.GitLabNamespace) {
	By("check GitLabNamespace has ready condition True")
	Eventually(func(g Gomega) {
		cr := &schedulerv1alpha1.GitLabNamespace{}
		g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(ns), cr)).To(Succeed())

		g.Expect(
			meta.IsStatusConditionTrue(cr.Status.Conditions, common.ConditionTypeReady)).
			To(BeTrue())
	}).Should(Succeed())
}

func expectNamespacePodsReady(namespace string) {
	By("check all pods are ready in namespace " + namespace)
	Eventually(func(g Gomega) {
		pods := &corev1.PodList{}
		g.Expect(k8sClient.List(ctx, pods, client.InNamespace(namespace))).To(Succeed())
		for _, p := range pods.Items {
			ready := false
			for _, c := range p.Status.Conditions {
				if c.Type == corev1.PodReady && c.Status == corev1.ConditionTrue {
					ready = true
					break
				}
			}
			g.Expect(ready).To(BeTrue(), "all pods should be ready")
		}
	}).Should(Succeed())
}

// wait for all pods to be in a ready state
func expectClusterPodsReady() {
	expectNamespacePodsReady("default")
}

func loadYaml(path string, out interface{}) {
	f, err := filepath.Abs(path)
	Expect(err).NotTo(HaveOccurred())
	secretBytes, err := ioutil.ReadFile(f)
	Expect(err).NotTo(HaveOccurred())

	Expect(yaml.Unmarshal(secretBytes, out)).To(Succeed())
}

func createOrUpdate(obj client.Object) {
	cp, ok := obj.DeepCopyObject().(client.Object)
	Expect(ok).To(BeTrue(), "runtime object should support client.Object")
	err := defaultClient.Get(ctx, client.ObjectKeyFromObject(cp), cp)
	Expect(client.IgnoreNotFound(err)).To(Succeed())
	if err == nil {
		obj.SetResourceVersion(cp.GetResourceVersion())
		Expect(defaultClient.Update(ctx, obj)).To(Succeed())
	} else {
		Expect(defaultClient.Create(ctx, obj)).To(Succeed())
	}
}

func requireEnvVar(name string) string {
	e := os.Getenv(name)
	if e == "" {
		AbortSuite(fmt.Sprintf("Requires %s to be set", name))
	}
	return e
}

// Load config example cluster before all specs.
// Requires GitLab OAuth env vars to already be set.
func beforeAllLoadCluster(out *schedulerv1alpha1.Cluster) {
	BeforeAll(func() {
		By("load auth secret example")
		authSecret := &corev1.Secret{}

		loadYaml("../config/examples/AuthSecret.yaml", authSecret)
		authSecret.StringData[constants.AuthSecretOAuthClientIDKey] = requireEnvVar(gitLabOauthClientIDEnvironmentVar)
		authSecret.StringData[constants.AuthSecretOAuthClientSecretKey] = requireEnvVar(gitLabOauthClientSecretEnvironmentVar)

		createOrUpdate(authSecret)

		By("load cluster CR")
		loadYaml("../config/examples/Cluster.yaml", out)
		createOrUpdate(out)
	})
}

func beforeAllLoadNamespace(out *schedulerv1alpha1.GitLabNamespace) {
	BeforeAll(func() {
		By("load GitLabNamespace config example")
		loadYaml("../config/examples/OpstraceNamespace.yaml", out)
		createOrUpdate(out)
	})
}

// get the domain of the deployed Observability endpoint.
// Local testing uses localhost via kind config.
func testDomain() string {
	d := os.Getenv("TEST_DOMAIN")
	if d == "" {
		return "localhost"
	}
	return d
}

// get scheme used for the deployed Observability endpoint.
func testScheme() string {
	s := os.Getenv("TEST_SCHEME")
	if s == "" {
		return "http"
	}
	return s
}
