package e2e

import (
	"context"
	"encoding/hex"
	"fmt"
	"net/url"
	"time"

	"github.com/anthhub/forwarder"
	"github.com/gogo/protobuf/types"
	jaegerapiv3 "github.com/jaegertracing/jaeger/proto-gen/api_v3"
	tracev1 "github.com/jaegertracing/jaeger/proto-gen/otel/trace/v1"
	. "github.com/onsi/ginkgo/v2"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"sigs.k8s.io/controller-runtime/pkg/client"

	. "github.com/onsi/gomega"
	"github.com/opstrace/opstrace/go/pkg/argusapi"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/go/pkg/testutils"
	schedulerv1alpha1 "github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
)

var _ = Describe("Tracing", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	namespace := &schedulerv1alpha1.GitLabNamespace{}
	var namespaceID int
	// api key for the wrong org
	var adminOrgAPIKey string
	// api key for the correct org
	var apiKey string
	// servicename for jaeger compatible query
	const serviceName = "test_otlp_tracing_api"
	// tracer configured with serviceName to create spans
	var tracer trace.Tracer

	// send a test span to the otel trace http endpoint
	sendSpan := func(groupID int, apiKey string, span trace.Span) error {
		opts := []otlptracehttp.Option{
			otlptracehttp.WithEndpoint(testDomain()),
			otlptracehttp.WithURLPath(fmt.Sprintf("/v1/traces/%d", groupID)),
			otlptracehttp.WithHeaders(
				map[string]string{
					"Authorization": fmt.Sprintf("Bearer %s", apiKey),
				},
			),
		}
		if testScheme() == "http" {
			opts = append(opts, otlptracehttp.WithInsecure())
		}
		exporter, err := otlptracehttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())
		defer func() { Expect(exporter.Shutdown(ctx)).To(Succeed()) }()
		spans := []sdktrace.ReadOnlySpan{readSpan(span)}
		return exporter.ExportSpans(ctx, spans)
	}

	// gets just one span from the tracer.
	// returns span context and span.
	getSingleSpan := func() (context.Context, trace.Span) {
		spanLabel := getRandomSpanLabel()
		// provide a unique name for lookup convenience
		operationName := "test_" + spanLabel
		spanCtx, span := tracer.Start(ctx, operationName)
		span.SetAttributes(attribute.String("testing", spanLabel))
		span.AddEvent("emitting test span")
		return spanCtx, span
	}

	// verify a generated span against one from the jaeger grpc API.
	verifySpan := func(a sdktrace.ReadOnlySpan, b *tracev1.Span) {
		By("verify span properties")
		tid := a.SpanContext().TraceID()
		Expect(tid[:]).To(BeEquivalentTo(b.TraceId))
		Expect(a.Name()).To(Equal(b.Name))

		attr := map[string]string{}
		for _, kv := range b.Attributes {
			attr[kv.Key] = kv.Value.GetStringValue()
		}
		for _, kv := range a.Attributes() {
			Expect(attr).To(HaveKeyWithValue(string(kv.Key), kv.Value.AsString()))
		}

		type eventKey struct {
			name string
			key  string
		}
		events := map[eventKey]string{}

		for _, e := range b.Events {
			for _, kv := range e.Attributes {
				events[eventKey{e.Name, kv.Key}] = kv.Value.GetStringValue()
			}
		}
		for _, e := range a.Events() {
			for _, kv := range e.Attributes {
				Expect(events).To(HaveKeyWithValue(eventKey{e.Name, string(kv.Key)}, kv.Value.AsString()))
			}
		}

		Expect(a.StartTime().UnixNano()).To(BeEquivalentTo(b.StartTimeUnixNano))
		Expect(a.EndTime().UnixNano()).To(BeEquivalentTo(b.EndTimeUnixNano))
	}

	// jaeger client opens grpc connection through proxy.
	// returns client and closer function.
	jaegerClient := func() (jaegerapiv3.QueryServiceClient, func()) {
		ports, close := testutils.PortForward(ctx, restConfig, &forwarder.Option{
			Namespace:   namespace.Namespace(),
			ServiceName: fmt.Sprintf("jaeger-%d-query", namespace.Spec.ID),
			RemotePort:  16685,
		}, Default)

		conn, err := grpc.DialContext(ctx, fmt.Sprintf("localhost:%d", ports[0].Local),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		Expect(err).NotTo(HaveOccurred())

		closer := func() {
			defer close()
			defer conn.Close()
		}
		return jaegerapiv3.NewQueryServiceClient(conn), closer
	}

	// use jaeger GRPC API to find the span and verify its properties.
	// returns async assertion that must be checked from the caller.
	queryForSpan := func(span trace.Span, matcher OmegaMatcher) {
		client, close := jaegerClient()
		defer close()

		rs := readSpan(span)
		attr := map[string]string{}
		for _, kv := range rs.Attributes() {
			attr[string(kv.Key)] = kv.Value.AsString()
		}
		query := &jaegerapiv3.TraceQueryParameters{
			ServiceName:   serviceName,
			OperationName: rs.Name(),
			Attributes:    attr,
			StartTimeMin: &types.Timestamp{
				Seconds: rs.StartTime().Unix() - 1,
			},
			StartTimeMax: &types.Timestamp{
				Seconds: rs.EndTime().Unix() + 1,
			},
			NumTraces: 1,
		}
		// wait for up to ~60s for span to appear in queries.
		// There should normally be up to a 5s delay for the next batch from the collector to Jaeger,
		// plus another <5s or so for the trace to appear in queries, but waiting longer should avoid flakes.
		Eventually(func(g Gomega) {
			rcv, err := client.FindTraces(ctx, &jaegerapiv3.FindTracesRequest{
				Query: query,
			})
			g.Expect(err).NotTo(HaveOccurred())
			chunk, err := rcv.Recv()
			g.Expect(err).NotTo(HaveOccurred())
			g.Expect(chunk.ResourceSpans).To(HaveLen(1))
			r1 := chunk.ResourceSpans[0]
			g.Expect(r1.InstrumentationLibrarySpans).To(HaveLen(1))
			s1 := r1.InstrumentationLibrarySpans[0]
			g.Expect(s1.Spans).To(HaveLen(1))

			verifySpan(rs, s1.Spans[0])
		}).WithTimeout(time.Minute).Should(matcher)
	}

	beforeAllLoadCluster(cluster)
	beforeAllLoadNamespace(namespace)

	BeforeAll(func() {
		expectClusterReady(cluster)
		expectGitLabNamespaceReady(namespace)
		namespaceID = int(namespace.Spec.ID)

		By("set up a shared tracer for our tests")
		r, err := resource.Merge(
			resource.Default(),
			resource.NewWithAttributes(
				"",
				semconv.ServiceNameKey.String(serviceName),
				semconv.ServiceVersionKey.String("v0.1.0"),
				attribute.String("environment", "e2e"),
			))
		Expect(err).NotTo(HaveOccurred())
		tp := sdktrace.NewTracerProvider(sdktrace.WithResource(r))
		tracer = tp.Tracer("otel_tracer")

		By("creating argus API token for tracing requests")
		argusCreds := &corev1.Secret{}
		Expect(k8sClient.Get(ctx, client.ObjectKey{
			Namespace: namespace.Namespace(),
			Name:      constants.ArgusAdminSecretName,
		}, argusCreds)).Should(Succeed())

		adminUser := argusCreds.Data[constants.ArgusAdminUserEnvVar]
		adminPass := argusCreds.Data[constants.ArgusAdminPasswordEnvVar]

		ports, close := testutils.PortForward(ctx, restConfig, &forwarder.Option{
			Namespace:   namespace.Namespace(),
			ServiceName: constants.ArgusServiceName,
			RemotePort:  constants.ArgusHTTPPort,
		}, Default)
		defer close()
		client, err := argusapi.New(fmt.Sprintf("http://localhost:%d", ports[0].Local),
			argusapi.Config{
				BasicAuth: url.UserPassword(string(adminUser), string(adminPass)),
			})
		Expect(err).NotTo(HaveOccurred())

		// NOTE(joe): using Argus org 1 as incorrect entry org may become obsolete.
		// In this case we would want to create another GitLabNamespace
		// and test API keys from there to make sure they are not valid here.
		Expect(client.UserSwitchContext(1)).To(Succeed())
		key, err := client.CreateAPIKey(argusapi.CreateAPIKeyRequest{
			Name: common.RandStringRunes(10),
			Role: "Viewer",
		})
		Expect(err).NotTo(HaveOccurred())
		adminOrgAPIKey = key.Key

		// switch to GitLab namespace org and create an api key
		Expect(client.UserSwitchContext(int(namespace.Spec.ID))).To(Succeed())
		key, err = client.CreateAPIKey(argusapi.CreateAPIKeyRequest{
			Name: common.RandStringRunes(10),
			Role: "Viewer",
		})
		Expect(err).NotTo(HaveOccurred())
		apiKey = key.Key
	})

	Context("Auth", func() {
		It("should not accept an empty api key", func() {
			_, span := getSingleSpan()
			span.End()
			Expect(sendSpan(namespaceID, "", span)).NotTo(Succeed())
			queryForSpan(span, Not(Succeed()))
		})

		It("should not accept an invalid api key", func() {
			_, span := getSingleSpan()
			span.End()
			Expect(sendSpan(namespaceID, "bad"+apiKey, span)).NotTo(Succeed())
			queryForSpan(span, Not(Succeed()))
		})

		It("should not accept a different org api key", func() {
			_, span := getSingleSpan()
			span.End()
			Expect(sendSpan(namespaceID, adminOrgAPIKey, span)).NotTo(Succeed())
			queryForSpan(span, Not(Succeed()))
		})
	})

	Context("Spans", func() {
		It("can save and retrieve single span", func() {
			_, span := getSingleSpan()
			span.End()
			Expect(sendSpan(namespaceID, apiKey, span)).To(Succeed())
			queryForSpan(span, Succeed())
		})

		It("can save and retrieve fragmented spans", func() {
			By("create chain of nested spans")
			rootContext, rootSpan := getSingleSpan()
			rootSpan.SetAttributes(attribute.String("fragment", "root"))
			rootSpan.AddEvent("emitting root test span")

			childContext, childSpan := tracer.Start(rootContext, "test_"+getRandomSpanLabel())
			childSpan.SetAttributes(attribute.String("fragment", "child"))
			childSpan.AddEvent("emitting child test span")

			_, grandchildSpan := tracer.Start(childContext, "test_"+getRandomSpanLabel())
			grandchildSpan.SetAttributes(attribute.String("fragment", "grandchild"))
			grandchildSpan.AddEvent("emitting grandchild test span")

			grandchildSpan.End()
			childSpan.End()
			rootSpan.End()

			getTraceID := func(span trace.Span) trace.TraceID {
				return readSpan(span).SpanContext().TraceID()
			}

			traceID := getTraceID(rootSpan)
			// ensure all the trace ids are identical
			Expect(
				[]trace.TraceID{getTraceID(rootSpan), getTraceID(childSpan), getTraceID(grandchildSpan)}).
				Should(HaveEach(traceID))

			By("sending spans separately")
			Expect(sendSpan(namespaceID, apiKey, rootSpan)).To(Succeed())
			Expect(sendSpan(namespaceID, apiKey, childSpan)).To(Succeed())
			Expect(sendSpan(namespaceID, apiKey, grandchildSpan)).To(Succeed())

			client, close := jaegerClient()
			defer close()

			By("query by trace id")
			var resourceSpans []*tracev1.ResourceSpans
			// allow 1m for spans to appear
			Eventually(func(g Gomega) {
				rcv, err := client.GetTrace(ctx, &jaegerapiv3.GetTraceRequest{
					TraceId: traceID.String(),
				})
				g.Expect(err).NotTo(HaveOccurred())
				chunk, err := rcv.Recv()
				g.Expect(err).NotTo(HaveOccurred())
				g.Expect(chunk.ResourceSpans).To(HaveLen(3))
				resourceSpans = chunk.ResourceSpans
			}).WithTimeout(time.Minute).Should(Succeed())

			By("check returned spans match ours and have correct parent references")
			spans := map[string]*tracev1.Span{}
			for _, s := range resourceSpans {
				Expect(s.InstrumentationLibrarySpans).To(HaveLen(1))
				Expect(s.InstrumentationLibrarySpans[0].Spans).To(HaveLen(1))
				span := s.InstrumentationLibrarySpans[0].Spans[0]
				spans[hex.EncodeToString(span.SpanId)] = span
			}

			// expected span chain to verify parent id references
			spanChain := []sdktrace.ReadOnlySpan{readSpan(rootSpan), readSpan(childSpan), readSpan(grandchildSpan)}
			for i, s := range spanChain {
				sID := s.SpanContext().SpanID().String()
				gotSpan, ok := spans[sID]
				Expect(ok).To(BeTrue(), "span map lookup")
				if i > 0 {
					parent := spanChain[i-1]
					Expect(hex.EncodeToString(gotSpan.ParentSpanId)).To(Equal(parent.SpanContext().SpanID().String()))
				}
				verifySpan(s, gotSpan)
			}
		})
	})

})

func readSpan(span trace.Span) sdktrace.ReadOnlySpan {
	ro, ok := span.(sdktrace.ReadOnlySpan)
	Expect(ok).To(BeTrue(), "convert to readonly span")
	return ro
}

func getRandomSpanLabel() string {
	return common.RandStringRunes(10)
}
