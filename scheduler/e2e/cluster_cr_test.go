package e2e

import (
	. "github.com/onsi/ginkgo/v2"

	schedulerv1alpha1 "github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

var _ = Describe("Cluster CR", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}

	beforeAllLoadCluster(cluster)

	AfterAll(func() {
		// TODO(joe): clean up cluster once we've resolved teardown issues
		// see https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1837
		//Expect(nsClient.Delete(ctx, cluster)).To(Succeed())
	})

	Context("Configs Example cluster", Ordered, func() {
		AfterAll(func() {
			// TODO(joe): don't delete secret until we can properly recreate the cluster.
			// we outrely on the same cluster in other tests.
			//Expect(defaultClient.Delete(ctx, authSecret)).To(Succeed())
		})

		It("can be created successfully", func() {
			expectClusterReady(cluster)

			expectClusterPodsReady()
		})

		PIt("can be deleted successfully", func() {
			// TODO: controller is not correctly deleting resources
			// see https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1793#note_1035380226
		})
	})

	Context("Configs example GitLabNamespace", Ordered, func() {
		namespace := &schedulerv1alpha1.GitLabNamespace{}

		beforeAllLoadNamespace(namespace)

		It("can be created successfully", func() {
			expectGitLabNamespaceReady(namespace)

			expectNamespacePodsReady(namespace.Namespace())
		})

		PIt("can be deleted successfully", func() {
			// TODO: implement when controller cleanup bugs fixed
		})
	})
})
