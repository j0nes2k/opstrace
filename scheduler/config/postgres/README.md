# kubernetes-postgresql
Kubernetes Statefullset manifests for PostgreSQL

These are applied when running opstrace on a KIND cluster (for local development)

See more https://devopscube.com/deploy-postgresql-statefulset/