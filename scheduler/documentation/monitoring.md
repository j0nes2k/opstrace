# Monitoring

### prometheus-operator

The Prometheus operator introduces additional resources in Kubernetes to
declare the desired state of a Prometheus and Alertmanager cluster as
well as Prometheus configuration. The resources it introduces are:

* Prometheus
* Alertmanager
* ServiceMonitor

The Prometheus resource declaratively describes the desired state of a
Prometheus deployment, while a ServiceMonitor describes the set of targets to be monitored by Prometheus.

The Prometheus resource includes a field called `serviceMonitorSelector` which defines a selection of ServiceMonitors to be used.

API Reference: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md

## System


## Users & Tenants