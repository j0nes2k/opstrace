# Scheduler Operator

An operator to provision and manage an Opstrace Cluster and schedule Tenants/Groups, which are then managed by the [Tenant Operator](../tenant-operator/).

## Documentation

* [API docs](./documentation/api.md)

## Supported Custom Resources

The following CRDs are supported:

* [`Cluster`](./api/v1alpha1/cluster_types.go) -> main CRD for configuring Opstrace
* [`GitLabNamespace`](./api/v1alpha1/gitlabnamespace_types.go) -> represents a GitLab namespace


<!-- markdownlint-disable MD044 -->
All custom resources use the api group `opstrace.com` and version `v1alpha1`.
To get a overview of the available scheduler CRDs see [API docs](./documentation/api.md).
