# Shared Makefile variables and targets for Go projects

# SHELLFLAGS compatible with bash and ash (alpine CI images).
# Exit on non-zero errors and undefined variables.
.SHELLFLAGS = -euc
SHELL=/bin/bash

# use buildkit for better multi-platform variable support.
# see https://docs.docker.com/engine/reference/builder/#automatic-platform-args-in-the-global-scope
export DOCKER_BUILDKIT=1

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# Registry address for pushing images to
export DOCKER_IMAGE_REGISTRY ?= registry.gitlab.com/gitlab-org/opstrace/opstrace

ifdef CI_COMMIT_TAG
  # We are doing a release!
  export DOCKER_IMAGE_TAG := ${CI_COMMIT_TAG}
else
  # Ordinary build
  # We need to use here only the most basic tools, as CI uses lots of different
  # containers, that provide different versions of tool, if at all.
  export DOCKER_IMAGE_TAG := $(shell grep -E '^\s*NEXT_RELEASE_TAG:\s*[a-d0-9.-]+\s*$$' $(root_dir)/.gitlab-ci.yml | cut -d: -f 2 | tr -d ' ')
  DOCKER_IMAGE_TAG := $(DOCKER_IMAGE_TAG)-$(shell git rev-parse --short=8 HEAD | tr -d '\n')
  ifneq ($(shell git status --porcelain),)
	DOCKER_IMAGE_TAG := $(DOCKER_IMAGE_TAG).dirty
  endif
endif
# DOCKER_IMAGE_NAME is lazily evaluated while traversing the per-dir Makefiles
export DOCKER_IMAGE = ${DOCKER_IMAGE_REGISTRY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}

export GO_BUILD_LDFLAGS = \
	-X github.com/opstrace/opstrace/go/pkg/constants.DockerImageTag=${DOCKER_IMAGE_TAG} \
	-X github.com/opstrace/opstrace/go/pkg/constants.DockerImageRegistry=${DOCKER_IMAGE_REGISTRY}

.PHONY: print-docker-image-name-tag
print-docker-image-name-tag:
	@echo ${DOCKER_IMAGE}

# load existing images into a kind cluster
.PHONY: kind-load-docker-images
kind-load-docker-images: NAME = opstrace
kind-load-docker-images:
	@# Loading all images at once causes:
	@#
	@# ERROR: failed to load image: command "docker exec --privileged -i opstrace-control-plane ctr --namespace=k8s.io images import --digests --snapshotter=overlayfs -" failed with error: exit status 1
	@# Command Output: ctr: image "registry.gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator:0.1.0-51490690.dirty": already exists
	@#
	@# NOTE(prozlach): kind will load images if they changed even if tag stays
	@# the same: https://github.com/kubernetes-sigs/kind/issues/1155
	@# NOTE(prozlach): even if image can be pulled from external registry, we
	@# we are still loading it into kind here. The justification for that is
	@# that it speeds development up, esp. for people with slower Internet
	@# links - i.e. no need to download the same image multiple times
	@for i in $(shell make -s print-docker-images); do \
		kind load docker-image --name $(NAME) $$i; \
	done

.PHONY: docker-ensure-default
docker-ensure-default:
	@if [[ ! $$(docker pull -q ${DOCKER_IMAGE}) ]]; then \
		make docker-build; \
	fi

.PHONY: lint
lint: ## Run golangci-lint against the project
	golangci-lint run --allow-parallel-runners --timeout 5m

.PHONY: lint-code
lint-code: lint

.PHONY: fmt
fmt: ## Run go fmt against code.
	go fmt ./...

.PHONY: vet
vet: ## Run go vet against code.
	go vet ./...
