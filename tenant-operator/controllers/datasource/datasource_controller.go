/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package datasource

import (
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sort"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers"
	"github.com/opstrace/opstrace/tenant-operator/controllers/config"
	v1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// DatasourceReconciler reconciles a Datasource object.
type DatasourceReconciler struct {
	// This Client, initialized using mgr.Client() above, is a split Client
	// that reads objects from the cache and writes to the apiserver
	Client    client.Client
	Scheme    *runtime.Scheme
	Context   context.Context
	Cancel    context.CancelFunc
	Recorder  record.EventRecorder
	Transport *http.Transport
	Log       logr.Logger
}

const (
	DatasourcesApiVersion = 1
	ControllerName        = "controller_datasource"
)

var _ reconcile.Reconciler = &DatasourceReconciler{}

// +kubebuilder:rbac:groups=opstrace.com,resources=datasources,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=datasources/status,verbs=get;update;patch

// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.7.0/pkg/reconcile
func (r *DatasourceReconciler) Reconcile(ctx context.Context, request ctrl.Request) (ctrl.Result, error) {
	if !config.Get().GetState().ArgusReady {
		r.Log.Info("no argus instance available")
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	// We can't create dashboards for groups that haven't been defined yet,
	// hence we list them and define dashboards that refer to existing groups
	// only.
	argusClient, err := r.getArgusClient()
	if err != nil {
		r.Log.Error(err, "failed to create argus client")
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, err
	}
	argusGroupsList, err := argusClient.Client.Groups()
	if err != nil {
		r.Log.Error(err, "failed to list argus groups currently defined")
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, err
	}
	argusGroupsSet := make(map[int64]struct{}, len(argusGroupsList))
	for _, argusGroup := range argusGroupsList {
		r.Log.V(5).Info("found group defined in argus", "groupID", argusGroup.ID)
		argusGroupsSet[argusGroup.ID] = struct{}{}
	}

	// Read the current state of known and cluster datasources
	currentState := NewDataSourcesState()
	err = currentState.Read(ctx, r.Client, request.Namespace)
	if err != nil {
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, err
	}

	if currentState.KnownDataSources == nil {
		r.Log.Info("no datasources configmap found")
		return reconcile.Result{}, nil
	}

	// Reconcile all data sources
	return r.reconcileDataSources(currentState, argusGroupsSet)
}

func (r *DatasourceReconciler) reconcileDataSources( // nolint:cyclop // requires a bigger refactoring
	state *DataSourcesState,
	argusGroupsSet map[int64]struct{},
) (reconcile.Result, error) {
	dataSourcesToAddOrUpdate := make([]opstracev1alpha1.DataSource, 0, len(state.ClusterDataSources.Items))
	var dataSourcesToDelete []string

	// check if a given datasource (by its key) is found on the cluster
	foundOnCluster := func(key string) bool {
		for _, ds := range state.ClusterDataSources.Items {
			if key == ds.Filename() {
				return true
			}
		}
		return false
	}

	for _, dataSource := range state.ClusterDataSources.Items {
		for _, dataSourceField := range dataSource.Spec.Datasources {
			if _, ok := argusGroupsSet[int64(dataSourceField.GroupId)]; !ok {
				// Let's wait for GroupReconciler to catch up, before we define
				// any datasources. Otherwise Argus will enter crash loop as
				// Datasource pointing to inexistent group will crash Argus
				// during start preventing GroupReconciler from defining the
				// group.
				// FIXME(prozlach): This is just a patch, as IMO the
				// Datasources and Groups should be reconciled together but
				// this would require rearchitecting the tenant operator.
				r.Log.Info(
					"datasource belongs to a group that has not been defined in argus yet",
					"datasourceName", dataSourceField.Name,
					"groupId", dataSourceField.GroupId,
				)
				return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
			} else {
				r.Log.V(5).Info(
					"datasource group OK",
					"datasourceName", dataSourceField.Name,
					"groupId", dataSourceField.GroupId,
				)
			}
		}
		dataSourcesToAddOrUpdate = append(dataSourcesToAddOrUpdate, dataSource)
	}

	// Data sources to delete: if a datasourcedashboard is in the configmap but cannot
	// be found on the cluster then we assume it has been deleted and remove
	// it from the configmap
	for ds := range state.KnownDataSources.Data {
		if !foundOnCluster(ds) {
			dataSourcesToDelete = append(dataSourcesToDelete, ds)
		}
	}

	// apply dataSourcesToDelete
	for _, ds := range dataSourcesToDelete {
		r.Log.Info("deleting datasource", "datasource", ds)
		if state.KnownDataSources.Data != nil {
			delete(state.KnownDataSources.Data, ds)
		}
	}

	// apply dataSourcesToAddOrUpdate
	var updated []opstracev1alpha1.DataSource // nolint
	for i := range dataSourcesToAddOrUpdate {
		pipeline := NewDatasourcePipeline(&dataSourcesToAddOrUpdate[i])
		err := pipeline.ProcessDatasource(state.KnownDataSources)
		if err != nil {
			r.manageError(&dataSourcesToAddOrUpdate[i], err)
			continue
		}
		updated = append(updated, dataSourcesToAddOrUpdate[i])
	}

	// update the hash of the newly reconciled datasources
	hash, err := r.updateHash(state.KnownDataSources)
	if err != nil {
		r.manageError(nil, err)
		return reconcile.Result{}, err
	}

	if state.KnownDataSources.Annotations == nil {
		state.KnownDataSources.Annotations = map[string]string{}
	}

	// Compare the last hash to the previous one, update if changed
	lastHash := state.KnownDataSources.Annotations[constants.LastConfigAnnotation]
	if lastHash != hash {
		state.KnownDataSources.Annotations[constants.LastConfigAnnotation] = hash

		// finally, update the configmap
		err = r.Client.Update(r.Context, state.KnownDataSources)
		if err != nil {
			r.Recorder.Event(state.KnownDataSources, "Warning", "UpdateError", err.Error())
		} else {
			r.manageSuccess(updated)
		}
	}
	return reconcile.Result{}, nil
}

func (i *DatasourceReconciler) updateHash(known *v1.ConfigMap) (string, error) {
	if known == nil || known.Data == nil {
		return "", nil
	}

	// Make sure that we always use the same order when creating the hash
	keys := make([]string, 0, len(known.Data))

	for key := range known.Data {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	hash := sha256.New()
	for _, key := range keys {
		_, err := io.WriteString(hash, key)
		if err != nil {
			return "", err
		}

		_, err = io.WriteString(hash, known.Data[key])
		if err != nil {
			return "", err
		}
	}

	return fmt.Sprintf("%x", hash.Sum(nil)), nil
}

// Handle error case: update datasource with error message and status.
func (r *DatasourceReconciler) manageError(datasource *opstracev1alpha1.DataSource, issue error) {
	r.Recorder.Event(datasource, "Warning", "ProcessingError", issue.Error())

	// datasource deleted
	if datasource == nil {
		return
	}

	datasource.Status.Phase = opstracev1alpha1.PhaseFailing
	datasource.Status.Message = issue.Error()

	err := r.Client.Status().Update(r.Context, datasource)
	if err != nil {
		// Ignore conclicts. Resource might just be outdated.
		if k8serrors.IsConflict(err) {
			return
		}
		r.Log.Error(err, "error updating datasource status")
	}
}

// manage success case: datasource has been imported successfully and the configmap
// is updated.
func (r *DatasourceReconciler) manageSuccess(datasources []opstracev1alpha1.DataSource) {
	for i, datasource := range datasources {
		r.Log.Info("datasource successfully imported",
			"datasource.Namespace", datasource.Namespace,
			"datasource.Name", datasource.Name)

		datasource.Status.Phase = opstracev1alpha1.PhaseReconciling
		datasource.Status.Message = "success"

		err := r.Client.Status().Update(r.Context, &datasources[i])
		if err != nil {
			r.Recorder.Event(&datasources[i], "Warning", "UpdateError", err.Error())
		}
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *DatasourceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	cmHandler := func(o client.Object) []reconcile.Request {
		if o.GetName() != constants.DatasourcesConfigMapName {
			return nil
		}
		ns := o.GetNamespace()
		list := &opstracev1alpha1.DataSourceList{}
		opts := &client.ListOptions{
			Namespace: ns,
		}
		err := r.Client.List(context.Background(), list, opts)
		if err != nil {
			return nil
		}
		requests := make([]reconcile.Request, len(list.Items))
		for i, ds := range list.Items {
			requests[i] = reconcile.Request{NamespacedName: types.NamespacedName{
				Namespace: ns,
				Name:      ds.GetName(),
			}}
		}
		return requests
	}
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.DataSource{}).
		Watches(&source.Kind{Type: &v1.ConfigMap{}}, handler.EnqueueRequestsFromMapFunc(cmHandler)).
		Complete(r)
}

// Get an authenticated grafana API client.
func (r *DatasourceReconciler) getArgusClient() (*common.ArgusClient, error) {
	state := config.Get().GetState()

	u := state.AdminUrl
	if u == "" {
		return nil, controllers.ErrUnableToGetGrafanaURL
	}

	parsedUrl, err := url.Parse(u)
	if err != nil {
		return nil, err
	}
	username := parsedUrl.User.Username()
	password, _ := parsedUrl.User.Password()

	return common.NewArgusClient(u, username, password, r.Transport, 0)
}
