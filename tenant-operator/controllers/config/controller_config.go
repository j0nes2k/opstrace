package config

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type ControllerState struct {
	AdminUrl   string
	ArgusReady bool
}

type ControllerConfig struct {
	mu             *sync.Mutex
	values         map[string]interface{}
	plugins        map[string]v1alpha1.PluginList
	dashboards     []*v1alpha1.DashboardRef
	state          ControllerState
	gatekeeperURL  string
	platformTarget common.EnvironmentTarget
}

var instance *ControllerConfig
var once sync.Once

func Get() *ControllerConfig {
	once.Do(func() {
		instance = &ControllerConfig{
			mu:         &sync.Mutex{},
			values:     map[string]interface{}{},
			plugins:    map[string]v1alpha1.PluginList{},
			dashboards: []*v1alpha1.DashboardRef{},
		}
	})
	return instance
}

// SetGatekeeperURL sets the gatekeeper endpoint for all controllers to consume.
func (c *ControllerConfig) SetGatekeeperURL(gatekeeperURL string) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.gatekeeperURL = gatekeeperURL
}

// GetGatekeeperURL returns the gatekeeper endpoint.
func (c *ControllerConfig) GetGatekeeperURL() string {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.gatekeeperURL
}

// SetPlatformTarget sets the gatekeeper endpoint for all controllers to consume.
func (c *ControllerConfig) SetPlatformTarget(platformTarget common.EnvironmentTarget) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.platformTarget = platformTarget
}

// GetPlatformTarget returns the gatekeeper endpoint.
func (c *ControllerConfig) GetPlatformTarget() common.EnvironmentTarget {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.platformTarget
}

// SetState sets the controller state for all controllers to consume.
func (c *ControllerConfig) SetState(state ControllerState) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.state = state
}

// GetState returns the controller state.
func (c *ControllerConfig) GetState() ControllerState {
	c.mu.Lock()
	defer c.mu.Unlock()
	// shallow copy is fine since its a simple struct
	copy := c.state

	return copy
}

// HydrateKnownDashboards loads known dashboards from a configmap to persist across operator restarts.
func (c *ControllerConfig) HydrateKnownDashboards(cl client.Client, ctx context.Context, namespace string) error {
	c.mu.Lock()
	defer c.mu.Unlock()

	dashboards := []*v1alpha1.DashboardRef{}
	selector := client.ObjectKey{
		Namespace: namespace,
		Name:      constants.InstalledDashboardsConfigMapName,
	}
	var configMap v1.ConfigMap

	err := cl.Get(ctx, selector, &configMap)
	if err != nil {
		if errors.IsNotFound(err) {
			// Nothing to load because dashboards haven't synced yet.
			// ConfigMap is only created first time Dashboards have been synced.
			return nil
		}
		return err
	}
	data := configMap.Data[constants.InstalledDashboardsDataKey]

	err = json.Unmarshal([]byte(data), &dashboards)
	if err != nil {
		return err
	}

	c.dashboards = dashboards
	return nil
}

// PersistKnownDashboards saves known dashboards into a configmap to persist across operator restarts.
func (c *ControllerConfig) PersistKnownDashboards(
	client client.Client,
	ctx context.Context,
	namespace string,
) (controllerutil.OperationResult, error) {
	c.mu.Lock()
	defer c.mu.Unlock()
	configMap := &v1.ConfigMap{}
	configMap.ObjectMeta = metav1.ObjectMeta{
		Name:      constants.InstalledDashboardsConfigMapName,
		Namespace: namespace,
	}
	configMap.Data = map[string]string{}

	bytes, err := json.Marshal(c.dashboards)
	if err != nil {
		return controllerutil.OperationResultNone, err
	}

	return controllerutil.CreateOrUpdate(ctx, client, configMap, func() error {
		configMap.Data[constants.InstalledDashboardsDataKey] = string(bytes)
		return nil
	})
}

func (c *ControllerConfig) GetDashboardId(groupId int64, name string) string {
	return fmt.Sprintf("%d/%s", groupId, name)
}

func (c *ControllerConfig) GetAllPlugins() v1alpha1.PluginList {
	c.mu.Lock()
	defer c.mu.Unlock()

	var plugins v1alpha1.PluginList
	for _, v := range c.plugins {
		plugins = append(plugins, v...)
	}
	return plugins
}

func (c *ControllerConfig) GetPluginsFor(dashboard *v1alpha1.Dashboard) v1alpha1.PluginList {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.plugins[c.GetDashboardId(dashboard.Spec.GroupID, dashboard.Name)]
}

func (c *ControllerConfig) SetPluginsFor(dashboard *v1alpha1.Dashboard) {
	id := c.GetDashboardId(dashboard.Spec.GroupID, dashboard.Name)
	c.mu.Lock()
	defer c.mu.Unlock()
	c.plugins[id] = dashboard.Spec.Plugins
}

func (c *ControllerConfig) RemovePluginsFor(dashboard *v1alpha1.DashboardRef) {
	id := c.GetDashboardId(dashboard.GroupID, dashboard.Name)
	delete(c.plugins, id)
}

func (c *ControllerConfig) AddDashboard(dashboard *v1alpha1.Dashboard, folderId *int64, folderName string) {
	groupId := dashboard.Spec.GroupID

	if i, exists := c.HasDashboard(dashboard.UID(), groupId); !exists {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.dashboards = append(c.dashboards, &v1alpha1.DashboardRef{
			Name:       dashboard.Name,
			GroupID:    groupId,
			UID:        dashboard.UID(),
			Hash:       dashboard.Hash(),
			FolderId:   folderId,
			FolderName: dashboard.Spec.CustomFolderName,
		})
	} else {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.dashboards[i] = &v1alpha1.DashboardRef{
			Name:       dashboard.Name,
			GroupID:    groupId,
			UID:        dashboard.UID(),
			Hash:       dashboard.Hash(),
			FolderId:   folderId,
			FolderName: folderName,
		}
	}
}

func (c *ControllerConfig) HasDashboard(uid string, groupId int64) (int, bool) {
	for i, v := range c.dashboards {
		if v.UID == uid && v.GroupID == groupId {
			return i, true
		}
	}
	return -1, false
}

func (c *ControllerConfig) InvalidateDashboards() {
	c.mu.Lock()
	defer c.mu.Unlock()
	for _, v := range c.dashboards {
		v.Hash = ""
	}
}

func (c *ControllerConfig) RemoveDashboard(uid string, groupId int64) {
	if i, exists := c.HasDashboard(uid, groupId); exists {
		c.mu.Lock()
		defer c.mu.Unlock()
		list := c.dashboards
		list[i] = list[len(list)-1]
		list = list[:len(list)-1]
		c.dashboards = list
	}
}

func (c *ControllerConfig) GetDashboards() []*v1alpha1.DashboardRef {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.dashboards != nil {
		return c.dashboards
	}
	return []*v1alpha1.DashboardRef{}
}

func (c *ControllerConfig) AddConfigItem(key string, value interface{}) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if key != "" && value != nil && value != "" {
		c.values[key] = value
	}
}

func (c *ControllerConfig) RemoveConfigItem(key string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	delete(c.values, key)
}

func (c *ControllerConfig) GetConfigItem(key string, defaultValue interface{}) interface{} {
	if c.HasConfigItem(key) {
		return c.values[key]
	}
	return defaultValue
}

func (c *ControllerConfig) GetConfigString(key, defaultValue string) string {
	if c.HasConfigItem(key) {
		return c.values[key].(string)
	}
	return defaultValue
}

func (c *ControllerConfig) GetConfigBool(key string, defaultValue bool) bool {
	if c.HasConfigItem(key) {
		return c.values[key].(bool)
	}
	return defaultValue
}

func (c *ControllerConfig) GetConfigTimestamp(key string, defaultValue time.Time) time.Time {
	if c.HasConfigItem(key) {
		return c.values[key].(time.Time)
	}
	return defaultValue
}

func (c *ControllerConfig) HasConfigItem(key string) bool {
	c.mu.Lock()
	defer c.mu.Unlock()
	_, ok := c.values[key]
	return ok
}

// RemoveAllConfig will remove all remaining configmaps and clear known Dashboards/Plugins/Datasources.
// Configmaps are used to store known dashboards and known datasources.
func (c *ControllerConfig) RemoveAllConfig(cl client.Client, ctx context.Context, namespace string) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.dashboards = []*v1alpha1.DashboardRef{}
	c.plugins = map[string]v1alpha1.PluginList{}

	// delete all configmaps in this namespace since we only deploy a single Tenant per namespace
	return cl.DeleteAllOf(ctx, &v1.ConfigMap{}, client.InNamespace(namespace))
}
