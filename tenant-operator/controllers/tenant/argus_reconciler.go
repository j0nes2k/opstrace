package tenant

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/config"
	"github.com/opstrace/opstrace/tenant-operator/controllers/tenant/argus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ArgusReconciler struct {
	DsHash     string
	ConfigHash string
	PluginsEnv string
	Teardown   bool
	Plugins    *PluginsHelperImpl
	Log        logr.Logger
}

func NewArgusReconciler(teardown bool, logger logr.Logger) *ArgusReconciler {
	return &ArgusReconciler{
		DsHash:     "",
		ConfigHash: "",
		PluginsEnv: "",
		Teardown:   teardown,
		Plugins:    NewPluginsHelper(logger),
		Log:        logger,
	}
}

func (i *ArgusReconciler) Reconcile(state *ArgusState, cr *v1alpha1.Tenant) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getAdminUserSecretDesiredState(cr))
	desired = desired.AddAction(i.getServiceDesiredState(state, cr))
	desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
	desired = desired.AddAction(i.getConfigDesiredState(cr))
	desired = desired.AddAction(i.getDatasourceConfigDesiredState(cr))
	desired = desired.AddAction(i.getIngressDesiredState(cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))

	// Consolidate plugins
	// No action, will update init container env var
	desired = desired.AddAction(i.getPluginsDesiredState(cr))

	// Reconcile the deployment last because it depends on the configuration
	// and plugins list computed in previous steps
	desired = desired.AddAction(i.getStatefulSetDesiredState(state, cr))

	// Check Deployment and Ingress readiness
	desired = desired.AddActions(i.getReadiness(state))

	return desired
}

func (i *ArgusReconciler) getReadiness(state *ArgusState) []common.Action {
	var actions []common.Action
	if i.Teardown {
		return actions
	}

	actions = append(actions, common.IngressReadyAction{
		Ref: state.ArgusIngress,
		Msg: "check argus ingress readiness",
	})

	return append(actions, common.StatefulSetReadyAction{
		Ref: state.ArgusStatefulSet,
		Msg: "check argus statefulset readiness",
	})
}

func (i *ArgusReconciler) getServiceDesiredState(state *ArgusState, cr *v1alpha1.Tenant) common.Action {
	if state.ArgusService != nil {
		if cr.Status.Argus.PreviousServiceName != "" && state.ArgusService.Name != "" {
			// if the previously known service is not the current service then delete the previous service
			if cr.Status.Argus.PreviousServiceName != state.ArgusService.Name {
				serviceName := cr.Status.Argus.PreviousServiceName
				// reset the status before next loop
				cr.Status.Argus.PreviousServiceName = ""
				return common.GenericDeleteAction{
					Ref: &v1.Service{
						ObjectMeta: metav1.ObjectMeta{
							Name:      serviceName,
							Namespace: cr.Namespace,
						},
					},
					Msg: "obsolete argus service",
				}
			}
		}
		if cr.Status.Argus.PreviousServiceName == "" {
			cr.Status.Argus.PreviousServiceName = state.ArgusService.Name
		}
	}
	svc := argus.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "argus service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "argus service",
		Mutator: func() error {
			return argus.ServiceMutator(cr, svc)
		},
	}
}

func (i *ArgusReconciler) getServiceAccountDesiredState(cr *v1alpha1.Tenant) common.Action {
	sa := argus.ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "argus service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "argus service account",
		Mutator: func() error {
			argus.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *ArgusReconciler) getConfigDesiredState(cr *v1alpha1.Tenant) common.Action {
	config, err := argus.Config(cr)

	if err != nil {
		return common.LogAction{
			Msg:   "failed to serialize argus config",
			Error: err,
		}
	}

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: config,
			Msg: "argus config",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: config,
		Msg: "argus config",
		Mutator: func() error {
			err := argus.ConfigMutator(cr, config)
			// Store the last config hash for the duration of this reconciliation for
			// later usage in the deployment
			i.ConfigHash = config.Annotations[constants.LastConfigAnnotation]
			return err
		},
	}
}

func (i *ArgusReconciler) getDatasourceConfigDesiredState(cr *v1alpha1.Tenant) common.Action {
	ds := argus.DatasourcesConfig(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: ds,
			Msg: "datasource config",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: ds,
		Msg: "datasource config",
		Mutator: func() error {
			// Only create the datasources configmap if it doesn't exist (don't mutate it here). Updates
			// are handled by the datasources controller. Save the current hash though for the deployment to use
			i.DsHash = ds.Annotations[constants.LastConfigAnnotation]
			return nil
		},
	}
}

func (i *ArgusReconciler) getAdminUserSecretDesiredState(cr *v1alpha1.Tenant) common.Action {
	s, err := argus.AdminSecret(cr)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to serialize argus adminuser secret",
			Error: err,
		}
	}

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "admin credentials secret",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "admin credentials secret",
		Mutator: func() error {
			return argus.AdminSecretMutator(cr, s)
		},
	}
}

func (i *ArgusReconciler) getIngressDesiredState(cr *v1alpha1.Tenant) common.Action {
	in := argus.Ingress(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: in,
			Msg: "argus ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: in,
		Msg: "argus ingress",
		Mutator: func() error {
			return argus.IngressMutator(cr, in)
		},
	}
}

func (i *ArgusReconciler) getStatefulSetDesiredState(state *ArgusState, cr *v1alpha1.Tenant) common.Action {
	_, err := state.PostgresEndpoint()
	if err != nil {
		return common.LogAction{
			Msg:   "must create a secret containing the postgres database endpoint",
			Error: err,
		}
	}
	sts := argus.StatefulSet(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sts,
			Msg: "argus statefulset",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sts,
		Msg: "argus statefulset",
		Mutator: func() error {
			return argus.StatefulSetMutator(cr, sts, i.ConfigHash, i.PluginsEnv, i.DsHash)
		},
	}
}

func (i *ArgusReconciler) getPluginsDesiredState(cr *v1alpha1.Tenant) common.Action {
	if i.Teardown {
		return nil
	}
	// Fetch all plugins of all dashboards
	requestedPlugins := config.Get().GetAllPlugins()

	// Consolidate plugins and create the new list of plugin requirements
	// If 'updated' is false then no changes have to be applied
	filteredPlugins, updated := i.Plugins.FilterPlugins(cr, requestedPlugins)
	if updated {
		i.reconcilePlugins(cr, filteredPlugins)

		// Build the new list of plugins for the init container to consume
		i.PluginsEnv = i.Plugins.BuildEnv(cr)

		// Reset the list of known dashboards to force the dashboard controller
		// to reimport them
		cfg := config.Get()
		cfg.InvalidateDashboards()

		return common.LogAction{
			Msg: fmt.Sprintf("plugins updated to %s", i.PluginsEnv),
		}
	} else {
		// Rebuild the env var from the installed plugins
		i.PluginsEnv = i.Plugins.BuildEnv(cr)
		return common.LogAction{
			Msg: "plugins unchanged",
		}
	}
}

func (i *ArgusReconciler) reconcilePlugins(cr *v1alpha1.Tenant, plugins v1alpha1.PluginList) {
	if i.Teardown {
		return
	}
	var validPlugins []v1alpha1.GrafanaPlugin  // nolint
	var failedPlugins []v1alpha1.GrafanaPlugin // nolint

	for _, plugin := range plugins {
		if !i.Plugins.PluginExists(plugin) {
			i.Log.Info(fmt.Sprintf("invalid plugin: %s@%s", plugin.Name, plugin.Version))
			failedPlugins = append(failedPlugins, plugin)
			continue
		}

		i.Log.Info(fmt.Sprintf("installing plugin: %s@%s", plugin.Name, plugin.Version))
		validPlugins = append(validPlugins, plugin)
	}

	cr.Status.Argus.InstalledPlugins = validPlugins
	cr.Status.Argus.FailedPlugins = failedPlugins
}

func (i *ArgusReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Tenant) common.Action {
	monitor := argus.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "argus servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "argus servicemonitor",
		Mutator: func() error {
			return argus.ServiceMonitorMutator(cr, monitor)
		},
	}
}
