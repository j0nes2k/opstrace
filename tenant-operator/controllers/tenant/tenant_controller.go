package tenant

import (
	"context"
	stdErr "errors"
	"fmt"
	"os"
	"reflect"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/config"
	"github.com/opstrace/opstrace/tenant-operator/controllers/tenant/argus"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	ControllerName = "tenant-controller"
	finalizerName  = "tenant.opstrace.com/finalizer"
)

// nolint:lll
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants;tenants/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/finalizers,verbs=update
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors,verbs=get;list;watch;create;update;patch;delete

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileTenant) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Tenant{}).
		// enqueue a reconcile for this tenant if any groups change.
		// this is primarily useful for tracking tenant teardown
		Watches(&source.Kind{Type: &opstracev1alpha1.Group{}},
			handler.EnqueueRequestsFromMapFunc(func(a client.Object) []reconcile.Request {
				return []reconcile.Request{
					{NamespacedName: types.NamespacedName{
						Name:      constants.TenantName,
						Namespace: a.GetNamespace(),
					}},
				}
			})).
		Owns(&appsv1.Deployment{}).
		Owns(&netv1.Ingress{}).
		Owns(&v1.ConfigMap{}).
		Owns(&v1.Service{}).
		Owns(&v1.ServiceAccount{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileTenant{}

// ReconcileTenant reconciles a Grafana object
type ReconcileTenant struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client   client.Client
	Scheme   *runtime.Scheme
	Plugins  *PluginsHelperImpl
	Context  context.Context
	Log      logr.Logger
	Cancel   context.CancelFunc
	Config   *config.ControllerConfig
	Recorder record.EventRecorder
	Teardown bool
}

// Reconcile reads the state of the cluster for a Grafana object and makes changes based on the state read
// and what is in the Grafana.Spec
func (r *ReconcileTenant) Reconcile(ctx context.Context, request reconcile.Request) (reconcile.Result, error) {
	tenant := &opstracev1alpha1.Tenant{}
	err := r.Client.Get(r.Context, request.NamespacedName, tenant)
	if err != nil {
		if errors.IsNotFound(err) {
			r.Log.Info("Tenant has been removed from API, deleting all remaining config in namespace")

			if err := r.Config.RemoveAllConfig(r.Client, r.Context, request.Namespace); err != nil {
				return reconcile.Result{}, err
			}
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := tenant.DeepCopy()

	if err := r.setDeletionFinalizer(ctx, cr); err != nil {
		return reconcile.Result{}, err
	}

	// Add installed dashboards to local config on start up
	if !r.Config.GetConfigBool(constants.ArgusConfigDashboardsSynced, false) {
		err = r.Config.HydrateKnownDashboards(r.Client, ctx, request.Namespace)
		if err != nil {
			r.Log.Error(err, "error reading known dashboards from cluster")
			// stop all reconciliation to prevent anything getting out of whack
			os.Exit(1)
		}
		r.Log.Info(fmt.Sprintf("hydrated %d known dashboards from cluster", len(r.Config.GetDashboards())))
		r.Config.AddConfigItem(constants.ArgusConfigDashboardsSynced, true)
	}

	// Read current state
	currentState := NewTenantState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		return r.manageError(cr, err, request)
	}
	// Get the actions required to reach the desired state
	groupsReconciler := NewGroupsReconciler(r.Teardown, r.Log)
	desiredState := groupsReconciler.Reconcile(&currentState.Groups, cr)

	if r.Teardown {
		// Stop any anciliary reconciliation against Argus,
		// but make sure to do this after groups reconciliation above
		config.Get().SetState(config.ControllerState{
			ArgusReady: false,
		})
	}

	argusReconciler := NewArgusReconciler(r.Teardown, r.Log)
	desiredState = append(desiredState, argusReconciler.Reconcile(&currentState.Argus, cr)...)
	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		return r.manageError(cr, err, request)
	}

	// Run the config map reconciler to discover jsonnet libraries
	err = reconcileConfigMaps(cr, r)
	if err != nil {
		return r.manageError(cr, err, request)
	}

	if r.Teardown {
		return reconcile.Result{}, r.teardownFinalizer(ctx, cr)
	}

	return r.manageSuccess(cr, currentState, request)
}

func (r *ReconcileTenant) setDeletionFinalizer(ctx context.Context, cr *opstracev1alpha1.Tenant) error {
	r.Teardown = false

	// examine DeletionTimestamp to determine if object is under deletion
	if cr.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,
		// then lets add the finalizer and update the object. This is equivalent
		// registering our finalizer.
		if !controllerutil.ContainsFinalizer(cr, finalizerName) {
			controllerutil.AddFinalizer(cr, finalizerName)

			if err := r.Client.Update(ctx, cr); err != nil {
				return fmt.Errorf("client update finalizer: %w", err)
			}
		}
	} else {
		r.Teardown = true
	}

	return nil
}

func (r *ReconcileTenant) teardownFinalizer(ctx context.Context, cr *opstracev1alpha1.Tenant) error {
	if controllerutil.ContainsFinalizer(cr, finalizerName) {
		// Successfully deleted everything we care about.
		// Remove our finalizer from the list and update it
		controllerutil.RemoveFinalizer(cr, finalizerName)

		if err := r.Client.Update(ctx, cr); err != nil {
			return fmt.Errorf("client remove finalizer: %w", err)
		}
	}
	return nil
}

func (r *ReconcileTenant) manageError(
	cr *opstracev1alpha1.Tenant,
	issue error,
	request reconcile.Request,
) (reconcile.Result, error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	instance := &opstracev1alpha1.Tenant{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		return reconcile.Result{}, err
	}
	r.Config.InvalidateDashboards()

	config.Get().SetState(config.ControllerState{
		ArgusReady: false,
	})

	if !reflect.DeepEqual(cr.Status, instance.Status) {
		err = r.Client.Status().Update(r.Context, cr)
		if err != nil {
			// Ignore conflicts, resource might just be outdated.
			if errors.IsConflict(err) {
				err = nil
			}
		}
	}

	return reconcile.Result{RequeueAfter: constants.RequeueDelay}, err
}

// Try to find a suitable url to grafana
func (r *ReconcileTenant) getAdminUrl(cr *opstracev1alpha1.Tenant, state *ArgusState) (string, error) {
	var servicePort = int32(argus.GetArgusPort(cr))

	if state.AdminSecret != nil {
		user := argus.GetAdminUser(cr, state.AdminSecret)
		pass := argus.GetAdminPassword(cr, state.AdminSecret)
		// Otherwise rely on the service
		if state.ArgusService != nil {
			return fmt.Sprintf("http://%s:%s@%v.%v.svc.cluster.local:%d",
				user,
				pass,
				state.ArgusService.Name,
				cr.Namespace,
				servicePort,
			), nil
		}
	}

	return "", stdErr.New("failed to find admin url")
}

func (r *ReconcileTenant) manageSuccess(
	cr *opstracev1alpha1.Tenant,
	state *TenantState,
	request reconcile.Request,
) (reconcile.Result, error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	instance := &opstracev1alpha1.Tenant{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		return r.manageError(cr, err, request)
	}

	// Make the Grafana API URL available to the dashboard controller
	url, err := r.getAdminUrl(cr, &state.Argus)
	if err != nil {
		return r.manageError(cr, err, request)
	}
	cr.Status.Argus.URL = &url

	if !reflect.DeepEqual(cr.Status, instance.Status) {
		err := r.Client.Status().Update(r.Context, cr)
		if err != nil {
			// Ignore conclicts. Resource might just be outdated.
			if errors.IsConflict(err) {
				return reconcile.Result{}, nil
			}
			return r.manageError(cr, err, request)
		}
	}

	// Publish controller state
	controllerState := config.ControllerState{
		AdminUrl:   url,
		ArgusReady: true,
	}

	config.Get().SetState(controllerState)

	r.Log.Info("desired Tenant state met")

	return reconcile.Result{}, nil
}
