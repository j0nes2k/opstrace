package argus

import (
	"os"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func GetAdminUser(cr *v1alpha1.Tenant, current *corev1.Secret) []byte {
	if cr.Spec.Overrides.Argus.Config.Security == nil || cr.Spec.Overrides.Argus.Config.Security.AdminUser == "" {
		// If a user is already set, don't change it
		if current != nil && current.Data[constants.ArgusAdminUserEnvVar] != nil {
			return current.Data[constants.ArgusAdminUserEnvVar]
		}
		return []byte(constants.DefaultAdminUser)
	}
	return []byte(cr.Spec.Overrides.Argus.Config.Security.AdminUser)
}

func GetAdminPassword(cr *v1alpha1.Tenant, current *corev1.Secret) []byte {
	if cr.Spec.Overrides.Argus.Config.Security == nil || cr.Spec.Overrides.Argus.Config.Security.AdminPassword == "" {
		// If a password is already set, don't change it
		if current != nil && current.Data[constants.ArgusAdminPasswordEnvVar] != nil {
			return current.Data[constants.ArgusAdminPasswordEnvVar]
		}
		return []byte(utils.RandStringRunes(10))
	}
	return []byte(cr.Spec.Overrides.Argus.Config.Security.AdminPassword)
}

func getData(cr *v1alpha1.Tenant, current *corev1.Secret) (map[string][]byte, error) {
	credentials := map[string][]byte{
		constants.ArgusAdminUserEnvVar:     GetAdminUser(cr, current),
		constants.ArgusAdminPasswordEnvVar: GetAdminPassword(cr, current),
	}

	// Make the credentials available to the environment when running the operator
	// outside of the cluster
	if err := os.Setenv(constants.ArgusAdminUserEnvVar, string(credentials[constants.ArgusAdminUserEnvVar])); err != nil {
		return credentials, err
	}
	err := os.Setenv(constants.ArgusAdminPasswordEnvVar, string(credentials[constants.ArgusAdminPasswordEnvVar]))

	return credentials, err
}

func AdminSecret(cr *v1alpha1.Tenant) (*corev1.Secret, error) {
	data, err := getData(cr, nil)

	return &corev1.Secret{
		ObjectMeta: v1.ObjectMeta{
			Name:      constants.ArgusAdminSecretName,
			Namespace: cr.Namespace,
		},
		Data: data,
		Type: corev1.SecretTypeOpaque,
	}, err
}

func AdminSecretMutator(cr *v1alpha1.Tenant, current *corev1.Secret) error {
	data, err := getData(cr, current)
	current.Data = data

	return err
}

func AdminSecretSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      constants.ArgusAdminSecretName,
	}
}
