package argus

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceMonitorName(cr *v1alpha1.Tenant) string {
	return constants.ArgusStatefulSetName
}

func getServiceMonitorLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
		// Configure the system-tenant prometheus to scrape this
		"tenant": "system",
	}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:     "admin-http",
			Interval: "60s",
		},
		{
			Port:     "metrics-plugin",
			Interval: "60s",
		},
	}
}

func getServiceMonitorSpec(cr *v1alpha1.Tenant) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app": constants.ArgusPodLabel,
			},
		},
		JobLabel: constants.SelectorLabelName,
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace},
		},
	}
}

func ServiceMonitor(cr *v1alpha1.Tenant) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getServiceMonitorName(cr),
			Namespace: cr.Namespace,
			Labels:    getServiceMonitorLabels(),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Tenant, current *monitoring.ServiceMonitor) error {
	currentSpec := &current.Spec
	spec := getServiceMonitorSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Argus.Components.ServiceMonitor.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		current.Annotations,
		cr.Spec.Overrides.Argus.Components.ServiceMonitor.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceMonitorLabels(),
		cr.Spec.Overrides.Argus.Components.ServiceMonitor.Labels,
	)

	return nil
}

func ServiceMonitorSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceMonitorName(cr),
	}
}
