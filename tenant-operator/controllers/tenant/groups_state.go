package tenant

import (
	"context"

	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type GroupsState struct {
	TenantGroups *v1alpha1.GroupList
}

func NewGroupsState() GroupsState {
	return GroupsState{}
}

func (i *GroupsState) Read(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	err := i.readGroups(ctx, cr, client)

	return err
}

// Returns true if no more groups exist in this tenant, which includes
// returning true if all remaining groups in the tenant are under deletion.
func (i *GroupsState) IsEmpty() bool {
	if i.TenantGroups == nil {
		// an error occurred somewhere when reading the groups so return false for safety
		return false
	}
	count := len(i.TenantGroups.Items)
	if count == 0 {
		return true
	}
	// If all remaining groups are under deletion, return true as well
	allUnderDeletion := true
	for _, g := range i.TenantGroups.Items {
		if g.ObjectMeta.DeletionTimestamp.IsZero() {
			allUnderDeletion = false
			break
		}
	}
	return allUnderDeletion
}

func (i *GroupsState) readGroups(ctx context.Context, cr *v1alpha1.Tenant, c client.Client) error {
	currentState := &v1alpha1.GroupList{}

	err := c.List(ctx, currentState, client.InNamespace(cr.Namespace))
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.TenantGroups = currentState.DeepCopy()

	return nil
}
