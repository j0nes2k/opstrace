package tenant

import (
	"context"
	stdErr "errors"

	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/tenant/argus"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ArgusState struct {
	ArgusService           *v1.Service
	ArgusServiceAccount    *v1.ServiceAccount
	ArgusConfig            *v1.ConfigMap
	ArgusIngress           *netv1.Ingress
	DataSourceConfig       *v1.ConfigMap
	AdminSecret            *v1.Secret
	PostgresEndpointSecret *v1.Secret
	ArgusServiceMonitor    *monitoring.ServiceMonitor
	ArgusStatefulSet       *appsv1.StatefulSet
}

func NewArgusState() ArgusState {
	return ArgusState{}
}

func (i *ArgusState) Read(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	err := i.readArgusService(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readArgusServiceAccount(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readArgusConfig(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readDatasourceConfig(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readArgusStatefulSet(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readArgusAdminUserSecret(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readPostgresEndpointSecret(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readArgusIngress(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readArgusServiceMonitor(ctx, cr, client)

	return err
}

// Get the Clickhouse user and password for the Jaeger clickhouse plugin
func (i *ArgusState) PostgresEndpoint() (*string, error) {
	var endpoint string
	var s = i.PostgresEndpointSecret

	if s != nil && s.Data[constants.ArgusPostgresURLKey] != nil {
		endpoint = string(s.Data[constants.ArgusPostgresURLKey])
	} else {
		return nil, stdErr.New("postgres endpoint secret not set")
	}

	return &endpoint, nil
}

func (i *ArgusState) readArgusService(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.Service{}
	selector := argus.ServiceSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusService = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusServiceAccount(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.ServiceAccount{}
	selector := argus.ServiceAccountSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusServiceAccount = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusConfig(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState, err := argus.Config(cr)
	if err != nil {
		return err
	}
	selector := argus.ConfigSelector(cr)
	err = client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusConfig = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readDatasourceConfig(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.ConfigMap{}
	selector := argus.DatasourceConfigSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.DataSourceConfig = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusIngress(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := argus.IngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusIngress = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusStatefulSet(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &appsv1.StatefulSet{}
	selector := argus.StatefulSetSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusStatefulSet = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusAdminUserSecret(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.Secret{}
	selector := argus.AdminSecretSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.AdminSecret = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readPostgresEndpointSecret(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.Secret{}
	selector := argus.PostgresEndpointSecretSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.PostgresEndpointSecret = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusServiceMonitor(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &monitoring.ServiceMonitor{}
	selector := argus.ServiceMonitorSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusServiceMonitor = currentState.DeepCopy()
	return nil
}
