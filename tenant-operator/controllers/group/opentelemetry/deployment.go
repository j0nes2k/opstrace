package opentelemetry

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "1024Mi"
	CpuLimit      = "500m"
)

var Replicas int32 = 1

func GetOtelDeploymentName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.OtelDeploymentNamePrefix, cr.Spec.ID)
}

func GetOtelDeploymentConfigName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.OtelDeploymentConfigPrefix, cr.Spec.ID)
}

func GetOtelDeploymentSelector(cr *v1alpha1.Group) map[string]string {
	return map[string]string{
		"app":   constants.OtelPodLabel,
		"group": fmt.Sprint(cr.Spec.ID),
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	var maxUnaval intstr.IntOrString = intstr.FromInt(25)
	var maxSurge intstr.IntOrString = intstr.FromInt(25)
	return v1.DeploymentStrategy{
		Type: "RollingUpdate",
		RollingUpdate: &v1.RollingUpdateDeployment{
			MaxUnavailable: &maxUnaval,
			MaxSurge:       &maxSurge,
		},
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	hash, err := GetOtelConfigHash(cr)
	if err != nil {
		return existing
	}
	return map[string]string{
		constants.LastConfigAnnotation: hash,
	}
}

func getPodLabels(cr *v1alpha1.Group) map[string]string {
	return map[string]string{
		"app":   constants.OtelPodLabel,
		"group": fmt.Sprint(cr.Spec.ID),
	}
}

func getVolumes(cr *v1alpha1.Group) []corev1.Volume {
	var volumes []corev1.Volume

	volumes = append(volumes, corev1.Volume{
		Name: GetOtelDeploymentConfigName(cr),
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: GetOtelDeploymentConfigName(cr),
				},
			},
		},
	})

	return volumes
}

func getVolumeMounts(cr *v1alpha1.Group) []corev1.VolumeMount {
	var mounts []corev1.VolumeMount

	mounts = append(mounts, corev1.VolumeMount{
		Name:      GetOtelDeploymentConfigName(cr),
		MountPath: "/etc/collector",
	})

	return mounts
}

func getReadinessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Path:   "/metrics",
				Port:   intstr.FromString("metrics"),
				Scheme: "HTTP",
			},
		},
		TimeoutSeconds:   1,
		PeriodSeconds:    10,
		SuccessThreshold: 1,
		FailureThreshold: 3,
	}
}

func getContainers(cr *v1alpha1.Group) []corev1.Container {
	return []corev1.Container{{
		Name:  "collector",
		Image: constants.DockerImageFullName(constants.OtelImageName),
		Args:  []string{"--config=/etc/collector/config.yaml"},
		Ports: []corev1.ContainerPort{
			{
				Name: constants.OtelIngressPortName,
				// Default port for otlp-http
				ContainerPort: 4318,
				Protocol:      "TCP",
			},
			{
				Name: constants.OtelJaegerIngressPortName,
				// default port for jaeger thrift-http
				ContainerPort: 14268,
				Protocol:      "TCP",
			},
			{
				Name:          "metrics",
				ContainerPort: 8888,
				Protocol:      "TCP",
			},
		},
		Resources:                getResources(),
		VolumeMounts:             getVolumeMounts(cr),
		ReadinessProbe:           getReadinessProbe(),
		TerminationMessagePath:   "/dev/termination-log",
		TerminationMessagePolicy: "File",
		ImagePullPolicy:          "IfNotPresent",
	}}
}

func getDeploymentSpec(cr *v1alpha1.Group, current v1.DeploymentSpec) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetOtelDeploymentSelector(cr),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetOtelDeploymentName(cr),
				Labels:      getPodLabels(cr),
				Annotations: getPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Volumes:            getVolumes(cr),
				Containers:         getContainers(cr),
				ServiceAccountName: GetOtelDeploymentName(cr),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Group) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetOtelDeploymentName(cr),
			Namespace: cr.Namespace,
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      GetOtelDeploymentName(cr),
	}
}

func DeploymentMutator(cr *v1alpha1.Group, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.OpenTelemetry.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.OpenTelemetry.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.OpenTelemetry.Components.Deployment.Labels,
	)

	return nil
}
