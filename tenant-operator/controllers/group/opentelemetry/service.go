package opentelemetry

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceName(cr *v1alpha1.Group) string {
	return GetOtelDeploymentName(cr)
}

func getServiceLabels(cr *v1alpha1.Group) map[string]string {
	return GetOtelDeploymentSelector(cr)
}

func getServiceAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	return existing
}

func getServicePorts() []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name:       constants.OtelIngressPortName,
			Port:       4318,
			Protocol:   "TCP",
			TargetPort: intstr.FromString(constants.OtelIngressPortName),
		},
		{
			Name:       constants.OtelJaegerIngressPortName,
			Port:       14268,
			Protocol:   "TCP",
			TargetPort: intstr.FromString(constants.OtelJaegerIngressPortName),
		},
		{
			Name:       "metrics",
			Port:       8888,
			Protocol:   "TCP",
			TargetPort: intstr.FromString("metrics"),
		},
	}
}

func getServiceSpec(cr *v1alpha1.Group) v1.ServiceSpec {
	return v1.ServiceSpec{
		Ports:    getServicePorts(),
		Selector: GetOtelDeploymentSelector(cr),
		Type:     v1.ServiceTypeClusterIP,
	}
}

func Service(cr *v1alpha1.Group) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceName(cr),
			Namespace:   cr.Namespace,
			Labels:      getServiceLabels(cr),
			Annotations: getServiceAnnotations(cr, nil),
		},
		Spec: getServiceSpec(cr),
	}
}

func ServiceMutator(cr *v1alpha1.Group, current *v1.Service) error {
	currentSpec := &current.Spec
	spec := getServiceSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.OpenTelemetry.Components.Service.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.OpenTelemetry.Components.Service.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceLabels(cr),
		cr.Spec.Overrides.OpenTelemetry.Components.Service.Labels,
	)

	return nil
}

func ServiceSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceName(cr),
	}
}
