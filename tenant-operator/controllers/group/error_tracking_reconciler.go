package group

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/errortracking"
)

type ErrorTrackingReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewErrorTrackingReconciler(teardown bool, logger logr.Logger) *ErrorTrackingReconciler {
	return &ErrorTrackingReconciler{
		Teardown: teardown,
		Log:      logger.WithName("tenanterrortracking"),
	}
}

func (e *ErrorTrackingReconciler) Reconcile(state *ErrorTrackingState, cr *v1alpha1.Group) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(e.getClickhouseQuotasDesiredState(state, cr))
	desired = desired.AddAction(e.getServiceAccountDesiredState(cr))
	desired = desired.AddAction(e.getDeploymentDesiredState(cr, state))
	desired = desired.AddAction(e.getServiceDesiredState(cr))
	desired = desired.AddAction(e.getIngressDesiredState(cr))
	desired = desired.AddAction(e.getIngressV2DesiredState(cr))
	desired = desired.AddAction(e.getServiceMonitorDesiredState(cr))

	desired = desired.AddActions(e.getReadiness(state))

	return desired
}

func (e *ErrorTrackingReconciler) getClickhouseQuotasDesiredState(
	state *ErrorTrackingState,
	cr *v1alpha1.Group,
) common.Action {
	user, err := state.ClickHouseUsername()
	if err != nil {
		return common.LogAction{
			Msg:   "failed to obtain errortracking clickhouse username",
			Error: err,
		}
	}

	cm := errortracking.ClickHouseQuotasConfigmap(cr, user)

	if e.Teardown {
		return common.GenericDeleteAction{
			Ref: cm,
			Msg: "group errortracking-clickhouse quota configmap",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: cm,
		Msg: "group errortracking-clickhouse quota configmap",
		Mutator: func() error {
			errortracking.ClickHouseQuotasConfigMapMutator(cr, cm, user)
			return nil
		},
	}
}

func (e *ErrorTrackingReconciler) getServiceAccountDesiredState(cr *v1alpha1.Group) common.Action {
	sa := errortracking.ServiceAccount(cr)

	if e.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "tenanterrortracking service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "tenanterrortracking service account",
		Mutator: func() error {
			errortracking.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (e *ErrorTrackingReconciler) getDeploymentDesiredState(
	cr *v1alpha1.Group,
	state *ErrorTrackingState,
) common.Action {
	deployment := errortracking.Deployment(cr)

	if e.Teardown {
		return common.GenericDeleteAction{
			Ref: deployment,
			Msg: "tenanterrortracking deployment",
		}
	}

	clickhouseSecret := state.ErrorTrackingClickHouseCredentials
	if clickhouseSecret == nil {
		return common.LogAction{
			Msg:   "tenanterrortracking clickhouse-credentials",
			Error: fmt.Errorf("failed to read credentials"),
		}
	}

	clickhouseEndpoint := errortracking.GetClickHouseCredentialsNativeEndpoint(clickhouseSecret)
	if clickhouseEndpoint == "" {
		return common.LogAction{
			Msg:   "tenanterrortracking clickhouse-credentials",
			Error: fmt.Errorf("configured endpoint undefined or empty"),
		}
	}

	clickhouseDSN := clickhouseEndpoint + "/" + constants.ErrorTrackingDatabaseName

	return common.GenericCreateOrUpdateAction{
		Ref: deployment,
		Msg: "tenanterrortracking deployment",
		Mutator: func() error {
			return errortracking.DeploymentMutator(cr, deployment, clickhouseDSN, cr.Spec.GetHost())
		},
	}
}

func (e *ErrorTrackingReconciler) getServiceDesiredState(cr *v1alpha1.Group) common.Action {
	service := errortracking.Service(cr)

	if e.Teardown {
		return common.GenericDeleteAction{
			Ref: service,
			Msg: "tenanterrortracking service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: service,
		Msg: "tenanterrortracking service",
		Mutator: func() error {
			return errortracking.ServiceMutator(cr, service)
		},
	}
}

func (e *ErrorTrackingReconciler) getIngressDesiredState(cr *v1alpha1.Group) common.Action {
	ing := errortracking.Ingress(cr)

	if e.Teardown {
		return common.GenericDeleteAction{
			Ref: ing,
			Msg: "tenanterrortracking ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: ing,
		Msg: "tenanterrortracking ingress",
		Mutator: func() error {
			return errortracking.IngressMutator(cr, ing)
		},
	}
}

func (e *ErrorTrackingReconciler) getIngressV2DesiredState(cr *v1alpha1.Group) common.Action {
	ing := errortracking.IngressV2(cr)

	if e.Teardown {
		return common.GenericDeleteAction{
			Ref: ing,
			Msg: "tenanterrortracking v2 ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: ing,
		Msg: "tenanterrortracking v2 ingress",
		Mutator: func() error {
			return errortracking.IngressV2Mutator(cr, ing)
		},
	}
}

func (e *ErrorTrackingReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Group) common.Action {
	sm := errortracking.ServiceMonitor(cr)

	if e.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: "tenanterrortracking service monitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sm,
		Msg: "tenanterrortracking service monitor",
		Mutator: func() error {
			return errortracking.ServiceMonitorMutator(cr, sm)
		},
	}
}

func (e *ErrorTrackingReconciler) getReadiness(state *ErrorTrackingState) []common.Action {
	if e.Teardown {
		actions := []common.Action{}
		if state.Ingress != nil {
			actions = append(actions, common.CheckGoneAction{
				Ref: state.Ingress,
				Msg: "check tenanterrortracking ingress is gone",
			})
		}
		if state.Deployment != nil {
			actions = append(actions, common.CheckGoneAction{
				Ref: state.Deployment,
				Msg: "check tenanterrortracking deployment is gone",
			})
		}
		return actions
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.Deployment,
			Msg: "check tenanterrortracking deployment readiness",
		},
		common.IngressReadyAction{
			Ref: state.Ingress,
			Msg: "check tenanterrortracking ingress readiness",
		},
	}
}
