//go:build integration
// +build integration

package group

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var _ = Describe("group controller", func() {
	const (
		GroupName      = "test-group"
		GroupNamespace = "default"

		timeout  = time.Second * 10
		duration = time.Second * 10
		interval = time.Millisecond * 500
	)

	var domain string = "https://staging.opstracegcp.com"

	AfterEach(func() {
		ctx := context.Background()
		group := &v1alpha1.Group{
			ObjectMeta: metav1.ObjectMeta{
				Name:      GroupName,
				Namespace: GroupNamespace,
			},
		}
		Expect(k8sClient.Delete(ctx, group)).To(Succeed())
	})

	Context("when creating a group", func() {
		It("should reconcile a group's resources", func() {
			ctx := context.Background()
			group := &v1alpha1.Group{
				TypeMeta: metav1.TypeMeta{
					APIVersion: "groups.opstrace.com/v1alpha1",
					Kind:       "Group",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      GroupName,
					Namespace: GroupNamespace,
				},
				Spec: v1alpha1.GroupSpec{
					Domain: &domain,
				},
			}
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())

			groupLookupKey := types.NamespacedName{
				Name:      GroupName,
				Namespace: GroupNamespace,
			}
			createdGroup := &v1alpha1.Group{}
			Eventually(func() bool {
				if err := k8sClient.Get(ctx, groupLookupKey, createdGroup); err != nil {
					return false
				}
				return true
			}, timeout, interval).Should(BeTrue())

			Expect(createdGroup.Spec.GetHost()).To(Equal(domain))
		})
	})
})
