package errortracking

import (
	"fmt"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/config"
	netv1 "k8s.io/api/networking/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func IngressV2(cr *v1alpha1.Group) *netv1.Ingress {
	return &netv1.Ingress{
		ObjectMeta: v1.ObjectMeta{
			Name:        getIngressV2Name(cr),
			Namespace:   cr.Namespace,
			Labels:      getIngressV2Labels(cr),
			Annotations: getIngressV2Annotations(cr, nil),
		},
		Spec: getIngressV2Spec(cr),
	}
}

func IngressV2Mutator(cr *v1alpha1.Group, current *netv1.Ingress) error {
	currentSpec := &current.Spec
	spec := getIngressV2Spec(cr)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ErrorTracking.Components.IngressV2.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getIngressV2Annotations(cr, current.Annotations),
		cr.Spec.Overrides.ErrorTracking.Components.IngressV2.Annotations,
	)
	current.Labels = common.MergeMap(
		getIngressV2Labels(cr),
		cr.Spec.Overrides.ErrorTracking.Components.IngressV2.Labels,
	)

	return nil
}

func IngressV2Selector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getIngressV2Name(cr),
	}
}

func getIngressV2Name(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-tokenauth-%d", constants.ErrorTrackingNamePrefix, cr.Spec.ID)
}

func getIngressV2TLS(cr *v1alpha1.Group) []netv1.IngressTLS {
	if !cr.Spec.UseTLS() {
		return nil
	}
	return []netv1.IngressTLS{
		{
			Hosts:      []string{cr.Spec.GetHost()},
			SecretName: constants.HTTPSCertSecretName,
		},
	}
}

func getIngressV2Labels(cr *v1alpha1.Group) map[string]string {
	return map[string]string{}
}

func getIngressV2Annotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	gatekeeperURL := config.Get().GetGatekeeperURL()
	domain := cr.Spec.GetHostURL()
	existing = common.MergeMap(existing, common.GetAuthIngressAnnotations(fmt.Sprint(cr.Spec.ID), gatekeeperURL, domain))
	existing = common.MergeMap(existing, map[string]string{
		"kubernetes.io/ingress.class":                         "nginx",
		"nginx.ingress.kubernetes.io/mergeable-ingress-type":  "minion",
		"nginx.ingress.kubernetes.io/client-body-buffer-size": "1m",
		// Send the original request in the X-Original-Url header
		"nginx.ingress.kubernetes.io/proxy-add-original-uri-header": "true",
		// Cache the auth response to avoid querying the auth endpoint too
		// frequently.
		// TODO: consider adding $request_uri in the auth cache key in case the
		// public key does not account for the project id"
		"nginx.ingress.kubernetes.io/auth-cache-key": "$remote_user",
		// 401: If something fails auth then it should try again a few seconds later to avoid overloading the service
		// 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
		// 50x: If Gatekeeper is returning 500 errors, allow them to clear up quickly when Gatekeeper comes back,
		//      but allow some caching to avoid the Gatekeep getting hammered with retries when down.
		"nginx.ingress.kubernetes.io/auth-cache-duration": "401 10s, 200 202 2m, 500 503 30s",
		// for now rewrite URLs to follow API base paths as configured for global deployments
		// of the errortracking-api. When we change the base path to `v1/errortracking/$GROUP_ID`,
		// this rewrite rule can be dropped or amended as the need be.
		"nginx.ingress.kubernetes.io/rewrite-target": "/$2",
	})
	return existing
}

func getIngressV2Spec(cr *v1alpha1.Group) netv1.IngressSpec {
	pathType := netv1.PathTypePrefix

	return netv1.IngressSpec{
		TLS: getIngressV2TLS(cr),
		Rules: []netv1.IngressRule{
			{
				// Route all the requests for ${domain}/errortracking to the
				// error tracking API. Authorization is handled by gatekeeper
				// using the nginx ingress annotations set above.
				Host: cr.Spec.GetHost(),
				IngressRuleValue: netv1.IngressRuleValue{
					HTTP: &netv1.HTTPIngressRuleValue{
						Paths: []netv1.HTTPIngressPath{
							{
								// capture non-base-path parts of the requested URL for the rewrite-target
								// URL to then do the right thing
								Path:     fmt.Sprintf("/v2/errortracking/%d(/|$)(.*)", cr.Spec.ID),
								PathType: &pathType,
								Backend: netv1.IngressBackend{
									Service: &netv1.IngressServiceBackend{
										Name: GetErrorTrackingAPIDeploymentName(cr),
										Port: netv1.ServiceBackendPort{
											Name: "http",
										},
									},
									Resource: nil,
								},
							},
						},
					},
				},
			},
		},
	}
}
