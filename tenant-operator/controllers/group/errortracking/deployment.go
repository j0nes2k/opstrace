package errortracking

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "4Gi"
	CpuLimit      = "2"
)

var Replicas int32 = 3

func Deployment(cr *v1alpha1.Group) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetErrorTrackingAPIDeploymentName(cr),
			Namespace: cr.Namespace,
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      GetErrorTrackingAPIDeploymentName(cr),
	}
}

func DeploymentMutator(cr *v1alpha1.Group, current *v1.Deployment, clickhouseDSN string, apiBaseURL string) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(cr, current.Spec, clickhouseDSN, apiBaseURL)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ErrorTracking.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getDeploymentAnnotations(current.Annotations),
		cr.Spec.Overrides.ErrorTracking.Components.Deployment.Annotations,
	)
	current.Labels = common.MergeMap(
		GetErrorTrackingAPIDeploymentLabels(cr),
		cr.Spec.Overrides.ErrorTracking.Components.Deployment.Labels,
	)
	return nil
}

func GetErrorTrackingAPIDeploymentName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.ErrorTrackingNamePrefix, cr.Spec.ID)
}

func GetErrorTrackingAPIDeploymentLabels(cr *v1alpha1.Group) map[string]string {
	return map[string]string{
		"app":   GetErrorTrackingAPIDeploymentName(cr),
		"group": fmt.Sprint(cr.Spec.ID),
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	maxUnavailable := intstr.FromString("25%")
	maxSurge := intstr.FromString("25%")

	return v1.DeploymentStrategy{
		Type: v1.RollingUpdateDeploymentStrategyType,
		RollingUpdate: &v1.RollingUpdateDeployment{
			MaxUnavailable: &maxUnavailable,
			MaxSurge:       &maxSurge,
		},
	}
}

func getDeploymentAnnotations(existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(existing map[string]string) map[string]string {
	return existing
}

func getContainerEnv(clickhouseDSN string, apiBaseURL string) []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:  "API_BASE_URL",
			Value: apiBaseURL,
		},
		{
			Name:  "CLICKHOUSE_DSN",
			Value: clickhouseDSN,
		},
	}
}

// func getReadinessProbe() *corev1.Probe {
// 	return &corev1.Probe{
// 		ProbeHandler: corev1.ProbeHandler{
// 			HTTPGet: &corev1.HTTPGetAction{
// 				Port:   intstr.FromInt(8081),
// 				Path:   "/readyz",
// 				Scheme: corev1.URISchemeHTTP,
// 			},
// 		},
// 		InitialDelaySeconds: 5,
// 		PeriodSeconds:       3,
// 	}
// }

// func getLivenessProbe() *corev1.Probe {
// 	return &corev1.Probe{
// 		ProbeHandler: corev1.ProbeHandler{
// 			HTTPGet: &corev1.HTTPGetAction{
// 				Path:   "/healthz",
// 				Port:   intstr.FromInt(8081),
// 				Scheme: "HTTP",
// 			},
// 		},
// 		InitialDelaySeconds: 30,
// 		PeriodSeconds:       3,
// 		FailureThreshold:    10,
// 	}
// }

func getContainers(clickhouseDSN, apiBaseURL string) []corev1.Container {
	return []corev1.Container{{
		Name:  constants.ErrorTrackingAPIName,
		Image: constants.DockerImageFullName(constants.ErrorTrackingImageName),
		Env:   getContainerEnv(clickhouseDSN, apiBaseURL),
		Ports: []corev1.ContainerPort{
			{
				Name:          "http",
				ContainerPort: 8080,
			},
			{
				Name:          "metrics",
				ContainerPort: 8081,
			},
		},
		Resources:       getResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		// ReadinessProbe:  getReadinessProbe(),
		// LivenessProbe:   getLivenessProbe(),
	}}
}

func getDeploymentSpec(
	cr *v1alpha1.Group,
	current v1.DeploymentSpec,
	clickhouseDSN string,
	apiBaseURL string,
) v1.DeploymentSpec {
	var terminationGracePeriod int64 = 10

	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetErrorTrackingAPIDeploymentLabels(cr),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetErrorTrackingAPIDeploymentName(cr),
				Labels:      GetErrorTrackingAPIDeploymentLabels(cr),
				Annotations: getPodAnnotations(current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:                    getContainers(clickhouseDSN, apiBaseURL),
				ServiceAccountName:            GetErrorTrackingAPIDeploymentName(cr),
				TerminationGracePeriodSeconds: &terminationGracePeriod,
				Affinity: common.WithPodAntiAffinity(metav1.LabelSelector{
					MatchLabels: GetErrorTrackingAPIDeploymentLabels(cr),
				}, nil),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}
