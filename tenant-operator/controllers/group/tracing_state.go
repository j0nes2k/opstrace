package group

import (
	"context"
	"fmt"
	"net/url"

	jaegerOperator "github.com/jaegertracing/jaeger-operator/apis/v1"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/opentelemetry"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type TracingState struct {
	JaegerClickHouseCredentials *v1.Secret
	JaegerClickHouseQuotas      *v1.ConfigMap
	JaegerService               *v1.Service
	JaegerPluginConfig          *v1.Secret
	JaegerIngress               *netv1.Ingress
	// Read the Deployment that the Jaeger operator manages
	JaegerDeployment     *appsv1.Deployment
	JaegerCR             *jaegerOperator.Jaeger
	JaegerServiceMonitor *monitoring.ServiceMonitor
	OtelService          *v1.Service
	OtelServiceAccount   *v1.ServiceAccount
	OtelPluginConfig     *v1.Secret
	OtelIngress          *netv1.Ingress
	OtelJaegerIngress    *netv1.Ingress
	OtelDeployment       *appsv1.Deployment
	OtelServiceMonitor   *monitoring.ServiceMonitor
}

func NewTracingState() *TracingState {
	return &TracingState{}
}

func (i *TracingState) Read(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	err := i.readJaegerClickHouseCredentials(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readJaegerClickHouseQuotas(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readJaegerService(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readJaegerPluginConfig(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readJaegerCR(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readJaegerDeployment(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readJaegerIngress(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readJaegerServiceMonitor(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOtelService(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOtelServiceAccount(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOtelPluginConfig(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOtelDeployment(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOtelIngress(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOtelJaegerIngress(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readOtelServiceMonitor(ctx, cr, client)

	return err
}

// Get the Clickhouse user and password for the Jaeger clickhouse plugin.
func (i *TracingState) JaegerClickHouseURL() (*url.URL, error) {
	if i.JaegerClickHouseCredentials == nil ||
		i.JaegerClickHouseCredentials.Data[constants.ClickHouseCredentialsNativeEndpointKey] == nil {
		return nil, fmt.Errorf("%s key not set in jaeger clickhouse credentials secret",
			constants.ClickHouseCredentialsNativeEndpointKey)
	}
	native := string(i.JaegerClickHouseCredentials.Data[constants.ClickHouseCredentialsNativeEndpointKey])

	return url.Parse(native)
}

func (i *TracingState) JaegerClickHouseUsername() (string, error) {
	if i.JaegerClickHouseCredentials == nil ||
		i.JaegerClickHouseCredentials.Data[constants.ClickHouseCredentialsUserKey] == nil {
		return "", fmt.Errorf("%s key not set in jaeger clickhouse credentials secret",
			constants.ClickHouseCredentialsUserKey,
		)
	}
	return string(i.JaegerClickHouseCredentials.Data[constants.ClickHouseCredentialsUserKey]), nil
}

func (i *TracingState) readJaegerClickHouseCredentials(
	ctx context.Context,
	cr *v1alpha1.Group,
	client client.Client,
) error {
	// These credentials are created and owned by the scheduler. The scheduler will create these when it creates the
	// user/password in clickhouse for a GitLabNamespace, and deploys the v1alpha1.Group
	currentState := &v1.Secret{}
	selector := jaeger.ClickHouseCredentialsSecretSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.JaegerClickHouseCredentials = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readJaegerClickHouseQuotas(
	ctx context.Context,
	cr *v1alpha1.Group,
	client client.Client,
) error {
	currentState := &v1.ConfigMap{}
	selector := jaeger.ClickHouseQuotasConfigMapSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.JaegerClickHouseQuotas = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readJaegerService(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &v1.Service{}
	selector := jaeger.ServiceSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.JaegerService = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readJaegerPluginConfig(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &v1.Secret{}
	selector := jaeger.JaegerPluginConfigSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.JaegerPluginConfig = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readJaegerIngress(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := jaeger.IngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.JaegerIngress = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readJaegerCR(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &jaegerOperator.Jaeger{}
	selector := jaeger.JaegerSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	i.JaegerCR = currentState.DeepCopy()
	return err
}

func (i *TracingState) readJaegerDeployment(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := jaeger.JaegerSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.JaegerDeployment = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readJaegerServiceMonitor(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &monitoring.ServiceMonitor{}
	selector := jaeger.ServiceMonitorSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.JaegerServiceMonitor = currentState.DeepCopy()
	return nil
}

//////////////// Otel ////////////////////

func (i *TracingState) readOtelService(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &v1.Service{}
	selector := opentelemetry.ServiceSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OtelService = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readOtelServiceAccount(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &v1.ServiceAccount{}
	selector := opentelemetry.ServiceAccountSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OtelServiceAccount = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readOtelPluginConfig(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &v1.Secret{}
	selector := opentelemetry.ConfigSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OtelPluginConfig = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readOtelIngress(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := opentelemetry.IngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OtelIngress = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readOtelJaegerIngress(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := opentelemetry.JaegerIngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OtelJaegerIngress = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readOtelDeployment(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := opentelemetry.DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OtelDeployment = currentState.DeepCopy()
	return nil
}

func (i *TracingState) readOtelServiceMonitor(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &monitoring.ServiceMonitor{}
	selector := opentelemetry.ServiceMonitorSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OtelServiceMonitor = currentState.DeepCopy()
	return nil
}
