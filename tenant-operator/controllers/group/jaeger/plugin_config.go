package jaeger

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/url"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gopkg.in/yaml.v3"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Default configuration for the Jaeger Plugin.
func NewJaegerPluginDefaults(cr *v1alpha1.Group, clickHouseURL *url.URL) v1alpha1.JaegerPluginConfigOptions {
	pass, _ := clickHouseURL.User.Password()

	return v1alpha1.JaegerPluginConfigOptions{
		Address:            fmt.Sprintf("tcp://%s", clickHouseURL.Host),
		MaxSpanCount:       100_000,
		BatchWriteSize:     10_000,
		BatchFlushInterval: "5s",
		Encoding:           "protobuf",
		Password:           pass,
		Username:           clickHouseURL.User.Username(),
		Database:           constants.JaegerDatabaseName,
		Tenant:             fmt.Sprint(cr.Spec.ID),
		MetricsEndpoint:    "0.0.0.0:9090",
		Replication:        true,
		TTLDays:            10,
	}
}

func GetJaegerPluginConfigHash(cr *v1alpha1.Group, clickHouseURL *url.URL) (string, error) {
	defaultsWithOverrides, err := getDefaultsWithOverrides(cr, clickHouseURL)
	if err != nil {
		return "", err
	}
	data, err := json.Marshal(defaultsWithOverrides)
	if err != nil {
		return "", err
	}
	sha := sha256.Sum256(data)

	return hex.EncodeToString(sha[:28]), nil
}

func getDefaultsWithOverrides(cr *v1alpha1.Group, clickHouseURL *url.URL) (v1alpha1.JaegerPluginConfigOptions, error) {
	defaults := NewJaegerPluginDefaults(cr, clickHouseURL)
	err := utils.PatchObject(&defaults, cr.Spec.Overrides.Jaeger.Config)

	return defaults, err
}

func getData(cr *v1alpha1.Group, clickHouseURL *url.URL) (*map[string][]byte, error) {
	defaultsWithOverrides, err := getDefaultsWithOverrides(cr, clickHouseURL)
	if err != nil {
		return nil, err
	}
	data, err := yaml.Marshal(defaultsWithOverrides)
	if err != nil {
		return nil, err
	}

	return &map[string][]byte{
		"config.yaml": data,
	}, nil
}

func JaegerPluginConfig(cr *v1alpha1.Group, clickHouseURL *url.URL) (*v1.Secret, error) {
	secret := &v1.Secret{}
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      GetJaegerPluginConfigName(cr),
		Namespace: cr.Namespace,
	}
	hash, err := GetJaegerPluginConfigHash(cr, clickHouseURL)
	if err != nil {
		return secret, err
	}
	// Store the hash of the current configuration for later
	// comparisons
	secret.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}

	data, err := getData(cr, clickHouseURL)
	if data != nil {
		secret.Data = *data
	}

	return secret, err
}

func JaegerPluginConfigMutator(cr *v1alpha1.Group, current *v1.Secret, clickHouseURL *url.URL) error {
	hash, err := GetJaegerPluginConfigHash(cr, clickHouseURL)
	if err != nil {
		return err
	}
	current.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}
	data, err := getData(cr, clickHouseURL)
	if err != nil {
		return err
	}
	if data != nil {
		current.Data = *data
	}

	return nil
}

func JaegerPluginConfigSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      GetJaegerPluginConfigName(cr),
	}
}
