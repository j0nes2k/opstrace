package jaeger

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceName(cr *v1alpha1.Group) string {
	return GetJaegerName(cr)
}

func getServiceLabels(cr *v1alpha1.Group) map[string]string {
	return GetJaegerSelector(cr)
}

func getServiceAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	return map[string]string{}
}

func getServicePorts() []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name:       "query",
			Port:       16686,
			TargetPort: intstr.FromString("query"),
		},
		{
			Name:       "admin-http",
			Port:       14269,
			TargetPort: intstr.FromString("admin-http"),
		},
		{
			Name:       "metrics-plugin",
			Port:       9090,
			TargetPort: intstr.FromInt(9090),
		},
		{
			Name:       "otlp-ingest",
			Port:       14250,
			TargetPort: intstr.FromInt(14250),
		},
	}
}

func getServiceSpec(cr *v1alpha1.Group) v1.ServiceSpec {
	return v1.ServiceSpec{
		Ports:    getServicePorts(),
		Selector: GetJaegerSelector(cr),
		Type:     v1.ServiceTypeClusterIP,
	}
}

func Service(cr *v1alpha1.Group) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceName(cr),
			Namespace:   cr.Namespace,
			Labels:      getServiceLabels(cr),
			Annotations: getServiceAnnotations(cr, nil),
		},
		Spec: getServiceSpec(cr),
	}
}

func ServiceMutator(cr *v1alpha1.Group, current *v1.Service) error {
	currentSpec := &current.Spec
	spec := getServiceSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Jaeger.Components.Service.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Jaeger.Components.Service.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceLabels(cr),
		cr.Spec.Overrides.Jaeger.Components.Service.Labels,
	)

	return nil
}

func ServiceSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceName(cr),
	}
}
