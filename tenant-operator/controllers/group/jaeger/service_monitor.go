package jaeger

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceMonitorName(cr *v1alpha1.Group) string {
	return GetJaegerName(cr)
}

func getServiceMonitorLabels(cr *v1alpha1.Group) map[string]string {
	labels := GetJaegerSelector(cr)
	// Configure the system-tenant prometheus to scrape this
	labels["tenant"] = "system"

	return labels
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:     "admin-http",
			Interval: "60s",
		},
		{
			Port:     "metrics-plugin",
			Interval: "60s",
		},
	}
}

func getServiceMonitorSpec(cr *v1alpha1.Group) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: GetJaegerSelector(cr),
		},
		JobLabel: constants.SelectorLabelName,
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace},
		},
	}
}

func ServiceMonitor(cr *v1alpha1.Group) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getServiceMonitorName(cr),
			Namespace: cr.Namespace,
			Labels:    getServiceMonitorLabels(cr),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Group, current *monitoring.ServiceMonitor) error {
	currentSpec := &current.Spec
	spec := getServiceMonitorSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Jaeger.Components.ServiceMonitor.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		current.Annotations,
		cr.Spec.Overrides.Jaeger.Components.ServiceMonitor.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceMonitorLabels(cr),
		cr.Spec.Overrides.Jaeger.Components.ServiceMonitor.Labels,
	)

	return nil
}

func ServiceMonitorSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceMonitorName(cr),
	}
}
