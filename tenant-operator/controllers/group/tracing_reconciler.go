package group

import (
	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	"github.com/opstrace/opstrace/tenant-operator/controllers/group/opentelemetry"
)

type TracingReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewTracingReconciler(teardown bool, logger logr.Logger) *TracingReconciler {
	return &TracingReconciler{
		Teardown: teardown,
		Log:      logger.WithName("tracing"),
	}
}

func (i *TracingReconciler) Reconcile(state *TracingState, cr *v1alpha1.Group) common.DesiredState {
	desired := common.DesiredState{}

	// The following order "should" work for creation/update/teardown.
	// It's possible we may have to change the order of some of these when tearing down.
	desired = desired.AddAction(i.getJaegerClickhouseQuotasDesiredState(state, cr))
	desired = desired.AddAction(i.getJaegerServiceDesiredState(cr))
	desired = desired.AddAction(i.getJaegerPluginConfigDesiredState(state, cr))
	desired = desired.AddAction(i.getJaegerDatasourceDesiredState(cr))
	desired = desired.AddAction(i.getJaegerIngressDesiredState(cr))
	desired = desired.AddAction(i.getJaegerDeploymentDesiredState(state, cr))
	desired = desired.AddAction(i.getJaegerServiceMonitorDesiredState(cr))

	desired = desired.AddAction(i.getOtelServiceDesiredState(cr))
	desired = desired.AddAction(i.getOtelServiceAccountDesiredState(cr))
	desired = desired.AddAction(i.getOtelConfigDesiredState(cr))
	desired = desired.AddAction(i.getOtelIngressDesiredState(cr))
	desired = desired.AddAction(i.getOtelJaegerIngressDesiredState(cr))
	desired = desired.AddAction(i.getOtelDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getOtelServiceMonitorDesiredState(cr))

	desired = desired.AddActions(i.getReadiness(state))

	return desired
}

func (i *TracingReconciler) getReadiness(state *TracingState) []common.Action {
	if i.Teardown {
		actions := []common.Action{}
		if state.JaegerDeployment != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.JaegerDeployment,
					Msg: "check jaeger deployment is gone",
				},
			)
		}
		if state.OtelDeployment != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.OtelDeployment,
					Msg: "check otel deployment is gone",
				},
			)
		}
		return actions
	}
	return []common.Action{
		common.IngressReadyAction{
			Ref: state.JaegerIngress,
			Msg: "check jaeger ingress readiness",
		},
		common.DeploymentReadyAction{
			Ref: state.JaegerDeployment,
			Msg: "check jaeger deployment readiness",
		},
		common.IngressReadyAction{
			Ref: state.OtelIngress,
			Msg: "check opentelemetry ingress readiness",
		},
		common.DeploymentReadyAction{
			Ref: state.OtelDeployment,
			Msg: "check opentelemetry deployment readiness",
		},
	}
}

func (i *TracingReconciler) getJaegerClickhouseQuotasDesiredState(
	state *TracingState,
	cr *v1alpha1.Group,
) common.Action {
	user, err := state.JaegerClickHouseUsername()
	if err != nil {
		return common.LogAction{
			Msg:   "failed to obtain jaeger clickhouse username",
			Error: err,
		}
	}

	cm := jaeger.ClickHouseQuotasConfigmap(cr, user)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: cm,
			Msg: "group jaeger-clickhouse quota configmap",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: cm,
		Msg: "group jaeger-clickhouse quota configmap",
		Mutator: func() error {
			jaeger.ClickHouseQuotasConfigMapMutator(cr, cm, user)
			return nil
		},
	}
}

func (i *TracingReconciler) getJaegerServiceDesiredState(cr *v1alpha1.Group) common.Action {
	svc := jaeger.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "jaeger service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "jaeger service",
		Mutator: func() error {
			return jaeger.ServiceMutator(cr, svc)
		},
	}
}

func (i *TracingReconciler) getJaegerPluginConfigDesiredState(state *TracingState, cr *v1alpha1.Group) common.Action {
	url, err := state.JaegerClickHouseURL()
	if err != nil {
		return common.LogAction{
			Msg:   "failed to obtain jaeger clickhouse url",
			Error: err,
		}
	}
	s, err := jaeger.JaegerPluginConfig(cr, url)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to serialize plugin_config into secret",
			Error: err,
		}
	}

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "jaeger plugin config secret",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "jaeger plugin config secret",
		Mutator: func() error {
			return jaeger.JaegerPluginConfigMutator(cr, s, url)
		},
	}
}

func (i *TracingReconciler) getJaegerDatasourceDesiredState(cr *v1alpha1.Group) common.Action {
	d := jaeger.Datasource(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: d,
			Msg: "jaeger datasource",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: d,
		Msg: "jaeger datasource",
		Mutator: func() error {
			return jaeger.DatasourceMutator(cr, d)
		},
	}
}

func (i *TracingReconciler) getJaegerIngressDesiredState(cr *v1alpha1.Group) common.Action {
	in := jaeger.Ingress(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: in,
			Msg: "jaeger ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: in,
		Msg: "jaeger ingress",
		Mutator: func() error {
			return jaeger.IngressMutator(cr, in)
		},
	}
}

func (i *TracingReconciler) getJaegerDeploymentDesiredState(state *TracingState, cr *v1alpha1.Group) common.Action {
	current := state.JaegerCR

	if i.Teardown {
		if current == nil {
			return common.LogAction{
				Msg: "jaeger already deleted",
			}
		}
		return common.GenericDeleteAction{
			Ref: current,
			Msg: "jaeger CR",
		}
	}

	if current == nil {
		return common.GenericCreateAction{
			Ref: jaeger.Jaeger(cr),
			Msg: "jaeger CR",
		}
	}

	existing := current.DeepCopy()

	url, err := state.JaegerClickHouseURL()
	if err != nil {
		return common.LogAction{
			Msg:   "failed to obtain jaeger clickhouse url",
			Error: err,
		}
	}

	err = jaeger.JaegerMutator(cr, current, url)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to mutate jaeger CR",
			Error: err,
		}
	}

	equal, err := jaeger.DeepEqual(existing, current)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to compare existing and desired jaeger definitions",
			Error: err,
		}
	}
	if equal {
		return common.LogAction{
			Msg: "jaeger CR unchanged",
		}
	}

	return common.GenericUpdateAction{
		Ref: current,
		Msg: "jaeger CR",
	}
}

func (i *TracingReconciler) getJaegerServiceMonitorDesiredState(cr *v1alpha1.Group) common.Action {
	monitor := jaeger.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "jaeger servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "jaeger servicemonitor",
		Mutator: func() error {
			return jaeger.ServiceMonitorMutator(cr, monitor)
		},
	}
}

///////////////////////// Otel /////////////////////////

func (i *TracingReconciler) getOtelServiceDesiredState(cr *v1alpha1.Group) common.Action {
	svc := opentelemetry.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "opentelemetry service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "opentelemetry service",
		Mutator: func() error {
			return opentelemetry.ServiceMutator(cr, svc)
		},
	}
}

func (i *TracingReconciler) getOtelServiceAccountDesiredState(cr *v1alpha1.Group) common.Action {
	sa := opentelemetry.ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "opentelemetry service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "opentelemetry service account",
		Mutator: func() error {
			opentelemetry.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *TracingReconciler) getOtelConfigDesiredState(cr *v1alpha1.Group) common.Action {
	s, err := opentelemetry.Config(cr)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to serialize otel config into configmap",
			Error: err,
		}
	}

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "opentelemetry config configmap",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "opentelemetry config configmap",
		Mutator: func() error {
			return opentelemetry.ConfigMutator(cr, s)
		},
	}
}

func (i *TracingReconciler) getOtelIngressDesiredState(cr *v1alpha1.Group) common.Action {
	in := opentelemetry.Ingress(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: in,
			Msg: "opentelemetry ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: in,
		Msg: "opentelemetry ingress",
		Mutator: func() error {
			return opentelemetry.IngressMutator(cr, in)
		},
	}
}

func (i *TracingReconciler) getOtelJaegerIngressDesiredState(cr *v1alpha1.Group) common.Action {
	in := opentelemetry.JaegerIngress(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: in,
			Msg: "opentelemetry jaeger ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: in,
		Msg: "opentelemetry jaeger ingress",
		Mutator: func() error {
			return opentelemetry.JaegerIngressMutator(cr, in)
		},
	}
}

func (i *TracingReconciler) getOtelDeploymentDesiredState(cr *v1alpha1.Group) common.Action {
	deploy := opentelemetry.Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "opentelemetry deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "opentelemetry deployment",
		Mutator: func() error {
			return opentelemetry.DeploymentMutator(cr, deploy)
		},
	}
}

func (i *TracingReconciler) getOtelServiceMonitorDesiredState(cr *v1alpha1.Group) common.Action {
	monitor := opentelemetry.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "opentelemetry servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "opentelemetry servicemonitor",
		Mutator: func() error {
			return opentelemetry.ServiceMonitorMutator(cr, monitor)
		},
	}
}
