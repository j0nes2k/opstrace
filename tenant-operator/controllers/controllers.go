package controllers

import "fmt"

var ErrUnableToGetGrafanaURL = fmt.Errorf("unable to get grafana admin url")
