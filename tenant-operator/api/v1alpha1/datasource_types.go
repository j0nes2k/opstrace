/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"fmt"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DataSourceSpec defines the desired state of DataSource.
type DataSourceSpec struct {
	Datasources []DataSourceFields `json:"datasources"`
	Name        string             `json:"name"`
}

// DataSourceStatus defines the observed state of Datasource.
type DataSourceStatus struct {
	Phase   StatusPhase `json:"phase"`
	Message string      `json:"message"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// DataSource is the Schema for the datasources API.
type DataSource struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DataSourceSpec   `json:"spec,omitempty"`
	Status DataSourceStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DataSourceList contains a list of DataSource.
// +kubebuilder:object:root=true
type DataSourceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DataSource `json:"items"`
}

type DataSourceFields struct {
	Name              string                    `json:"name"`
	Type              string                    `json:"type"`
	Uid               string                    `json:"uid,omitempty"`
	Access            string                    `json:"access,omitempty"`
	GroupId           int                       `json:"groupId,omitempty"`
	Url               string                    `json:"url,omitempty"`
	Password          string                    `json:"password,omitempty"`
	User              string                    `json:"user,omitempty"`
	Database          string                    `json:"database,omitempty"`
	BasicAuth         bool                      `json:"basicAuth"`
	BasicAuthUser     string                    `json:"basicAuthUser,omitempty"`
	BasicAuthPassword string                    `json:"basicAuthPassword,omitempty"`
	WithCredentials   bool                      `json:"withCredentials"`
	IsDefault         bool                      `json:"isDefault"`
	JsonData          *DataSourceJsonData       `json:"jsonData,omitempty"`
	SecureJsonData    *DataSourceSecureJsonData `json:"secureJsonData,omitempty"`
	Version           int                       `json:"version,omitempty"`
	Editable          bool                      `json:"editable"`
}

// DataSourceJsonData contains the most common json options.
// See https://grafana.com/docs/administration/provisioning/#datasources
type DataSourceJsonData struct {
	OauthPassThru           bool     `json:"oauthPassThru,omitempty"`
	TlsAuth                 bool     `json:"tlsAuth,omitempty"`
	TlsAuthWithCACert       bool     `json:"tlsAuthWithCACert,omitempty"`
	TlsSkipVerify           bool     `json:"tlsSkipVerify,omitempty"`
	GraphiteVersion         string   `json:"graphiteVersion,omitempty"`
	TimeInterval            string   `json:"timeInterval,omitempty"`
	EsVersion               string   `json:"esVersion,omitempty"`
	TimeField               string   `json:"timeField,omitempty"`
	Interval                string   `json:"interval,omitempty"`
	LogMessageField         string   `json:"logMessageField,omitempty"`
	LogLevelField           string   `json:"logLevelField,omitempty"`
	AuthType                string   `json:"authType,omitempty"`
	AssumeRoleArn           string   `json:"assumeRoleArn,omitempty"`
	DefaultRegion           string   `json:"defaultRegion,omitempty"`
	CustomMetricsNamespaces string   `json:"customMetricsNamespaces,omitempty"`
	TsdbVersion             string   `json:"tsdbVersion,omitempty"`
	TsdbResolution          string   `json:"tsdbResolution,omitempty"`
	Sslmode                 string   `json:"sslmode,omitempty"`
	Encrypt                 string   `json:"encrypt,omitempty"`
	PostgresVersion         int      `json:"postgresVersion,omitempty"`
	Timescaledb             bool     `json:"timescaledb,omitempty"`
	MaxOpenConns            int      `json:"maxOpenConns,omitempty"`
	MaxIdleConns            int      `json:"maxIdleConns,omitempty"`
	ConnMaxLifetime         int      `json:"connMaxLifetime,omitempty"`
	KeepCookies             []string `json:"keepCookies,omitempty"`
	// Useful fields for clickhouse datasource
	// See https://github.com/Vertamedia/clickhouse-grafana/tree/master/dist/README.md#configure-the-datasource-with-provisioning
	// See https://github.com/Vertamedia/clickhouse-grafana/tree/master/src/datasource.ts#L44
	AddCorsHeader               bool   `json:"addCorsHeader,omitempty"`
	DefaultDatabase             string `json:"defaultDatabase,omitempty"`
	UsePOST                     bool   `json:"usePOST,omitempty"`
	UseYandexCloudAuthorization bool   `json:"useYandexCloudAuthorization,omitempty"`
	XHeaderUser                 string `json:"xHeaderUser,omitempty"`
	XHeaderKey                  string `json:"xHeaderKey,omitempty"`
	// Custom HTTP headers for datasources
	// See https://grafana.com/docs/grafana/latest/administration/provisioning/#datasources
	HTTPHeaderName1 string `json:"httpHeaderName1,omitempty"`
	HTTPHeaderName2 string `json:"httpHeaderName2,omitempty"`
	HTTPHeaderName3 string `json:"httpHeaderName3,omitempty"`
	HTTPHeaderName4 string `json:"httpHeaderName4,omitempty"`
	HTTPHeaderName5 string `json:"httpHeaderName5,omitempty"`
	HTTPHeaderName6 string `json:"httpHeaderName6,omitempty"`
	HTTPHeaderName7 string `json:"httpHeaderName7,omitempty"`
	HTTPHeaderName8 string `json:"httpHeaderName8,omitempty"`
	HTTPHeaderName9 string `json:"httpHeaderName9,omitempty"`
	// Fields for Stackdriver data sources
	TokenUri           string `json:"tokenUri,omitempty"`
	ClientEmail        string `json:"clientEmail,omitempty"`
	AuthenticationType string `json:"authenticationType,omitempty"`
	DefaultProject     string `json:"defaultProject,omitempty"`
	// Fields for Azure data sources
	AppInsightsAppId             string `json:"appInsightsAppId,omitempty"`
	AzureLogAnalyticsSameAs      string `json:"azureLogAnalyticsSameAs,omitempty"`
	ClientId                     string `json:"clientId,omitempty"`
	ClusterURL                   string `json:"clusterUrl,omitempty"`
	CloudName                    string `json:"cloudName,omitempty"`
	LogAnalyticsDefaultWorkspace string `json:"logAnalyticsDefaultWorkspace,omitempty"`
	LogAnalyticsClientId         string `json:"logAnalyticsClientId,omitempty"`
	LogAnalyticsSubscriptionId   string `json:"logAnalyticsSubscriptionId,omitempty"`
	LogAnalyticsTenantId         string `json:"logAnalyticsTenantId,omitempty"`
	SubscriptionId               string `json:"subscriptionId,omitempty"`
	TenantId                     string `json:"tenantId,omitempty"`
	// Fields for InfluxDB data sources
	HTTPMode      string `json:"httpMode,omitempty"`
	Version       string `json:"version,omitempty"`
	Organization  string `json:"organization,omitempty"`
	DefaultBucket string `json:"defaultBucket,omitempty"`
	// Fields for Loki data sources
	MaxLines      int                           `json:"maxLines,omitempty"`
	DerivedFields []DataSourceJsonDerivedFields `json:"derivedFields,omitempty"`
	// Fields for Prometheus data sources
	CustomQueryParameters       string                                      `json:"customQueryParameters,omitempty"`
	HTTPMethod                  string                                      `json:"httpMethod,omitempty"`
	ExemplarTraceIdDestinations []DataSourceJsonExemplarTraceIdDestinations `json:"exemplarTraceIdDestinations,omitempty"`
	// Fields for tracing data sources
	TracesToLogs DataSourceJsonTracesToLogs `json:"tracesToLogs,omitempty"`
	ServiceMap   DataSourceJsonServiceMap   `json:"serviceMap,omitempty"`
	NodeGraph    DatasourceJsonNodeGraph    `json:"nodeGraph,omitempty"`
	Search       DataSourceJsonSearch       `json:"search,omitempty"`
	// Fields for Github data sources
	GithubUrl string `json:"githubUrl,omitempty"`
	// Fields for Alertmanager data sources
	Implementation string `json:"implementation,omitempty"`
	// Fields for AWS Prometheus data sources
	SigV4Auth          bool   `json:"sigV4Auth,omitempty"`
	SigV4AuthType      string `json:"sigV4AuthType,omitempty"`
	SigV4ExternalId    string `json:"sigV4ExternalId,omitempty"`
	SigV4AssumeRoleArn string `json:"sigV4AssumeRoleArn,omitempty"`
	SigV4Region        string `json:"sigV4Region,omitempty"`
	SigV4Profile       string `json:"sigV4Profile,omitempty"`
	// Fields for Instana data sources
	// See https://github.com/instana/instana-grafana-datasource/blob/main/provisioning/datasources/datasource.yml
	Url               string `json:"url,omitempty"`
	ApiToken          string `json:"apiToken,omitempty"`
	UseProxy          bool   `json:"useProxy,omitempty"`
	ShowOffline       bool   `json:"showOffline,omitempty"`
	AllowInfraExplore bool   `json:"allowInfraExplore,omitempty"`
	// Extra field for MySQL data source
	Timezone string `json:"timezone,omitempty"`
	// Fields for Grafana Clickhouse data sources
	Server   string `json:"server,omitempty"`
	Port     int    `json:"port,omitempty"`
	Username string `json:"username,omitempty"`
}

type DataSourceJsonDerivedFields struct {
	DatasourceUid string `json:"datasourceUid,omitempty"`
	MatcherRegex  string `json:"matcherRegex,omitempty"`
	Name          string `json:"name,omitempty"`
	Url           string `json:"url,omitempty"`
}

type DataSourceJsonExemplarTraceIdDestinations struct {
	DatasourceUid   string `json:"datasourceUid,omitempty"`
	Name            string `json:"name,omitempty"`
	Url             string `json:"url,omitempty"`
	UrlDisplayLabel string `json:"urlDisplayLabel,omitempty"`
}

type DataSourceJsonTracesToLogs struct {
	DatasourceUid      string   `json:"datasourceUid,omitempty"`
	SpanEndTimeShift   string   `json:"spanEndTimeShift,omitempty"`
	SpanStartTimeShift string   `json:"spanStartTimeShift,omitempty"`
	Tags               []string `json:"tags,omitempty"`
	FilterBySpanId     bool     `json:"filterBySpanID,omitempty"`
	FilterByTraceID    bool     `json:"filterByTraceID,omitempty"`
	LokiSearch         bool     `json:"lokiSearch,omitempty"`
}

type DataSourceJsonServiceMap struct {
	DatasourceUid string `json:"datasourceUid,omitempty"`
}

type DataSourceJsonSearch struct {
	Hide bool `json:"hide,omitempty"`
}

type DatasourceJsonNodeGraph struct {
	Enabled bool `json:"enabled,omitempty"`
}

// DataSourceSecureJsonData contains the most common secure json options.
// See https://grafana.com/docs/administration/provisioning/#datasources
type DataSourceSecureJsonData struct {
	TlsCaCert         string `json:"tlsCACert,omitempty"`
	TlsClientCert     string `json:"tlsClientCert,omitempty"`
	TlsClientKey      string `json:"tlsClientKey,omitempty"`
	Password          string `json:"password,omitempty"`
	BasicAuthPassword string `json:"basicAuthPassword,omitempty"`
	AccessKey         string `json:"accessKey,omitempty"`
	SecretKey         string `json:"secretKey,omitempty"`
	// Custom HTTP headers for datasources
	// See https://grafana.com/docs/grafana/latest/administration/provisioning/#datasources
	HTTPHeaderValue1 string `json:"httpHeaderValue1,omitempty"`
	HTTPHeaderValue2 string `json:"httpHeaderValue2,omitempty"`
	HTTPHeaderValue3 string `json:"httpHeaderValue3,omitempty"`
	HTTPHeaderValue4 string `json:"httpHeaderValue4,omitempty"`
	HTTPHeaderValue5 string `json:"httpHeaderValue5,omitempty"`
	HTTPHeaderValue6 string `json:"httpHeaderValue6,omitempty"`
	HTTPHeaderValue7 string `json:"httpHeaderValue7,omitempty"`
	HTTPHeaderValue8 string `json:"httpHeaderValue8,omitempty"`
	HTTPHeaderValue9 string `json:"httpHeaderValue9,omitempty"`
	// Fields for Stackdriver data sources
	PrivateKey string `json:"privateKey,omitempty"`
	// Fields for Azure data sources
	ClientSecret             string `json:"clientSecret,omitempty"`
	AppInsightsApiKey        string `json:"appInsightsApiKey,omitempty"`
	LogAnalyticsClientSecret string `json:"logAnalyticsClientSecret,omitempty"`
	// Fields for InfluxDB data sources
	Token string `json:"token,omitempty"`
	// Fields for Github data sources
	AccessToken string `json:"accessToken,omitempty"`
	// Fields for AWS data sources
	SigV4AccessKey string `json:"sigV4AccessKey,omitempty"`
	SigV4SecretKey string `json:"sigV4SecretKey,omitempty"`
}

func init() {
	SchemeBuilder.Register(&DataSource{}, &DataSourceList{})
}

// Filename returns a unique per namespace key of the datasource.
func (ds *DataSource) Filename() string {
	return fmt.Sprintf("%v_%v.yaml", ds.Namespace, strings.ToLower(ds.Name))
}
