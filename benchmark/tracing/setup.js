const TRACING_API = __ENV.TRACING_API
const PROJECT_ID = __ENV.PROJECT_ID
const API_KEY = __ENV.API_KEY

export default {
    urls: {
        otlphttp: `${TRACING_API}/v1/traces/${PROJECT_ID}`,
    },
    headers: {
        default: {
            'Authorization': `Bearer ${API_KEY}`,
        },
    }
}
