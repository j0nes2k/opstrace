import crypto from 'k6/crypto';

const DEFAULT_PAYLOAD_FILE = 'payload/2k.json'
const PAYLOAD_FILE = __ENV.PAYLOAD_FILE || DEFAULT_PAYLOAD_FILE

const defaultPayload = JSON.parse(open(PAYLOAD_FILE))

export default function () {
    let clone = Object.assign({}, defaultPayload)

    const uuid = Array.from(new Uint8Array(crypto.randomBytes(16)),
        (b) => b.toString(16).padStart(2, '0')).join('')

    clone['event_id'] = uuid
    clone['timestamp'] = new Date().toISOString()

    return clone
}
