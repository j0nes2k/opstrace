package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strconv"

	"github.com/go-openapi/loads"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"

	"github.com/opstrace/opstrace/go/pkg/errortracking"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	"github.com/opstrace/opstrace/go/pkg/metrics"
)

var (
	port                   = getEnvInt("PORT", 8080)
	metricsPort            = getEnvInt("METRICS_PORT", 8081)
	clickHouseDsn          = getEnv("CLICKHOUSE_DSN", "")
	apiBaseURL             = getEnv("API_BASE_URL", "")
	clickhouseMaxOpenConns = getEnvInt("CLICKHOUSE_MAX_OPEN_CONNS", 100)
	clickhouseMaxIdleConns = getEnvInt("CLICKHOUSE_MAX_IDLE_CONNS", 20)
)

func getEnvInt(key string, fallback int) int {
	s := os.Getenv(key)
	if len(s) == 0 {
		return fallback
	}

	v, err := strconv.Atoi(s)
	if err != nil {
		return fallback
	}

	return v
}

func getEnv(key string, fallback string) string {
	s := os.Getenv(key)
	if s == "" {
		return fallback
	}
	return s
}

// nolint:funlen
func main() {
	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
	})

	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.WithError(err).Fatal("failed to load embedded swagger")
	}

	if clickHouseDsn == "" {
		log.Fatal("CLICKHOUSE_DSN env variable not set")
	}
	if apiBaseURL == "" {
		log.Fatal("API_BASE_URL env variable not set")
	}

	baseURL, err := url.Parse(apiBaseURL)
	if err != nil {
		log.WithError(err).Fatal("invalid base domain url")
	}

	opts := &errortracking.DatabaseOptions{
		MaxOpenConns: clickhouseMaxOpenConns,
		MaxIdleConns: clickhouseMaxIdleConns,
	}

	db, err := errortracking.NewDB(clickHouseDsn, opts)
	if err != nil {
		log.WithError(err).Fatal("failed to set up clickhouse")
	}

	ctrl := errortracking.NewController(baseURL, db)

	api := operations.NewErrorTrackingAPI(swaggerSpec)
	// Use logrus for logging.
	api.Logger = log.Infof
	// Set up the ingestion endpoints at the
	// /projects/{projectId}/(store|envelope) routes.
	api.EventsPostProjectsAPIProjectIDEnvelopeHandler = events.PostProjectsAPIProjectIDEnvelopeHandlerFunc(
		ctrl.PostEnvelopeHandler,
	)
	api.EventsPostProjectsAPIProjectIDStoreHandler = events.PostProjectsAPIProjectIDStoreHandlerFunc(
		ctrl.PostStoreHandler,
	)
	// Set up the /projects/{projectId}/errors route that returns a list of
	// errors.
	api.ErrorsListErrorsHandler = errors.ListErrorsHandlerFunc(
		ctrl.ListErrors,
	)
	// Set up the GET /projects/{projectId}/errors/{fingerprint} route handler
	// that returns information about an error.
	api.ErrorsGetErrorHandler = errors.GetErrorHandlerFunc(
		ctrl.GetError,
	)
	// Set up the PUT /projects/{projectId}/errors/{fingerprint} route handler
	// that updates information about an error.
	api.ErrorsUpdateErrorHandler = errors.UpdateErrorHandlerFunc(
		ctrl.UpdateError,
	)

	// Set up the GET /projects/{projectID}/errors/{fingerprint}/events router
	// handle that returns information about the events related to an error.
	api.ErrorsListEventsHandler = errors.ListEventsHandlerFunc(
		ctrl.ListEvents,
	)

	// Set up the DELETE /projects/{projectId} route handle that deletes errors
	// from the given project.
	api.ProjectsDeleteProjectHandler = projects.DeleteProjectHandlerFunc(
		ctrl.DeleteProject,
	)

	// Set up base metrics before we instrument the middleware
	prometheus.MustRegister(metrics.ConfiguredCollectors()...)

	server := restapi.NewServer(api)
	server.ConfigureAPI()
	// nolint:errcheck
	defer server.Shutdown()

	server.Port = port
	server.EnabledListeners = []string{"http"}

	// Set up metrics server
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	go func() {
		log.Debug("metrics server starting")
		srv := &http.Server{
			Addr:    fmt.Sprintf("0.0.0.0:%d", metricsPort),
			Handler: mux,
		}
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				log.WithError(err).Error("unable to start metric server")
			}
		}
	}()

	if err := server.Serve(); err != nil {
		log.WithError(err).Fatal("server failed")
	}
}
