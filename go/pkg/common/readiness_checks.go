package common

import (
	netv1 "k8s.io/api/networking/v1"
)

const (
	ConditionStatusSuccess = "True"
)

func IsIngressReady(ingress *netv1.Ingress) bool {
	if ingress == nil {
		return false
	}

	return len(ingress.Status.LoadBalancer.Ingress) > 0
}
