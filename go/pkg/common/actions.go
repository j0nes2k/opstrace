package common

import (
	"context"
	stdErr "errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/go-logr/logr"
	"github.com/jackc/pgx/v4"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/kubectl/pkg/polymorphichelpers"
	logf "sigs.k8s.io/controller-runtime/pkg/log"

	v14 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type ActionOperation string

const (
	OperationCreated ActionOperation = "(created)"
	OperationUpdated ActionOperation = "(updated)"
	OperationDeleted ActionOperation = "(deleted)"
	OperationNoop    ActionOperation = "(unchanged)"
	OperationLog     ActionOperation = "(log)"
)

type Action interface {
	Run(runner ActionRunner) (string, ActionOperation, error)
}

// The desired state is defined by a list of actions that have to be run to
// get from the current state to the desired state.
type DesiredState []Action

func (d *DesiredState) AddAction(action Action) DesiredState {
	if action != nil {
		*d = append(*d, action)
	}
	return *d
}

func (d *DesiredState) AddActions(actions []Action) DesiredState {
	for _, action := range actions {
		d.AddAction(action)
	}
	return *d
}

type ActionRunner struct {
	scheme *runtime.Scheme
	client client.Client
	ctx    context.Context
	log    logr.Logger
	cr     client.Object
}

func NewActionRunner(ctx context.Context, client client.Client, scheme *runtime.Scheme, cr client.Object) ActionRunner {
	return ActionRunner{
		scheme: scheme,
		client: client,
		log:    logf.Log.WithName("action-runner"),
		ctx:    ctx,
		cr:     cr,
	}
}

func (i *ActionRunner) RunAll(desiredState DesiredState) error {
	for _, action := range desiredState {
		msg, op, err := action.Run(*i)
		status := "SUCCESS"

		if err != nil {
			status = "FAILED"
		}

		if op == OperationLog && err != nil {
			i.log.V(1).Error(err, fmt.Sprintf("%8s %12s %s", status, "", msg))
			// Note(Arun): We should wrap the errors here with appropriate message as a part of future improvement.
			// nolint:wrapcheck
			return err
		}

		if op == OperationLog {
			i.log.V(1).Info(fmt.Sprintf("%8s %12s %s", "INFO", "", msg))
		} else {
			i.log.V(1).Info(fmt.Sprintf("%8s %12s %s", status, op, msg))
		}

		if err != nil {
			// nolint:wrapcheck
			return err
		}
	}

	return nil
}

func (i *ActionRunner) exposeSecret(ns string, ref *v14.SecretEnvSource, vars []string) error {
	secret := v14.Secret{}
	key := client.ObjectKey{
		Namespace: ns,
		Name:      ref.Name,
	}

	err := i.client.Get(i.ctx, key, &secret)
	if err != nil {
		return fmt.Errorf("failed to get secret: %w", err)
	}

	for _, exposedVar := range vars {
		for secretKey, secretValue := range secret.Data {
			if exposedVar == secretKey {
				os.Setenv(secretKey, string(secretValue))
				i.log.V(1).Info(fmt.Sprintf("found value for %s in secret %s", exposedVar, ref.Name))
			}
		}
	}

	return nil
}

func (i *ActionRunner) exposeConfigMap(ns string, ref *v14.ConfigMapEnvSource, vars []string) error {
	configMap := v14.ConfigMap{}
	key := client.ObjectKey{
		Namespace: ns,
		Name:      ref.Name,
	}

	err := i.client.Get(i.ctx, key, &configMap)
	if err != nil {
		return fmt.Errorf("failed to fetch key from configMap %w", err)
	}

	for _, exposedVar := range vars {
		for configMapKey, configMapValue := range configMap.Data {
			if exposedVar == configMapKey {
				os.Setenv(configMapKey, configMapValue)
				i.log.V(1).Info(fmt.Sprintf("found value for %s in config map %s", exposedVar, ref.Name))
			}
		}
	}
	return nil
}

func (i *ActionRunner) createOrUpdate(
	obj client.Object,
	mutator controllerutil.MutateFn,
	skipOwnerRef bool,
) (ActionOperation, error) {
	if !skipOwnerRef {
		err := controllerutil.SetControllerReference(i.cr.(v1.Object), obj.(v1.Object), i.scheme)
		if err != nil {
			return OperationNoop, fmt.Errorf("failed to set controller ref %w", err)
		}
	}

	op, err := controllerutil.CreateOrUpdate(i.ctx, i.client, obj, mutator)
	if err != nil {
		return OperationLog, fmt.Errorf("failed to create or update because of %w", err)
	}

	if op == controllerutil.OperationResultNone {
		return OperationNoop, nil
	}

	if op == controllerutil.OperationResultCreated {
		return OperationCreated, nil
	}

	return OperationUpdated, nil
}

func (i *ActionRunner) create(obj client.Object) (ActionOperation, error) {
	err := i.client.Create(i.ctx, obj)
	if err != nil {
		// Don't return error if object already exists
		if errors.IsAlreadyExists(err) {
			return OperationNoop, nil
		}
	}
	// TODO: Fix error check here to be wrapped before returning
	// nolint:wrapcheck
	return OperationCreated, err
}

func (i *ActionRunner) update(obj client.Object) (ActionOperation, error) {
	err := i.client.Update(i.ctx, obj)

	// TODO: Fix error check here to be wrapped before returning
	// nolint:wrapcheck
	return OperationUpdated, err
}

func (i *ActionRunner) delete(obj client.Object) (ActionOperation, error) {
	err := i.client.Delete(i.ctx, obj)

	// TODO: Fix error check here to be wrapped before returning
	// nolint:wrapcheck
	return OperationDeleted, client.IgnoreNotFound(err)
}

func (i *ActionRunner) ingressReady(obj client.Object) error {
	ready := IsIngressReady(obj.(*netv1.Ingress))
	if !ready {
		return stdErr.New("ingress not ready")
	}
	return nil
}

func (i *ActionRunner) isResourceGone(obj client.Object) error {
	// `obj` passed here should never be an uninitialised object reference because though `obj`
	// will pass a `nil` check, further operations on `obj`, say e.g. obj.GetDeletionTimestamp()
	// will panic with an invalid memory address or nil pointer dereference when the pointer is
	// eventually followed.
	//
	// Ideally, we should only enqueue `CheckGoneAction` with non-nil object references, e.g
	//
	// actions := []Action{}
	// var sampleObj *appsv1.Deployment
	// ... build sampleObj ...
	// if sampleObj != nil {
	// 	actions = append(actions,
	// 		CheckGoneAction{
	// 			Ref: sampleObj,
	// 			Msg: "check sampleObj is gone",
	// 		},
	// 	)
	// }
	if obj != nil {
		// during a teardown, if the underlying object was found to still exist, report on its
		// status to make sure we can track when it was marked for deletion
		deletionTime := obj.GetDeletionTimestamp()
		if deletionTime == nil || deletionTime.IsZero() {
			return stdErr.New("resource exists, hasn't been deleted")
		}
		return fmt.Errorf(
			"resource still exists, was marked for deletion %f seconds ago",
			time.Since(deletionTime.Time).Seconds(),
		)
	}
	return nil
}

// An action to create generic kubernetes resources
// (resources that don't require special treatment).
type GenericCreateOrUpdateAction struct {
	Ref          client.Object
	Msg          string
	Mutator      controllerutil.MutateFn
	SkipOwnerRef bool
}

// Log a message and optionally an error. If an error is present,
// the runner will immediately stop at this action and enqueue
// another reconciliation.
type LogAction struct {
	Msg string
	// if error is set, the runner will immediately stop at this action and
	// enqueue another reconciliation
	Error error
}

type CheckGoneAction struct {
	Ref client.Object
	Msg string
}

type ClickHouseAction struct {
	URL                   url.URL
	SQL                   string
	Msg                   string
	Database              string
	ForgetErrorIfContains string
}

type PostgresAction struct {
	URL string
	SQL string
	Msg string
}

type IngressReadyAction struct {
	Ref client.Object
	Msg string
}

type DeploymentReadyAction struct {
	Ref client.Object
	Msg string
}

type StatefulSetReadyAction struct {
	Ref client.Object
	Msg string
}

// An action to delete generic kubernetes resources
// (resources that don't require special treatment).
type GenericDeleteAction struct {
	Ref client.Object
	Msg string
}

// An action to create generic kubernetes resources.
type GenericCreateAction struct {
	Ref client.Object
	Msg string
}

// An action to update generic kubernetes resources.
type GenericUpdateAction struct {
	Ref client.Object
	Msg string
}

// Expose credentials from a secret as an env var to the operator container.
type ExposeSecretEnvVarAction struct {
	Ref       *v14.SecretEnvSource
	Msg       string
	Namespace string
}

// Expose credentials from a secret as an env var to the operator container.
type ExposeConfigMapEnvVarAction struct {
	Ref       *v14.ConfigMapEnvSource
	Msg       string
	Namespace string
}

func (i PostgresAction) Run(_ ActionRunner) (string, ActionOperation, error) {
	ctx := context.Background()
	// Mat: maybe we should keep this open on the runner?
	conn, err := pgx.Connect(ctx, i.URL)
	if err != nil {
		return i.Msg, OperationNoop, fmt.Errorf("failed to connect to pg: %w", err)
	}
	_, err = conn.Exec(ctx, i.SQL)
	if err != nil {
		return i.Msg, OperationNoop, fmt.Errorf("failed to exec query: %w", err)
	}
	err = conn.Close(ctx)
	// TODO: Fix error check here to be wrapped before returning
	// nolint:wrapcheck
	return i.Msg, OperationLog, err
}

func (i ClickHouseAction) Run(_ ActionRunner) (string, ActionOperation, error) {
	ctx := context.Background()
	pass, _ := i.URL.User.Password()
	// Mat: maybe we should keep this open on the runner?
	conn, err := clickhouse.Open(&clickhouse.Options{
		Addr: []string{i.URL.Host},
		Auth: clickhouse.Auth{
			Database: i.Database,
			Username: i.URL.User.Username(),
			Password: pass,
		},
		Debug:           false,
		DialTimeout:     time.Second,
		MaxOpenConns:    1,
		MaxIdleConns:    1,
		ConnMaxLifetime: 5 * time.Second,
	})
	if err != nil {
		// TODO: Add error wrapping to satisfy wrapcheck
		// nolint:wrapcheck
		return i.Msg, OperationNoop, err
	}
	err = conn.Exec(ctx, i.SQL)
	if err != nil {
		// Crude way to stop error being thrown based on a match with the error string
		if strings.Contains(err.Error(), i.ForgetErrorIfContains) {
			return fmt.Sprintf("%s: Ignoring error: %s", i.Msg, err.Error()), OperationNoop, nil
		}
		// TODO: Add error wrapping to satisfy wrapcheck
		// nolint:wrapcheck
		return i.Msg, OperationNoop, err
	}
	err = conn.Close()

	// TODO: Add error wrapping to satisfy wrapcheck
	// nolint:wrapcheck
	return i.Msg, OperationLog, err
}

func (i GenericCreateOrUpdateAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	op, err := runner.createOrUpdate(i.Ref, i.Mutator, i.SkipOwnerRef)
	return i.Msg, op, err
}

func (i GenericCreateAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	op, err := runner.create(i.Ref)
	return i.Msg, op, err
}

func (i GenericUpdateAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	op, err := runner.update(i.Ref)
	return i.Msg, op, err
}

func (i GenericDeleteAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	op, err := runner.delete(i.Ref)
	return i.Msg, op, err
}

func (i LogAction) Run(_ ActionRunner) (string, ActionOperation, error) {
	return i.Msg, OperationLog, i.Error
}

func (i IngressReadyAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	return i.Msg, OperationLog, runner.ingressReady(i.Ref)
}

func (i CheckGoneAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	return i.Msg, OperationLog, runner.isResourceGone(i.Ref)
}

// Note(Arun): I'm not sure what would be best to statisfy unparam linter
// nolint:unparam
func (i DeploymentReadyAction) Run(_ ActionRunner) (string, ActionOperation, error) {
	if i.Ref == nil {
		return i.Msg, OperationLog, stdErr.New("deployment doesn't exist yet")
	}
	viewer := polymorphichelpers.DeploymentStatusViewer{}
	u, err := runtime.DefaultUnstructuredConverter.ToUnstructured(i.Ref)
	if err != nil {
		// TODO: Add error wrapping to satisfy wrapcheck
		// nolint:wrapcheck
		return i.Msg, OperationLog, err
	}

	msg, ready, err := viewer.Status(&unstructured.Unstructured{Object: u}, 0)
	if ready {
		return strings.TrimRight(msg, "\n"), OperationLog, err
	}

	return i.Msg, OperationLog, stdErr.New(msg)
}

// nolint:unparam
func (i StatefulSetReadyAction) Run(_ ActionRunner) (string, ActionOperation, error) {
	if i.Ref == nil {
		return i.Msg, OperationLog, stdErr.New("statefulset doesn't exist yet")
	}
	viewer := polymorphichelpers.StatefulSetStatusViewer{}
	u, err := runtime.DefaultUnstructuredConverter.ToUnstructured(i.Ref)
	if err != nil {
		// TODO: Add error wrapping to satisfy wrapcheck
		// nolint:wrapcheck
		return i.Msg, OperationLog, err
	}

	msg, ready, err := viewer.Status(&unstructured.Unstructured{Object: u}, 0)
	if ready {
		return strings.TrimRight(msg, "\n"), OperationLog, err
	}

	return i.Msg, OperationLog, stdErr.New(msg)
}

func (i ExposeConfigMapEnvVarAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	return i.Msg, OperationLog, runner.exposeConfigMap(
		i.Namespace,
		i.Ref,
		[]string{constants.ArgusAdminUserEnvVar,
			constants.ArgusAdminPasswordEnvVar,
		})
}

func (i ExposeSecretEnvVarAction) Run(runner ActionRunner) (string, ActionOperation, error) {
	return i.Msg, OperationLog, runner.exposeSecret(
		i.Namespace,
		i.Ref,
		[]string{constants.ArgusAdminUserEnvVar, constants.ArgusAdminPasswordEnvVar},
	)
}
