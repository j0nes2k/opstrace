CREATE TABLE IF NOT EXISTS error_tracking_ignored_errors ON CLUSTER '{cluster}'
(
  project_id UInt64,
  fingerprint UInt32,
  user_id Nullable(UInt64) DEFAULT NULL,
  updated_at DateTime64(6, 'UTC')
) ENGINE = ReplicatedReplacingMergeTree(updated_at)
ORDER BY (project_id, fingerprint);
