package errortracking

import (
	"fmt"
	"testing"

	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"github.com/stretchr/testify/assert"
)

func TestQueryBuilder(t *testing.T) {
	b := &queryBuilder{}

	for _, tc := range []struct {
		name     string
		sql      string
		params   []interface{}
		expected string
	}{
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = 1",
			params:   []interface{}{},
			expected: "SELECT * FROM table WHERE thing = 1",
		},
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = ?",
			params:   []interface{}{1},
			expected: "SELECT * FROM table WHERE thing = $1",
		},
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = ? AND otherthing = ?",
			params:   []interface{}{1, 2},
			expected: "SELECT * FROM table WHERE thing = $1 AND otherthing = $2",
		},
	} {
		t.Log(tc.name)

		sql := "SELECT * FROM table"
		b.reset(sql)

		assert.Equal(t, b.sql, sql)
		assert.Len(t, b.args, 0)

		b.build(tc.sql, tc.params...)
		assert.EqualValues(t, tc.params, b.args)
		assert.Equal(t, tc.expected, b.sql)
	}
}

func stringPointer(s string) *string {
	return &s
}

func int64Pointer(i int64) *int64 {
	return &i
}

func TestBuildListErrorsQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.ListErrorsParams
		expected string
		args     []interface{}
		err      error
	}{
		{
			name: "should build a simple query",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("last_seen_desc"),
				Status:    stringPointer("unresolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY last_seen_at DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(0), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom sort",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("unresolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(0), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom status",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("ignored"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = TRUE ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $2 OFFSET $3",
			args:     []interface{}{uint64(1), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom query parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     stringPointer("foo"),
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("ignored"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = TRUE AND (error_tracking_errors.name ILIKE $2 OR error_tracking_errors.description ILIKE $3) ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $4 OFFSET $5",
			args:     []interface{}{uint64(1), "%foo%", "%foo%", int64(20), 0},
			err:      nil,
		},
		{
			name: "should fail with invalid custom cursor parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(10),
				Query:     nil,
				Cursor:    stringPointer("foobar"),
				Sort:      stringPointer("frequency_desc"),
				Status:    stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected end of JSON input"),
		},
		{
			name: "should not fail with valid custom cursor parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(9),
				Query:     nil,
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY event_count DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(1), int64(9), 18},
			err:      nil,
		},
		{
			name: "should not fail with custom cursor parameter without page field",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				Query:     nil,
				// cursor is the base64 encoded json string: {"foo":"bar"}
				Cursor: stringPointer("eyJmb28iOiJiYXIifQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY event_count DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(1), int64(7), 0},
			err:      nil,
		},
		{
			name: "should fail with custom cursor parameter with invalid json",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				Query:     nil,
				// cursor is the base64 encoded json string: "foobar"
				Cursor: stringPointer("Zm9vYmFyCg=="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("invalid character"),
		},
		{
			name: "should fail with unexpected status",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("_ignored"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected error status _ignored"),
		},
	} {
		t.Log(tc.name)
		sql, args, err := buildListErrorsQuery(tc.params)
		if tc.err != nil {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

func TestBuildGetErrorQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.GetErrorParams
		expected string
		args     []interface{}
	}{
		{
			name: "should build a simple query",
			params: errors.GetErrorParams{
				ProjectID:   1,
				Fingerprint: 1,
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND fingerprint = $2",
			args:     []interface{}{uint64(1), uint32(1)},
		},
	} {
		t.Log(tc.name)
		sql, args := buildGetErrorQuery(tc.params)
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

func TestBuildListEventsQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.ListEventsParams
		expected string
		args     []interface{}
		err      error
	}{
		{
			name: "should build a simple query",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(20),
				Cursor:      nil,
				Sort:        stringPointer("occurred_at_desc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom sort",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(20),
				Cursor:      nil,
				Sort:        stringPointer("occurred_at_asc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at ASC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(20), 0},
			err:      nil,
		},
		{
			name: "should fail with invalid custom cursor parameter",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(10),
				Cursor:      stringPointer("foobar"),
				Sort:        stringPointer("occurred_at_desc"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected end of JSON input"),
		},
		{
			name: "should not fail with valid custom cursor parameter",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(9),
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(9), 18},
			err:      nil,
		},
		{
			name: "should not fail with custom cursor parameter without page field",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(7),
				// cursor is the base64 encoded json string: {"foo":"bar"}
				Cursor: stringPointer("eyJmb28iOiJiYXIifQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(7), 0},
			err:      nil,
		},
		{
			name: "should fail with custom cursor parameter with invalid json",
			params: errors.ListEventsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				// cursor is the base64 encoded json string: "foobar"
				Cursor: stringPointer("Zm9vYmFyCg=="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("invalid character"),
		},
	} {
		t.Log(tc.name)
		sql, args, err := buildListEventsQuery(tc.params)
		if tc.err != nil {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}
