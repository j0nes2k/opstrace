package errortracking

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/go-openapi/loads"
	"github.com/stretchr/testify/assert"

	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
)

// TODO: Satisfy thelper linter
// nolint:thelper
func GetAPIHandler(t *testing.T, ctrl *Controller) *operations.ErrorTrackingAPI {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	assert.Nil(t, err, "failed to load swagger definition")

	api := operations.NewErrorTrackingAPI(swaggerSpec)
	api.Logger = t.Logf

	api.EventsPostProjectsAPIProjectIDEnvelopeHandler = events.PostProjectsAPIProjectIDEnvelopeHandlerFunc(
		ctrl.PostEnvelopeHandler,
	)
	api.EventsPostProjectsAPIProjectIDStoreHandler = events.PostProjectsAPIProjectIDStoreHandlerFunc(
		ctrl.PostStoreHandler,
	)
	api.ErrorsListErrorsHandler = errors.ListErrorsHandlerFunc(
		ctrl.ListErrors,
	)
	api.ErrorsGetErrorHandler = errors.GetErrorHandlerFunc(
		ctrl.GetError,
	)
	api.ErrorsUpdateErrorHandler = errors.UpdateErrorHandlerFunc(
		ctrl.UpdateError,
	)
	api.ErrorsListEventsHandler = errors.ListEventsHandlerFunc(
		ctrl.ListEvents,
	)
	api.ProjectsDeleteProjectHandler = projects.DeleteProjectHandlerFunc(
		ctrl.DeleteProject,
	)

	err = api.Validate()
	assert.Nil(t, err, "invalid api")

	return api
}

type mockDB struct {
	err                 error
	event               *ErrorTrackingErrorEvent
	listErrorParams     *errors.ListErrorsParams
	listErrors          []*models.Error
	getErrorParams      *errors.GetErrorParams
	getError            *models.Error
	updateErrorParams   *errors.UpdateErrorParams
	updateError         *models.Error
	listEventsParams    *errors.ListEventsParams
	listEvents          []*models.ErrorEvent
	deleteProjectParams *projects.DeleteProjectParams
}

func (m *mockDB) InsertErrorTrackingErrorEvent(e *ErrorTrackingErrorEvent) error {
	m.event = e
	return m.err
}

func (m *mockDB) ListErrors(params errors.ListErrorsParams) ([]*models.Error, error) {
	m.listErrorParams = &params
	return m.listErrors, m.err
}

func (m *mockDB) GetError(params errors.GetErrorParams) (*models.Error, error) {
	m.getErrorParams = &params
	return m.getError, m.err
}

func (m *mockDB) UpdateError(params errors.UpdateErrorParams) (*models.Error, error) {
	m.updateErrorParams = &params
	return m.updateError, m.err
}

func (m *mockDB) ListEvents(params errors.ListEventsParams) ([]*models.ErrorEvent, error) {
	m.listEventsParams = &params
	return m.listEvents, m.err
}

func (m *mockDB) DeleteProject(params projects.DeleteProjectParams) error {
	m.deleteProjectParams = &params
	return m.err
}

func TestPostEnvelopeHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	payload, err := ioutil.ReadFile("testdata/envelope.txt")
	assert.Nil(t, err, "failed to load test data")

	// List of content-type headers we need to support. See
	// gen/restapi/configure_error_tracking.go for more details.
	contentTypes := []string{
		"application/json",
		"application/x-sentry-envelope",
		"application/octet-stream",
		// empty string because NodeJS does not set a content-type in the request
		"",
		// javascript browser SDK sets "text/plain"
		"text/plain",
		"text/plain;charset=UTF-8",
	}

	for _, tc := range []struct {
		name       string
		payload    []byte
		statusCode int
		setup      func()
		teardown   func()
		encoded    bool
	}{
		{
			name:       "should insert a valid error event",
			payload:    payload,
			statusCode: http.StatusOK,
		},
		{
			name:       "should insert a valid gzip'ed error event",
			payload:    payload,
			statusCode: http.StatusOK,
			encoded:    true,
		},
		{
			name:       "should fail to insert empty error event",
			payload:    []byte(`{}`),
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "should fail when db insert fails",
			payload:    payload,
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				assert.Equal(t, uint64(1), db.event.ProjectID)
				assert.Equal(t, "development", db.event.Environment)
				assert.Equal(t, "ruby", db.event.Platform)
				// reset mock database error state
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}

		for _, contentType := range contentTypes {
			t.Logf("\twhen content type is %s", contentType)

			r := bytes.NewReader(tc.payload)
			if tc.encoded {
				var buf bytes.Buffer
				zw := gzip.NewWriter(&buf)
				zw.Write(tc.payload)
				zw.Close()
				r = bytes.NewReader(buf.Bytes())
			}

			req, err := http.NewRequest("POST", ts.URL+apiBasePath+"/projects/api/1/envelope", r)

			assert.Nil(t, err)
			req.Header.Set("Content-Type", contentType)
			if tc.encoded {
				req.Header.Set("Content-Encoding", "gzip")
			}

			res, err := http.DefaultClient.Do(req)

			assert.Nil(t, err)
			assert.Equal(t, tc.statusCode, res.StatusCode)
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestPostStoreHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	payload, err := ioutil.ReadFile("testdata/ruby_event.json")
	assert.Nil(t, err, "failed to load test data")

	base64compressedPayload, err := ioutil.ReadFile("testdata/ruby_base64_zlib.txt")
	assert.Nil(t, err, "failed to load test data")

	for _, tc := range []struct {
		name       string
		payload    []byte
		statusCode int
		setup      func()
		teardown   func()
	}{
		{
			name:       "should insert a valid error event",
			payload:    payload,
			statusCode: http.StatusOK,
		},
		{
			name:       "should insert a valid base64 encoded and zlib compressed error event",
			payload:    base64compressedPayload,
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail to insert empty error event",
			payload:    []byte(`{}`),
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "should fail when db insert fails",
			payload:    payload,
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				assert.Equal(t, uint64(1), db.event.ProjectID)
				assert.Equal(t, "development", db.event.Environment)
				assert.Equal(t, "ruby", db.event.Platform)
				// reset mock database error state
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		r := bytes.NewReader(tc.payload)
		req, err := http.NewRequest("POST", ts.URL+apiBasePath+"/projects/api/1/store", r)
		req.Header.Set("Content-Type", "application/json")

		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestListErrorsHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	makeError := func(name string, projectID uint64, fingerprint uint32) *models.Error {
		return &models.Error{
			Actor:                 "actor",
			ApproximatedUserCount: 1,
			Description:           "description",
			EventCount:            1,
			Fingerprint:           fingerprint,
			Name:                  name,
			ProjectID:             projectID,
			Status:                "unresolved",
		}
	}

	db.listErrors = []*models.Error{
		makeError("a", 1, 1),
		makeError("b", 1, 1),
		makeError("c", 1, 1),
	}

	for _, tc := range []struct {
		name       string
		statusCode int
		query      string
		setup      func()
		teardown   func()
	}{
		{
			name:       "should list errors",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept last_seen_desc sort parameter",
			query:      "sort=last_seen_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept first_seen_desc sort parameter",
			query:      "sort=first_seen_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept frequency_desc sort parameter",
			query:      "sort=frequency_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with unknown sort parameter",
			query:      "sort=foo",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should accept unresolved status parameter",
			query:      "status=unresolved",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept resolved status parameter",
			query:      "status=resolved",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept ignored status parameter",
			query:      "status=ignored",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept ignored status parameter",
			query:      "status=ignored",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with invalid status parameter",
			query:      "status=foobar",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should accept query parameter",
			query:      "query=foobar",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept limit parameter",
			query:      "limit=10",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with invalid limit parameter",
			query:      "limit=a",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should fail when db query fails",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+apiBasePath+"/projects/1/errors?"+tc.query, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		// check the response payload matches the mock values
		if tc.statusCode == http.StatusOK {
			body, err := ioutil.ReadAll(res.Body)
			assert.Nil(t, err)
			defer res.Body.Close()

			var errors []*models.Error
			err = json.Unmarshal(body, &errors)
			assert.Nil(t, err)
			assert.ElementsMatch(t, errors, db.listErrors)

			// only check the link header is set, there are tests to check the
			// link is built appropriately
			assert.NotEmpty(t, res.Header.Get("Link"))
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestBuildErrorsLink(t *testing.T) {
	baseURL, _ := url.Parse("http://localhost:8080")
	route := "/errortracking/api/v1/projects/2/errors"
	testURL, _ := url.Parse(route)
	endpoint := baseURL.String() + route

	assert.Equal(t, "http://localhost:8080/errortracking/api/v1/projects/2/errors", endpoint)

	for _, tc := range []struct {
		name   string
		params errors.ListErrorsParams
		link   string
	}{
		{
			name: "should return valid next link",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
			},
			link: fmt.Sprintf(`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="next"`, endpoint),
		},
		{
			name: "should return valid next and prev links",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with limit",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Limit:  int64Pointer(2),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&limit=2>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with query",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Query:  stringPointer("foobar"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&query=foobar>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&query=foobar>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with sort",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("first_seen_desc"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&sort=first_seen_desc>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&sort=first_seen_desc>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with status",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Status: stringPointer("resolved"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&status=resolved>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&status=resolved>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next link with all params",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				Limit:  int64Pointer(2),
				Query:  stringPointer("foobar"),
				Sort:   stringPointer("first_seen_desc"),
				Status: stringPointer("resolved"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2&query=foobar&sort=first_seen_desc&status=resolved>; rel="next"`,
				endpoint,
			),
		},
	} {
		t.Log(tc.name)
		link, err := buildListErrorsLink(baseURL, tc.params)
		assert.Nil(t, err)
		assert.Equal(t, tc.link, link)
	}
}

func TestGetErrorsHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	for _, tc := range []struct {
		name       string
		statusCode int
		uri        string
		setup      func()
		teardown   func()
	}{
		{
			name:       "should get error",
			statusCode: http.StatusOK,
			uri:        apiBasePath + "/projects/1/errors/1",
			setup: func() {
				db.getError = &models.Error{}
			},
			teardown: func() {
				db.getError = nil
			},
		},
		{
			name:       "should return not found",
			statusCode: http.StatusNotFound,
			uri:        apiBasePath + "/projects/1/errors/1",
		},
		{
			name:       "should fail when db query fails",
			uri:        apiBasePath + "/projects/1/errors/1",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+tc.uri, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestUpdateErrorHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	for _, tc := range []struct {
		name       string
		statusCode int
		uri        string
		params     map[string]interface{}
		setup      func()
		teardown   func()
	}{
		{
			name:       "should update error",
			statusCode: http.StatusOK,
			uri:        apiBasePath + "/projects/1/errors/1",
			params: map[string]interface{}{
				"status":        "ignored",
				"updated_by_id": 1,
			},
			setup: func() {
				db.updateError = &models.Error{}
			},
			teardown: func() {
				db.updateError = nil
			},
		},
		{
			name:       "should return not found",
			statusCode: http.StatusNotFound,
			uri:        apiBasePath + "v1/projects/1/errors/1",
		},
		{
			name:       "should fail when db query fails",
			uri:        apiBasePath + "/projects/1/errors/1",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}

		body, err := json.Marshal(tc.params)
		assert.Nil(t, err)

		req, err := http.NewRequest("PUT", ts.URL+tc.uri, bytes.NewReader(body))
		assert.Nil(t, err)
		req.Header.Set("Content-type", "application/json")

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)

		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestListEventsHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db)

	api := GetAPIHandler(t, ctrl)
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	apiBasePath := api.Context().BasePath()

	makeErrorEvent := func(name string, projectID uint64, fingerprint uint32) *models.ErrorEvent {
		return &models.ErrorEvent{
			Actor:       "actor",
			Description: "description",
			Environment: "env",
			Fingerprint: fingerprint,
			Name:        name,
			Payload:     "payload",
			Platform:    "platform",
			ProjectID:   projectID,
		}
	}

	db.listEvents = []*models.ErrorEvent{
		makeErrorEvent("a", 1, 1),
		makeErrorEvent("b", 1, 1),
		makeErrorEvent("c", 1, 1),
	}

	for _, tc := range []struct {
		name       string
		statusCode int
		query      string
		setup      func()
		teardown   func()
		events     []*models.ErrorEvent
	}{
		{
			name:       "should list error events",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept occurred_at_desc sort parameter",
			query:      "sort=occurred_at_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept occurred_at_asc sort parameter",
			query:      "sort=occurred_at_asc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with unknown sort parameter",
			query:      "sort=foo",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should accept limit parameter",
			query:      "limit=10",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with invalid limit parameter",
			query:      "limit=a",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should fail when db query fails",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+apiBasePath+"/projects/1/errors/1/events?"+tc.query, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		// check the response payload matches the mock values
		if tc.statusCode == http.StatusOK {
			body, err := ioutil.ReadAll(res.Body)
			assert.Nil(t, err)
			defer res.Body.Close()

			var events []*models.ErrorEvent
			err = json.Unmarshal(body, &events)
			assert.Nil(t, err)
			assert.ElementsMatch(t, events, db.listEvents)

			// only check the link header is set, there are tests to check the
			// link is built appropriately
			assert.NotEmpty(t, res.Header.Get("Link"))
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestBuildEventsLink(t *testing.T) {
	baseURL, _ := url.Parse("http://localhost:8080")
	route := "/projects/2/errors/1/events"
	testURL, _ := url.Parse(route)
	endpoint := baseURL.String() + route

	assert.Equal(t, "http://localhost:8080/projects/2/errors/1/events", endpoint)

	for _, tc := range []struct {
		name   string
		params errors.ListEventsParams
		link   string
	}{
		{
			name: "should return valid next link",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
			},
			link: fmt.Sprintf(`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="next"`, endpoint),
		},
		{
			name: "should return valid next and prev links",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with limit",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Limit:  int64Pointer(2),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&limit=2>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with sort",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&sort=occurred_at_desc>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&sort=occurred_at_desc>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next link with all params",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				Limit: int64Pointer(2),
				Sort:  stringPointer("occurred_at_asc"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2&sort=occurred_at_asc>; rel="next"`,
				endpoint,
			),
		},
	} {
		t.Log(tc.name)
		link, err := buildListEventsLink(baseURL, tc.params)
		assert.Nil(t, err)
		assert.Equal(t, tc.link, link)
	}
}

func TestDeleteProject(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	for _, tc := range []struct {
		name       string
		statusCode int
		uri        string
		setup      func()
		teardown   func()
	}{
		{
			name:       "should delete a project",
			statusCode: http.StatusCreated,
			uri:        apiBasePath + "/projects/1",
		},
		{
			name:       "should fail when db query fails",
			statusCode: http.StatusInternalServerError,
			uri:        apiBasePath + "/projects/1",
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}

		req, err := http.NewRequest("DELETE", ts.URL+tc.uri, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)

		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}
