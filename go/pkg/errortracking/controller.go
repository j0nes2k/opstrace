package errortracking

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"

	"github.com/go-openapi/runtime/middleware"
	log "github.com/sirupsen/logrus"

	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
)

type Controller struct {
	db      Database
	baseURL *url.URL
}

func NewController(baseURL *url.URL, db Database) *Controller {
	return &Controller{
		baseURL: baseURL,
		db:      db,
	}
}

func readCompressedBody(req *http.Request) ([]byte, error) {
	r, err := gzip.NewReader(req.Body)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		// nolint:wrapcheck
		return nil, err
	}
	defer r.Close()
	// nolint:wrapcheck
	return io.ReadAll(r)
}

func readBody(req *http.Request) ([]byte, error) {
	// Parse the http request body into a string and unmarshall it manually.
	defer req.Body.Close()
	bts, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %w", err)
	}
	return bts, nil
}

func isGzipEncoded(req *http.Request) bool {
	return req.Header.Get("Content-Encoding") == "gzip"
}

func uncompressPayload(payload []byte) ([]byte, error) {
	raw, err := base64.StdEncoding.DecodeString(string(payload))
	if err != nil {
		return nil, fmt.Errorf("failed to decode base64 payload: %w", err)
	}

	r := bytes.NewReader(raw)
	zr, err := zlib.NewReader(r)
	if err != nil {
		return nil, fmt.Errorf("failed to read compressed payload: %w", err)
	}

	bts, err := ioutil.ReadAll(zr)
	if err != nil {
		return nil, fmt.Errorf("failed to read compressed payload: %w", err)
	}
	return bts, nil
}

// PostEnvelopeHandler handles the POST request sent to the
// /projects/{projectID}/envelope route.
func (c *Controller) PostEnvelopeHandler(
	params events.PostProjectsAPIProjectIDEnvelopeParams,
) middleware.Responder {
	var err error
	var payload []byte

	if isGzipEncoded(params.HTTPRequest) {
		payload, err = readCompressedBody(params.HTTPRequest)
	} else {
		payload, err = readBody(params.HTTPRequest)
	}

	if err != nil {
		log.WithError(err).Debug("failed to read envelope body")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	e, err := NewEnvelopeFrom(payload)
	if err != nil {
		log.WithError(err).Debug("failed to parse envelope payload")
		// TODO: This is to help track down json parsing issues. Remove before
		// moving to prod.
		// nolint:errcheck
		bts, _ := httputil.DumpRequest(params.HTTPRequest, false)
		fmt.Println(string(bts))
		fmt.Println(string(payload))
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	// Data model for sentry envelope can have multiple item types.
	// See https://develop.sentry.dev/sdk/envelopes/#data-model
	// We currently accept only event[https://develop.sentry.dev/sdk/envelopes/#event] envelopes.
	// Transaction[https://develop.sentry.dev/sdk/envelopes/#transaction] envelopes were a noop in Rails implementation.
	// See details of transaction payload[https://develop.sentry.dev/sdk/event-payloads/transaction/] which can include
	// an exception, but we don't support it yet.
	if e.Type.Type != "event" {
		log.WithField("type", e.Type.Type).Debug("envelope type not supported")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	// nolint:lll
	// Ref: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/api/error_tracking/collector.rb#L100
	if e.Type.Type == "event" && e.Event != nil {
		err = e.Event.Validate()
		if err != nil {
			log.WithError(err).Debug("failed to validate event body")
			return events.NewPostProjectsAPIProjectIDStoreBadRequest()
		}

		err = c.db.InsertErrorTrackingErrorEvent(NewErrorTrackingErrorEvent(params.ProjectID, e.Event, payload))
		if err != nil {
			log.WithError(err).Debug("failed to create event from envelope")
			return events.NewPostProjectsAPIProjectIDEnvelopeInternalServerError()
		}
	}

	return events.NewPostProjectsAPIProjectIDEnvelopeOK()
}

func (c *Controller) PostStoreHandler(
	params events.PostProjectsAPIProjectIDStoreParams,
) middleware.Responder {
	var err error
	var payload []byte

	if isGzipEncoded(params.HTTPRequest) {
		payload, err = readCompressedBody(params.HTTPRequest)
	} else {
		payload, err = readBody(params.HTTPRequest)
	}

	if err != nil {
		log.WithError(err).Debug("failed to read store body")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	// Sentry client SDK weird behavior alert!
	//
	// Some client sdks set the content-type to x-sentry-envelope. But this
	// might or might not be a json string. When it's not a json object it comes
	// as base64 encoded blob of the zlib compressed json string.
	if !json.Valid(payload) {
		payload, err = uncompressPayload(payload)
		if err != nil {
			log.WithError(err).Debug("failed to uncompress store body")
			return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
		}
	}

	e, err := NewEventFrom(payload)
	if err != nil {
		log.WithError(err).Debug("failed to parse store payload")
		// TODO: This is to help track down json parsing issues. Remove before
		// moving to prod.
		// nolint:errcheck
		bts, _ := httputil.DumpRequest(params.HTTPRequest, false)
		fmt.Println(string(bts))
		fmt.Println(string(payload))
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}
	// nolint:lll
	// Ref: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/api/error_tracking/collector.rb#L140
	err = e.Validate()
	if err != nil {
		log.WithError(err).Debug("failed to validate event body")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	err = c.db.InsertErrorTrackingErrorEvent(NewErrorTrackingErrorEvent(params.ProjectID, e, payload))
	if err != nil {
		log.WithError(err).Debug("failed to create event from store")
		return events.NewPostProjectsAPIProjectIDEnvelopeInternalServerError()
	}

	return events.NewPostProjectsAPIProjectIDStoreOK()
}

func (c *Controller) ListErrors(params errors.ListErrorsParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("sort", *params.Sort).
		WithField("status", *params.Status).
		WithField("limit", *params.Limit)

	if params.Query != nil {
		logger = logger.WithField("query", *params.Query)
	}

	if params.Cursor != nil {
		logger = logger.WithField("cursor", *params.Cursor)
	}
	logger.Debug("got list errors request")

	res, err := c.db.ListErrors(params)
	if err != nil {
		log.WithError(err).Error("listing errors")
		return errors.NewListErrorsInternalServerError()
	}

	resp := errors.NewListErrorsOK()
	resp.Payload = res
	link, err := buildListErrorsLink(c.baseURL, params)
	if err != nil {
		log.WithError(err).Error("failed to build link for listing errors")
		return errors.NewListErrorsInternalServerError()
	}
	resp.Link = link

	return resp
}

func buildListErrorsLink(baseURL *url.URL, params errors.ListErrorsParams) (string, error) {
	page, err := decodePage(params.Cursor)
	if err != nil {
		return "", err
	}
	prev := page - 1
	prevLink := ""
	next := page + 1

	buildListErrorsLinkPage := func(page int) (string, error) {
		u, err := url.Parse(baseURL.String() + params.HTTPRequest.URL.String())
		if err != nil {
			return "", fmt.Errorf("failed to parsed url %w", err)
		}
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Query != nil {
			q.Set("query", *params.Query)
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		if params.Status != nil {
			q.Set("status", *params.Status)
		}
		cursor, err := encodePage(page)
		if err != nil {
			return "", err
		}
		q.Set("cursor", cursor)
		u.RawQuery = q.Encode()

		return u.String(), nil
	}

	if prev >= 1 {
		prevLink, err = buildListErrorsLinkPage(prev)
		if err != nil {
			return "", err
		}
	}
	nextLink, err := buildListErrorsLinkPage(next)
	if err != nil {
		return "", err
	}

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link, nil
}

func (c *Controller) GetError(params errors.GetErrorParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("fingerprint", params.Fingerprint)

	logger.Debug("got get error request")

	res, err := c.db.GetError(params)
	if err != nil {
		logger.WithError(err).Error("get error")
		return errors.NewGetErrorInternalServerError()
	}

	if res == nil {
		return errors.NewGetErrorNotFound()
	}

	return errors.NewGetErrorOK().WithPayload(res)
}

func (c *Controller) UpdateError(params errors.UpdateErrorParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("fingerprint", params.Fingerprint).
		WithField("userID", params.Body.UpdatedByID).
		WithField("status", params.Body.Status)

	logger.Debug("got update error request")

	res, err := c.db.UpdateError(params)
	if err != nil {
		logger.WithError(err).Error("update error")
		return errors.NewUpdateErrorInternalServerError()
	}

	if res == nil {
		return errors.NewUpdateErrorNotFound()
	}

	return errors.NewUpdateErrorOK().WithPayload(res)
}

func (c *Controller) ListEvents(params errors.ListEventsParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("sort", *params.Sort).
		WithField("limit", *params.Limit)

	if params.Cursor != nil {
		logger = logger.WithField("cursor", *params.Cursor)
	}
	logger.Debug("got list errors request")

	res, err := c.db.ListEvents(params)
	if err != nil {
		log.WithError(err).Error("listing errors")
		return errors.NewListErrorsInternalServerError()
	}

	resp := errors.NewListEventsOK()
	resp.Payload = res
	link, err := buildListEventsLink(c.baseURL, params)
	if err != nil {
		log.WithError(err).Error("failed to build link for listing events")
		return errors.NewListErrorsInternalServerError()
	}
	resp.Link = link

	return resp
}

func buildListEventsLink(baseURL *url.URL, params errors.ListEventsParams) (string, error) {
	page, err := decodePage(params.Cursor)
	if err != nil {
		return "", err
	}
	prev := page - 1
	prevLink := ""
	next := page + 1

	buildListEventsLinkPage := func(page int) (string, error) {
		u, err := url.Parse(baseURL.String() + params.HTTPRequest.URL.String())
		if err != nil {
			return "", fmt.Errorf("failed to parsed url %w", err)
		}
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		cursor, err := encodePage(page)
		if err != nil {
			return "", err
		}
		q.Set("cursor", cursor)
		u.RawQuery = q.Encode()

		return u.String(), nil
	}

	if prev >= 1 {
		prevLink, err = buildListEventsLinkPage(prev)
		if err != nil {
			return "", err
		}
	}
	nextLink, err := buildListEventsLinkPage(next)
	if err != nil {
		return "", err
	}

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link, nil
}

func (c *Controller) DeleteProject(params projects.DeleteProjectParams) middleware.Responder {
	logger := log.WithField("projectId", params.ID)
	logger.Debug("got delete project request")
	// TODO: handle project not found but since this endpoint is to be used for
	// testing just go ahead and proceed with the deletion.
	err := c.db.DeleteProject(params)
	if err != nil {
		return projects.NewDeleteProjectInternalServerError()
	}

	return projects.NewDeleteProjectCreated()
}
