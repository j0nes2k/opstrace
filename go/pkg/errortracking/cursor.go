package errortracking

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

// Decode the given Cursor parameter. The cursor string is a base 64 encoded
// json. If the json contains the page parameter it returns its value otherwise
// returns 1. If there's an error returns 0 and the error.
//
//nolint:lll
// See https://gitlab.com/gitlab-org/gitlab/-/blob/f8956d0d6910d3efa8adcab6cd5c8abb43e352d6/lib/gitlab/error_tracking/error_repository/click_house_strategy.rb#L94-99
func decodePage(cursor64 *string) (int, error) {
	if cursor64 == nil {
		return 1, nil
	}

	raw, err := base64.StdEncoding.DecodeString(*cursor64)
	if err != nil {
		return 0, fmt.Errorf("failed to decode from base64 %w", err)
	}

	cursor := struct {
		Page *int `json:"page,omitempty"`
	}{}

	err = json.Unmarshal(raw, &cursor)
	if err != nil {
		return 0, fmt.Errorf("failed to unmarshal cursor %w", err)
	}

	if cursor.Page != nil {
		return *cursor.Page, nil
	}

	return 1, nil
}

// Decode the given Cursor parameter. The cursor string is a base 64 encoded
// json. If the json contains the page parameter it returns its value otherwise
// returns 1. If there's an error returns 0 and the error.
//
//nolint:lll
// See https://gitlab.com/gitlab-org/gitlab/-/blob/f8956d0d6910d3efa8adcab6cd5c8abb43e352d6/lib/gitlab/error_tracking/error_repository/click_house_strategy.rb#L94-99
func encodePage(page int) (string, error) {
	cursor := struct {
		Page int `json:"page"`
	}{
		Page: page,
	}

	j, err := json.Marshal(&cursor)
	if err != nil {
		return "", fmt.Errorf("failed to marshal cursor %w", err)
	}
	return base64.StdEncoding.EncodeToString(j), nil
}
