package errortracking_test

import (
	"bytes"
	"compress/zlib"
	"encoding/base64"
	"io/ioutil"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/opstrace/opstrace/go/pkg/errortracking"
)

func TestNewEnvelopeFrom(t *testing.T) {
	type testCase struct {
		name     string
		filename string
	}

	matches, err := filepath.Glob("testdata/*envelope*.txt")
	assert.Nil(t, err, "failed to load the testdata files")
	var cases = make([]testCase, len(matches))
	for i, name := range matches {
		cases[i] = testCase{
			name:     "should parse envelope in " + name,
			filename: name,
		}
	}

	for _, tc := range cases {
		t.Log(tc.name)
		payload, err := ioutil.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		e, err := errortracking.NewEnvelopeFrom(payload)
		assert.Nil(t, err, "failed to parse test envelope from %v", tc.filename)

		assert.NotPanics(t, func() {
			errortracking.NewErrorTrackingErrorEvent(uint64(1), e.Event, payload)
		}, "failed to convert error event")
	}
}

func TestNewEventFrom(t *testing.T) {
	type testCase struct {
		name     string
		filename string
	}

	matches, err := filepath.Glob("testdata/*event*.json")
	assert.Nil(t, err, "failed to load the testdata files")
	var cases = make([]testCase, len(matches))
	for i, name := range matches {
		cases[i] = testCase{
			name:     "should parse event in " + name,
			filename: name,
		}
	}

	for _, tc := range cases {
		t.Logf(tc.name)
		payload, err := ioutil.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		e, err := errortracking.NewEventFrom(payload)
		assert.Nil(t, err, "failed to parse event")

		assert.NotPanics(t, func() {
			errortracking.NewErrorTrackingErrorEvent(uint64(1), e, payload)
		}, "failed to convert error event")
	}
}

func TestCompressedEventPayload(t *testing.T) {
	payload, err := ioutil.ReadFile("testdata/ruby_base64_zlib.txt")
	assert.Nil(t, err)

	raw, err := base64.StdEncoding.DecodeString(string(payload))
	assert.Nil(t, err)

	r := bytes.NewReader(raw)
	zr, err := zlib.NewReader(r)
	assert.Nil(t, err)

	result, err := ioutil.ReadAll(zr)
	assert.Nil(t, err)

	_, err = errortracking.NewEventFrom(result)
	assert.Nil(t, err)
}
