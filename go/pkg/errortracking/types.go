package errortracking

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

type EnvelopeMetadata struct {
	EventID string    `json:"event_id"`
	SentAt  time.Time `json:"sent_at"`
}

type EnvelopeType struct {
	Type   string `json:"type"`
	Length int    `json:"length"`
}

type Frame struct {
	Function    string                 `json:"function,omitempty"`
	Symbol      string                 `json:"symbol,omitempty"`
	Module      string                 `json:"module,omitempty"`
	Package     string                 `json:"package,omitempty"`
	Filename    string                 `json:"filename,omitempty"`
	AbsPath     string                 `json:"abs_path,omitempty"`
	Lineno      int                    `json:"lineno,omitempty"`
	Colno       int                    `json:"colno,omitempty"`
	PreContext  []string               `json:"pre_context,omitempty"`
	ContextLine string                 `json:"context_line,omitempty"`
	PostContext []string               `json:"post_context,omitempty"`
	InApp       bool                   `json:"in_app,omitempty"`
	Vars        map[string]interface{} `json:"vars,omitempty"`
}

type Stacktrace struct {
	Frames        []Frame `json:"frames,omitempty"`
	FramesOmitted []uint  `json:"frames_omitted,omitempty"`
}

// Exception holds core attributes. See https://develop.sentry.dev/sdk/event-payloads/exception/#attributes.
type Exception struct {
	Type       string      `json:"type,omitempty"`
	Value      string      `json:"value,omitempty"`
	Module     string      `json:"module,omitempty"`
	ThreadID   interface{} `json:"thread_id,omitempty"`
	Stacktrace *Stacktrace `json:"stacktrace,omitempty"`
}

type Span struct {
	TraceID      string                 `json:"trace_id"`
	SpanID       string                 `json:"span_id"`
	ParentSpanID string                 `json:"parent_span_id"`
	Op           string                 `json:"op,omitempty"`
	Description  string                 `json:"description,omitempty"`
	Status       string                 `json:"status,omitempty"`
	Tags         map[string]string      `json:"tags,omitempty"`
	StartTime    time.Time              `json:"start_timestamp"`
	EndTime      time.Time              `json:"timestamp"`
	Data         map[string]interface{} `json:"data,omitempty"`
}

// MessageObj is representation of https://develop.sentry.dev/sdk/event-payloads/message/.
type MessageObj struct {
	Formatted string `json:"formatted"`
}

// Event is a representation of https://develop.sentry.dev/sdk/event-payloads/.
// nolint:lll
// Relevant schema on Gitlab App https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/app/validators/json_schemas/error_tracking_event_payload.json
type Event struct {
	Dist        string    `json:"dist,omitempty"`
	Environment string    `json:"environment,omitempty"`
	EventID     string    `json:"event_id,omitempty"`
	Level       string    `json:"level,omitempty"`
	Message     string    `json:"message,omitempty"`
	Platform    string    `json:"platform,omitempty"`
	Timestamp   time.Time `json:"timestamp"`
	Transaction string    `json:"transaction,omitempty"`
	// Exception can be an object with the attribute values [] or a flat list of objects.
	// See https://develop.sentry.dev/sdk/event-payloads/exception/.
	// This is field is decoded in a special way
	Exception []*Exception `json:"-"`
	// The fields below are only relevant for transactions.
	Type      string    `json:"type,omitempty"`
	StartTime time.Time `json:"start_timestamp"`
	Spans     []*Span   `json:"spans,omitempty"`
	// This will hold a pointer to  the first exception that has a stacktrace
	// since the first exception may not provide adequate context (e.g. in the
	// Go SDK).
	exception *Exception `json:"-"`
}

// DateTimeFormats is the list of supported error event timestamp formats.
var DateTimeFormats = []string{
	time.RFC3339,
	"2006-01-02T15:04:05",
}

// ExceptionValues holds the exception in the values field.
// See https://develop.sentry.dev/sdk/event-payloads/exception/.
type ExceptionValues struct {
	Values []*Exception `json:"values"`
}

// UnixOrRFC3339Time is a helper struct to parse a timestamp sent by the sentry sdk.
// Sentry sdk can inconsistently send timestamps as unix timestamp in milliseconds or
// RFC3339 (ISO 8601) timestamp.
// See https://develop.sentry.dev/sdk/event-payloads/#required-attributes (timestamp).
type UnixOrRFC3339Time struct {
	t time.Time
}

// UnmarshalJSON loads either an RFC3339-formatted time (e.g.
// '"2020-11-03T15:15:09Z"') or unix epoch timestamp (e.g. '1498827360.000').
// However few sdks are missing full RFC3339 format so handles those as well.
func (u *UnixOrRFC3339Time) UnmarshalJSON(data []byte) error {
	var (
		placeholder interface{}
		lastErr     error
	)
	lastErr = json.Unmarshal(data, &placeholder)
	if lastErr != nil {
		return fmt.Errorf("failed to unmarshal timestamp: %w", lastErr)
	}
	switch val := placeholder.(type) {
	case string:
		for _, layout := range DateTimeFormats {
			parsed, err := time.Parse(layout, val)
			if err == nil {
				u.t = parsed
				return nil
			}
			lastErr = err
		}
		if lastErr != nil {
			return fmt.Errorf("failed to parse timestamp: %w", lastErr)
		}
	case float64:
		f, err := strconv.ParseFloat(string(data), 64)
		if err != nil {
			return fmt.Errorf("failed to parse timestamp as float: %w", err)
		}

		i := int64(f * 1000)

		u.t = time.Unix(0, i*int64(time.Millisecond))
	}

	return nil
}

func (u *UnixOrRFC3339Time) Time() time.Time {
	return u.t
}

// UnmarshalJSON parses the event payload and validates if the required attributes are present.
// nolint:lll,gocyclo,cyclop
// For ref: see parsing in Gitlab app: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/error_tracking/collector/sentry_request_parser.rb
// and validation: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/error_tracking/collector/payload_validator.rb
func (e *Event) UnmarshalJSON(data []byte) error {
	var objMap map[string]*json.RawMessage

	err := json.Unmarshal(data, &objMap)
	if err != nil {
		return fmt.Errorf("failed to unmarshal event into objMap: %w", err)
	}

	for key, val := range objMap {
		switch key {
		case "dist":
			err = json.Unmarshal(*val, &e.Dist)
		case "environment":
			err = json.Unmarshal(*val, &e.Environment)
		case "event_id":
			err = json.Unmarshal(*val, &e.EventID)
		case "level":
			err = json.Unmarshal(*val, &e.Level)
		case "platform":
			err = json.Unmarshal(*val, &e.Platform)
		case "transaction":
			err = json.Unmarshal(*val, &e.Transaction)
		case "message":
			// message can be an object or a string. See https://develop.sentry.dev/sdk/event-payloads/message/.
			// Remove leading whitespace to correctly identify if the value is an object
			messageBts := bytes.TrimLeft(*val, " \t\r\n")
			isObject := len(messageBts) > 0 && messageBts[0] == '{'
			if isObject {
				var obj = &MessageObj{}
				err = json.Unmarshal(*val, obj)
				if err != nil {
					return fmt.Errorf("failed to unmarshal message into MessageObj: %w", err)
				}
				e.Message = obj.Formatted
			} else {
				err = json.Unmarshal(*val, &e.Message)
			}
		case "timestamp":
			var ts UnixOrRFC3339Time
			err = json.Unmarshal(*val, &ts)
			if err != nil {
				// nolint:wrapcheck
				return err
			}
			e.Timestamp = ts.Time()
		case "exception":
			// exception can be an object or an array. See https://develop.sentry.dev/sdk/event-payloads/exception/.
			// Remove leading whitespace to correctly identify it.
			exceptionBts := bytes.TrimLeft(*val, " \t\r\n")
			isObject := len(exceptionBts) > 0 && exceptionBts[0] == '{'
			isArray := len(exceptionBts) > 0 && exceptionBts[0] == '['

			if isArray {
				// directly unmarshall into []Exception values
				err = json.Unmarshal(*val, &e.Exception)
			} else if isObject {
				var obj = ExceptionValues{}
				err = json.Unmarshal(*val, &obj)
				if err != nil {
					return fmt.Errorf("failed to unmarshal exception into ExceptionValues: %w", err)
				}
				e.Exception = obj.Values
			}
		}
		if err != nil {
			// nolint:wrapcheck
			return err
		}
	}

	// Find pointer to the first exception that has a stacktrace since the first
	// exception may not provide adequate context (e.g. in the Go SDK).
	// Original ruby code:
	//nolint:lll
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L41-48
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L56-67
	for i, ex := range e.Exception {
		if ex.Stacktrace == nil {
			continue
		}
		e.exception = e.Exception[i]
		break
	}

	return nil
}

// Validate Event has required fields set.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/error_tracking/error.rb#L20-25
func (e *Event) Validate() error {
	// Note(Arun): Currently exception is a mandatory field as per our prior implementation.
	// nolint:lll
	// See https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/app/validators/json_schemas/error_tracking_event_payload.json#L4 for the list of required fields.
	if e.exception == nil {
		return fmt.Errorf("invalid error event; exception key not present")
	}

	if e.Name() == "" {
		return fmt.Errorf("invalid error event: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event: description is not set")
	}

	if e.Actor() == "" {
		return fmt.Errorf("invalid error event: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event: platform is not set")
	}

	return nil
}

// Actor returns the transaction name. If the error does not have one set it
// returns the first item in stacktrace that has a function and module name.
// Original ruby code:
//nolint:lll
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L11
// - https://gitlab.com/gitlab-org/gitlab/-/blob/daa5796df89d775c635dc900a532efb657877b75/app/services/error_tracking/collect_error_service.rb#L66
func (e Event) Actor() string {
	if e.Transaction != "" {
		return e.Transaction
	}

	// If no exception is present return early
	if e.exception == nil {
		return ""
	}
	// This means there's no actor name and we should fail.
	l := len(e.exception.Stacktrace.Frames)
	if l == 0 {
		return ""
	}

	// Some SDKs do not have a transaction attribute. So we build it by
	// combining function name and module name from the last item in stacktrace.
	//nolint:lll
	// https://gitlab.com/gitlab-org/gitlab/-/blob/8565b9d4770baafa560915b6b06da6026ecab41c/app/services/error_tracking/collect_error_service.rb#L62

	f := e.exception.Stacktrace.Frames[l-1]
	return fmt.Sprintf("%s(%s)", f.Function, f.Module)
}

// Name returns the exception type.
// Original ruby code:
//nolint:lll
// https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L9
func (e Event) Name() string {
	if e.exception != nil {
		return e.exception.Type
	}
	return ""
}

// Description returns the exception value.
// Original ruby code:
//nolint:lll
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L10
func (e Event) Description() string {
	if e.exception != nil {
		return e.exception.Value
	}
	return ""
}

type Envelope struct {
	Metadata *EnvelopeMetadata
	Type     *EnvelopeType
	Event    *Event
}

type envelopeScanner struct {
	s *bufio.Scanner
}

func (e *envelopeScanner) do(v interface{}) error {
	ok := e.s.Scan()
	if !ok {
		// nolint:wrapcheck
		return e.s.Err()
	}
	err := json.Unmarshal(e.s.Bytes(), v)
	// nolint:wrapcheck
	return err
}

// NewEnvelopeFrom expects a payload request containing 3 objects: sentry
// metadata, type data and event data. It parses the payload and returns an
// Envelope object or an error.
//
// For reference, this is how the upstream sentry-go client sdk encodes the
// payload:
// https://github.com/getsentry/sentry-go/blob/409df0940aada10321428286de0c0b59fde0a796/transport.go#L96
func NewEnvelopeFrom(payload []byte) (*Envelope, error) {
	e := &Envelope{
		Metadata: &EnvelopeMetadata{},
		Type:     &EnvelopeType{},
		Event:    &Event{},
	}

	r := bytes.NewReader(payload)
	s := &envelopeScanner{
		s: bufio.NewScanner(r),
	}

	e.Metadata = &EnvelopeMetadata{}
	e.Type = &EnvelopeType{}
	e.Event = &Event{}

	err := s.do(e.Metadata)
	if err != nil {
		return nil, err
	}
	err = s.do(e.Type)
	if err != nil {
		return nil, err
	}
	err = s.do(e.Event)
	if err != nil {
		return nil, err
	}

	return e, nil
}

// NewEventFrom parses a payload request to an Event object.
func NewEventFrom(payload []byte) (*Event, error) {
	e := &Event{}

	err := e.UnmarshalJSON(payload)
	return e, err
}
