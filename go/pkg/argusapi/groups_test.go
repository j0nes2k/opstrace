package argusapi

import (
	"testing"

	"github.com/gobs/pretty"
)

const (
	getGroupsJSON    = `[{"id":2222,"name":"Main Group."},{"id":2,"name":"Test Group."}]`
	getGroupJSON     = `{"id":2222,"name":"Main Group.","address":{"address1":"","address2":"","city":"","zipCode":"","state":"","country":""}}`
	createdGroupJSON = `{"message":"Groupanization created","orgId":2222}`
	updatedGroupJSON = `{"message":"Groupanization updated"}`
	deletedGroupJSON = `{"message":"Groupanization deleted"}`
	mainGroupName    = "Main Group."
)

func TestGroups(t *testing.T) {
	server, client := gapiTestTools(t, 200, getGroupsJSON)
	defer server.Close()

	groups, err := client.Groups()
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(groups))

	if len(groups) != 2 {
		t.Error("Length of returned groups should be 2")
	}
	if groups[0].ID != 2222 || groups[0].Name != mainGroupName {
		t.Error("Not correctly parsing returned groupanizations.")
	}
}

func TestGroupByName(t *testing.T) {
	server, client := gapiTestTools(t, 200, getGroupJSON)
	defer server.Close()

	group := mainGroupName
	resp, err := client.GroupByName(group)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(resp))

	if resp.ID != 2222 || resp.Name != group {
		t.Error("Not correctly parsing returned groupanization.")
	}
}

func TestGroup(t *testing.T) {
	server, client := gapiTestTools(t, 200, getGroupJSON)
	defer server.Close()

	group := int64(2222)
	resp, err := client.Group(group)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(resp))

	if resp.ID != group || resp.Name != mainGroupName {
		t.Error("Not correctly parsing returned groupanization.")
	}
}

func TestNewGroup(t *testing.T) {
	server, client := gapiTestTools(t, 200, createdGroupJSON)
	defer server.Close()

	resp, err := client.NewGroup(Group{ID: int64(2222), Name: "test-group"})
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(resp))

	if resp != 2222 {
		t.Error("Not correctly parsing returned creation message.")
	}
}

func TestUpdateGroup(t *testing.T) {
	server, client := gapiTestTools(t, 200, updatedGroupJSON)
	defer server.Close()

	err := client.UpdateGroup(int64(2222), "test-group")
	if err != nil {
		t.Error(err)
	}
}

func TestDeleteGroup(t *testing.T) {
	server, client := gapiTestTools(t, 200, deletedGroupJSON)
	defer server.Close()

	err := client.DeleteGroup(int64(1))
	if err != nil {
		t.Error(err)
	}
}
