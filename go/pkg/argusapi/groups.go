package argusapi

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// Group represents a group.
type Group struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

// Groups fetches and returns the groups.
func (c *Client) Groups() ([]Group, error) {
	groups := make([]Group, 0)
	err := c.request("GET", "/api/groups/", nil, nil, &groups)
	if err != nil {
		return groups, err
	}

	return groups, err
}

// GroupByName fetches and returns the group whose name it's passed.
func (c *Client) GroupByName(name string) (Group, error) {
	group := Group{}
	err := c.request("GET", fmt.Sprintf("/api/groups/name/%s", name), nil, nil, &group)
	if err != nil {
		return group, err
	}

	return group, err
}

// Group fetches and returns the group whose ID it's passed.
func (c *Client) Group(id int64) (Group, error) {
	group := Group{}
	err := c.request("GET", fmt.Sprintf("/api/groups/%d", id), nil, nil, &group)
	if err != nil {
		return group, fmt.Errorf("/api/groups/%d call failed: %w", id, err)
	}

	return group, nil
}

// NewGroup creates a new group.
func (c *Client) NewGroup(group Group) (int64, error) {
	id := int64(0)
	data, err := json.Marshal(group)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		// nolint:wrapcheck
		return id, err
	}
	tmp := struct {
		ID int64 `json:"orgId"`
	}{}

	err = c.request("POST", "/api/groups", nil, bytes.NewBuffer(data), &tmp)
	if err != nil {
		return id, err
	}

	return tmp.ID, err
}

// UpdateGroup updates a group.
func (c *Client) UpdateGroup(id int64, name string) error {
	dataMap := map[string]string{
		"name": name,
	}
	data, err := json.Marshal(dataMap)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		// nolint:wrapcheck
		return err
	}

	return c.request("PUT", fmt.Sprintf("/api/groups/%d", id), nil, bytes.NewBuffer(data), nil)
}

// DeleteGroup deletes the group whose ID it's passed.
func (c *Client) DeleteGroup(id int64) error {
	return c.request("DELETE", fmt.Sprintf("/api/groups/%d", id), nil, nil, nil)
}
